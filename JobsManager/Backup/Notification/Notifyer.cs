﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Notification
{
    public class Notifyer
    {
        public int BeginImportation(int mode, DateTime jobTicketProductionDate)
        {
            using (DBMService.DatabaseManagerServiceClient dbmClient = new DBMService.DatabaseManagerServiceClient())
            {
                String dateID = String.Format("{0}{1}{2}", jobTicketProductionDate.Day, jobTicketProductionDate.Month, jobTicketProductionDate.Year);

                //Console.WriteLine("Start Data Importation Phase DateID :: " + dateID);

                return dbmClient.RetrieveFiles(dateID, mode);
            }
        }

        public bool CheckDatabaseManagerConnection(bool bWritetoFile)
        {
            String dir = System.Configuration.ConfigurationManager.AppSettings["COPYING_REPORT"];
            String logFileName = dir + "\\" + Dns.GetHostName().ToUpper() + "_CopyingReport.txt";

            try
            {
                using (DBMService.DatabaseManagerServiceClient dbmClient = new DBMService.DatabaseManagerServiceClient())
                {
                    dbmClient.Open();
                    dbmClient.Close();
                    
                    String message = "Connection OK - DatabaseManagerService";

                    Console.WriteLine(message);

                    if (bWritetoFile)
                    {
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileName, true))
                        {
                            file.WriteLine(message);
                        }
                    }
                    return true;
                }
            }
            catch (System.ServiceModel.CommunicationException commProblem)
            {
                String message = "Connection FAILED - DatabaseManagerService";

                Console.WriteLine(message);

                if (bWritetoFile)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileName, true))
                    {
                        file.WriteLine(message);
                    }
                }
            }
            return false;
        }
    }
}
