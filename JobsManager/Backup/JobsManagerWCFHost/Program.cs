﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Description;
using System.Configuration;
using System.ServiceModel.Configuration;
using JobsManager;
using LogSys;

namespace JobsManagerWCFHost
{

    static public class RunApp
    {
        static public void Run(bool bLive)
        {
            String modeType = bLive ? "LIVE" : "TEST";

            if (bLive)
            {
                modeType = "LIVE";
            }
            else
            {
                var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];
                modeType = modeStr;

                if (modeStr == "LIVE") modeType = "TEST";
            }

            if (modeType == "UI")
            {
                try
                {
                    Informer.OnDemandTicketProcessing = false;
                    Console.Title = "JobsManager";
                    Console.WriteLine("Select Runtime Mode - (1)TEST Or (2)LIVE-TEST");

                    String readAns = Console.ReadLine().ToLower();
                    //Database.Mode = (MODE)Convert.ToInt32(readAns);


                    var selection = (MODE)Convert.ToInt32(readAns);
                    Database.Mode = (MODE)selection;
                    if ((MODE)selection == MODE.Live) Database.Mode = MODE.Test;


                    ModeTitle();

                    //--------------------------------------------------//

                    DisplayServicesSettings();


                    Console.WriteLine("");
                    Console.WriteLine("Perform Connection Checks? Y or N");
                    String answer = Console.ReadLine().ToLower();

                    bool bProceed = true;


                    if (answer == "y")
                    {
                        Notification.Notifyer databaseManager = new Notification.Notifyer();
                        databaseManager.CheckDatabaseManagerConnection(false);
                        Console.WriteLine("");
                    }


                    if (Database.Mode == MODE.Test)
                    {
                        Console.WriteLine("On Demand Ticket Processing? Y Or N");
                        String onDemandResponse = Console.ReadLine().ToLower();

                        if (onDemandResponse == "y")
                        {
                            Informer.OnDemandTicketProcessing = true;

                            Console.WriteLine("Configure Test Mode? Y or N (N:Will run without conditions)");
                            String response = Console.ReadLine().ToLower();

                            if (response == "y")
                            {
                                Console.WriteLine("Job tickets LIMIT: ");
                                String limit = Console.ReadLine().ToLower();

                                JobsManager.TestModeConditions.JobticketsLimit = Convert.ToInt32(limit);

                                Console.WriteLine("Select processing type? Y or N");
                                String processingType = Console.ReadLine().ToLower();

                                if (processingType == "y")
                                {
                                    Console.WriteLine("Select ticket generation process::");
                                    Console.WriteLine("1) Mimick live mode");
                                    Console.WriteLine("2) Historical Correlations");
                                    Console.WriteLine("3) Range Correlations");
                                    Console.WriteLine("4) Cup and Handle Pattern");
                                    Console.WriteLine("5) Head and Shoulders pattern");
                                    Console.WriteLine("6) More...");

                                    Console.WriteLine("Press to continue");
                                    Console.ReadLine();

                                }
                            }
                        }
                    }

                    Console.WriteLine("Initialisation Phase....");
                    StartService();
                }
                catch (Exception e)
                {
                    String thread = System.Threading.Thread.CurrentThread.ToString();
                    String level = "Critical Error";
                    String application = Console.Title;

                    String selector = "JobsManager";

                    switch (Database.Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selector += "_LIVE-TEST";
                            }
                            break;
                    }
                    ApplicationErrorLogger.Log(thread, level, application, "Main program error experienced", e.ToString(), selector);

                    Console.ReadLine();
                }
            }
            else
            {
                if (modeType == "LIVE")
                {
                    Database.Mode = (MODE)0;
                }
                else if (modeType == "TEST")
                {
                    Database.Mode = (MODE)1;
                }
                else if (modeType == "LIVE-TEST")
                {
                    Database.Mode = (MODE)2;
                }

                ModeTitle();

                System.Threading.Thread.Sleep(40000);

                DisplayServicesSettings();

                //Test connectivity
                Notification.Notifyer databaseManager = new Notification.Notifyer();
                databaseManager.CheckDatabaseManagerConnection(true);
                Console.WriteLine("");

                StartService();

                Console.ReadLine();
            }
        }

        static public void DisplayServicesSettings()
        {
            Console.WriteLine("Services Settings");

            ClientSection clientSection =
            ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;

            ChannelEndpointElementCollection endpointCollection =
                clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;
            List<string> endClientPointNames = new List<string>();
            foreach (ChannelEndpointElement endpointElement in endpointCollection)
            {
                Console.WriteLine(endpointElement.Address);
            }
        }

        static public void StartService()
        {
            try
            {
                ServiceHost sv = new ServiceHost(typeof(JobsManager.JobsManagerService));

                //JobsManagerService jbmService = new JobsManagerService();
                //jbmService.StartProcesses();

                sv.Open();

                try
                {
                    foreach (ServiceEndpoint se in sv.Description.Endpoints)
                        Console.WriteLine(se.Address.ToString());

                    Console.WriteLine("");
                    Console.WriteLine("JobsManagerService Started");
                    Console.WriteLine("");
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR starting WCF service. Exception - " + e.Message);
                }

                while (true)
                {
                    Console.ReadLine();
                }

                Console.WriteLine("Service :: Closed - Finished");
                sv.Close();
            }
            catch (Exception e)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }
                ApplicationErrorLogger.Log(thread, level, application, "StartService()", e.ToString(), selector);
            }
        }

        static public void ModeTitle()
        {
            switch (Database.Mode)
            {
                case MODE.Test:
                    {
                        Console.Title += " :: TEST-MODE";
                    }
                    break;

                case MODE.Live:
                    {
                        Console.Title += " :: LIVE-MODE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        Console.Title += " :: LIVE-TEST-MODE";
                    }
                    break;
            }
        }
    }

    class Program
    {
        //static public ServiceHost sv = null;

        static void Main(string[] args)
        {
            RunApp.Run(false);
        }
    }
}
