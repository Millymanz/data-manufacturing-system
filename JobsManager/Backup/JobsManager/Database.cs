﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using LogSys;

namespace JobsManager
{
    static public class Exchanges
    {
        static public List<String> List = new List<string>();

        static public void InitialiseExchangeList()
        {
            string[] exchangesArray = System.Configuration.ConfigurationManager.AppSettings["EXCHANGES"].Split(';');
            Exchanges.List = exchangesArray.ToList();
        }
    }


    //Temp will be in its own file
    public class TradeDate
    {
        private DateTime date;
        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }
    }


    //temps

    public class DayTradeSummary
    {
        private DateTime date;
        private double open;
        private double high;
        private double low;
        private double close;
        private double adjustmentclose;
        private int volume;

        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public double Open
        {
            get { return open; }
            set { open = value; }
        }

        public double High
        {
            get { return high; }
            set { high = value; }
        }

        public double Low
        {
            get { return low; }
            set { low = value; }
        }

        public double Close
        {
            get { return close; }
            set { close = value; }
        }

        public int Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public double AdjustmentClose
        {
            get { return adjustmentclose; }
            set { adjustmentclose = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }
    }

    public enum MODE
    {
        Live = 0,
        Test = 1,
        LiveTest = 2
    }


    public class Database
    {
        static public MODE Mode;

        private List<KeyValuePair<String, String>> symbolLookUpTable = null;

        public Database()
        {
            PopulateSymbolLookUpTable();
        }

        public List<String> GetHCSymbolsTaskList()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            List<DateTime> listLatestDayTradeDate = new List<DateTime>();

            //This code assumes that the index dbs are update with the latest day trade summaries
            //
            //
            
            //foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            foreach (var item in Exchanges.List)
            {
                DateTime latestDayTradeDate = new DateTime();
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT MAX(dbo.DayTradeSummaries.Date) AS MaxDate FROM dbo.DayTradeSummaries";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        latestDayTradeDate = Convert.ToDateTime(rdr["MaxDate"].ToString());
                    }
                }
                listLatestDayTradeDate.Add(latestDayTradeDate);
            }

            var ldTradeDate = (from dateItem in listLatestDayTradeDate select dateItem).Max();


            String dateCriteria = String.Format("{0}-{1}-{2}", ldTradeDate.Year, ldTradeDate.Month, ldTradeDate.Day);

            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {


                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate < '" + dateCriteria + "'";
                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    hcSymbolTaskList.Add(symbol);
                }
            }

            if (hcSymbolTaskList.Count == 0)//this condition is true for both uptodate and empty db table..?
            {
                foreach (var item in Exchanges.List)
                {
                    String stockExchange = item.ToString();

                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                        con.Open();

                        SqlDataReader rdr = sqlCommand.ExecuteReader();

                        while (rdr.Read())
                        {
                            String symbol = rdr["SymbolID"].ToString();

                            hcSymbolTaskList.Add(symbol);
                        }
                    }
                }
            }
            else
            {
                //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
                //exclude these symbols from the hcSymbolTaskList
                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate >= '" + dateCriteria + "'";

            }
            //if (hcSymbolTaskList.Count > 200)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 200);
            //}
            //else
            return hcSymbolTaskList;
        }

        public List<String> GetHCSymbolsTaskList(String exchange, out DateTime lastMaxDate)
        {
            lastMaxDate = DateTime.Now;

            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //
            String stockExchange = exchange;

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            String tempSelector = selector;
            String commandTxt = "SELECT DISTINCT dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime FROM dbo.tblTrades WHERE dbo.tblTrades.StockExchange = '" + stockExchange + "' AND dbo.tblTrades.DateTime IN (SELECT MAX(dbo.tblTrades.DateTime) AS MaxDate FROM dbo.tblTrades) ";

            if (stockExchange == "Forex")
            {
                tempSelector += "_Forex";
                commandTxt = "SELECT DISTINCT dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime FROM dbo.tblTrades WHERE dbo.tblTrades.DateTime IN (SELECT MAX(dbo.tblTrades.DateTime) AS MaxDate FROM dbo.tblTrades) ";
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    //Updated symbols only, latest
                    //String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE dbo.DayTradeSummaries.Date IN (SELECT MAX(dbo.DayTradeSummaries.Date) AS MaxDate FROM dbo.DayTradeSummaries) ";
                    //String commandTxt = "SELECT DISTINCT dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime FROM dbo.tblTrades WHERE dbo.tblTrades.StockExchange AND dbo.tblTrades.DateTime IN (SELECT MAX(dbo.tblTrades.DateTime) AS MaxDate FROM dbo.tblTrades) ";

                    //For testing
                    //String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";


                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    bool bDate = true;

                    while (rdr.Read())
                    {
                        if (bDate)
                        {
                            lastMaxDate = DateTime.Parse(rdr["DateTime"].ToString());
                            bDate = false;
                        }

                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
            }
            return hcSymbolTaskList;
        }

        public int GetAllTimeTicketNo()
        {
            int count = 0;

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //Updated symbols only, latest
                String commandTxt = "SELECT COUNT(*) As TotalTickets FROM dbo.[Jobs]";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    count = Convert.ToInt32(rdr["TotalTickets"].ToString());
                }
            }
            return count;
        }

        public List<String> GetFlagMarkers()
        {
            List<String> exchangesList = new List<String>();

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //Updated symbols only, latest
                String commandTxt = "SELECT [Exchange],[Notify] FROM [JobsManager].[dbo].[Notify] D WHERE D.Notify = '1'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    exchangesList.Add(rdr["Exchange"].ToString());
                }
            }
            return exchangesList;
        }

        public void UpdateFlagMarker(String exchange)
        {
            List<String> exchangesList = new List<String>();

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //Updated symbols only, latest
                String commandTxt = "UPDATE [JobsManager].[dbo].[Notify] SET [JobsManager].[dbo].[Notify].Notify = '0' WHERE [JobsManager].[dbo].[Notify].Exchange = '" + exchange + "'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;
                con.Open();

                int rowsAffected = sqlCommand.ExecuteNonQuery();

            }
        }


        public List<String> GetHCAllSymbolsTaskList()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //

            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT TOP 20 dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }
            return hcSymbolTaskList;
        }

        public List<String> GetRCSymbolsTaskList()
        {
            //specify exchange
            List<String> rcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //assumes range correlation table is empty

            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        rcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }

            //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
            //exclude these symbols from the hcSymbolTaskList

            //if (hcSymbolTaskList.Count > 500)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 500);
            //}
            //else
            return rcSymbolTaskList;
        }


        public Dictionary<String, List<DateTime>> GetTradeDates(List<String> symbolList, String stockExchange)
        {
            String criteria = "";
            bool bfirstItem = true;
            //foreach (var sym in symbolList)
            //{
            //    if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "'"; bfirstItem = false; }
            //    else
            //    {
            //        criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "'";
            //    }
            //}
            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.tblTrades.SymbolID ='" + sym + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.tblTrades.SymbolID ='" + sym + "'";
                }
            }

            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();


            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            String tempSelector = selector;
            if (stockExchange == "Forex") tempSelector += "_Forex";


            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                String commandTxt = "SELECT dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime FROM dbo.tblTrades WHERE " + criteria + " ORDER BY dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime ASC";

                //temporary solution
                //String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " AND dbo.DayTradeSummaries.Date >='2000-01-01' ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0; //waiting period 
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    //TradeDate tradeDate = new TradeDate();

                    //tradeDate.SymbolID = rdr["SymbolID"].ToString();
                    //tradeDate.Date = Convert.ToDateTime(rdr["Date"].ToString());

                    String sym = rdr["SymbolID"].ToString();
                    DateTime dateItem = Convert.ToDateTime(rdr["DateTime"].ToString());

                    if (!tradeDateList.ContainsKey(sym))
                    {
                        List<DateTime> dateList = new List<DateTime>();
                        dateList.Add(dateItem);

                        tradeDateList[sym] = dateList;
                    }
                    else
                    {
                        tradeDateList[sym].Add(dateItem);
                    }
                }
            }
            return tradeDateList;
        }

        public Dictionary<String, List<DateTime>> GetTradeDatesRange(List<String> symbolList, DateTime startDate, DateTime endDate)
        {
            String criteria = "";
            bool bfirstItem = true;
            
            String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'";
                }
            }

            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();

            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String sym = rdr["SymbolID"].ToString();
                        DateTime dateItem = Convert.ToDateTime(rdr["Date"].ToString());

                        if (!tradeDateList.ContainsKey(sym))
                        {
                            List<DateTime> dateList = new List<DateTime>();
                            dateList.Add(dateItem);

                            tradeDateList[sym] = dateList;

                        }
                        else
                        {
                            tradeDateList[sym].Add(dateItem);
                        }
                    }
                }
            }

            return tradeDateList;
        }

        public List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummaries(List<String> symbolList, DateTime startDate, DateTime endDate)
        {

            List<KeyValuePair<String, List<DayTradeSummary>>> stockSymbolsHistory = new List<KeyValuePair<String, List<DayTradeSummary>>>();

            foreach (string symbol in symbolList)
            {
                List<DayTradeSummary> dataList = new List<DayTradeSummary>();

                List<Double> dataListTemp = new List<Double>();

                String stockExchange = LookUpSymbolStockExchange(symbol);

                try
                {
                    String selector = stockExchange;
                    switch (Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selector += "_LIVE-TEST";
                            }
                            break;
                    }

                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ConnectionString))
                    {
                        conn.Open();

                        String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
                        String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

                        //SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);

                        SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);

                        SqlDataReader reader = sqlCommand.ExecuteReader();


                        while (reader.Read())
                        {
                            String SymbolID = reader["SymbolID"].ToString();

                            //Double data = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            DayTradeSummary dayTrade = new DayTradeSummary();
                            dayTrade.SymbolID = reader["SymbolID"].ToString();
                            dayTrade.Date = Convert.ToDateTime(reader["Date"].ToString());

                            dayTrade.Open = Convert.ToDouble(reader["Open"].ToString());
                            dayTrade.High = Convert.ToDouble(reader["High"].ToString());
                            dayTrade.Low = Convert.ToDouble(reader["Low"].ToString());
                            dayTrade.Close = Convert.ToDouble(reader["Close"].ToString());
                            dayTrade.Volume = Convert.ToInt32(reader["Volume"].ToString());


                            dayTrade.AdjustmentClose = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            dataList.Add(dayTrade);

                            //dataListTemp.Add(dayTrade.AdjustmentClose);

                        }


                        stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(symbol, dataList));

                        //masterSymbolDataList.Add(new KeyValuePair<string, List<double>>(symbol, dataListTemp));

                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Write("Msg " + e.ToString());
                }
            }
            return stockSymbolsHistory;
        }

        private void PopulateSymbolLookUpTable()
        {
            symbolLookUpTable = new List<KeyValuePair<string, string>>();

            //foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = "TRADES";
                //String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                String tempSelector = selector;
                if (stockExchange == "Forex") tempSelector += "_Forex";

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];

                try
                {
                    using (SqlConnection c = new SqlConnection(settings.ToString()))
                    {
                        c.Open();

                        String query = "SELECT DISTINCT dbo.tblTrades.SymbolID FROM dbo.tblTrades WHERE dbo.tblTrades.StockExchange ='"+ stockExchange +"' ORDER BY dbo.tblTrades.SymbolID ASC";
                        if (stockExchange == "Forex") query = "SELECT DISTINCT dbo.tblTrades.SymbolID FROM dbo.tblTrades ORDER BY dbo.tblTrades.SymbolID ASC";

                        SqlCommand sqlCommand = new SqlCommand(query, c);

                        //SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID ASC", c);
                        sqlCommand.CommandTimeout = 0;

                        SqlDataReader reader = sqlCommand.ExecuteReader();

                        while (reader.Read())
                        {
                            String SymbolID = reader["SymbolID"].ToString();
                            symbolLookUpTable.Add(new KeyValuePair<string, string>(SymbolID, stockExchange));
                        }
                    }
                }
                catch (Exception ex)
                {
                    String thread = System.Threading.Thread.CurrentThread.ToString();
                    String level = "Error";
                    String application = Console.Title;
                    String selectorX = "JobsManager";

                    switch (Database.Mode)
                    {
                        case MODE.Test:
                            {
                                selectorX += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selectorX += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selectorX += "_LIVE-TEST";
                            }
                            break;
                    }

                    ApplicationErrorLogger.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
                }
            }
        }

        private String LookUpSymbolStockExchange(string sym)
        {
            var items = from kvp in symbolLookUpTable
                        where kvp.Key == sym
                        select kvp;

            return items.FirstOrDefault().Value;
        }

        public void SaveCompatibleListToDB(JobStruct jobDetails, Dictionary<String, KeyValuePair<DateTime, List<String>>> hcCompatiableList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }


            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.HistoricallyCompatibleStocks WHERE dbo.HistoricallyCompatibleStocks.CriteriaSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "SaveCompatibleListToDB()", ex.ToString(), selectorX);

                Console.WriteLine("Generic Exception for :: " + ex.Message);
            }

            //Dictionary<String, KeyValuePair<DateTime, List<String>>>

            foreach (KeyValuePair<String, KeyValuePair<DateTime, List<String>>> compatibleList in hcCompatiableList)
            {
                foreach (var item in compatibleList.Value.Value)
                {
                    DataRow row = tempTable.NewRow();
                    row["Date"] = String.Format("{0}-{1}-{2}", compatibleList.Value.Key.Year, compatibleList.Value.Key.Month, compatibleList.Value.Key.Day);

                    //row["Date"] = String.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    row["StockExchange"] = jobDetails.ApplicableExchange;
                    //row["JobId"] = jobDetails.JobId;
                    row["CriteriaSymbolID"] = compatibleList.Key;
                    row["ID"] = Guid.NewGuid().ToString();

                    row["CompatibleSymbolID"] = item;
                    row["CompatibleStockExchange"] = jobDetails.ApplicableExchange;
                    tempTable.Rows.Add(row);
                }
            }



            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.HistoricallyCompatibleStocks (ID, CriteriaSymbolID, StockExchange, CompatibleSymbolID, CompatibleStockExchange, Date) VALUES(@ID, @CriteriaSymbolID, @StockExchange, @CompatibleSymbolID, @CompatibleStockExchange, @Date)", conObj);

                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;

                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.UniqueIdentifier));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CriteriaSymbolID", SqlDbType.NVarChar, 20, "CriteriaSymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibleSymbolID", SqlDbType.NVarChar, 20, "CompatibleSymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibleStockExchange", SqlDbType.NVarChar, 20, "CompatibleStockExchange"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.Date, 18, "Date"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NChar, 20, "StockExchange"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, jobDetails.JobId, ex.ToString(), selectorX);

                Console.WriteLine("Generic Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }

        public List<String> GetCompatibleListFromDB(String stockExchange)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<String> symbolList = new List<string>();

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT HistoricallyCompatibleStocks.CriteriaSymbolID FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "'";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["CriteriaSymbolID"].ToString();

                        symbolList.Add(symbol.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
            }

            return symbolList;
        }

        public Dictionary<String, KeyValuePair<DateTime, List<String>>> GetCompatibleListDataFromDB(String stockExchange, List<String> stocklist)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            Dictionary<String, KeyValuePair<DateTime, List<String>>> symbolList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            foreach (String item in stocklist)
            {
                List<String> compList = new List<string>();

                DateTime dateTemp = new DateTime();

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        //String commandTxt = "SELECT * FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "' ORDER BY dbo.HistoricallyCompatibleStocks.SymbolID ASC";
                        String commandTxt = "SELECT dbo.HistoricallyCompatibleStocks.CompatibleSymbolID, dbo.HistoricallyCompatibleStocks.Date FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "' AND HistoricallyCompatibleStocks.CriteriaSymbolID ='" + item + "'";

                        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                        sqlCommand.CommandTimeout = 0;
                        con.Open();

                        SqlDataReader rdr = sqlCommand.ExecuteReader();

                        while (rdr.Read())
                        {
                            String compatibleSymbolID = rdr["CompatibleSymbolID"].ToString();

                            String date = rdr["Date"].ToString();

                            dateTemp = DateTime.Parse(date);

                            compList.Add(compatibleSymbolID);
                        }
                    }
                    symbolList.Add(item, new KeyValuePair<DateTime, List<string>>(dateTemp, compList));
                }
                catch (Exception ex)
                {
                    String thread = System.Threading.Thread.CurrentThread.ToString();
                    String level = "Error";
                    String application = Console.Title;
                    String selectorX = "JobsManager";

                    switch (Database.Mode)
                    {
                        case MODE.Test:
                            {
                                selectorX += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selectorX += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selectorX += "_LIVE-TEST";
                            }
                            break;
                    }

                    ApplicationErrorLogger.Log(thread, level, application, "GetCompatibleListDataFromDB() StockExchange:" + stockExchange + " SymbolID:" + item, ex.ToString(), selectorX);
                }
            }

            return symbolList;
        }

        public List<String> DeleteNonCompatiableSymbolsFromDB(String stockExchange)
        {
            //Or stocks with incomplete or not update dates
            String criteria = "";
            bool bfirstItem = true;

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<String> symbolList = new List<string>();

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {

                    foreach (var sym in symbolList)
                    {
                        if (bfirstItem) { criteria += "dbo.HistoricallyCompatibleStocks.CompatibleSymbolID ='" + sym + "' OR dbo.HistoricallyCompatibleStocks.CriteriaSymbolID = '" + sym + "'"; bfirstItem = false; }
                        else
                        {
                            criteria += " OR dbo.HistoricallyCompatibleStocks.CompatibleSymbolID ='" + sym + "' OR dbo.HistoricallyCompatibleStocks.CriteriaSymbolID = '" + sym + "'";
                        }
                    }

                    String commandTxt = "DELETE FROM dbo.HistoricallyCompatibleStocks WHERE " + criteria;

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "DeleteNonCompatiableSymbolsFromDB() StockExchange:" + stockExchange, ex.ToString(), selectorX);
            }
            return symbolList;
        }

        public List<String> GetNonCompatibleListFromDB(String stockExchange)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<String> symbolList = new List<string>();
            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //String commandTxt = "SELECT DISTINCT NonCompatibleStocks.NonMatchingSymbolID FROM dbo.NonCompatibleStocks WHERE NonCompatibleStocks.NonMatchingStockExchange ='" + stockExchange + "'";
                String commandTxt = "SELECT DISTINCT NonCompatibleStocks.SymbolID FROM dbo.NonCompatibleStocks WHERE NonCompatibleStocks.StockExchange ='" + stockExchange + "'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    symbolList.Add(symbol.Trim());
                }
            }

            return symbolList;
        }

        //public void SaveNonCompatibleListToDB(JobStruct jobDetails, List<String> hcNonCompatiableList, String CriteriaSymbolID, String StockExchange)

        public void SaveNonCompatibleListToDBOld(JobStruct jobDetails, List<NonCompatibleStock> nonCompatibleStockList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for :: " + ex.Message);
            }

            foreach (var item in nonCompatibleStockList)
            {
                DataRow row = tempTable.NewRow();
                row["ID"] = Guid.NewGuid().ToString();
                row["CriteriaSymbolID"] = item.CriteriaSymbolID;
                row["StockExchange"] = item.Exchange;

                row["NonMatchingSymbolID"] = item.NonMatchingSymbolID;
                row["NonMatchingStockExchange"] = jobDetails.ApplicableExchange;
                tempTable.Rows.Add(row);
            }

            try
            {
                //will be stored procedure to popualate the two tables

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    //--------------------------------------------------//
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;
                   

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }

        //public void SaveNonCompatibleListToDB(JobStruct jobDetails, List<String> hcNonCompatiableList)
        //{
        //    var settings = System.Configuration.ConfigurationManager.ConnectionStrings["JobsManager"];

        //    String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks WHERE dbo.NonCompatibleStocks.SymbolID='QFQFQQF'";

        //    DataTable tempTable = null;
        //    try
        //    {
        //        using (SqlConnection c = new SqlConnection(settings.ToString()))
        //        {
        //            c.Open();
        //            using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
        //            {
        //                tempTable = new DataTable();

        //                tempTable.TableName = "tempname";
        //                a.Fill(tempTable);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Generic Exception for :: " + ex.Message);
        //    }

        //    //Dictionary<String, KeyValuePair<DateTime, List<String>>>

        //    foreach (var item in hcNonCompatiableList)
        //    {
        //        DataRow row = tempTable.NewRow();
        //        row["ID"] = Guid.NewGuid().ToString();
        //        row["SymbolID"] = item;
        //        row["StockExchange"] = jobDetails.ApplicableExchange;

        //        tempTable.Rows.Add(row);
        //    }

        //    try
        //    {
        //        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
        //        {
        //            SqlConnection conObj = new SqlConnection(settings.ToString());
        //            dataAdapter.SelectCommand.CommandTimeout = 0;

        //            conObj.Open();
        //            //--------------------------------------------------//
        //            dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
        //            dataAdapter.InsertCommand.CommandType = CommandType.Text;

        //            // Create a parameter for the ReturnValue.
        //            SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
        //            parameter.Direction = ParameterDirection.Output;


        //            dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
        //            dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
        //            dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

        //            int res = dataAdapter.Update(tempTable);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Generic Exception for " + jobDetails.JobId + ":: " + ex.Message);
        //    }
        //}

        public void SaveNonCompatibleListToDB(JobStruct jobDetails, List<String> hcNonCompatiableList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks WHERE dbo.NonCompatibleStocks.SymbolID='QFQFQQF'";

            //String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks JOIN dbo.NonCompatibleDates ON dbo.NonCompatibleStocks.ID = dbo.NonCompatibleDates.RefID WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "SaveNonCompatibleListToDB()", ex.ToString(), selectorX);
            }

            foreach (var item in hcNonCompatiableList)
            {
                DataRow row = tempTable.NewRow();
                row["ID"] = Guid.NewGuid().ToString();
                row["SymbolID"] = item;
                row["StockExchange"] = jobDetails.ApplicableExchange;

                tempTable.Rows.Add(row);
            }

            try
            {
                //will be stored procedure to popualate the two tables

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    //--------------------------------------------------//
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;


                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, jobDetails.JobId, ex.ToString(), selectorX);
            }
        }

        public void SaveNonCompatibleListToDBNew(JobStruct jobDetails, List<NonCompatibleStock> nonCompatibleStockList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks JOIN dbo.NonCompatibleDates ON dbo.NonCompatibleStocks.ID = dbo.NonCompatibleDates.RefID WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            //String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks JOIN dbo.NonCompatibleDates ON dbo.NonCompatibleStocks.ID = dbo.NonCompatibleDates.RefID WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for :: " + ex.Message);
            }

            foreach (var item in nonCompatibleStockList)
            {
                String guid = Guid.NewGuid().ToString();

                foreach (var itemDateList in item.NonCompatibleDateList)
                {
                    DataRow row = tempTable.NewRow();
                    row["ID"] = guid;
                    row["CriteriaSymbolID"] = item.CriteriaSymbolID;
                    row["StockExchange"] = item.Exchange;

                    row["NonMatchingSymbolID"] = item.NonMatchingSymbolID;
                    row["NonMatchingStockExchange"] = jobDetails.ApplicableExchange;
                    row["RefID"] = guid;
                    row["Date"] = itemDateList;

                    tempTable.Rows.Add(row);
                }
            }

            try
            {
                //will be stored procedure to popualate the two tables

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    conObj.Open();

                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    dataAdapter.InsertCommand = new SqlCommand("proc_InsertNonCompatibleStocksAndDates", conObj);
                    dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                    
                    //

                    //conObj.Open();
                    ////--------------------------------------------------//
                    //dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
                    //dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    //// Create a parameter for the ReturnValue.
                    //SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    //parameter.Direction = ParameterDirection.Output;


                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }


        public void DeleteAllCompatiableSymbolsFromDB(String stockExchange)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                List<String> symbolList = new List<string>();
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {

                    String commandTxt = "DELETE FROM dbo.HistoricallyCompatibleStocks WHERE dbo.HistoricallyCompatibleStocks.StockExchange = '" + stockExchange + "'";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "DeleteAllCompatiableSymbolsFromDB()", ex.ToString(), selectorX);
            }
        }


        public bool RegisterProcessorEngine(String ticket, String machineProcessedOn, String FMDA_EngineName, DateTime jobStartTime)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];
            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_RegisterProcessingEngine", con);
                    con.Open();

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Ticket", ticket);
                    sqlCommand.Parameters.AddWithValue("@MachineProcessedOn", machineProcessedOn);
                    sqlCommand.Parameters.AddWithValue("@FMDA_EngineName", FMDA_EngineName);
                    sqlCommand.Parameters.AddWithValue("@JobStartTime", jobStartTime);

                    int affectedRows = sqlCommand.ExecuteNonQuery();

                    if (affectedRows > 0) return true;
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "RegisterProcessorEngine()", ex.ToString(), selectorX);
            }
            return false;
        }

        public bool InsertJobticketsIntoDB(List<JobTicketDef> jobTicketDefList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.Jobs WHERE dbo.Jobs.JobTicket ='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for :: " + ex.Message);
            }


            foreach (var jobTicketDef in jobTicketDefList)
            {
                    DataRow row = tempTable.NewRow();
                    row["ID"] = Guid.NewGuid().ToString();

                    row["JobTicket"] = jobTicketDef.JobTicket;
                    row["JobId"] = jobTicketDef.JobId;

                    row["SymbolID"] = jobTicketDef.SymbolID;
                    row["ProcessType"] = jobTicketDef.ProcessType;

                    row["StockExchange"] = jobTicketDef.Exchange;
                    row["CompatibilityStartDate"] = jobTicketDef.CompatibilityStartDate;
                    row["EndDate"] = jobTicketDef.EndDate;
                    row["JobTicketCreationDate"] = jobTicketDef.JobTicketCreationDate;

                    tempTable.Rows.Add(row);
            }

            
            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();

                    dataAdapter.InsertCommand = new SqlCommand("proc_InsertJobTickets", conObj);

                    dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@JobId", SqlDbType.NVarChar, 200, "JobId"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ProcessType", SqlDbType.NVarChar, 20, "ProcessType"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Exchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibilityStartDate", SqlDbType.Date, 20, "CompatibilityStartDate"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Date, 20, "EndDate"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@JobTicketCreationDate", SqlDbType.Date, 20,  "JobTicketCreationDate"));
                    
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@JobTicket", SqlDbType.NVarChar, 100, "JobTicket"));

                    int res = dataAdapter.Update(tempTable);

                    if (res > 0) return true;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for:: " + ex.Message);
            }
            return false;
        }

        public List<IncompletedJobTicket> GetIncompletedJobTickets()
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<IncompletedJobTicket> incompletedJobTicketList = new List<IncompletedJobTicket>();

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.Jobs.JobTicket, dbo.Jobs.CompatibilityStartDate, dbo.Jobs.EndDate, dbo.Jobs.ProcessType, dbo.Jobs.StockExchange, dbo.Jobs.SymbolID FROM dbo.Jobs WHERE dbo.Jobs.Completed = 0";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        IncompletedJobTicket incompletedJobTicket = new IncompletedJobTicket();
                        DateTime st = new DateTime();
                        DateTime ed = new DateTime();

                        incompletedJobTicket.JobTicket = rdr["JobTicket"].ToString();
                        
                        String[] stArray = rdr["CompatibilityStartDate"].ToString().Split(' ');
                        String[] edArray = rdr["EndDate"].ToString().Split(' ');
                        
                        DateTime startD = Convert.ToDateTime(stArray.FirstOrDefault());
                        DateTime endD = Convert.ToDateTime(edArray.FirstOrDefault());


                        String startDateStr = String.Format("{0}-{1}-{2}", startD.Year, startD.Month, startD.Day);
                        String endDateStr = String.Format("{0}-{1}-{2}", endD.Year, endD.Month, endD.Day);

                        incompletedJobTicket.CompatibilityStartDate = startDateStr;
                        incompletedJobTicket.EndDate = endDateStr;

                        incompletedJobTicket.ProcessType = rdr["ProcessType"].ToString();
                        incompletedJobTicket.Exchange = rdr["StockExchange"].ToString();
                        incompletedJobTicket.SymbolID = rdr["SymbolID"].ToString();

                        incompletedJobTicketList.Add(incompletedJobTicket);
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception for:: " + ex.Message);
            }

            return incompletedJobTicketList;
        }

        public bool JobTicketCompleted(String jobTicket, DateTime endDateTime, String fileNameOutput, String calculationType)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_JobTicketCompleted", con);
                    con.Open();
                    
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@JobTicket", jobTicket);
                    sqlCommand.Parameters.AddWithValue("@EndTime", endDateTime);
                    sqlCommand.Parameters.AddWithValue("@FileNameOutput", fileNameOutput);
                    sqlCommand.Parameters.AddWithValue("@CalculationType", calculationType);


                    int affectedRows = sqlCommand.ExecuteNonQuery();

                    if (affectedRows > 0) return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception:: " + ex.Message);
            }
            return false;
        }

        public bool TotalJobCompletionCheck(DateTime dateID)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_CheckAllJobCompletions", con);
                    con.Open();

                    DateTime dateTempID = new DateTime(dateID.Year, dateID.Month, dateID.Day);
                    DateTime dayEndTime = dateTempID.AddDays(1);

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@DayStartDatetime", dateTempID);
                    sqlCommand.Parameters.AddWithValue("@DayEndDatetime", dayEndTime);

                    
                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    int affectedRows = -99;

                    while (rdr.Read())
                    {
                        affectedRows = Convert.ToInt32(rdr["Total"].ToString());
                    }
                    if (affectedRows == 0) return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception:: " + ex.Message);
            }
            return false;
        }
    }
}

