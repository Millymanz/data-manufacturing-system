﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Threading;
using System.ServiceModel;

using System.ServiceModel.Channels;
using System.Net;
using System.Net.Sockets;
using LogSys;

namespace JobsManager
{
    static public class TestModeConditions
    {
       static public int JobticketsLimit = -1;
       static public String[] ProcessingType;
    }

    public enum JobPhase
    {
        DataConsumption = 0,
        SingleEx_FMDA_Processing = 1,
        Intermarket_FMDA_Processing = 2
    }

    public struct JobStruct
    {
        public String JobId;
        public JobPhase JobPhaseMember;
        public String ApplicableExchange;
    }

    public struct JobTicketDef
    {
        public String JobTicket;
        public String JobId;
        public String SymbolID;
        public String Exchange;
        public String CompatibilityStartDate;
        public String EndDate;
        public String JobTicketCreationDate;
        public String ProcessType;
    }

    public struct IncompletedJobTicket
    {
        public String JobTicket;
        public String SymbolID;
        public String Exchange;
        public String CompatibilityStartDate;
        public String EndDate;
        public String ProcessType;
    }

    public struct NonCompatibleStock
    {
        public String CriteriaSymbolID;
        public String Exchange;
        public String NonMatchingSymbolID;
        public String NonMatchingStockExchange;
        public List<DateTime> NonCompatibleDateList;
        public List<int> NonCompatibleDateIndexList;

    }

    static public class Informer
    {
        static public Boolean OnDemandTicketProcessing = false;
    }

    static public class JobsManager
    {
        static private Queue taskQueue = new Queue();

        static private Database dataModel = new Database();

       // static private bool[][] jobsFlagArray = new bool[System.Configuration.ConfigurationManager.AppSettings.Count][];

        static private bool[][] jobsFlagArray = new bool[Exchanges.List.Count][];


        //static private bool[][] jobsFlagArray = new bool[1][];

        static private bool bFirstProcessCall = true;

        static private bool bLastProcessCall = false;

        static int totals = 0;

        static private List<IncompletedJobTicket> _incompletedJobsList = new List<IncompletedJobTicket>();

        static private DateTime _DateID = new DateTime();

        //This is used to control tickets being added to the queue
        //If the current queue of jobs have not been processed then
        //new items cannot be added
        static private bool WorkInProgress = false;

        static private bool PauseJobRuns = false;



        static public void ProcessingCycleListenner()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            while (true)
            {
                if (WorkInProgress == false)
                {
                    UdpClient udpClient = new UdpClient(11000);
                    try
                    {
                        //IPEndPoint object will allow us to read datagrams sent from any source.
                        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), 11000);

                        Console.WriteLine("");
                        Console.WriteLine("[Waiting for Processing Cycle Notification..]");
                        Console.WriteLine("");


                        // Blocks until a message returns on this socket from a remote host.
                        Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                        string returnData = Encoding.ASCII.GetString(receiveBytes);

                        Console.WriteLine("Service  " + returnData);

                        String[] conditionArray = returnData.Split('_');

                        if (conditionArray.FirstOrDefault() == "bUpdateDBServicesANDStartProcessingCycle")
                        {
                            FlagCheckerHandler();
                           // BeginProcessingCycle();
                        }
                        udpClient.Close();
                    }
                    catch (Exception e)
                    {
                        String thread = System.Threading.Thread.CurrentThread.ToString();
                        String level = "Error";
                        String application = Console.Title;

                        String selector = "JobsManager";

                        switch (Database.Mode)
                        {
                            case MODE.Test:
                                {
                                    selector += "_TEST";
                                }
                                break;

                            case MODE.Live:
                                {
                                    selector += "_LIVE";
                                }
                                break;

                            case MODE.LiveTest:
                                {
                                    selector += "_LIVE-TEST";
                                }
                                break;
                        }

                        ApplicationErrorLogger.Log(thread, level, application, "ProcessingCycleListenner()", e.ToString(), selector);
                    }
                }
            }
        }

        static public void InitialiseJobsFlagArray()
        {
            int countExchanges = Exchanges.List.Count;

            for (int i = 0; i < countExchanges; i++)
            {
                jobsFlagArray[i] = new bool[4];
            }

            ResetJobsFlagArray();

            //-----------------------------------------//
            if (Informer.OnDemandTicketProcessing)
            {
                WorkInProgress = true;
                //First stage flags set
                for (int i = 0; i < jobsFlagArray.Length; i++)
                {
                    jobsFlagArray[i][(int)JobPhase.DataConsumption] = true;
                }
            }
        }

        static private void BeginProcessingCycle()
        {
            //bool bAllTrue = AllFlagsTrue(); //Temp disable phase2 and other have not be introduce yet so work is not 100% allflagstrue yet

            //if (bAllTrue)
            {
                ResetJobsFlagArray();

                //First stage flags set
                for (int i = 0; i < jobsFlagArray.Length; i++)
                {
                    jobsFlagArray[i][(int)JobPhase.DataConsumption] = true;
                }
                Console.WriteLine("[Beginning Processing Cycle]");
                Console.WriteLine("Preparing JobTickets");
                WorkInProgress = true;

            }
        }

        static private void ResetJobsFlagArray()
        {
            for (int i = 0; i < jobsFlagArray.Length; i++)
            {
                for (int j = 0; j < jobsFlagArray[i].Length; j++)
                {
                    jobsFlagArray[i][j] = false;
                }
            }
        }

        static public Thread CreateJobsThread(JobStruct jobId)
        {
            Object jobObj = jobId;

            ParameterizedThreadStart threadStart = new ParameterizedThreadStart(CreateJobs);

            Thread jobCreatorThread = new Thread(threadStart);
            jobCreatorThread.Name = jobId.ApplicableExchange + " Thread";
            jobCreatorThread.Start(jobId);
            
            return jobCreatorThread;

        }

        static public KeyValuePair<String, List<String>> GetTask(String FMDA_EngineName)
        {
            if (PauseJobRuns) return new KeyValuePair<string, List<string>>(); 

            KeyValuePair<String, List<String>> taskKVP = new KeyValuePair<string, List<string>>();

            bool bRegisterationComplete = false;

            totals++;

            if (bFirstProcessCall)
            {
                bFirstProcessCall = false;
            }


            if (taskQueue.Count > 0)
            {
                //task = (KeyValuePair<String, List<String>>) taskQueue.Dequeue();
                taskKVP = (KeyValuePair<String, List<String>>)taskQueue.Dequeue();

                bRegisterationComplete = RegisterProcessorEngine(taskKVP.Key, FMDA_EngineName);

                Console.WriteLine("{0} :: {1}", taskKVP.Key, taskQueue.Count);
            }
            else
            {
                if ((_DateID > DateTime.MinValue) && (_DateID < DateTime.MaxValue))
                {
                    CheckAllJobsCompletion(_DateID);
                }
            }

            if (taskQueue.Count == 0) WorkInProgress = false;//Open to creating new jobs from hench forth

            if ((taskQueue.Count == 0) && (bLastProcessCall == false))
            {
                bLastProcessCall = true;
            }

            if (bRegisterationComplete)
            {
                String dateStr = String.Format("*{0}{1}{2}", _DateID.Day, _DateID.Month, _DateID.Year);
                String tempTicket = taskKVP.Key  + dateStr;

                KeyValuePair<String, List<String>> tk = new KeyValuePair<string, List<string>>(tempTicket, taskKVP.Value);
                return tk;
            }
            return new KeyValuePair<string,List<string>>();
        }


        static public void CreateJobs(object jobId)
        {
            JobStruct jobStruct = (JobStruct)jobId;
            String jobIdentification = jobStruct.JobId;

            int ticketNoCount = 0;

            //Starttime
            DateTime startTime = DateTime.Now;
            String triggerlogFileName = jobStruct.ApplicableExchange + "-JobsManagerLog.txt";
            if (!System.IO.File.Exists(triggerlogFileName))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(triggerlogFileName)) { }
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
            {
                file.WriteLine(String.Format("StartTime  {0}  {1}", DateTime.Now.TimeOfDay, DateTime.Now));
                //file.WriteLine("");
            }

            //Check distinct list of symbols between the staging stocks and live db stocks, if theres a difference
            //in count or size then, A) work out which symbols are missing from the staging db
            // B) work out which symbols should be deleted from staging db
            // C) Do nothing if well matched

            //get latest date then let list be based on latest date condition

            DateTime lastMaxDate = new DateTime();

            List<String> symbolsTaskList = dataModel.GetHCSymbolsTaskList(jobStruct.ApplicableExchange, out lastMaxDate);

            List<String> currentCompatibleSymbolList = dataModel.GetCompatibleListFromDB(jobStruct.ApplicableExchange);

            List<String> bucketList = new List<String>();

            bool bAddSymbols = false;

            bool bSymbolNameChange = false; //SymbolNameChange or more no longer active

            if (symbolsTaskList.Count > currentCompatibleSymbolList.Count)
            {
                //A) work out which symbols are missing from the staging db
                foreach (var sym in symbolsTaskList)
                {
                    bool bExist = false;

                    foreach (var currentSym in currentCompatibleSymbolList)
                    {
                        if (sym == currentSym)
                        {
                            bExist = true;
                        }
                    }

                    if (bExist == false)
                    {
                        bAddSymbols = true;
                        bucketList.Add(sym);
                    }
                }
            }

            else if (symbolsTaskList.Count < currentCompatibleSymbolList.Count)
            {
                // B) work out which symbols should be deleted from staging db
                foreach (var currentSym in currentCompatibleSymbolList)
                {
                    bool bExist = false;

                    foreach (var sym in symbolsTaskList)
                    {
                        if (sym == currentSym)
                        {
                            bExist = true;
                        }
                    }

                    if (bExist == false)
                    {
                        bucketList.Add(currentSym);
                    }
                }
            }

            else if (currentCompatibleSymbolList.Count == 0)
            {
                bAddSymbols = true;
                bucketList = symbolsTaskList;
            }

            else if (symbolsTaskList.Count == currentCompatibleSymbolList.Count)
            {
                foreach (var sym in symbolsTaskList)
                {
                    bool bExist = false;

                    foreach (var currentSym in currentCompatibleSymbolList)
                    {
                        if (sym == currentSym)
                        {
                            bExist = true;
                        }
                    }

                    if (bExist == false)
                    {
                        bSymbolNameChange = true;
                        bucketList.Add(sym);
                    }
                }
            }

            //DateTime startDate = new DateTime();
            //DateTime endDate = new DateTime();

            Dictionary<String, KeyValuePair<DateTime, List<String>>> result = null;


            if ((bAddSymbols) && (bSymbolNameChange == false))
            {
                //Deal with reoccuring non-compatible stocks
                List<String> tempList = new List<string>();

                bool bExist = false;

                List<String> currentNonCompatibleSymbolList = dataModel.GetNonCompatibleListFromDB(jobStruct.ApplicableExchange);

                bool bRunCompatibilityCheck = true;

                if (currentNonCompatibleSymbolList.Count > 0)
                {
                    foreach (var sym in bucketList)
                    {
                        foreach (var nonSym in currentNonCompatibleSymbolList)
                        {
                            if (sym == nonSym)
                            {
                                bExist = true;
                            }
                        }

                        if (bExist == false)
                        {
                            tempList.Add(sym);
                        }
                    }

                    if (tempList.Count > 0)
                    {
                        //if new stocks then process the compatibility again from scratch

                        //bucketList.Clear();

                        dataModel.DeleteAllCompatiableSymbolsFromDB(jobStruct.ApplicableExchange);

                        bucketList.AddRange(tempList);
                    }
                    else
                    {
                        bRunCompatibilityCheck = false;
                    }
                }

                if (bRunCompatibilityCheck && bucketList.Count > 0)
                {
                    result = HistoricalCompatiabilityCheck(bucketList, jobStruct);

                    //Save compatible list in the temp db
                    SaveToDB(jobStruct, result);
                }
                else
                {
                    result = dataModel.GetCompatibleListDataFromDB(jobStruct.ApplicableExchange, currentCompatibleSymbolList);
                }
            }

            else if ((bAddSymbols == false) && (bSymbolNameChange))
            {
                //Symbol name change management

                //dataModel.UpdateCompatiableSymbols(bucketList)...?
            }

            else if ((bAddSymbols == false) && (bSymbolNameChange == false))
            {
                //remove symbols
                //redo this code
               // dataModel.DeleteNonCompatiableSymbolsFromDB(jobStruct.ApplicableExchange);

                //it is safe to say staging db has update date compatiblity list
            }

            int totalNoJobsthisExchange = 0;
            //Create job tickets

            if (result != null)
            {
                totalNoJobsthisExchange = JobticketProduction(jobIdentification, jobStruct.ApplicableExchange, ref ticketNoCount, result, lastMaxDate);
            }

            //end time
            TimeSpan span = DateTime.Now - startTime;

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
            {
                file.WriteLine(String.Format("EndTime  {0}  {1} Total Duration::{2}", DateTime.Now.TimeOfDay, DateTime.Now, span));
                file.WriteLine("");
            }

            //-------------------------------------------------//

            int min = 0;

            Console.WriteLine(" ");
            Console.WriteLine("READY ::" + jobStruct.ApplicableExchange + " " + totalNoJobsthisExchange);
            Console.WriteLine(" ");
        }

        static private void SaveToDB(JobStruct jobDetails, Dictionary<String, KeyValuePair<DateTime, List<String>>> compatibleList)
        {
            dataModel.SaveCompatibleListToDB(jobDetails, compatibleList);
        }

        static public void FlagChecker()
        {
            while (true)
            {
                var threadListing = new List<Thread>();

                //Phase 1 Job
                for (int i = 0; i < jobsFlagArray.Length; i++)
                {
                    if ((jobsFlagArray[i][(int)JobPhase.DataConsumption] == true) && (jobsFlagArray[i][(int)JobPhase.SingleEx_FMDA_Processing] == false)
                        && (jobsFlagArray[i][(int)JobPhase.Intermarket_FMDA_Processing] == false))
                    {
                        String jobId = Guid.NewGuid().ToString();

                        JobStruct jobStructure = new JobStruct();
                        jobStructure.JobId = jobId;
                        jobStructure.JobPhaseMember = JobPhase.SingleEx_FMDA_Processing;

                        jobStructure.ApplicableExchange = Exchanges.List[i];

                       threadListing.Add(CreateJobsThread(jobStructure));
                    }
                }
                //move this bit down once phase 2 is implemented
                foreach (var th in threadListing)
                {
                    th.Join();
                }
                //WorkInProgress = false;

                bool bAllTrue = AllFlagsTrue();
                //for (int ch = 0; ch < jobsFlagArray.Length; ch++)
                //{
                //    for (int g = 0; g < jobsFlagArray[ch].Length; g++)
                //    {
                //        if (jobsFlagArray[ch][g] == false)
                //        {
                //            bAllTrue = false;
                //        }
                //    }
                //}

                //Phase 2 Job
                if (bAllTrue)
                {




                    //ResetJobsFlagArray();
                }


                //ttemp just to break out

                //break;
            }
        }

        static public void FlagCheckerHandler()
        {
            List<String> exchangesList = null;
            if (Informer.OnDemandTicketProcessing)
            {
                exchangesList = Exchanges.List;
            }
            else
            {
                exchangesList = dataModel.GetFlagMarkers();
            }

            foreach (var item in exchangesList)
            {
                String jobId = Guid.NewGuid().ToString();

                JobStruct jobStructure = new JobStruct();
                jobStructure.JobId = jobId;
                jobStructure.JobPhaseMember = JobPhase.SingleEx_FMDA_Processing;

                jobStructure.ApplicableExchange = item;

                CreateJobsThread(jobStructure);

                if (Informer.OnDemandTicketProcessing == false)
                {
                    dataModel.UpdateFlagMarker(item);
                }
            }

        }


        static private bool AllFlagsTrue()
        {
            bool bAllTrue = true;
            for (int ch = 0; ch < jobsFlagArray.Length; ch++)
            {
                for (int g = 0; g < jobsFlagArray[ch].Length; g++)
                {
                    if (jobsFlagArray[ch][g] == false)
                    {
                        bAllTrue = false;
                    }
                }
            }
            return bAllTrue;
        }

        //Single Exchange PreProcess Analysis
        //
        //[unique][AllTimeTicketNo][TicketNo][CalculationLevel][TypeOfProcessing][Symbol][StockExchange][TaskParameter]


        static private int PrepareForHistoricalCorrelationsAnalysis(String jobIdentification, ref int allTimeTicketNo, String exchange, ref int ticketNoCount, Dictionary<String, KeyValuePair<DateTime, List<String>>> result, DateTime lastMaxDate)
        {
            int totalNoJobs = 0;

            //turn list into job tasks
            List<JobTicketDef> jobTicketDefList = new List<JobTicketDef>();

            bool bPlaceTicketsOnQueue = false;

            foreach (var item in result)
            {
                String startDateStr = String.Format("{0}-{1}-{2}", item.Value.Key.Year, item.Value.Key.Month, item.Value.Key.Day);
                String endDateStr = String.Format("{0}-{1}-{2}", lastMaxDate.Year, lastMaxDate.Month, lastMaxDate.Day);

                String taskParameter = String.Format(startDateStr + "@" + endDateStr);

                ticketNoCount++; //Not zero based
                

                //[unique][AllTimeTicketNo][TicketNo][CalculationLevel][TypeOfProcessing][Symbol][StockExchange][TaskParameter]
                String ticket = String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", jobIdentification, allTimeTicketNo, ticketNoCount, 0, "HC", item.Key, exchange, taskParameter);

                JobTicketDef jobTicketDef = CreateJobticketDef(ticket, jobIdentification, item.Key, exchange, startDateStr, endDateStr, "HC");

                if (JobsTicketTypeDuplicationCheck(jobTicketDef) == false)
                {
                    jobTicketDefList.Add(jobTicketDef);
                }

                allTimeTicketNo++;
            }
            
            //store in the database
            if (jobTicketDefList.Count > 0)
            {
                bPlaceTicketsOnQueue = dataModel.InsertJobticketsIntoDB(jobTicketDefList);
            }

            if (bPlaceTicketsOnQueue)
            {
                for (int i = 0; i < jobTicketDefList.Count; i++)
                {
                    var compatibleStocksList = result[jobTicketDefList[i].SymbolID].Value;
                    taskQueue.Enqueue(new KeyValuePair<String, List<String>>(jobTicketDefList[i].JobTicket, compatibleStocksList));
                    totalNoJobs++;
                }
            }
            return totalNoJobs;
        }

        static private Dictionary<String, KeyValuePair<DateTime, List<String>>> HistoricalCompatiabilityCheckNewVersion(List<String> symbolsTaskList, JobStruct jobDetails)
        {
            //Dictionary<String, List<String>> hcCompatiableList = new Dictionary<String, List<String>>();
            Dictionary<String, KeyValuePair<DateTime, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            //List<String> symbolsTaskListEE = new List<string>();
            //symbolsTaskListEE.Add("BPI");
            //symbolsTaskListEE.Add("AAU");
            //symbolsTaskList = symbolsTaskListEE;

             GC.Collect();
            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange);

            //startDate = tradeDateList.FirstOrDefault().Value.FirstOrDefault();
            //endDate = tradeDateList.FirstOrDefault().Value.Last();

            List<NonCompatibleStock> nonCompatibleStockList = new List<NonCompatibleStock>();

            List<String> nonCompatibles = new List<string>();

            foreach (var sym in symbolsTaskList)
            {
                String symTask = sym;

                List<DateTime> symTaskDateList = tradeDateList[symTask];
                DateTime criteriaStartDate = symTaskDateList.FirstOrDefault();

                List<String> distinctList = new List<string>();

                foreach (KeyValuePair<string, List<DateTime>> item in tradeDateList)
                {
                    bool bIsCompatible = true;
                    if (symTask != item.Key)
                    {
                        NonCompatibleStock nonCompatibleStock = new NonCompatibleStock();
                        List<DateTime> nonCompatibleDateList = new List<DateTime>();

                        //Get dates which are equal and overlap to the IPO of the criteria stock
                        List<DateTime> vaildDates = (from suitableDates in item.Value
                                         where suitableDates >= criteriaStartDate
                                         select suitableDates).ToList();


                        if (vaildDates.Count > 0)
                        {
                            foreach (var dateItem in symTaskDateList)
                            {
                                if (vaildDates.Contains(dateItem) == false)
                                {
                                    nonCompatibleDateList.Add(dateItem);
                                    bIsCompatible = false;
                                }
                            }

                            if (bIsCompatible)
                            {

                                if (hcCompatiableList.ContainsKey(symTask) == false)
                                {
                                    distinctList.Add(item.Key);
                                }
                            }
                            else
                            {
                                nonCompatibleStock.CriteriaSymbolID = symTask;
                                nonCompatibleStock.Exchange = jobDetails.ApplicableExchange;
                                nonCompatibleStock.NonMatchingSymbolID = item.Key;

                                nonCompatibleStock.NonCompatibleDateList = nonCompatibleDateList;
                                nonCompatibleStockList.Add(nonCompatibleStock);
                            }
                        }
                    }
                }

                hcCompatiableList.Add(symTask, new KeyValuePair<DateTime, List<String>>(criteriaStartDate, distinctList));


            }

            //to overcome outofmemory issue store index position instead

            //Save to the database
            if (nonCompatibleStockList.Count > 0) dataModel.SaveNonCompatibleListToDBNew(jobDetails, nonCompatibleStockList);

            return hcCompatiableList;

        }

        static private Dictionary<String, KeyValuePair<DateTime, List<String>>> HistoricalCompatiabilityCheckNewTemp(List<String> symbolsTaskList, JobStruct jobDetails)
        {
            //Dictionary<String, List<String>> hcCompatiableList = new Dictionary<String, List<String>>();
            Dictionary<String, KeyValuePair<DateTime, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            //List<String> symbolsTaskListEE = new List<string>();
            //symbolsTaskListEE.Add("BPI");
            //symbolsTaskListEE.Add("AAU");
            //symbolsTaskList = symbolsTaskListEE;


            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange);

            //startDate = tradeDateList.FirstOrDefault().Value.FirstOrDefault();
            //endDate = tradeDateList.FirstOrDefault().Value.Last();

            List<NonCompatibleStock> NonCompatibleStockList = new List<NonCompatibleStock>();

            List<String> nonCompatibles = new List<string>();

            foreach (var sym in symbolsTaskList)
            {
                GC.Collect();

                DateTime startDateItem = new DateTime();

                String symTask = sym;
                List<String> tempList = new List<String>();

                bool bStartDate = true;

                bool bIsCompatible = false;

                List<int> nonCompatibleDateList = new List<int>();

                foreach (KeyValuePair<string, List<DateTime>> item in tradeDateList)
                {
                    if (symTask != item.Key)
                    {
                        NonCompatibleStock nonCompatibleStock = new NonCompatibleStock();

                        if (tradeDateList[symTask].Count <= item.Value.Count)
                        {
                            bool bMatch = true;

                            for (int p = 0; p < item.Value.Count; p++)
                            {

                                DateTime criteriaStartDate = tradeDateList[symTask].FirstOrDefault();

                                if (item.Value[p] >= criteriaStartDate)
                                {
                                    bool bFirstTimeCheck = true;

                                    for (int i = 0; i < tradeDateList[symTask].Count; i++)
                                    {
                                        //Sets index to the ruight value so that correct dates 
                                        //within the list can be compared, given that acll stocks are assumed to have
                                        //started in 2000/01/01
                                        int index = p + i;

                                        if (index < item.Value.Count)
                                        {
                                            if (bFirstTimeCheck)
                                            {
                                                bFirstTimeCheck = false;
                                                //Does the first acceptable comparable startdate match the criteria first date
                                                if (criteriaStartDate != item.Value[index])
                                                {
                                                    nonCompatibleDateList.Add(i);
                                                    bMatch = false;
                                                    //break;
                                                }
                                            }

                                            if (tradeDateList[symTask][i] != item.Value[index])
                                            {
                                                nonCompatibleDateList.Add(i);

                                                bMatch = false;
                                            }
                                        }
                                        else
                                        {
                                            nonCompatibleDateList.Add(i);

                                            bMatch = false;
                                        }
                                    }
                                    if (bMatch)
                                    {
                                        bIsCompatible = true;
                                        tempList.Add(item.Key);

                                        if (bStartDate) startDateItem = criteriaStartDate;

                                    }
                 
                                }
                            }
                        }

                        nonCompatibleStock.NonCompatibleDateIndexList = nonCompatibleDateList;
                        nonCompatibleStock.CriteriaSymbolID = symTask;
                        nonCompatibleStock.Exchange = jobDetails.ApplicableExchange;
                        nonCompatibleStock.NonMatchingSymbolID = item.Key;

                        NonCompatibleStockList.Add(nonCompatibleStock);
                    }
                }

                if (bIsCompatible)
                {
                    List<String> distinctList = tempList.Distinct().ToList();

                    if (hcCompatiableList.ContainsKey(symTask) == false)
                    {
                        hcCompatiableList.Add(symTask, new KeyValuePair<DateTime, List<String>>(startDateItem, distinctList));
                    }
                }
                else
                {
                    //Sorry this needs to be a list of NonCompatibleStocksList which in turn holds NonCompatibleStock which also then holds the dates NonCompatibleDateList
                    //run code to verify
                }
            }

            //to overcome outofmemory issue store index position instead

            //Save to the database
            if (NonCompatibleStockList.Count > 0) dataModel.SaveNonCompatibleListToDBNew(jobDetails, NonCompatibleStockList);

            return hcCompatiableList;

        }

        static private Dictionary<String, KeyValuePair<DateTime, List<String>>> HistoricalCompatiabilityCheck(List<String> symbolsTaskList, JobStruct jobDetails)
        {
            //Dictionary<String, List<String>> hcCompatiableList = new Dictionary<String, List<String>>();
            Dictionary<String, KeyValuePair<DateTime, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            //List<String> symbolsTaskListEE = new List<string>();
            //symbolsTaskListEE.Add("BPI");
            //symbolsTaskListEE.Add("AAU");
            //symbolsTaskList = symbolsTaskListEE;


            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange);

            //startDate = tradeDateList.FirstOrDefault().Value.FirstOrDefault();
            //endDate = tradeDateList.FirstOrDefault().Value.Last();

            List<String> nonCompatibles = new List<string>();

            foreach (var sym in symbolsTaskList)
            {
                DateTime startDateItem = new DateTime();

                String symTask = sym;
                List<String> tempList = new List<String>();

                //hcCompatiableList.Add(tradeDateList.FirstOrDefault().Key);

                bool bStartDate = true;

                bool bIsCompatible = false;


                foreach (KeyValuePair<string, List<DateTime>> item in tradeDateList)
                {
                    if (symTask != item.Key)
                    {

                        if (tradeDateList[symTask].Count <= item.Value.Count)
                        {
                            bool bMatch = true;

                            for (int p = 0; p < item.Value.Count; p++)
                            {
                                DateTime criteriaStartDate = tradeDateList[symTask].FirstOrDefault();

                                if (item.Value[p] >= criteriaStartDate)
                                {
                                    bool bFirstTimeCheck = true;

                                    for (int i = 0; i < tradeDateList[symTask].Count; i++)
                                    {
                                        int index = p + i;

                                        if (index < item.Value.Count)
                                        {
                                            if (bFirstTimeCheck)
                                            {
                                                bFirstTimeCheck = false;
                                                //Does the first acceptable comparable startdate match the criteria first date
                                                if (criteriaStartDate != item.Value[index])
                                                {
                                                    bMatch = false;
                                                    break;
                                                }
                                            }

                                            if (tradeDateList[symTask][i] != item.Value[index])
                                            {
                                                bMatch = false;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            bMatch = false;
                                            break;
                                        }
                                    }
                                    if (bMatch)
                                    {
                                        bIsCompatible = true;
                                        tempList.Add(item.Key);

                                        //if (bStartDate) startDateItem = item.Value.FirstOrDefault();

                                        if (bStartDate) startDateItem = criteriaStartDate;
                                        //hcCompatiableList.Add(new KeyValuePair<string>symTask, item.Key);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (bIsCompatible)
                {
                    List<String> distinctList = tempList.Distinct().ToList();

                    if (hcCompatiableList.ContainsKey(symTask) == false)
                    {
                        hcCompatiableList.Add(symTask, new KeyValuePair<DateTime, List<String>>(startDateItem, distinctList));
                    }
                }
                else
                {
                    //workout the non compatible
                    nonCompatibles.Add(symTask);
                }
            }

            //Save to the database
            if (nonCompatibles.Count > 0) dataModel.SaveNonCompatibleListToDB(jobDetails, nonCompatibles);

            return hcCompatiableList;
        }


        static private Dictionary<String, KeyValuePair<DateTime, List<String>>> HistoricalCompatiabilityCheckOldChanged(List<String> symbolsTaskList, JobStruct jobDetails)
        {
            //Dictionary<String, List<String>> hcCompatiableList = new Dictionary<String, List<String>>();
            Dictionary<String, KeyValuePair<DateTime, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            //List<String> symbolsTaskListEE = new List<string>();
            //symbolsTaskListEE.Add("BPI");
            //symbolsTaskListEE.Add("AAU");
            //symbolsTaskList = symbolsTaskListEE;


            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange);

            //startDate = tradeDateList.FirstOrDefault().Value.FirstOrDefault();
            //endDate = tradeDateList.FirstOrDefault().Value.Last();
            
            List<NonCompatibleStock> NonCompatibleStockList = new List<NonCompatibleStock>();

            List<String> nonCompatibles = new List<string>();

            foreach (var sym in symbolsTaskList)
            {
                GC.Collect();

                DateTime startDateItem = new DateTime();

                String symTask = sym;
                List<String> tempList = new List<String>();

                bool bStartDate = true;

                bool bIsCompatible = false;
                
                List<DateTime> nonCompatibleDateList = new List<DateTime>();

                foreach (KeyValuePair<string, List<DateTime>> item in tradeDateList)
                {
                    if (symTask != item.Key)
                    {
                        NonCompatibleStock nonCompatibleStock = new NonCompatibleStock();

                        if (tradeDateList[symTask].Count <= item.Value.Count)
                        {
                            bool bMatch = true;

                            for (int p = 0; p < item.Value.Count; p++)
                            {

                                DateTime criteriaStartDate = tradeDateList[symTask].FirstOrDefault();

                                if (item.Value[p] >= criteriaStartDate)
                                {
                                    bool bFirstTimeCheck = true;

                                    for (int i = 0; i < tradeDateList[symTask].Count; i++)
                                    {
                                        //Sets index to the ruight value so that correct dates 
                                        //within the list can be compared, given that acll stocks are assumed to have
                                        //started in 2000/01/01
                                        int index = p + i;

                                        if (index < item.Value.Count)
                                        {
                                            if (bFirstTimeCheck)
                                            {
                                                bFirstTimeCheck = false;
                                                //Does the first acceptable comparable startdate match the criteria first date
                                                if (criteriaStartDate != item.Value[index])
                                                {
                                                    nonCompatibleDateList.Add(tradeDateList[symTask][i]);
                                                    bMatch = false;
                                                    break;
                                                }
                                            }

                                            if (tradeDateList[symTask][i] != item.Value[index])
                                            {
                                                nonCompatibleDateList.Add(tradeDateList[symTask][i]);

                                                bMatch = false;
                                                break; 
                                            }
                                        }
                                        else
                                        {
                                            nonCompatibleDateList.Add(tradeDateList[symTask][i]);

                                            bMatch = false;
                                           break;
                                        }
                                    }
                                    if (bMatch)
                                    {
                                        bIsCompatible = true;
                                        tempList.Add(item.Key);

                                        //if (bStartDate) startDateItem = item.Value.FirstOrDefault();
                                        if (bStartDate) startDateItem = criteriaStartDate;
                                    }
                                    else
                                    {
                                        // noncompatibledatelist.add(tradedatelist[symtask][p]);
                                        break;
                                    }
                                }
                            }
                        }
                        //nonCompatibleStock.NonCompatibleDateList = nonCompatibleDateList;
                        //nonCompatibleStock.CriteriaSymbolID = symTask;
                        //nonCompatibleStock.Exchange = jobDetails.ApplicableExchange;
                        //nonCompatibleStock.NonMatchingSymbolID = item.Key;

                        //NonCompatibleStockList.Add(nonCompatibleStock);
                    }
                }

                if (bIsCompatible)
                {
                    List<String> distinctList = tempList.Distinct().ToList();

                    if (hcCompatiableList.ContainsKey(symTask) == false)
                    {
                        hcCompatiableList.Add(symTask, new KeyValuePair<DateTime, List<String>>(startDateItem, distinctList));
                    }
                }
                else
                {
                    
                    //nonCompatibleStock.NonCompatibleDateList = nonCompatibleDateList;
                    //nonCompatibleStock.CriteriaSymbolID = symTask;
                    //nonCompatibleStock.Exchange = jobDetails.ApplicableExchange;
                    //nonCompatibleStock.NonMatchingSymbolID = item.Key;

                    //NonCompatibleStockList.Add(nonCompatibleStock);

                    //Sorry this needs to be a list of NonCompatibleStocksList which in turn holds NonCompatibleStock which also then holds the dates NonCompatibleDateList
                    //run code to verify

                    //workout the non compatible
                    nonCompatibles.Add(symTask);
                }
            }

            //to overcome outofmemory issue store index position instead

            //Save to the database
            if (NonCompatibleStockList.Count > 0) dataModel.SaveNonCompatibleListToDB(jobDetails, nonCompatibles);



            return hcCompatiableList;

        }

        static private bool RegisterProcessorEngine(String ticket, String FMDA_EngineName)
        {
            String computerName = "";
            try
            {
                var remp = OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                string[] computer_name = Dns.GetHostEntry(remp.Address).HostName.Split(new Char[] { '.' });
                computerName = computer_name[0].ToString();
            }
            catch (Exception e)
            {
                computerName = "NOTFOUND " + e;
            }

            DateTime startDateTime = DateTime.Now;
            return dataModel.RegisterProcessorEngine(ticket, computerName, FMDA_EngineName, startDateTime);
        }

        static private JobTicketDef CreateJobticketDef(String ticket, String JobId, String SymbolID, String Exchange, String startDateStr, String endtDateStr, String processType)
        {
            JobTicketDef jTD = new JobTicketDef();
            jTD.JobTicket = ticket;
            jTD.JobId = JobId;
            jTD.SymbolID = SymbolID;
            jTD.Exchange = Exchange;
            jTD.CompatibilityStartDate = startDateStr;
            jTD.EndDate = endtDateStr;
            jTD.JobTicketCreationDate = String.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            jTD.ProcessType = processType;

            return jTD;
        }

        static private int JobticketProduction(String jobIdentification, String exchange, ref int ticketNoCount, Dictionary<String, KeyValuePair<DateTime, List<String>>> result, DateTime lastMaxDate)
        {
            int totalNoJobs = 0;

            if (result != null)
            {
                _DateID = DateTime.Now;
                //Place incomplete jobs back on the queue for processing

                int allTimeTicketNo = dataModel.GetAllTimeTicketNo();

                //temp renable!!
                _incompletedJobsList.AddRange(dataModel.GetIncompletedJobTickets());

                int count = 0;
                bool testModeLimitReached = false;

                if ((_incompletedJobsList.Count > 0) && (result.Count > 0))
                {
                    foreach (var item in _incompletedJobsList)
                    {
                        if (Database.Mode == MODE.Test)
                        {
                            if (count == TestModeConditions.JobticketsLimit)
                            {
                                testModeLimitReached = true; break;
                            }
                        }

                        if (taskQueue.Contains(item.JobTicket) == false)
                        {
                            if (result.ContainsKey(item.SymbolID))
                            {
                                var compatibleStocksList = result[item.SymbolID].Value;
                                taskQueue.Enqueue(new KeyValuePair<String, List<String>>(item.JobTicket, compatibleStocksList));
                                totalNoJobs++;
                            }
                        }
                        count++;
                    }
                }

                //-------------------Beginning of SingleMarket Calculations--------------//

                if ((Database.Mode != MODE.Test) || ((Database.Mode == MODE.Test) && (testModeLimitReached == false)))
                {
                    totalNoJobs += PrepareForHistoricalCorrelationsAnalysis(jobIdentification, ref allTimeTicketNo, exchange, ref ticketNoCount, result, lastMaxDate);

                    FlagMarking(exchange, JobPhase.SingleEx_FMDA_Processing);
                }
                //-------------------End of SingleMarket Calculations--------------//


                //Intermarket calculations
                //then mark flag

                //Clear incompleted jobs list until next time
                _incompletedJobsList.Clear();
            }

            return totalNoJobs;

        }

        static private bool JobsTicketTypeDuplicationCheck(JobTicketDef jobTicketDef)
        {
            var exist = from incompItem in _incompletedJobsList
                        where incompItem.SymbolID == jobTicketDef.SymbolID &&
                        incompItem.ProcessType == jobTicketDef.ProcessType &&
                        incompItem.Exchange == jobTicketDef.Exchange &&
                        incompItem.CompatibilityStartDate == jobTicketDef.CompatibilityStartDate &&
                        incompItem.EndDate == jobTicketDef.EndDate
                        select incompItem;

            if (exist.Count() == 0)
            {
                return false;
            }
            return true;
        }

        static public void JobTicketCompleted(String jobTicket, String fileNameOutput, String calculationType)
        {
            if (dataModel != null)
            {
                dataModel.JobTicketCompleted(jobTicket, DateTime.Now, fileNameOutput, calculationType);
            }
        }

        static public void CheckAllJobsCompletion(DateTime dateID)
        {
            if (dataModel != null)
            {
                if (dataModel.TotalJobCompletionCheck(dateID))
                {
                    Notification.Notifyer notifyImporter = new Notification.Notifyer();

                    if (notifyImporter.BeginImportation((int)Database.Mode, _DateID) == -1)
                    {
                        Console.WriteLine("Connection Failed - Application MODE did not MATCH!");
                    }
                }
            }
        }

        static public void FlagMarking(string stockExchange, JobPhase jobPhase)
        {
           // int countExchanges = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TOTALNOEXCHANGES"]);
            int countExchanges = Exchanges.List.Count();

            for (int i = 0; i < countExchanges; i++)
            {
                if (Exchanges.List[i] == stockExchange)
                {
                    jobsFlagArray[i][(int)jobPhase] = true;
                }
            } 
        }

        static public void AppControl()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            String portKey = "APPCONTROLPORT";            
            portKey += Database.Mode == MODE.Live ? "_LIVE" : "_TEST";

            int port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[portKey]);

            UdpClient udpClient = new UdpClient(port);

            while (true)
            {
                udpClient.EnableBroadcast = true;
                try
                {
                    //IPEndPoint object will allow us to read datagrams sent from any source.
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), 0);

                    // Blocks until a message returns on this socket from a remote host.
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);

                    //Check received text  if PAUSE_JOBS or RUN_JOBS

                    //set PauseJobRuns accordingly
                    if (returnData == "PAUSE_JOBS") PauseJobRuns = true;
                    else if (returnData == "RUN_JOBS") PauseJobRuns = false;

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }
            udpClient.Close();
        }
    }
}
