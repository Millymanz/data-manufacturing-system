﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace JobsManager
{
    // NOTE: If you change the interface name "IService1" here, you must also update the reference to "IService1" in App.config.
    [ServiceContract]
    public interface IJobsManagerService
    {
        [OperationContract]
        void TaskCompleted(String jobTicket, String fileNameOutput, String calculationType);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        KeyValuePair<String, List<String>> GetTask(String FMDA_EngineName, int mode);

        [OperationContract]
        DateTime GetMasterNodeDateTime();

        [OperationContract]
        bool ModeCheck(int mode);


        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }


    //Must be maintained with the App settings
    //Find a way to make this dynamic and only specified from the app settings instead of hard coded.
    [DataContract]
    public enum Exchange
    {
        [EnumMember]
        AMEX = 0,

        [EnumMember]
        LSE = 1,

        [EnumMember]
        NASDAQ = 2,

        [EnumMember]
        NYSE = 3
    }


}
