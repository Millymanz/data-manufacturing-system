﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Concurrent;

namespace JobsManager
{
    static public class Exchanges
    {
        static public List<String> List = new List<string>();

        static public void InitialiseExchangeList()
        {
            string[] exchangesArray = System.Configuration.ConfigurationManager.AppSettings["EXCHANGES"].Split(';');
            Exchanges.List = exchangesArray.ToList();
        }
    }


    //Temp will be in its own file
    public class TradeDate
    {
        private DateTime date;
        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }
    }


    //temps

    public class DayTradeSummary
    {
        private DateTime date;
        private double open;
        private double high;
        private double low;
        private double close;
        private double adjustmentclose;
        private int volume;

        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public double Open
        {
            get { return open; }
            set { open = value; }
        }

        public double High
        {
            get { return high; }
            set { high = value; }
        }

        public double Low
        {
            get { return low; }
            set { low = value; }
        }

        public double Close
        {
            get { return close; }
            set { close = value; }
        }

        public int Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public double AdjustmentClose
        {
            get { return adjustmentclose; }
            set { adjustmentclose = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }
    }

    public enum MODE
    {
        Live = 0,
        Test = 1,
        LiveTest = 2
    }


    public class Database
    {
        static public MODE Mode;

        private List<KeyValuePair<String, String>> symbolLookUpTable = null;

        private Object thisLock = new Object();
        public static bool bAlreadyOccupiedFlag = false;
        public static bool bAlreadyOccupiedFinalFlag = false;


        private Object _registerationLock = new Object();
        private Object _registerationBulkLock = new Object();
        public static bool bAlreadyRegBulkOccupiedFlag = false;

        private Object _jobTicketCompletedLock = new Object();


        public Database()
        {
            //Populate this using a file with all Forex symbols!!
            //Make sure that it does not create a ticket for symbols that are not in the DB

            //Or change the import code in the db
            PopulateSymbolLookUpTable();
        }

        public List<String> GetHCSymbolsTaskList()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            List<DateTime> listLatestDayTradeDate = new List<DateTime>();

            //This code assumes that the index dbs are update with the latest day trade summaries
            //
            //
            
            //foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            foreach (var item in Exchanges.List)
            {
                DateTime latestDayTradeDate = new DateTime();
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT MAX(dbo.DayTradeSummaries.Date) AS MaxDate FROM dbo.DayTradeSummaries";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        latestDayTradeDate = Convert.ToDateTime(rdr["MaxDate"].ToString());
                    }
                }
                listLatestDayTradeDate.Add(latestDayTradeDate);
            }

            var ldTradeDate = (from dateItem in listLatestDayTradeDate select dateItem).Max();


            String dateCriteria = String.Format("{0}-{1}-{2}", ldTradeDate.Year, ldTradeDate.Month, ldTradeDate.Day);

            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {


                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate < '" + dateCriteria + "'";
                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    hcSymbolTaskList.Add(symbol);
                }
            }

            if (hcSymbolTaskList.Count == 0)//this condition is true for both uptodate and empty db table..?
            {
                foreach (var item in Exchanges.List)
                {
                    String stockExchange = item.ToString();

                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                        con.Open();

                        SqlDataReader rdr = sqlCommand.ExecuteReader();

                        while (rdr.Read())
                        {
                            String symbol = rdr["SymbolID"].ToString();

                            hcSymbolTaskList.Add(symbol);
                        }
                    }
                }
            }
            else
            {
                //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
                //exclude these symbols from the hcSymbolTaskList
                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate >= '" + dateCriteria + "'";

            }
            //if (hcSymbolTaskList.Count > 200)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 200);
            //}
            //else
            return hcSymbolTaskList;
        }

        public List<String> GetSymbolsWithDataUpdates(String exchange, String timeFrame)
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //
            String stockExchange = exchange;

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            if (exchange == "Forex" || exchange == "FOREX") selector += "_Forex";

            String tempSelector = selector;
         
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];
            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_GetLatestUpdatedSymbols", con);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@TimeFrame", timeFrame);

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    bool bDate = true;

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //ApplicationErrorLogger.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
            }
            return hcSymbolTaskList;
        }


        public DateTime GetEarliestDateTime(String exchange)
        {
            DateTime earliest = DateTime.Now;

            String stockExchange = exchange;

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;
            }

            String tempSelector = selector;

           var commandTxt = "SELECT dbo.tblTrades.SymbolID, MAX(dbo.tblTrades.DateTime) AS MinDate FROM dbo.tblTrades";

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        earliest = DateTime.Parse(rdr["MinDate"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                //ApplicationErrorLogger.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
            }
            return earliest;
        }

        public int GetAllTimeTicketNo()
        {
            int count = 0;

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //Updated symbols only, latest
                String commandTxt = "SELECT COUNT(*) As TotalTickets FROM dbo.[Jobs]";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    count = Convert.ToInt32(rdr["TotalTickets"].ToString());
                }
            }
            return count;
        }

        public List<String> GetFlagMarkers()
        {
            List<String> exchangesList = new List<String>();

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //Updated symbols only, latest
                String commandTxt = "SELECT [Exchange],[Notify] FROM [JobsManager].[dbo].[Notify] D WHERE D.Notify = '1'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    exchangesList.Add(rdr["Exchange"].ToString());
                }
            }
            return exchangesList;
        }

        public void UpdateFlagMarker(String exchange)
        {
            List<String> exchangesList = new List<String>();

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //Updated symbols only, latest
                String commandTxt = "UPDATE [JobsManager].[dbo].[Notify] SET [JobsManager].[dbo].[Notify].Notify = '0' WHERE [JobsManager].[dbo].[Notify].Exchange = '" + exchange + "'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;
                con.Open();

                int rowsAffected = sqlCommand.ExecuteNonQuery();

            }
        }


        public List<String> GetHCAllSymbolsTaskList()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //

            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT TOP 20 dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }
            return hcSymbolTaskList;
        }

        public List<String> GetRCSymbolsTaskList()
        {
            //specify exchange
            List<String> rcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //assumes range correlation table is empty

            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        rcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }

            //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
            //exclude these symbols from the hcSymbolTaskList

            //if (hcSymbolTaskList.Count > 500)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 500);
            //}
            //else
            return rcSymbolTaskList;
        }


        public Dictionary<String, List<DateTime>> GetTradeDates(List<String> symbolList, String stockExchange, String timeFrame)
        {
            String criteria = "";
            bool bfirstItem = true;

            //foreach (var sym in symbolList)
            //{
            //    if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "'"; bfirstItem = false; }
            //    else
            //    {
            //        criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "'";
            //    }
            //}

            //foreach (var sym in symbolList)
            //{
            //    //if (bfirstItem) { criteria += "dbo.tblTrades.DateTime >='2015-02-17 21:00' AND dbo.tblTrades.SymbolID ='" + sym + "'"; bfirstItem = false; }
            //    //else
            //    //{
            //    //    criteria += " OR dbo.tblTrades.DateTime >='2015-02-17 21:00' AND dbo.tblTrades.SymbolID ='" + sym + "'";
            //    //}

            //    //if (bfirstItem) { criteria += "dbo.tblTrades.DateTime BETWEEN '2015-02-17 17:00' AND '2015-02-17 23:00' AND dbo.tblTrades.SymbolID ='" + sym + "'"; bfirstItem = false; }
            //    //else
            //    //{
            //    //    criteria += " OR dbo.tblTrades.DateTime BETWEEN '2015-02-17 17:00' AND '2015-02-17 23:00' AND dbo.tblTrades.SymbolID ='" + sym + "'";
            //    //}

            //}


            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.tblTrades.SymbolID ='" + sym + "' AND dbo.tblTrades.TimeFrame = '"+ timeFrame +"'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.tblTrades.SymbolID ='" + sym + "' AND dbo.tblTrades.TimeFrame = '"+ timeFrame +"'";
                }
            }
            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();


            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            String tempSelector = selector;
            if (stockExchange == "Forex") tempSelector += "_Forex";


            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //String commandTxt = "SELECT dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime FROM dbo.tblTrades WHERE " + criteria + " ORDER BY dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime ASC";

                String commandTxt = "SELECT dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime FROM dbo.tblTrades WHERE " + criteria + " ORDER BY dbo.tblTrades.SymbolID, dbo.tblTrades.DateTime ASC";


                //temporary solution
                //String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " AND dbo.DayTradeSummaries.Date >='2000-01-01' ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0; //waiting period 
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    //TradeDate tradeDate = new TradeDate();

                    //tradeDate.SymbolID = rdr["SymbolID"].ToString();
                    //tradeDate.Date = Convert.ToDateTime(rdr["Date"].ToString());

                    String sym = rdr["SymbolID"].ToString();
                    DateTime dateItem = Convert.ToDateTime(rdr["DateTime"].ToString());

                    if (!tradeDateList.ContainsKey(sym))
                    {
                        List<DateTime> dateList = new List<DateTime>();
                        dateList.Add(dateItem);

                        tradeDateList[sym] = dateList;
                    }
                    else
                    {
                        tradeDateList[sym].Add(dateItem);
                    }
                }
            }
            return tradeDateList;
        }

        public Dictionary<String, List<DateTime>> GetTradeDatesRange(List<String> symbolList, DateTime startDate, DateTime endDate)
        {
            String criteria = "";
            bool bfirstItem = true;
            
            String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'";
                }
            }

            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();

            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String sym = rdr["SymbolID"].ToString();
                        DateTime dateItem = Convert.ToDateTime(rdr["Date"].ToString());

                        if (!tradeDateList.ContainsKey(sym))
                        {
                            List<DateTime> dateList = new List<DateTime>();
                            dateList.Add(dateItem);

                            tradeDateList[sym] = dateList;

                        }
                        else
                        {
                            tradeDateList[sym].Add(dateItem);
                        }
                    }
                }
            }

            return tradeDateList;
        }

        public List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummaries(List<String> symbolList, DateTime startDate, DateTime endDate)
        {

            List<KeyValuePair<String, List<DayTradeSummary>>> stockSymbolsHistory = new List<KeyValuePair<String, List<DayTradeSummary>>>();

            foreach (string symbol in symbolList)
            {
                List<DayTradeSummary> dataList = new List<DayTradeSummary>();

                List<Double> dataListTemp = new List<Double>();

                String stockExchange = LookUpSymbolStockExchange(symbol);

                try
                {
                    String selector = stockExchange;
                    switch (Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selector += "_LIVE-TEST";
                            }
                            break;
                    }

                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ConnectionString))
                    {
                        conn.Open();

                        String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
                        String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

                        //SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);

                        SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);

                        SqlDataReader reader = sqlCommand.ExecuteReader();


                        while (reader.Read())
                        {
                            String SymbolID = reader["SymbolID"].ToString();

                            //Double data = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            DayTradeSummary dayTrade = new DayTradeSummary();
                            dayTrade.SymbolID = reader["SymbolID"].ToString();
                            dayTrade.Date = Convert.ToDateTime(reader["Date"].ToString());

                            dayTrade.Open = Convert.ToDouble(reader["Open"].ToString());
                            dayTrade.High = Convert.ToDouble(reader["High"].ToString());
                            dayTrade.Low = Convert.ToDouble(reader["Low"].ToString());
                            dayTrade.Close = Convert.ToDouble(reader["Close"].ToString());
                            dayTrade.Volume = Convert.ToInt32(reader["Volume"].ToString());


                            dayTrade.AdjustmentClose = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            dataList.Add(dayTrade);

                            //dataListTemp.Add(dayTrade.AdjustmentClose);

                        }


                        stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(symbol, dataList));

                        //masterSymbolDataList.Add(new KeyValuePair<string, List<double>>(symbol, dataListTemp));

                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Write("Msg " + e.ToString());
                }
            }
            return stockSymbolsHistory;
        }

        private void PopulateSymbolLookUpTable()
        {

            //Populate this using a file with all Forex symbols!!
            symbolLookUpTable = new List<KeyValuePair<string, string>>();

            //foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            foreach (var item in Exchanges.List)
            {
                String stockExchange = item.ToString();

                String selector = "TRADES";
                //String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                String tempSelector = selector;
                if (stockExchange == "Forex") tempSelector += "_Forex";

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[tempSelector];

                try
                {
                    using (SqlConnection c = new SqlConnection(settings.ToString()))
                    {
                        c.Open();

                        String query = "SELECT DISTINCT dbo.tblTrades.SymbolID FROM dbo.tblTrades WHERE dbo.tblTrades.StockExchange ='"+ stockExchange +"' ORDER BY dbo.tblTrades.SymbolID ASC";
                        if (stockExchange == "Forex") query = "SELECT DISTINCT dbo.tblTrades.SymbolID FROM dbo.tblTrades ORDER BY dbo.tblTrades.SymbolID ASC";

                        SqlCommand sqlCommand = new SqlCommand(query, c);

                        //SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID ASC", c);
                        sqlCommand.CommandTimeout = 0;

                        SqlDataReader reader = sqlCommand.ExecuteReader();

                        while (reader.Read())
                        {
                            String SymbolID = reader["SymbolID"].ToString();
                            symbolLookUpTable.Add(new KeyValuePair<string, string>(SymbolID, stockExchange));
                        }
                    }
                }
                catch (Exception ex)
                {
                    String thread = System.Threading.Thread.CurrentThread.ToString();
                    String level = "Error";
                    String application = Console.Title;
                    String selectorX = "JobsManager";

                    switch (Database.Mode)
                    {
                        case MODE.Test:
                            {
                                selectorX += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selectorX += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selectorX += "_LIVE-TEST";
                            }
                            break;
                    }

                    //ApplicationErrorLogger.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
                }
            }
        }

        private String LookUpSymbolStockExchange(string sym)
        {
            var items = from kvp in symbolLookUpTable
                        where kvp.Key == sym
                        select kvp;

            return items.FirstOrDefault().Value;
        }

        public void SaveCompatibleListToDB(JobStruct jobDetails, Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>> hcCompatiableList, String timeFrame)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }


            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.PeriodicCompatibilityLookUp WHERE dbo.PeriodicCompatibilityLookUp.CriteriaSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                Console.WriteLine("SaveCompatibleListToDB Exception for :: " + ex.Message);
            }

            foreach (KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>> compatibleList in hcCompatiableList)
            {
                foreach (var item in compatibleList.Value.Value)
                {
                    DataRow row = tempTable.NewRow();
                    row["Exchange"] = jobDetails.ApplicableExchange;
                    row["CriteriaSymbolID"] = compatibleList.Key;
                    row["ID"] = Guid.NewGuid().ToString();

                    row["CompatibleSymbolID"] = item;
                    row["CompatibleExchange"] = jobDetails.ApplicableExchange;

                    row["StartDateTime"] = compatibleList.Value.Key.StartDateTime.ToString("yyyy-MM-dd HH:mm");
                    row["EndDateTime"] = compatibleList.Value.Key.EndDateTime.ToString("yyyy-MM-dd HH:mm"); 
                    row["TimeFrame"] = timeFrame;

                    tempTable.Rows.Add(row);
                }
            }



            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.PeriodicCompatibilityLookUp (ID, CriteriaSymbolID, Exchange, CompatibleSymbolID, CompatibleExchange, StartDateTime, EndDateTime, TimeFrame) VALUES(@ID, @CriteriaSymbolID, @Exchange, @CompatibleSymbolID, @CompatibleExchange, @StartDateTime, @EndDateTime, @TimeFrame)", conObj);

                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CriteriaSymbolID", SqlDbType.NVarChar, 20, "CriteriaSymbolID"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Exchange", SqlDbType.NVarChar, 20, "Exchange"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibleSymbolID", SqlDbType.NVarChar, 20, "CompatibleSymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibleExchange", SqlDbType.NVarChar, 20, "Exchange"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StartDateTime", SqlDbType.SmallDateTime, 18, "StartDateTime"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@EndDateTime", SqlDbType.SmallDateTime, 18, "EndDateTime"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@TimeFrame", SqlDbType.NVarChar, 10, "TimeFrame"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //ApplicationErrorLogger.Log(thread, level, application, jobDetails.JobId, ex.ToString(), selectorX);

                Console.WriteLine("SaveCompatibleListToDB Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }

        public void SaveCompatibleListToDBLogBulkVrs(JobStruct jobDetails, List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> hcCompatiableList, String timeFrame)
        {
            String selector = "PeriodicCompOutput";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var fileName = System.Configuration.ConfigurationManager.AppSettings[selector].ToString();

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            //File.Create(fileName);


            try
            {

                using (CsvFileWriter writer = new CsvFileWriter(fileName))
                {
                    foreach (KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>> compatibleList in hcCompatiableList)
                    {
                        foreach (var item in compatibleList.Value.Value)
                        {
                            CsvRow row = new CsvRow();

                            // row.Add(Guid.NewGuid().ToString()); //ID
                            row.Add(compatibleList.Key); //CriteriaSymbolID

                            row.Add(jobDetails.ApplicableExchange); //Exchange                       

                            row.Add(item);//CompatibleSymbolID
                            row.Add(jobDetails.ApplicableExchange); //CompatibleExchange

                            row.Add(compatibleList.Value.Key.StartDateTime.ToString("yyyy-MM-dd HH:mm")); //StartDateTime
                            row.Add(compatibleList.Value.Key.EndDateTime.ToString("yyyy-MM-dd HH:mm")); //EndDateTime
                            row.Add(timeFrame);

                            writer.WriteRow(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("SaveComp", ex);
            }


            selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }



            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "dbo.proc_PeriodicComp";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var rows = sqlCommand.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                Console.WriteLine("SaveCompatibleListToDBLogBulkVrs Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }


        public void SaveCompatibleListToDBLog(JobStruct jobDetails, List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> hcCompatiableList, String timeFrame)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }


            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.PeriodicCompatibilityLookUp WHERE dbo.PeriodicCompatibilityLookUp.CriteriaSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                Console.WriteLine("SaveCompatibleListToDB Exception for :: " + ex.Message);
            }

            foreach (KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>> compatibleList in hcCompatiableList)
            {
                foreach (var item in compatibleList.Value.Value)
                {
                    DataRow row = tempTable.NewRow();
                    row["Exchange"] = jobDetails.ApplicableExchange;
                    row["CriteriaSymbolID"] = compatibleList.Key;
                    row["ID"] = Guid.NewGuid().ToString();

                    row["CompatibleSymbolID"] = item;
                    row["CompatibleExchange"] = jobDetails.ApplicableExchange;

                    row["StartDateTime"] = compatibleList.Value.Key.StartDateTime.ToString("yyyy-MM-dd HH:mm");
                    row["EndDateTime"] = compatibleList.Value.Key.EndDateTime.ToString("yyyy-MM-dd HH:mm"); 
                    row["TimeFrame"] = timeFrame;

                    tempTable.Rows.Add(row);
                }
            }



            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.PeriodicCompatibilityLookUp (ID, CriteriaSymbolID, Exchange, CompatibleSymbolID, CompatibleExchange, StartDateTime, EndDateTime, TimeFrame) VALUES(@ID, @CriteriaSymbolID, @Exchange, @CompatibleSymbolID, @CompatibleExchange, @StartDateTime, @EndDateTime, @TimeFrame)", conObj);

                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CriteriaSymbolID", SqlDbType.NVarChar, 20, "CriteriaSymbolID"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Exchange", SqlDbType.NVarChar, 20, "Exchange"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibleSymbolID", SqlDbType.NVarChar, 20, "CompatibleSymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibleExchange", SqlDbType.NVarChar, 20, "Exchange"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StartDateTime", SqlDbType.SmallDateTime, 18, "StartDateTime"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@EndDateTime", SqlDbType.SmallDateTime, 18, "EndDateTime"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@TimeFrame", SqlDbType.NVarChar, 10, "TimeFrame"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //ApplicationErrorLogger.Log(thread, level, application, jobDetails.JobId, ex.ToString(), selectorX);

                Console.WriteLine("SaveCompatibleListToDB Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }

        public List<String> GetCompatibleListFromDB(String stockExchange, String timeFrame)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<String> symbolList = new List<string>();

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT PeriodicCompatibilityLookUp.CriteriaSymbolID FROM dbo.PeriodicCompatibilityLookUp WHERE PeriodicCompatibilityLookUp.Exchange ='" + stockExchange +
                        "' AND PeriodicCompatibilityLookUp.TimeFrame ='" + timeFrame + "'";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["CriteriaSymbolID"].ToString();

                        symbolList.Add(symbol.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //.Log(thread, level, application, "PopulateSymbolLookUpTable()", ex.ToString(), selectorX);
            }

            return symbolList;
        }

        public List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> GetCompatibleListDataFromDB(String stockExchange, List<String> stocklist, String timeFrame)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

             var compsPeriodList = new List<CompatibilityPeriod>();

             try
             {
                 using (SqlConnection con = new SqlConnection(settings.ToString()))
                 {
                     String commandTxt = "SELECT PeriodicCompatibilityLookUp.StartDateTime, PeriodicCompatibilityLookUp.EndDateTime "
                     + "FROM dbo.PeriodicCompatibilityLookUp WHERE PeriodicCompatibilityLookUp.Exchange ='" + stockExchange + "' AND PeriodicCompatibilityLookUp.TimeFrame ='" + timeFrame + "'";

                     SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                     sqlCommand.CommandTimeout = 0;
                     con.Open();

                     SqlDataReader rdr = sqlCommand.ExecuteReader();

                     while (rdr.Read())
                     {
                         String date = rdr["StartDateTime"].ToString();
                         String datex = rdr["EndDateTime"].ToString();

                         var compsPeriod = new CompatibilityPeriod();
                         compsPeriod.StartDateTime = DateTime.Parse(date);
                         compsPeriod.EndDateTime = DateTime.Parse(datex);

                         compsPeriodList.Add(compsPeriod);
                     }
                 }
             }
             catch (Exception ex)
             {
                 Console.WriteLine(ex.ToString());
             }


            var symbolList = new List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>>();

            //var masterList = new Dictionary<string, List<string>>();

            foreach (var item in compsPeriodList)
            {
              
                DateTime dateTemp = new DateTime();

                CompatibilityPeriod compsPeriod = new CompatibilityPeriod();
                var masterList = new Dictionary<string, List<string>>();

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        String commandTxt = "SELECT PeriodicCompatibilityLookUp.CriteriaSymbolID, PeriodicCompatibilityLookUp.CompatibleSymbolID FROM dbo.PeriodicCompatibilityLookUp "
                        + "WHERE PeriodicCompatibilityLookUp.StartDateTime ='" + item.StartDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "' AND PeriodicCompatibilityLookUp.EndDateTime ='" + item.EndDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "' AND PeriodicCompatibilityLookUp.TimeFrame ='" 
                        + timeFrame + "'  ORDER BY PeriodicCompatibilityLookUp.CriteriaSymbolID ASC";

                        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                        sqlCommand.CommandTimeout = 0;
                        con.Open();

                        SqlDataReader rdr = sqlCommand.ExecuteReader();
                        
                        string symbolCriteria = "";
                        string previousSymbolCriteria = "";
                        List<string> compList = new List<string>();
                        int iter = 0;

                        while (rdr.Read())
                        {
                            String compatibleSymbolID = rdr["CompatibleSymbolID"].ToString();
                            symbolCriteria = rdr["CriteriaSymbolID"].ToString();

                            if (previousSymbolCriteria != symbolCriteria)
                            {
                                masterList.Add(symbolCriteria, new List<string>());
                            }

                            List<string> compListCheck = null;
                            if (masterList.TryGetValue(symbolCriteria, out compListCheck))
                            {
                                compsPeriod.StartDateTime = item.StartDateTime;
                                compsPeriod.EndDateTime = item.EndDateTime;
                                compsPeriod.TimeFrame = timeFrame;

                                masterList[symbolCriteria].Add(compatibleSymbolID);
                            }
                            previousSymbolCriteria = symbolCriteria;

                            iter++;
                        }
                    }

                    foreach (var compatibleItems in masterList)
                    {
                        symbolList.Add(new KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>
                            (compatibleItems.Key, new KeyValuePair<CompatibilityPeriod, List<String>>(compsPeriod, compatibleItems.Value)));
                    }

                }
                catch (Exception ex)
                {
                    String thread = System.Threading.Thread.CurrentThread.ToString();
                    String level = "Error";
                    String application = Console.Title;
                    String selectorX = "JobsManager";

                    switch (Database.Mode)
                    {
                        case MODE.Test:
                            {
                                selectorX += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selectorX += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selectorX += "_LIVE-TEST";
                            }
                            break;
                    }

                    //ApplicationErrorLogger.Log(thread, level, application, "GetCompatibleListDataFromDB() StockExchange:" + stockExchange + " SymbolID:" + item, ex.ToString(), selectorX);
                }
            }

            return symbolList;
        }

        public List<String> DeleteNonCompatiableSymbolsFromDB(String stockExchange)
        {
            //Or stocks with incomplete or not update dates
            String criteria = "";
            bool bfirstItem = true;

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<String> symbolList = new List<string>();

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {

                    foreach (var sym in symbolList)
                    {
                        if (bfirstItem) { criteria += "dbo.PeriodicCompatibilityLookUp.CompatibleSymbolID ='" + sym + "' OR dbo.PeriodicCompatibilityLookUp.CriteriaSymbolID = '" + sym + "'"; bfirstItem = false; }
                        else
                        {
                            criteria += " OR dbo.PeriodicCompatibilityLookUp.CompatibleSymbolID ='" + sym + "' OR dbo.PeriodicCompatibilityLookUp.CriteriaSymbolID = '" + sym + "'";
                        }
                    }

                    String commandTxt = "DELETE FROM dbo.PeriodicCompatibilityLookUp WHERE " + criteria;

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //ApplicationErrorLogger.Log(thread, level, application, "DeleteNonCompatiableSymbolsFromDB() StockExchange:" + stockExchange, ex.ToString(), selectorX);
            }
            return symbolList;
        }

        public List<String> GetNonCompatibleListFromDB(String stockExchange, String timeFrame)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<String> symbolList = new List<string>();
            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //String commandTxt = "SELECT DISTINCT NonCompatibleStocks.NonMatchingSymbolID FROM dbo.NonCompatibleStocks WHERE NonCompatibleStocks.NonMatchingStockExchange ='" + stockExchange + "'";
                String commandTxt = "SELECT DISTINCT NonCompatibleSymbols.CriteriaSymbolID FROM dbo.NonCompatibleSymbols WHERE NonCompatibleSymbols.Exchange ='" + stockExchange +
                    "' AND NonCompatibleSymbols.TimeFrame ='" + timeFrame + "'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    symbolList.Add(symbol.Trim());
                }
            }

            return symbolList;
        }

        //public void SaveNonCompatibleListToDB(JobStruct jobDetails, List<String> hcNonCompatiableList, String CriteriaSymbolID, String StockExchange)

        public void SaveNonCompatibleListToDBOld(JobStruct jobDetails, List<NonCompatibleStock> nonCompatibleStockList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SaveNonCompatibleListToDBOld Exception for :: " + ex.Message);
            }

            foreach (var item in nonCompatibleStockList)
            {
                DataRow row = tempTable.NewRow();
                row["ID"] = Guid.NewGuid().ToString();
                row["CriteriaSymbolID"] = item.CriteriaSymbolID;
                row["StockExchange"] = item.Exchange;

                row["NonMatchingSymbolID"] = item.NonMatchingSymbolID;
                row["NonMatchingStockExchange"] = jobDetails.ApplicableExchange;
                tempTable.Rows.Add(row);
            }

            try
            {
                //will be stored procedure to popualate the two tables

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    //--------------------------------------------------//
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;
                   

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SaveNonCompatibleListToDBOld Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }

        //public void SaveNonCompatibleListToDB(JobStruct jobDetails, List<String> hcNonCompatiableList)
        //{
        //    var settings = System.Configuration.ConfigurationManager.ConnectionStrings["JobsManager"];

        //    String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks WHERE dbo.NonCompatibleStocks.SymbolID='QFQFQQF'";

        //    DataTable tempTable = null;
        //    try
        //    {
        //        using (SqlConnection c = new SqlConnection(settings.ToString()))
        //        {
        //            c.Open();
        //            using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
        //            {
        //                tempTable = new DataTable();

        //                tempTable.TableName = "tempname";
        //                a.Fill(tempTable);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Generic Exception for :: " + ex.Message);
        //    }

        //    //Dictionary<String, KeyValuePair<DateTime, List<String>>>

        //    foreach (var item in hcNonCompatiableList)
        //    {
        //        DataRow row = tempTable.NewRow();
        //        row["ID"] = Guid.NewGuid().ToString();
        //        row["SymbolID"] = item;
        //        row["StockExchange"] = jobDetails.ApplicableExchange;

        //        tempTable.Rows.Add(row);
        //    }

        //    try
        //    {
        //        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
        //        {
        //            SqlConnection conObj = new SqlConnection(settings.ToString());
        //            dataAdapter.SelectCommand.CommandTimeout = 0;

        //            conObj.Open();
        //            //--------------------------------------------------//
        //            dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
        //            dataAdapter.InsertCommand.CommandType = CommandType.Text;

        //            // Create a parameter for the ReturnValue.
        //            SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
        //            parameter.Direction = ParameterDirection.Output;


        //            dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
        //            dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
        //            dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

        //            int res = dataAdapter.Update(tempTable);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Generic Exception for " + jobDetails.JobId + ":: " + ex.Message);
        //    }
        //}

        public void SaveNonCompatibleListToDB(JobStruct jobDetails, List<String> hcNonCompatiableList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks WHERE dbo.NonCompatibleStocks.SymbolID='QFQFQQF'";

            //String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks JOIN dbo.NonCompatibleDates ON dbo.NonCompatibleStocks.ID = dbo.NonCompatibleDates.RefID WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //.Log(thread, level, application, "SaveNonCompatibleListToDB()", ex.ToString(), selectorX);
            }

            foreach (var item in hcNonCompatiableList)
            {
                DataRow row = tempTable.NewRow();
                row["ID"] = Guid.NewGuid().ToString();
                row["SymbolID"] = item;
                row["StockExchange"] = jobDetails.ApplicableExchange;

                tempTable.Rows.Add(row);
            }

            try
            {
                //will be stored procedure to popualate the two tables

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();
                    //--------------------------------------------------//
                    dataAdapter.InsertCommand = new SqlCommand("INSERT INTO dbo.NonCompatibleStocks (ID, SymbolID, StockExchange) VALUES(@ID, @SymbolID, @StockExchange)", conObj);
                    dataAdapter.InsertCommand.CommandType = CommandType.Text;

                    // Create a parameter for the ReturnValue.
                    SqlParameter parameter = dataAdapter.InsertCommand.Parameters.Add("@Result", SqlDbType.Int);
                    parameter.Direction = ParameterDirection.Output;


                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@StockExchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

               // ApplicationErrorLogger.Log(thread, level, application, jobDetails.JobId, ex.ToString(), selectorX);
            }
        }

        public void SaveNonCompatibleListToDBNew(JobStruct jobDetails, Dictionary<String, List<NotCompatible>> nonCompatibleStockList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

           // String grabSchema = "SELECT * FROM dbo.NonCompatibleSymbols JOIN dbo.NonCompatibleDates_Diagnostics ON dbo.NonCompatibleSymbols.ID = dbo.NonCompatibleDates_Diagnostics.RefID WHERE dbo.NonCompatibleSymbols.NonMatchingSymbolID='QFQFQQF'";
            String grabSchema = "SELECT * FROM dbo.NonCompatibleSymbols WHERE dbo.NonCompatibleSymbols.NonMatchingSymbolID='QFQFQQF'";

            //String grabSchema = "SELECT * FROM dbo.NonCompatibleStocks JOIN dbo.NonCompatibleDates ON dbo.NonCompatibleStocks.ID = dbo.NonCompatibleDates.RefID WHERE dbo.NonCompatibleStocks.NonMatchingSymbolID='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SaveNonCompatibleListToDBNew Exception for :: " + ex.Message);
            }

            foreach (var item in nonCompatibleStockList)
            {
                foreach (var itemDateList in item.Value)
                {
                    String guid = Guid.NewGuid().ToString();

                    DataRow row = tempTable.NewRow();
                    row["ID"] = guid;
                    row["CriteriaSymbolID"] = item.Key;
                    row["Exchange"] = itemDateList.Exchange;

                    row["NonMatchingSymbolID"] = itemDateList.NonMatchingSymbolID;
                    row["NonMatchingStockExchange"] = itemDateList.NonMatchingExchange;
                    row["TimeFrame"] = itemDateList.TimeFrame;
                    //row["RefID"] = guid;

                    var sqlFormattedDate = itemDateList.MissingDateTime.ToString("yyyy-MM-dd HH:mm");
                    row["DateTime"] = sqlFormattedDate;

                    tempTable.Rows.Add(row);                  
                }
            }

            try
            {
                //will be stored procedure to popualate the two tables

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    conObj.Open();

                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    dataAdapter.InsertCommand = new SqlCommand("proc_InsertNonCompatibleStocksAndDates", conObj);
                    dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 100, "ID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CriteriaSymbolID", SqlDbType.NVarChar, 20, "CriteriaSymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Exchange", SqlDbType.NChar, 20, "Exchange"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@NonMatchingSymbolID", SqlDbType.NVarChar, 20, "NonMatchingSymbolID"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@NonMatchingStockExchange", SqlDbType.NVarChar, 20, "NonMatchingStockExchange"));
                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@DateTime", SqlDbType.SmallDateTime, 20, "DateTime"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@TimeFrame", SqlDbType.NVarChar, 10, "TimeFrame"));

                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@RefID", SqlDbType.NVarChar, 100, "RefID"));
                    //dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@DateTime", SqlDbType.SmallDateTime));


                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@DateTime", SqlDbType.SmallDateTime, 20, "DateTime"));



                    int res = dataAdapter.Update(tempTable);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SaveNonCompatibleListToDBNew Exception for " + jobDetails.JobId + ":: " + ex.Message);
            }
        }


        public void DeleteAllCompatiableSymbolsFromDB(String stockExchange, String timeFrame)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                List<String> symbolList = new List<string>();
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {

                    String commandTxt = "DELETE FROM dbo.PeriodicCompatibilityLookUp WHERE dbo.PeriodicCompatibilityLookUp.Exchange = '" + stockExchange + 
                        "' AND dbo.PeriodicCompatibilityLookUp.TimeFrame = '" + timeFrame + "'";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }

                //ApplicationErrorLogger.Log(thread, level, application, "DeleteAllCompatiableSymbolsFromDB()", ex.ToString(), selectorX);
            }
        }


        public bool RegisterProcessorEngine(String ticket, String machineProcessedOn, String FMDA_EngineName, DateTime jobStartTime)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];
            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_RegisterProcessingEngine", con);
                    con.Open();

                    sqlCommand.CommandTimeout = 0;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Ticket", ticket);
                    sqlCommand.Parameters.AddWithValue("@MachineProcessedOn", machineProcessedOn);
                    sqlCommand.Parameters.AddWithValue("@FMDA_EngineName", FMDA_EngineName);
                    sqlCommand.Parameters.AddWithValue("@JobStartTime", jobStartTime);

                    int affectedRows = sqlCommand.ExecuteNonQuery();

                    if (affectedRows > 0) return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("Ticket Failed To Register : " + ticket, ex);
            }
            return false;
        }

        public bool RegisterProcessorEngineFileVersion(String ticket, String machineProcessedOn, String FMDA_EngineName, DateTime jobStartTime)
        {
            lock (_registerationLock)
            {
                String selector = "RegisterProcessingEngine";
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;
                }

                String fileName = "";

                try
                {
                    fileName = System.Configuration.ConfigurationManager.AppSettings[selector].ToString();

                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        CsvRow row = new CsvRow();
                        row.Add(ticket);
                        row.Add(jobStartTime.ToString("yyyy-MM-dd HH:mm"));
                        row.Add(machineProcessedOn);
                        row.Add(FMDA_EngineName);

                        writer.WriteRow(row);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Error("Ticket Failed To Register : " + ticket, ex);
                    LogHelper.Error("File " + fileName, ex);
                    return false;
                }
                return true;
            }
        }

        public void WriteCompletedTicketsToFile(ConcurrentQueue<CompletedTicket> completedTickets)
        {
            int count = completedTickets.Count;

            LogHelper.Info("CompletedTickets to be written out : " + count);
            Console.WriteLine("CompletedTickets to be written out : " + count);

            while (completedTickets.Count > 0)
            {
                var completedTicket = new CompletedTicket();
                completedTickets.TryDequeue(out completedTicket);

                RegisterProcessorEngineFileVersion(completedTicket.Ticket, completedTicket.ComputerName, 
                    completedTicket.FEngine, completedTicket.StartDateTime);

                //LogHelper.Info("Dequeuing Countdown :: " + completedTickets.Count);
            }

            LogHelper.Info("Finished writting Completed Tickets to CSV file : " + count);
            Console.WriteLine("Finished writting Completed Tickets to CSV file : " + count);
        }

        public bool RegisterProcessorEngineBulk()
        {
            if (bAlreadyRegBulkOccupiedFlag == false)
            {
                bAlreadyRegBulkOccupiedFlag = true;

                lock (_registerationBulkLock)
                {
                    bool success = true;

                    String selector = "RegisterProcessingEngine";
                    switch (Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;
                    }

                    var fileName = System.Configuration.ConfigurationManager.AppSettings[selector].ToString();

                    if (File.Exists(fileName))
                    {
                        //Import into database then delete the file
                        selector = "JobsManager";
                        switch (Mode)
                        {
                            case MODE.Test:
                                {
                                    selector += "_TEST";
                                }
                                break;

                            case MODE.Live:
                                {
                                    selector += "_LIVE";
                                }
                                break;
                        }

                        var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];
                        try
                        {
                            using (SqlConnection con = new SqlConnection(settings.ToString()))
                            {
                                SqlCommand sqlCommand = new SqlCommand("proc_RegisterProcessingEngineBulk", con);
                                con.Open();
                                sqlCommand.CommandTimeout = 0;

                                sqlCommand.CommandType = CommandType.StoredProcedure;

                                var affectedRows = sqlCommand.ExecuteNonQuery();
                                LogHelper.Info("Processing Registeration Times Logged");
                                Console.WriteLine("Processing Registeration Times Logged");

                            }
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            LogHelper.Error("Registeration Exception:: ", ex);
                            Console.WriteLine("Registeration Exception:: " + ex.Message);
                        }
                        finally
                        {
                           // File.Delete(fileName); //TEmp hold
                            LogHelper.Info("Registeration File Deleted!");
                        }
                    }
                    else
                    {
                        LogHelper.Info("File Does Not Exist");
                        Console.WriteLine("File Does Not Exist");
                    }
                    return success;
                }
            }
            return false;
        }


        public bool InsertJobticketsIntoDB(List<JobTicketDef> jobTicketDefList)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            String grabSchema = "SELECT * FROM dbo.Jobs WHERE dbo.Jobs.JobTicket ='QFQFQQF'";

            DataTable tempTable = null;
            try
            {
                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(grabSchema, c))
                    {
                        tempTable = new DataTable();

                        tempTable.TableName = "tempname";
                        a.Fill(tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("InsertJobticketsIntoDB Exception for :: " + ex.Message);
            }


            foreach (var jobTicketDef in jobTicketDefList)
            {
                    DataRow row = tempTable.NewRow();
                    row["ID"] = Guid.NewGuid().ToString();

                    row["JobTicket"] = jobTicketDef.JobTicket;
                    row["JobId"] = jobTicketDef.JobId;

                    row["SymbolID"] = jobTicketDef.SymbolID;
                    row["ProcessType"] = jobTicketDef.ProcessType;

                    row["StockExchange"] = jobTicketDef.Exchange;
                    row["CompatibilityStartDate"] = jobTicketDef.CompatibilityStartDate;
                    row["EndDate"] = jobTicketDef.EndDate;
                    row["JobTicketCreationDate"] = jobTicketDef.JobTicketCreationDate;

                    tempTable.Rows.Add(row);
            }
            
            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(grabSchema, settings.ToString()))
                {
                    SqlConnection conObj = new SqlConnection(settings.ToString());
                    dataAdapter.SelectCommand.CommandTimeout = 0;

                    conObj.Open();

                    dataAdapter.InsertCommand = new SqlCommand("proc_InsertJobTickets", conObj);

                    dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@JobId", SqlDbType.NVarChar, 200, "JobId"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@SymbolID", SqlDbType.NVarChar, 20, "SymbolID"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@ProcessType", SqlDbType.NVarChar, 20, "ProcessType"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Exchange", SqlDbType.NVarChar, 20, "StockExchange"));

                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@CompatibilityStartDate", SqlDbType.SmallDateTime, 20, "CompatibilityStartDate"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.SmallDateTime, 20, "EndDate"));
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@JobTicketCreationDate", SqlDbType.Date, 20,  "JobTicketCreationDate"));
                    
                    dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@JobTicket", SqlDbType.NVarChar, 200, "JobTicket"));
                   // dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Completed", SqlDbType.Bit, ,"Completed"));


                    int res = dataAdapter.Update(tempTable);

                    //if (res > 0) return true;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("InsertJobticketsIntoDB Exception for:: " + ex.Message);
            }

            //bool newTicketsPlaceOnQueue = false;
            ////Call another proc to retrieve the item which wore successfully placed in the db
            //try
            //{
            //    using (SqlConnection con = new SqlConnection(settings.ToString()))
            //    {
            //        String commandTxt = "SELECT * FROM dbo.TemporaryNewJobs";

            //        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
            //        con.Open();

            //        SqlDataReader rdr = sqlCommand.ExecuteReader();

            //        //clear if new items that werent duplicates were found
            //        if (rdr.HasRows) { jobTicketDefList.Clear(); newTicketsPlaceOnQueue = true; }

            //        while (rdr.Read())
            //        {
            //            var jobTicketDef = new JobTicketDef();
            //            DateTime st = new DateTime();
            //            DateTime ed = new DateTime();

            //            jobTicketDef.JobTicket = rdr["JobTicket"].ToString();

            //            String[] stArray = rdr["CompatibilityStartDate"].ToString().Split(' ');
            //            String[] edArray = rdr["EndDate"].ToString().Split(' ');

            //            DateTime startD = Convert.ToDateTime(stArray.FirstOrDefault());
            //            DateTime endD = Convert.ToDateTime(edArray.FirstOrDefault());


            //            String startDateStr = String.Format("{0}-{1}-{2}", startD.Year, startD.Month, startD.Day);
            //            String endDateStr = String.Format("{0}-{1}-{2}", endD.Year, endD.Month, endD.Day);

            //            jobTicketDef.CompatibilityStartDate = startDateStr;
            //            jobTicketDef.EndDate = endDateStr;

            //            jobTicketDef.ProcessType = rdr["ProcessType"].ToString();
            //            jobTicketDef.Exchange = rdr["StockExchange"].ToString();
            //            jobTicketDef.SymbolID = rdr["SymbolID"].ToString();

            //            jobTicketDefList.Add(jobTicketDef);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Generic Exception for:: " + ex.Message);
            //}

            return true;
        }

        public bool InsertJobticketsIntoDBBulkVersion(List<JobTicketDef> jobTicketDefList)
        {            
            String selector = "JobsTicketOutput";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var fileName = System.Configuration.ConfigurationManager.AppSettings[selector].ToString();

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            try
            {

                using (CsvFileWriter writer = new CsvFileWriter(fileName))
                {

                    foreach (var jobTicketDef in jobTicketDefList)
                    {
                        CsvRow row = new CsvRow();

                        //row["ID"] = Guid.NewGuid().ToString();

                        row.Add(jobTicketDef.JobTicket);
                        row.Add(jobTicketDef.JobId);
                        row.Add(jobTicketDef.SymbolID);

                        row.Add(jobTicketDef.Exchange);
                        row.Add(jobTicketDef.CompatibilityStartDate);
                        row.Add(jobTicketDef.EndDate);
                        row.Add(jobTicketDef.JobTicketCreationDate);
                        row.Add(jobTicketDef.ProcessType);

                        writer.WriteRow(row);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("InserJobTicketIntoDBBulkVersion ", ex);
            }



            selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "dbo.proc_JobsTickets";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var rows = sqlCommand.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                Console.WriteLine("InsertJobticketsIntoDBBulkVersion Exception for :: " + ex.Message);
            }      

            return true;
        }


        private void DeleteTemporaryJobs(String selector)
        {
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "DELETE FROM dbo.TemporaryNewJobs";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DeleteTemporaryJobs Exception for:: " + ex.Message);
            }
        }

        public List<JobTicketDef> GetIncompletedJobTickets()
        {
            List<JobTicketDef> jobTicketDefList = new List<JobTicketDef>();

            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            List<IncompletedJobTicket> incompletedJobTicketList = new List<IncompletedJobTicket>();

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.Jobs.JobTicket, dbo.Jobs.CompatibilityStartDate, dbo.Jobs.EndDate, dbo.Jobs.ProcessType, dbo.Jobs.StockExchange, dbo.Jobs.SymbolID FROM dbo.Jobs WHERE dbo.Jobs.Completed = 0";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        var jobTicketDef = new JobTicketDef();
                        DateTime st = new DateTime();
                        DateTime ed = new DateTime();

                        jobTicketDef.JobTicket = rdr["JobTicket"].ToString();

                        String[] stArray = rdr["CompatibilityStartDate"].ToString().Split(' ');
                        String[] edArray = rdr["EndDate"].ToString().Split(' ');

                        DateTime startD = Convert.ToDateTime(stArray.FirstOrDefault());
                        DateTime endD = Convert.ToDateTime(edArray.FirstOrDefault());


                        String startDateStr = String.Format("{0}-{1}-{2}", startD.Year, startD.Month, startD.Day);
                        String endDateStr = String.Format("{0}-{1}-{2}", endD.Year, endD.Month, endD.Day);

                        jobTicketDef.CompatibilityStartDate = startDateStr;
                        jobTicketDef.EndDate = endDateStr;

                        jobTicketDef.ProcessType = rdr["ProcessType"].ToString();
                        jobTicketDef.Exchange = rdr["StockExchange"].ToString();
                        jobTicketDef.SymbolID = rdr["SymbolID"].ToString();

                        jobTicketDefList.Add(jobTicketDef);
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("GetIncompletedJobTickets Exception for:: " + ex.Message);
            }

            return jobTicketDefList;
        }

        public bool JobTicketCompleted(String jobTicket, DateTime endDateTime, String fileNameOutput, String calculationType)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_JobTicketCompleted", con);
                    con.Open();
                    sqlCommand.CommandTimeout = 0;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@JobTicket", jobTicket);
                    sqlCommand.Parameters.AddWithValue("@EndTime", endDateTime);
                    sqlCommand.Parameters.AddWithValue("@FileNameOutput", fileNameOutput);
                    sqlCommand.Parameters.AddWithValue("@CalculationType", calculationType);


                    int affectedRows = sqlCommand.ExecuteNonQuery();

                    if (affectedRows > 0) return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("JobTicketCompleted Exception:: " + ex.Message);
            }
            return false;
        }

        public bool JobTicketCompletedFileVersion(String jobTicket, DateTime endDateTime, String fileNameOutput, String calculationType)
        {
            lock (_jobTicketCompletedLock)
            {
                String selector = "JobsTicketCompletedOutput";
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;
                }

                var fileName = System.Configuration.ConfigurationManager.AppSettings[selector].ToString();

                try
                {
                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        CsvRow row = new CsvRow();
                        row.Add(jobTicket);
                        row.Add(endDateTime.ToString("yyyy-MM-dd HH:mm"));
                        row.Add(fileNameOutput);
                        row.Add(calculationType);

                        writer.WriteRow(row);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Error("JobTicketCompletedFileVersion", ex);
                }
                return false;
            }
        }

        public bool ManageTickets(ConcurrentQueue<CompletedTicket> completedTickets)
        {
            if (bAlreadyOccupiedFlag == false)
            {
                //LogHelper.Info("ManageTickets");

                //LogHelper.Info("_bAlreadyOccupiedFlag : " + bAlreadyOccupiedFlag.ToString());
                //LogHelper.Info("About to Enter Locking Code");


                LogHelper.Info("completedTickets : " + bAlreadyOccupiedFlag.ToString());

                bAlreadyOccupiedFlag = true;

                lock (thisLock)
                {
                    if (bAlreadyOccupiedFinalFlag == false)
                    {
                        bAlreadyOccupiedFinalFlag = true;

                        LogHelper.Info("ManageTickets");

                        LogHelper.Info("_bAlreadyOccupiedFlag : " + bAlreadyOccupiedFlag.ToString());
                        LogHelper.Info("About to Enter Locking Code");

                        WriteCompletedTicketsToFile(completedTickets);

                        RegisterProcessorEngineBulk();

                        bool success = true;

                        String selector = "JobsTicketCompletedOutput";
                        switch (Mode)
                        {
                            case MODE.Test:
                                {
                                    selector += "_TEST";
                                }
                                break;

                            case MODE.Live:
                                {
                                    selector += "_LIVE";
                                }
                                break;
                        }

                        var fileName = System.Configuration.ConfigurationManager.AppSettings[selector].ToString();

                        LogHelper.Info("Locked Code Block :: Importing -> " + selector);
                        Console.WriteLine("Locked Code Block :: Importing -> " + selector);


                        if (File.Exists(fileName))
                        {
                            //Import into database then delete the file
                            selector = "JobsManager";
                            switch (Mode)
                            {
                                case MODE.Test:
                                    {
                                        selector += "_TEST";
                                    }
                                    break;

                                case MODE.Live:
                                    {
                                        selector += "_LIVE";
                                    }
                                    break;
                            }

                            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];
                            try
                            {
                                using (SqlConnection con = new SqlConnection(settings.ToString()))
                                {
                                    SqlCommand sqlCommand = new SqlCommand("proc_JobTicketCompletedBulk", con);
                                    con.Open();
                                    sqlCommand.CommandTimeout = 0;

                                    sqlCommand.CommandType = CommandType.StoredProcedure;


                                    var affectedRows = sqlCommand.ExecuteNonQuery();
                                    LogHelper.Info("Manage Tickets Import Success  bAlreadyOccupiedFlag : " + bAlreadyOccupiedFlag.ToString());
                                    Console.WriteLine("Manage Tickets Import Success  bAlreadyOccupiedFlag : " + bAlreadyOccupiedFlag.ToString());

                                }
                            }
                            catch (Exception ex)
                            {
                                success = false;
                                LogHelper.Error("Manage Tickets Exception:: ", ex);
                                Console.WriteLine("Manage Tickets Exception:: " + ex.Message);
                            }
                            finally
                            {
                                File.Delete(fileName);
                                LogHelper.Info("ManageTickets File Deleted!");
                            }
                        }
                        else
                        {
                            LogHelper.Info("File Does Not Exist");
                            Console.WriteLine("File Does Not Exist");
                        }
                        return success;
                    }                    
                }
            }
            return false;
        }

        public bool TotalJobCompletionCheck(DateTime dateID)
        {
            String selector = "JobsManager";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_CheckAllJobCompletions", con);
                    con.Open();

                    DateTime dateTempID = new DateTime(dateID.Year, dateID.Month, dateID.Day);
                    DateTime dayEndTime = dateTempID.AddDays(1);

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@DayStartDatetime", dateTempID);
                    sqlCommand.Parameters.AddWithValue("@DayEndDatetime", dayEndTime);

                    
                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    int affectedRows = -99;

                    while (rdr.Read())
                    {
                        affectedRows = Convert.ToInt32(rdr["Total"].ToString());
                    }
                    if (affectedRows == 0)
                    {
                        //ManageTickets(); Temp Hold
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TotalJobCompletionCheck Exception:: " + ex.Message);
            }
            return false;
        }
    }
}

