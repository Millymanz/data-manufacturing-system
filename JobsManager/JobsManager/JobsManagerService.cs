﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace JobsManager
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class JobsManagerService : IJobsManagerService
    {
        public JobsManagerService()
        {
            //UdpClient udp = new UdpClient();
            //IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, 11098);//10198

            //byte[] sendBytes4 = Encoding.ASCII.GetBytes("bSpinUpProcessingEngines_TEST_7");
            //udp.Send(sendBytes4, sendBytes4.Length, groupEP);
            //-----------------------------------------//


            Exchanges.InitialiseExchangeList();

            JobsManager.InitialiseJobsFlagArray();

            ThreadStart threadStartACon = new ThreadStart(JobsManager.AppControl);
            Thread jobCreatorThreadAcon = new Thread(threadStartACon);
            jobCreatorThreadAcon.Start();


            if (Informer.OnDemandTicketProcessing)
            {
                JobsManager.FlagCheckerHandler();
            }


            Console.WriteLine("FlagChecker thread started ");

            ThreadStart threadStartCycle = new ThreadStart(JobsManager.ProcessingCycleListenner);
            Thread ProcessingCycleListennerThread = new Thread(threadStartCycle);
            ProcessingCycleListennerThread.Start();

            Console.WriteLine("ProcessingCycleListenner thread started");
        }

        public void StartProcesses()
        {
            JobsManager.InitialiseJobsFlagArray();

            //ThreadStart threadStart = new ThreadStart(JobsManager.FlagChecker);
            //Thread jobCreatorThread = new Thread(threadStart);
            //jobCreatorThread.Start();


            Console.WriteLine("FlagChecker thread started ");

            ThreadStart threadStartCycle = new ThreadStart(JobsManager.ProcessingCycleListenner);
            Thread ProcessingCycleListennerThread = new Thread(threadStartCycle);
            ProcessingCycleListennerThread.Start();

            Console.WriteLine("ProcessingCycleListenner thread started");
        }

        public void TaskCompleted(String jobTicket, String fileNameOutput, String calculationType)
        {
            JobsManager.JobTicketCompleted(jobTicket, fileNameOutput, calculationType);
        }

        public KeyValuePair<String, List<String>> GetTask(String FMDA_EngineName, int mode)
        {
            MODE modeItem = (MODE)mode;

            if (Database.Mode == modeItem)
            {
                return JobsManager.GetTask(FMDA_EngineName);
            }
            return new KeyValuePair<string, List<string>>();
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public DateTime GetMasterNodeDateTime()
        {
            return DateTime.Now;
        }

        public bool ModeCheck(int mode)
        {
            MODE modeItem = (MODE)mode;

            if (Database.Mode == modeItem)
            {
                return true;
            }
            return false;
        }
    }
}
