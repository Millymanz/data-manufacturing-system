﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Threading;
using System.ServiceModel;

using System.ServiceModel.Channels;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

namespace JobsManager
{
    static public class TestModeConditions
    {
       static public int JobticketsLimit = -1;
       static public String[] ProcessingType;
    }

    public enum JobPhase
    {
        DataConsumption = 0,
        SingleEx_FMDA_Processing = 1,
        Intermarket_FMDA_Processing = 2
    }

    public struct JobStruct
    {
        public String JobId;
        public JobPhase JobPhaseMember;
        public String ApplicableExchange;
    }

    public struct JobTicketDef
    {
        public String JobTicket;
        public String JobId;
        public String SymbolID;
        public String Exchange;
        public String CompatibilityStartDate;
        public String EndDate;
        public String JobTicketCreationDate;
        public String ProcessType;
    }

    public struct IncompletedJobTicket
    {
        public String JobTicket;
        public String SymbolID;
        public String Exchange;
        public String CompatibilityStartDate;
        public String EndDate;
        public String ProcessType;
    }

    public struct NonCompatibleStock
    {
        public String CriteriaSymbolID;
        public String Exchange;
        public String NonMatchingSymbolID;
        public String NonMatchingStockExchange;
        public List<DateTime> NonCompatibleDateList;
        public List<int> NonCompatibleDateIndexList;

    }

    public class CompatibilityPeriod
    {
        public String TimeFrame;
        public DateTime StartDateTime;
        public DateTime EndDateTime;
    }

    public class StatsLog
    {
        public String WorkTitle;
        public String TimeFrame;
        public String TotalTimeSpan;
        public int DataPoints;
    }

    public struct NotCompatible
    {
        public String SymbolID;
        public String Exchange;
        public String NonMatchingSymbolID;
        public String NonMatchingExchange;
        public DateTime MissingDateTime;
        public String TimeFrame;
    }

    public struct CompletedTicket
    {
        public String Ticket;
        public String ComputerName;
        public String FEngine;
        public DateTime StartDateTime;
    }

    static public class Informer
    {
        static public Boolean OnDemandTicketProcessing = false;
    }

    static public class JobsManager
    {
        //static private Queue taskQueue = new Queue();
        static private ConcurrentQueue<KeyValuePair<String, List<String>>> taskQueue = new ConcurrentQueue<KeyValuePair<String, List<String>>>();


        static private ConcurrentQueue<CompletedTicket> _completedTickets = new ConcurrentQueue<CompletedTicket>();
        static private Database dataModel = new Database();

       // static private bool[][] jobsFlagArray = new bool[System.Configuration.ConfigurationManager.AppSettings.Count][];

        static private bool[][] jobsFlagArray = new bool[Exchanges.List.Count][];


        //static private bool[][] jobsFlagArray = new bool[1][];

        static private bool bFirstProcessCall = true;

        static private bool bLastProcessCall = false;

        static int totals = 0;

        static private List<IncompletedJobTicket> _incompletedJobsList = new List<IncompletedJobTicket>();

        static private DateTime _DateID = new DateTime();

        //This is used to control tickets being added to the queue
        //If the current queue of jobs have not been processed then
        //new items cannot be added
        static private bool WorkInProgress = false;

        static private bool PauseJobRuns = false;

        static public bool AllTicketsCompletedImported = false;


        static JobsManager()
        {
            Logger.Setup();
        }

        static public void ProcessingCycleListenner()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            while (true)
            {
                if (WorkInProgress == false)
                {
                    UdpClient udpClient = new UdpClient(11000);
                    try
                    {
                        //IPEndPoint object will allow us to read datagrams sent from any source.
                        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), 11000);

                        Console.WriteLine("");
                        Console.WriteLine("[Waiting for Processing Cycle Notification..]");
                        Console.WriteLine("");


                        // Blocks until a message returns on this socket from a remote host.
                        Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                        string returnData = Encoding.ASCII.GetString(receiveBytes);

                        Console.WriteLine("Service  " + returnData);

                        String[] conditionArray = returnData.Split('_');

                        if (conditionArray.FirstOrDefault() == "bUpdateDBServicesANDStartProcessingCycle")
                        {
                            FlagCheckerHandler();
                           // BeginProcessingCycle();
                        }
                        udpClient.Close();
                    }
                    catch (Exception e)
                    {
                        String thread = System.Threading.Thread.CurrentThread.ToString();
                        String level = "Error";
                        String application = Console.Title;

                        String selector = "JobsManager";

                        switch (Database.Mode)
                        {
                            case MODE.Test:
                                {
                                    selector += "_TEST";
                                }
                                break;

                            case MODE.Live:
                                {
                                    selector += "_LIVE";
                                }
                                break;

                            case MODE.LiveTest:
                                {
                                    selector += "_LIVE-TEST";
                                }
                                break;
                        }

                        //ApplicationErrorLogger.Log(thread, level, application, "ProcessingCycleListenner()", e.ToString(), selector);
                    }
                }
            }
        }

        static public void InitialiseJobsFlagArray()
        {
            int countExchanges = Exchanges.List.Count;

            for (int i = 0; i < countExchanges; i++)
            {
                jobsFlagArray[i] = new bool[4];
            }

            ResetJobsFlagArray();

            //-----------------------------------------//
            if (Informer.OnDemandTicketProcessing)
            {
                WorkInProgress = true;
                //First stage flags set
                for (int i = 0; i < jobsFlagArray.Length; i++)
                {
                    jobsFlagArray[i][(int)JobPhase.DataConsumption] = true;
                }
            }
        }

        static private void BeginProcessingCycle()
        {
            //bool bAllTrue = AllFlagsTrue(); //Temp disable phase2 and other have not be introduce yet so work is not 100% allflagstrue yet

            //if (bAllTrue)
            {
                ResetJobsFlagArray();

                //First stage flags set
                for (int i = 0; i < jobsFlagArray.Length; i++)
                {
                    jobsFlagArray[i][(int)JobPhase.DataConsumption] = true;
                }
                Console.WriteLine("[Beginning Processing Cycle]");
                Console.WriteLine("Preparing JobTickets");
                WorkInProgress = true;

            }
        }

        static private void ResetJobsFlagArray()
        {
            for (int i = 0; i < jobsFlagArray.Length; i++)
            {
                for (int j = 0; j < jobsFlagArray[i].Length; j++)
                {
                    jobsFlagArray[i][j] = false;
                }
            }
        }

        static public Thread CreateJobsThread(JobStruct jobId)
        {
            Object jobObj = jobId;

            ParameterizedThreadStart threadStart = new ParameterizedThreadStart(CreateJobs);

            Thread jobCreatorThread = new Thread(threadStart);
            jobCreatorThread.Name = jobId.ApplicableExchange + " Thread";
            jobCreatorThread.Start(jobId);
            
            return jobCreatorThread;

        }

        static public KeyValuePair<String, List<String>> GetTask(String FMDA_EngineName)
        {
            if (PauseJobRuns) return new KeyValuePair<string, List<string>>(); 

            KeyValuePair<String, List<String>> taskKVP = new KeyValuePair<string, List<string>>();

            bool bRegisterationComplete = false;

            totals++;

            if (bFirstProcessCall)
            {
                bFirstProcessCall = false;
            }


            if (taskQueue.Any())
            {
               // taskKVP = (KeyValuePair<String, List<String>>)taskQueue.Dequeue();

                if (taskQueue.TryDequeue(out taskKVP) == false)
                {
                    LogHelper.Info("Failed To Dequeue");
                }
                bRegisterationComplete = RegisterProcessorEngine(taskKVP.Key, "None");

                Console.WriteLine("{0} :: {1}", taskKVP.Key, taskQueue.Count);

                //if (bRegisterationComplete == false) LogHelper.Info("FAILED TO REGISTER :: " + taskKVP.Key);
            }
            else
            {
                if ((_DateID > DateTime.MinValue) && (_DateID < DateTime.MaxValue) && AllTicketsCompletedImported == false)
                {
                    CheckAllJobsCompletion(_DateID);
                }
            }

            if (taskQueue.Count == 0) WorkInProgress = false;//Open to creating new jobs from hench forth

            if ((taskQueue.Count == 0) && (bLastProcessCall == false))
            {
                bLastProcessCall = true;
            }

            if (bRegisterationComplete)
            {
                String dateStr = String.Format("*{0}{1}{2}", _DateID.Day, _DateID.Month, _DateID.Year);
                String tempTicket = taskKVP.Key  + dateStr;

                var tk = new KeyValuePair<string, List<string>>(tempTicket, taskKVP.Value);
                return tk;
            }
            return new KeyValuePair<string,List<string>>();
        }

        //private static void EmailNotificationCheck(String Exchange)
        //{
        //    var message = new StringBuilder();
        //    message.AppendLine("Jobs tickets are currently in production for the following Exchange/Asset Class" + Exchange + "\n\n");
        //    message.AppendLine("Date :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
        //    new EmailManager().Send(null, "STAGE (3a) JobsManager Jobs Production In Progress", message.ToString());
        //}

        private static void EmailNotificationCheck(String Exchange, String title, String msg)
        {
            var message = new StringBuilder();
            message.AppendLine(msg + Exchange + "\n\n");
            message.AppendLine("Date :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            new EmailManager().Send(null, title, message.ToString());
        }


        static public void CreateJobs(object jobId)
        {
            LogHelper.Info("CreateJobs");
            dataModel.ManageTickets(_completedTickets);

            String selectorOutput = Database.Mode == MODE.Test ?  "OUTPUTPATH_TEST" : "OUTPUTPATH_LIVE";

            String directoryOutput = System.Configuration.ConfigurationManager.AppSettings[selectorOutput];
            if (Directory.Exists(directoryOutput))
            {
                Directory.Delete(directoryOutput, true);
                Directory.CreateDirectory(directoryOutput);
            }


            AllTicketsCompletedImported = false;
            Database.bAlreadyOccupiedFlag = false;
            Database.bAlreadyOccupiedFinalFlag = false;
            Database.bAlreadyRegBulkOccupiedFlag = false;

            LogHelper.Info("bAlreadyOccupiedFlag : " + Database.bAlreadyOccupiedFlag.ToString() 
                + " AllTicketsCompletedImported : " + AllTicketsCompletedImported.ToString());
            

            JobStruct jobStruct = (JobStruct)jobId;
            String jobIdentification = jobStruct.JobId;

            int ticketNoCount = 0;

            //Starttime
            DateTime startTime = DateTime.Now;
            String triggerlogFileName = jobStruct.ApplicableExchange + "-JobsManagerLog.txt";
            if (!System.IO.File.Exists(triggerlogFileName))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(triggerlogFileName)) { }
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
            {
                file.WriteLine(String.Format("StartTime  {0}  {1}", DateTime.Now.TimeOfDay, DateTime.Now));
                //file.WriteLine("");
            }

            //Check distinct list of symbols between the staging stocks and live db stocks, if theres a difference
            //in count or size then, A) work out which symbols are missing from the staging db
            // B) work out which symbols should be deleted from staging db
            // C) Do nothing if well matched

            //get latest date then let list be based on latest date condition

            string msg = "Jobs tickets are currently in production for the following Exchange/Asset Class";
            string title = "STAGE (3a) JobsManager Jobs Production In Progress";
            EmailNotificationCheck(jobStruct.ApplicableExchange, title, msg);


            List<String> timeframeList = System.Configuration.ConfigurationManager.AppSettings["TimeFrameList"].Split(',').ToList();

            List<StatsLog> statsLogList = new List<StatsLog>();

            Console.WriteLine("Creating Job Tickets New");


            foreach (var timeframeItem in timeframeList)
            {
                List<String> symbolsDataUpdatesTaskList = dataModel.GetSymbolsWithDataUpdates(jobStruct.ApplicableExchange, timeframeItem);

                Console.WriteLine(String.Format("Number of Symbols with New Data : {0} TimeFrame : {1}", symbolsDataUpdatesTaskList.Count, timeframeItem));

                List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> result = null;

                //Deal with reoccuring non-compatible stocks
                List<String> tempList = new List<string>();

                if (symbolsDataUpdatesTaskList.Any())
                {
                    dataModel.DeleteAllCompatiableSymbolsFromDB(jobStruct.ApplicableExchange, timeframeItem);

                    result = PeriodicCompatiability(symbolsDataUpdatesTaskList, jobStruct, timeframeItem, statsLogList);

                    if (result != null)
                    {
                        //Save compatible list in the temp db
                        SaveToDBLog(jobStruct, result, timeframeItem);
                    }
                    else
                    {
                        Console.WriteLine("None are compatible");
                        LogHelper.Info("None are compatible");
                    }
                }
                else
                {
                    var currentCompatibleSymbolList = dataModel.GetCompatibleListFromDB(jobStruct.ApplicableExchange, timeframeItem);
                    result = dataModel.GetCompatibleListDataFromDB(jobStruct.ApplicableExchange, currentCompatibleSymbolList, timeframeItem);
                }

                int totalNoJobsthisExchange = 0;
                //Create job tickets

                if (result != null && result.Any())
                {
                    totalNoJobsthisExchange = JobticketProduction(jobIdentification, jobStruct.ApplicableExchange, ref ticketNoCount, result, timeframeItem);
                }

                //-------------------------------------------------//

                string status = totalNoJobsthisExchange > 0 ? "READY" : "No Symbol Updates";

                Console.WriteLine(" ");
                Console.WriteLine(status + " ::" + jobStruct.ApplicableExchange + " " + totalNoJobsthisExchange);
                Console.WriteLine(" ");
            }

            //end time
            TimeSpan span = DateTime.Now - startTime;

            StatsLog statsLog = new StatsLog()
            {
                WorkTitle = "Total Time Taken for Jobs to be Created Including db calls and other methods",
                TimeFrame = "All TimeFrames",
                DataPoints = 0,
                TotalTimeSpan = String.Format("Hours:{0} Minutes:{1} Seconds:{2}", span.Hours, span.Minutes, span.Seconds)
            };

            statsLogList.Add(statsLog);


            String msgLog = "";
            foreach (var item in statsLogList)
            {
                msgLog += String.Format("{0} :: DataPoints {1} TotalTimeSpan : {2} TimeFrame : {3} \n\n <br><br>", item.WorkTitle, item.DataPoints, item.TotalTimeSpan, item.TimeFrame);
            }

            title = "STAGE (3b) JobsManager Jobs Ticket Production Completed";


            int engineSpinUpNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EngineSpinUpNo"].ToString());
            string spinCommand = "bSpinUpProcessingEngines_TEST_" + engineSpinUpNo;

            msgLog += spinCommand;

            UdpClient udp = new UdpClient();
            IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, 11098);

            byte[] sendBytes4 = Encoding.ASCII.GetBytes(spinCommand);
            udp.Send(sendBytes4, sendBytes4.Length, groupEP);
            //------------------------------------------------------------//

            EmailNotificationCheck(jobStruct.ApplicableExchange, title, msgLog);
        }

        //static private void Is

        static private void SaveToDB(JobStruct jobDetails, Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>> compatibleList, String timeFrame)
        {
            dataModel.SaveCompatibleListToDB(jobDetails, compatibleList, timeFrame);
        }

        static private void SaveToDBLog(JobStruct jobDetails, List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> compatibleList, String timeFrame)
        {
            //dataModel.SaveCompatibleListToDBLog(jobDetails, compatibleList, timeFrame);
            dataModel.SaveCompatibleListToDBLogBulkVrs(jobDetails, compatibleList, timeFrame);
        }

        static public void FlagChecker()
        {
            while (true)
            {
                var threadListing = new List<Thread>();

                //Phase 1 Job
                for (int i = 0; i < jobsFlagArray.Length; i++)
                {
                    if ((jobsFlagArray[i][(int)JobPhase.DataConsumption] == true) && (jobsFlagArray[i][(int)JobPhase.SingleEx_FMDA_Processing] == false)
                        && (jobsFlagArray[i][(int)JobPhase.Intermarket_FMDA_Processing] == false))
                    {
                        String jobId = Guid.NewGuid().ToString();

                        JobStruct jobStructure = new JobStruct();
                        jobStructure.JobId = jobId;
                        jobStructure.JobPhaseMember = JobPhase.SingleEx_FMDA_Processing;

                        jobStructure.ApplicableExchange = Exchanges.List[i];

                       threadListing.Add(CreateJobsThread(jobStructure));
                    }
                }
                //move this bit down once phase 2 is implemented
                foreach (var th in threadListing)
                {
                    th.Join();
                }
                //WorkInProgress = false;

                bool bAllTrue = AllFlagsTrue();
                //for (int ch = 0; ch < jobsFlagArray.Length; ch++)
                //{
                //    for (int g = 0; g < jobsFlagArray[ch].Length; g++)
                //    {
                //        if (jobsFlagArray[ch][g] == false)
                //        {
                //            bAllTrue = false;
                //        }
                //    }
                //}

                //Phase 2 Job
                if (bAllTrue)
                {




                    //ResetJobsFlagArray();
                }


                //ttemp just to break out

                //break;
            }
        }

        static public void FlagCheckerHandler()
        {
            List<String> exchangesList = null;
            if (Informer.OnDemandTicketProcessing)
            {
                exchangesList = Exchanges.List;
            }
            else
            {
                exchangesList = dataModel.GetFlagMarkers();
            }

            foreach (var item in exchangesList)
            {
                String jobId = Guid.NewGuid().ToString();

                JobStruct jobStructure = new JobStruct();
                jobStructure.JobId = jobId;
                jobStructure.JobPhaseMember = JobPhase.SingleEx_FMDA_Processing;

                jobStructure.ApplicableExchange = item;

                CreateJobsThread(jobStructure);

                if (Informer.OnDemandTicketProcessing == false)
                {
                    dataModel.UpdateFlagMarker(item);
                }
            }

        }


        static private bool AllFlagsTrue()
        {
            bool bAllTrue = true;
            for (int ch = 0; ch < jobsFlagArray.Length; ch++)
            {
                for (int g = 0; g < jobsFlagArray[ch].Length; g++)
                {
                    if (jobsFlagArray[ch][g] == false)
                    {
                        bAllTrue = false;
                    }
                }
            }
            return bAllTrue;
        }

        //Single Exchange PreProcess Analysis
        //
        //[unique][AllTimeTicketNo][TicketNo][CalculationLevel][TypeOfProcessing][Symbol][StockExchange][TaskParameter]


        static private int PrepareForCorrelationAnalysis(String jobIdentification, ref int allTimeTicketNo, String exchange, ref int ticketNoCount, List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> result, String timeFrame)
        {
            int totalNoJobs = 0;

            //turn list into job tasks
            List<JobTicketDef> jobTicketDefList = new List<JobTicketDef>();

            Database database = new Database();
            var earliestDateTime = database.GetEarliestDateTime(exchange);

            foreach (var item in result)
            {
                var startDateStr = item.Value.Key.StartDateTime.ToString("yyyy-MM-dd HH:mm");
                var endDateStr = item.Value.Key.EndDateTime.ToString("yyyy-MM-dd HH:mm");


                String taskParameter = String.Format(startDateStr + "@" + endDateStr);

                ticketNoCount++; //Not zero based

                var correlationType = IsHistorical(earliestDateTime, item.Value.Key) ? "HC" : "RC";                

                //[unique][AllTimeTicketNo][TicketNo][CalculationLevel][TypeOfProcessing][Symbol][Exchange][TaskParameter][TimeFrame]
                String ticket = String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", jobIdentification, allTimeTicketNo, ticketNoCount, 0, correlationType, item.Key, exchange, taskParameter, timeFrame);

                JobTicketDef jobTicketDef = CreateJobticketDef(ticket, jobIdentification, item.Key, exchange, startDateStr, endDateStr, correlationType);

                if (JobsTicketTypeDuplicationCheck(jobTicketDef) == false)
                {
                    if (CheckForDataPoints(jobTicketDef.SymbolID, DateTime.Parse(jobTicketDef.CompatibilityStartDate),
                        DateTime.Parse(jobTicketDef.EndDate), jobTicketDef.Exchange, timeFrame))
                    {
                        jobTicketDefList.Add(jobTicketDef);
                    }
                }
                allTimeTicketNo++;
            }

            //store in the database
            if (jobTicketDefList.Count > 0)
            {
               //dataModel.InsertJobticketsIntoDB(jobTicketDefList);
                dataModel.InsertJobticketsIntoDBBulkVersion(jobTicketDefList);
            }         
            return totalNoJobs;
        }

        private static void PlaceTicketOnQueue(List<JobTicketDef> jobTicketDefList, int totalNoJobs, string timeFrame, 
            List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> result)
        {
            for (int i = 0; i < jobTicketDefList.Count; i++)
            {
                //Obtain compatibleList from collection for entry into queue
                var compatibleList = result.Where(m => m.Key == jobTicketDefList[i].SymbolID && m.Value.Key.TimeFrame == timeFrame)
                              .Select(t => t.Value.Value).FirstOrDefault();

                if (compatibleList != null)
                {
                    if (compatibleList.Any())
                    {
                        Console.WriteLine("CREATED :: {0}" + jobTicketDefList[i].JobTicket);                       

                        taskQueue.Enqueue(new KeyValuePair<String, List<String>>(jobTicketDefList[i].JobTicket, compatibleList));
                        totalNoJobs++;
                    }
                }
            }

            LogHelper.Info("PlaceTicketOnQueue :: Queue Created Count : " + taskQueue.Count);

        }

        private static bool CheckForDataPoints(string symbol, DateTime startDateTime, DateTime endDateTime, string exchange, string timeFrame)
        {
            bool bCheck = false;

            using (var proxyDataGrid = new Notification.DataGridService.DatabaseWcfServiceClient())
            {
                bCheck = proxyDataGrid.DataExistForTradeSummaries(new List<string>() { symbol }.ToArray(), startDateTime, endDateTime, exchange, timeFrame);
            }
            return bCheck;
        }

        private static bool IsHistorical(DateTime earliestDateTime, CompatibilityPeriod compatibility)
        {
            DateTime startDateRange = earliestDateTime;//earliest date

            DateTime endDateRange = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    {
                        endDateRange = endDateRange.AddDays(-1);
                    }
                    break;

                case DayOfWeek.Sunday:
                    {
                        endDateRange = endDateRange.AddDays(-2);
                    }
                    break;
            }

            if (compatibility.StartDateTime.Year == earliestDateTime.Year
                && compatibility.StartDateTime.Month == earliestDateTime.Month
                && compatibility.StartDateTime.Day == earliestDateTime.Day
                &&
                compatibility.EndDateTime.Year == endDateRange.Year
                && compatibility.EndDateTime.Month == endDateRange.Month
                && compatibility.EndDateTime.Day == endDateRange.Day)
            {
                return true;

            }         
            return false;
        }

        static private Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>> HistoricalCompatiabilityCheckNew(List<String> symbolsTaskList, JobStruct jobDetails, String timeFrame)
        {
            Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>>();

            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange, timeFrame);

            var notCompatible = new Dictionary<String, List<NotCompatible>>();
            var compatible = new Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>>();

            foreach (var item in tradeDateList)//check criteria
            {
                bool bCompatible = true;
                var tempNoCompatibleList = new List<NotCompatible>();
                var tempCompatibleList = new List<String>();

                var compatibilityPeriod = new CompatibilityPeriod();


                foreach (var tradeItem in tradeDateList) //Checking against
                {
                    if (item.Key != tradeItem.Key)
                    {
                        foreach (var datetimeItem in item.Value)
                        {
                            if (tradeItem.Value.Contains(datetimeItem) == false)
                            {
                                bCompatible = false;
                                var notcompa = new NotCompatible();
                                notcompa.SymbolID = item.Key;
                                
                                notcompa.Exchange = jobDetails.ApplicableExchange;
                                notcompa.NonMatchingExchange = jobDetails.ApplicableExchange;

                                notcompa.NonMatchingSymbolID = tradeItem.Key;
                                notcompa.MissingDateTime = datetimeItem;
                                notcompa.TimeFrame = timeFrame;

                                tempNoCompatibleList.Add(notcompa);
                            }
                        }
                        if (bCompatible)
                        {
                            compatibilityPeriod.StartDateTime = item.Value.FirstOrDefault();
                            compatibilityPeriod.EndDateTime = item.Value.LastOrDefault();

                            tempCompatibleList.Add(tradeItem.Key);
                        }
                    }
                }
                notCompatible.Add(item.Key, tempNoCompatibleList);

                if (tempCompatibleList.Any()) compatible.Add(item.Key, new KeyValuePair<CompatibilityPeriod, List<String>>(compatibilityPeriod, tempCompatibleList));
            }
            //return compatible;

            //Save to the database
            if (notCompatible.Count > 0) dataModel.SaveNonCompatibleListToDBNew(jobDetails, notCompatible);

            return compatible;
        }

        //static private List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> PeriodicCompatiability(List<String> symbolsTaskList, JobStruct jobDetails, String timeFrame, List<StatsLog> statsLogList)
        //{
        //    DateTime startTime = DateTime.Now;

        //    Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>>();

        //    Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange, timeFrame);

        //    var notCompatible = new Dictionary<String, List<NotCompatible>>();
        //    var compatible = new List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>>();

        //    foreach (var item in tradeDateList)//check criteria
        //    {
        //        var tempNoCompatibleList = new List<NotCompatible>();

        //        foreach (var tradeItem in tradeDateList) //Checking against
        //        {
        //            if (item.Key != tradeItem.Key)
        //            {
        //                bool bLogStartDateTime = true;

        //                CompatibilityPeriod compatibilityPeriodHolder = null;
        //                var tempCompatibleList = new List<String>();
        //                bool bFirst = true;

        //                for (int b = 0; b < item.Value.Count; b++)
        //                {
        //                    if (tradeItem.Value.Contains(item.Value[b]))
        //                    {
        //                        if (bFirst)
        //                        {
        //                            tempCompatibleList.Add(item.Key);
        //                            bFirst = false;
        //                        }

        //                        if (bLogStartDateTime)
        //                        {
        //                            compatibilityPeriodHolder = new CompatibilityPeriod();                                      

        //                            compatibilityPeriodHolder.StartDateTime = item.Value[b];
        //                            bLogStartDateTime = false;
        //                        }
        //                    }
        //                    else //no match
        //                    {
        //                        if (bLogStartDateTime == false)
        //                        {
        //                            compatibilityPeriodHolder.TimeFrame = timeFrame;
        //                            compatibilityPeriodHolder.EndDateTime = item.Value[b - 1];
        //                            bLogStartDateTime = true;

        //                            compatible.Add(new KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>(tradeItem.Key,
        //                                new KeyValuePair<CompatibilityPeriod, List<String>>(compatibilityPeriodHolder, tempCompatibleList)));//new;y added
        //                        }
        //                    }
        //                }

        //                if (bLogStartDateTime == false)
        //                {
        //                    compatibilityPeriodHolder.EndDateTime = item.Value.LastOrDefault();
        //                    compatibilityPeriodHolder.TimeFrame = timeFrame;

        //                    compatible.Add(new KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>(tradeItem.Key,
        //                        new KeyValuePair<CompatibilityPeriod, List<String>>(compatibilityPeriodHolder, tempCompatibleList)));                      
        //                }                                          
        //            }
        //        }
        //    }

        //    //Save to the database
        //    if (notCompatible.Count > 0) dataModel.SaveNonCompatibleListToDBNew(jobDetails, notCompatible);

        //    TimeSpan span = DateTime.Now - startTime;

        //    StatsLog statsLog = new StatsLog();
        //    statsLog.WorkTitle = "PeriodicCompatiability Non-Parallel ForEach";
        //    statsLog.TimeFrame = timeFrame;
        //    statsLog.DataPoints = tradeDateList.Count;
        //    statsLog.TotalTimeSpan = String.Format("Hours:{0} Minutes:{1} Seconds:{2}", span.Hours, span.Minutes, span.Seconds);

        //    statsLogList.Add(statsLog); 

        //    return compatible;
        //}

        static private List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> PeriodicCompatiability(List<String> symbolsTaskList, JobStruct jobDetails, String timeFrame, List<StatsLog> statsLogList)
        {
            DateTime startTime = DateTime.Now;

            Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>> hcCompatiableList = new Dictionary<String, KeyValuePair<CompatibilityPeriod, List<String>>>();

            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList, jobDetails.ApplicableExchange, timeFrame);

            var notCompatible = new Dictionary<String, List<NotCompatible>>();
            var compatible = new System.Collections.Concurrent.ConcurrentBag<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>>();

            try
            {
                Parallel.ForEach(tradeDateList, item =>
                {
                    var tempNoCompatibleList = new List<NotCompatible>();

                    foreach (var tradeItem in tradeDateList) //Checking against
                    {
                        if (item.Key != tradeItem.Key)
                        {
                            bool bLogStartDateTime = true;

                            CompatibilityPeriod compatibilityPeriodHolder = null;
                            var tempCompatibleList = new List<String>();
                            bool bFirst = true;

                            //for (int b = 0; b < item.Value.Count; b++)
                            int b = 0;
                            foreach (var secondItem in item.Value)
                            {
                                if (tradeItem.Value.Contains(secondItem))
                                {
                                    if (bFirst)
                                    {
                                        tempCompatibleList.Add(item.Key);
                                        bFirst = false;
                                    }

                                    if (bLogStartDateTime)
                                    {
                                        compatibilityPeriodHolder = new CompatibilityPeriod();

                                        compatibilityPeriodHolder.StartDateTime = secondItem;
                                        bLogStartDateTime = false;
                                    }
                                }
                                else //no match
                                {
                                    if (bLogStartDateTime == false)
                                    {
                                        compatibilityPeriodHolder.TimeFrame = timeFrame;
                                        compatibilityPeriodHolder.EndDateTime = item.Value.ElementAt(b - 1);
                                        bLogStartDateTime = true;

                                        compatible.Add(new KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>(tradeItem.Key,
                                            new KeyValuePair<CompatibilityPeriod, List<String>>(compatibilityPeriodHolder, tempCompatibleList)));//new;y added
                                    }
                                }
                                b++;
                            }

                            if (bLogStartDateTime == false)
                            {
                                compatibilityPeriodHolder.EndDateTime = item.Value.LastOrDefault();
                                compatibilityPeriodHolder.TimeFrame = timeFrame;

                                compatible.Add(new KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>(tradeItem.Key,
                                    new KeyValuePair<CompatibilityPeriod, List<String>>(compatibilityPeriodHolder, tempCompatibleList)));
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            //Save to the database
            if (notCompatible.Count > 0) dataModel.SaveNonCompatibleListToDBNew(jobDetails, notCompatible);

            TimeSpan span = DateTime.Now - startTime;

            StatsLog statsLog = new StatsLog();
            statsLog.WorkTitle = "PeriodicCompatiability Parallel ForEach";
            statsLog.TimeFrame = timeFrame;
            statsLog.DataPoints = tradeDateList.Count;
            statsLog.TotalTimeSpan = String.Format("Hours:{0} Minutes:{1} Seconds:{2} Milliseconds:{3}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds);

            statsLogList.Add(statsLog);

            return compatible.ToList();
        }



        static private bool RegisterProcessorEngine(String ticket, String FMDA_EngineName)
        {
            String computerName = "";
            try
            {
                var remp = OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                string[] computer_name = Dns.GetHostEntry(remp.Address).HostName.Split(new Char[] { '.' });
                computerName = computer_name[0].ToString();
            }
            catch (Exception e)
            {
                computerName = "Unknown"; 
            }
            if (String.IsNullOrEmpty(computerName)) computerName = "Unknown";

            DateTime startDateTime = DateTime.Now;

            var completedTicket = new CompletedTicket
            {
                Ticket = ticket,
                ComputerName = computerName,
                FEngine = FMDA_EngineName,
                StartDateTime = startDateTime
            };


            if (_completedTickets.Where(m => m.Ticket == ticket).Any() == false)
            {
                _completedTickets.Enqueue(completedTicket);
                return true;
            }
            else
            {
                LogHelper.Info("Duplicate Found : " + taskQueue.Count + " : " + ticket);
            }
            return false;
            //return dataModel.RegisterProcessorEngineFileVersion(ticket, computerName, FMDA_EngineName, startDateTime);
        }

        static private JobTicketDef CreateJobticketDef(String ticket, String JobId, String SymbolID, String Exchange, String startDateStr, String endtDateStr, String processType)
        {
            JobTicketDef jTD = new JobTicketDef();
            jTD.JobTicket = ticket;
            jTD.JobId = JobId;
            jTD.SymbolID = SymbolID;
            jTD.Exchange = Exchange;
            jTD.CompatibilityStartDate = startDateStr;
            jTD.EndDate = endtDateStr;
            jTD.JobTicketCreationDate = String.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            jTD.ProcessType = processType;

            return jTD;
        }

        static private int JobticketProduction(String jobIdentification, String exchange, ref int ticketNoCount, List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> result, String timeFrame)
        {
            int totalNoJobs = 0;

            if (result != null)
            {
                ExpandAndShift(result);

                _DateID = DateTime.Now;

                int allTimeTicketNo = dataModel.GetAllTimeTicketNo();


                //-------------------Beginning of SingleMarket Calculations--------------//
                PrepareForCorrelationAnalysis(jobIdentification, ref allTimeTicketNo, exchange, ref ticketNoCount, result, timeFrame);

                FlagMarking(exchange, JobPhase.SingleEx_FMDA_Processing);
                //-------------------End of SingleMarket Calculations--------------//


                var jobTicketDefList = dataModel.GetIncompletedJobTickets();
                totalNoJobs = jobTicketDefList.Count;

                LogHelper.Info("JobticketProduction :: PeriodCompatibilityCount : " + result.Count);
                LogHelper.Info("JobticketProduction :: IncompletedJobTickets : " + totalNoJobs);

                PlaceTicketOnQueue(jobTicketDefList, totalNoJobs, timeFrame, result);

                //Intermarket calculations
                //then mark flag
            }

            LogHelper.Info("JobticketProduction :: Tickets : " + totalNoJobs);


            return totalNoJobs;

        }

        static private void ExpandAndShift(List<KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>> result)
        {
            var tempResult = new List<KeyValuePair<string, KeyValuePair<CompatibilityPeriod, List<string>>>>();

            foreach (var item in result)
            {
                int dataPointLimit = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinimumDataPoints"].ToString());
                int seconds = TimeFrameConverter.ToSecondsInt(item.Value.Key.TimeFrame);
                int limitMuliplier = 1;
                
                var endDateTime = item.Value.Key.EndDateTime;
                var endDateTimeCheck = item.Value.Key.StartDateTime;                

                while (endDateTimeCheck <= endDateTime)
                {
                    var startDateTimeSteps = item.Value.Key.StartDateTime;
                    var tempEndDateTime = endDateTime;

                    while (startDateTimeSteps < endDateTime && tempEndDateTime <= endDateTime)
                    {
                        int increment = (seconds * limitMuliplier);
                        startDateTimeSteps = item.Value.Key.StartDateTime + new TimeSpan(0, 0, increment);

                        tempEndDateTime = startDateTimeSteps + new TimeSpan(0, 0, (seconds * dataPointLimit));

                        var compatibilityPeriodHolder = new CompatibilityPeriod();
                        compatibilityPeriodHolder.TimeFrame = item.Value.Key.TimeFrame;
                        compatibilityPeriodHolder.StartDateTime = startDateTimeSteps;
                        compatibilityPeriodHolder.EndDateTime = tempEndDateTime;


                        tempResult.Add(new KeyValuePair<String, KeyValuePair<CompatibilityPeriod, List<String>>>(item.Key,
                            new KeyValuePair<CompatibilityPeriod, List<String>>(compatibilityPeriodHolder, item.Value.Value)));


                        limitMuliplier++;
                    }
                    dataPointLimit++;

                    endDateTimeCheck = item.Value.Key.StartDateTime + new TimeSpan(0, 0, (seconds * dataPointLimit));

                }
            }
            if (tempResult.Any()) result.AddRange(tempResult);

        }

        static private bool JobsTicketTypeDuplicationCheck(JobTicketDef jobTicketDef)
        {
            var exist = from incompItem in _incompletedJobsList
                        where incompItem.SymbolID == jobTicketDef.SymbolID &&
                        incompItem.ProcessType == jobTicketDef.ProcessType &&
                        incompItem.Exchange == jobTicketDef.Exchange &&
                        incompItem.CompatibilityStartDate == jobTicketDef.CompatibilityStartDate &&
                        incompItem.EndDate == jobTicketDef.EndDate
                        select incompItem;

            if (exist.Count() == 0)
            {
                return false;
            }
            return true;
        }

        static public void JobTicketCompleted(String jobTicket, String fileNameOutput, String calculationType)
        {
            if (dataModel != null)
            {
                //dataModel.JobTicketCompleted(jobTicket, DateTime.Now, fileNameOutput, calculationType);

                dataModel.JobTicketCompletedFileVersion(jobTicket, DateTime.Now, fileNameOutput, calculationType);
            }
        }

        static public void CheckAllJobsCompletion(DateTime dateID)
        {
            if (dataModel != null)
            {
                AllTicketsCompletedImported = dataModel.ManageTickets(_completedTickets); //Temp Hold

                //if (dataModel.TotalJobCompletionCheck(dateID))
                if (AllTicketsCompletedImported)
                {
                    Notification.Notifyer notifyImporter = new Notification.Notifyer();

                    if (notifyImporter.BeginImportation((int)Database.Mode, _DateID) == -1)
                    {
                        Console.WriteLine("Connection Failed - Application MODE did not MATCH!");
                    }
                }
            }
        }

        static public void FlagMarking(string stockExchange, JobPhase jobPhase)
        {
           // int countExchanges = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TOTALNOEXCHANGES"]);
            int countExchanges = Exchanges.List.Count();

            for (int i = 0; i < countExchanges; i++)
            {
                if (Exchanges.List[i] == stockExchange)
                {
                    jobsFlagArray[i][(int)jobPhase] = true;
                }
            } 
        }

        static public void AppControl()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            String portKey = "APPCONTROLPORT";            
            portKey += Database.Mode == MODE.Live ? "_LIVE" : "_TEST";

            int port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[portKey]);

            UdpClient udpClient = new UdpClient(port);

            while (true)
            {
                udpClient.EnableBroadcast = true;
                try
                {
                    //IPEndPoint object will allow us to read datagrams sent from any source.
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), 0);

                    // Blocks until a message returns on this socket from a remote host.
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);

                    //Check received text  if PAUSE_JOBS or RUN_JOBS

                    //set PauseJobRuns accordingly
                    if (returnData == "PAUSE_JOBS") PauseJobRuns = true;
                    else if (returnData == "RUN_JOBS") PauseJobRuns = false;

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }
            udpClient.Close();
        }
    }
}
