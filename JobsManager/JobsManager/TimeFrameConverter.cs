﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobsManager
{
    public static class TimeFrameConverter
    {
        public static string ToSecondsString(int interval)
        {
            string timeframe = "";

            switch (interval)
            {
                case 60:
                    {
                        timeframe = "1min";
                    } break;

                case 120:
                    {
                        timeframe = "2min";
                    } break;

                case 180:
                    {
                        timeframe = "3min";
                    } break;

                case 240:
                    {
                        timeframe = "4min";
                    } break;

                case 300:
                    {
                        timeframe = "5min";
                    } break;

                case 360:
                    {
                        timeframe = "6min";
                    } break;

                case 600:
                    {
                        timeframe = "10min";
                    } break;

                case 900:
                    {
                        timeframe = "15min";
                    } break;

                case 1800:
                    {
                        timeframe = "30min";
                    } break;

                case 3600:
                    {
                        timeframe = "1hour";
                    } break;

                case 7200:
                    {
                        timeframe = "2hour";
                    } break;

                case 10800:
                    {
                        timeframe = "3hour";
                    } break;
            }
            return timeframe;
        }

        public static int ToSecondsInt(string timeframe)
        {
            int interval = 0;
            switch (timeframe)
            {
                case "1min":
                    {
                        interval = 60;
                    } break;

                case "2min":
                    {
                        interval = 120;

                    } break;

                case "3min":
                    {
                        interval = 180;
                    } break;

                case "4min":
                    {
                        interval = 240;
                    } break;

                case "5min":
                    {
                        interval = 300;
                    } break;

                case "6min":
                    {
                        interval = 360;
                    } break;

                case "10min":
                    {
                        interval = 360;
                    } break;

                case "15min":
                    {
                        interval = 900;
                    } break;

                case "30min":
                    {
                        interval = 1800;
                    } break;

                case "1hour":
                    {
                        interval = 3600;
                    } break;

                case "2hour":
                    {
                        interval = 7200;
                    } break;

                case "3hour":
                    {
                        interval = 10800;
                    } break;
            }
            return interval;
        }
    }
}
