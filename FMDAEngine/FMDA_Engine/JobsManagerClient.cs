﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStore;
using System.Configuration;
using System.Net;
//using LogSys;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;
using System.IO;

namespace FMDA_Engine
{
    public enum TicketProtocol
    {
        JobId = 0,
        AllTimeTicketNo,
        TicketNo, 
        CalculationLevel, 
        TypeOfProcessing, 
        Symbol, 
        StockExchange, 
        TaskParameter,
        TimeFrame
    }

    public class JobsManagerClient
    {

        //Holde the method for translating the ticket protocol into the relevant method call from
        //the appropiate cclass

        private String _FMDA_EngineName = "";

        private KeyValuePair<String, List<String>> taskKVP = new KeyValuePair<string,List<string>>();

        public String FMDA_EngineName 
        {
            get { return _FMDA_EngineName; }
        }

        public DateTime _startTime = new DateTime();

        //must consume net tcp binding

        public JobsManagerClient(DateTime tm)
        {
            _startTime = tm;

            Logger.Setup();

        }

        public bool CheckJobsManagerConnection(bool bWriteFile)
        {
            String name = Dns.GetHostName().ToUpper();
            var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];

            System.Threading.Thread.Sleep(20000);

            try
            {
                bool matchingMode = false;
                using (JobsManagerService.JobsManagerServiceClient jbClient = new FMDA_Engine.JobsManagerService.JobsManagerServiceClient())
                {
                    jbClient.Open();

                    matchingMode = jbClient.ModeCheck((int)Database.Mode);
                    LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + "...Connection to JobsManager..OK");

                    jbClient.Close();

                    String message = "";
 
                    if (matchingMode)
                    {
                        message = "Connection OK - JobsManagerService";
                    }
                    else
                    {
                        message = "Connection OK - JobsManagerService: Application MODES do not MATCH";
                    }

                    Console.WriteLine(message);
                    
                    //if (bWriteFile)
                    //{
                    //    String dir = System.Configuration.ConfigurationManager.AppSettings["COPYING_REPORT"];
                    //    String logFileName = dir + "\\" + Dns.GetHostName().ToUpper() + "_CopyingReport.txt";


                    //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileName, true))
                    //    {
                    //        file.WriteLine(message);
                    //    }
                    //}

                    return matchingMode;
                }
            }
            catch (System.ServiceModel.CommunicationException commProblem)
            {
                Console.WriteLine("Connection FAILED - JobsManagerService");
                LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + " :: Connection to JobsManager... FAILED!");
            }
            return false;
        }

        public String GetJobTicket()
        {
            try
            {
               using (JobsManagerService.JobsManagerServiceClient jbClient = new FMDA_Engine.JobsManagerService.JobsManagerServiceClient())
               {
                   var temp = jbClient.GetTask(_FMDA_EngineName, (int)Database.Mode);

                   if (temp.key != null)
                   {
                       taskKVP = new KeyValuePair<String, List<String>>(temp.key, temp.value.ToList());
                       return taskKVP.Key;           

                   }
               }
            }
            catch (Exception e)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                //ApplicationErrorLogger.Log(thread, level, application, "GetJobTicket()", e.ToString(), selector);
            }
            return "";           
        }

        public void ProcessTicket(String ticket)
        {
            //[unique][AllTimeTicketNo][TicketNo][CalculationLevel][TypeOfProcessing][Symbol][StockExchange][TaskParameter]

            String[] decomposedTicket = ticket.Split(';');
            
            String[] dates = decomposedTicket[(int)TicketProtocol.TaskParameter].Split('@');
            
            DateTime startDate = DateTime.Parse(dates[0]);
            DateTime endDate = DateTime.Parse(dates[1]);

            String timeFrame = decomposedTicket[(int)TicketProtocol.TimeFrame];
            
            bool taskCompleted = false;

            switch (decomposedTicket[(int)TicketProtocol.TypeOfProcessing])
            {
                case "HC":
                case "RC":
                    {
                        Console.WriteLine("[Correlation Analysis");

                        CorrelationProcessor correlationProcesor = new CorrelationProcessor();

                        taskCompleted = correlationProcesor.HistoricalCorrelationAnalysisMapReducer(ticket, decomposedTicket[(int)TicketProtocol.Symbol],
                            decomposedTicket[(int)TicketProtocol.StockExchange], startDate, endDate, taskKVP.Value, timeFrame, decomposedTicket[(int)TicketProtocol.TypeOfProcessing]);
                        Console.WriteLine("[Ticket Completed]");
                        Console.WriteLine("");
                        Console.WriteLine("");

                    }
                    break;
            }

            Console.WriteLine("[Notify JobsManager]");

            try
            {
                if (taskCompleted)
                {
                    using (JobsManagerService.JobsManagerServiceClient jbClient = new FMDA_Engine.JobsManagerService.JobsManagerServiceClient())
                    {
                        jbClient.TaskCompleted(ticket, DateStamp.FullName, decomposedTicket[(int)TicketProtocol.TypeOfProcessing]);
                    }
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                //ApplicationErrorLogger.Log(thread, level, application, "ProcessTicket()", ex.ToString(), selectorX);
            }
        }
    }

    public class TaskHelperClientManager
    {
        public bool CheckTaskHelperConnections(int i, bool bWritetoFile)
        {
            String name = Dns.GetHostName().ToUpper();
            var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];

            var portConfigs = GetNewPorts();

            Object connObject = null;
            try
            {
                bool matchingMode = false;
                String message = "";

                switch (i)
                {
                    case 0:
                        {
                            TaskHelperServiceOne.TaskHelperServiceOneClient processObject =
                                new TaskHelperServiceOne.TaskHelperServiceOneClient("NetTcpBinding_ITaskHelperServiceOne", portConfigs["NetTcpBinding_ITaskHelperServiceOne"]);
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + "...Connection to TaskHelperOne..OK");

                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceOne Port:" + portConfigs["NetTcpBinding_ITaskHelperServiceOne"];
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceOne: Application MODES do not MATCH";
                            }
                        } break;
                    case 1:
                        {
                            TaskHelperServiceTwo.TaskHelperServiceTwoClient processObject = 
                                new TaskHelperServiceTwo.TaskHelperServiceTwoClient("NetTcpBinding_ITaskHelperServiceTwo", portConfigs["NetTcpBinding_ITaskHelperServiceTwo"]);
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + "...Connection to TaskHelperTwo..OK");

                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceTwo Port:" + portConfigs["NetTcpBinding_ITaskHelperServiceTwo"];
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceTwo: Application MODES do not MATCH";
                            }

                        } break;

                    case 2:
                        {
                            TaskHelperServiceThree.TaskHelperServiceThreeClient processObject = 
                                new TaskHelperServiceThree.TaskHelperServiceThreeClient("NetTcpBinding_ITaskHelperServiceThree", portConfigs["NetTcpBinding_ITaskHelperServiceThree"]);
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + "...Connection to TaskHelperThree..OK");

                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceThree Port:" + portConfigs["NetTcpBinding_ITaskHelperServiceThree"];
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceThree: Application MODES do not MATCH";
                            }

                        } break;

                    case 3:
                        {
                            TaskHelperServiceFour.TaskHelperServiceFourClient processObject =
                                new TaskHelperServiceFour.TaskHelperServiceFourClient("NetTcpBinding_ITaskHelperServiceFour", portConfigs["NetTcpBinding_ITaskHelperServiceFour"]);
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + "...Connection to TaskHelperFour..OK");

                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceFour Port:" + portConfigs["NetTcpBinding_ITaskHelperServiceFour"];
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceFour: Application MODES do not MATCH";
                            }

                        } break;

                    case 4:
                        {
                            TaskHelperServiceFive.TaskHelperServiceFiveClient processObject = 
                                new TaskHelperServiceFive.TaskHelperServiceFiveClient("NetTcpBinding_ITaskHelperServiceFive", portConfigs["NetTcpBinding_ITaskHelperServiceFive"]);
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + "...Connection to TaskHelperFive..OK");

                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceFive Port:" + portConfigs["NetTcpBinding_ITaskHelperServiceFive"];
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceFive: Application MODES do not MATCH";
                            }

                        } break;
                }

                Console.WriteLine(message);

                //if (bWritetoFile)
                //{
                //    String dir = System.Configuration.ConfigurationManager.AppSettings["COPYING_REPORT"];
                //    String logFileName = dir + "\\" + Dns.GetHostName().ToUpper() + "_CopyingReport.txt";

                //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileName, true))
                //    {
                //        file.WriteLine(message);
                //    }
                //}

                return matchingMode;

            }
            catch (System.ServiceModel.CommunicationException commProblem)
            {
                switch (i)
                {
                    case 0:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceOne");
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + " :: Connection to TaskHelperServiceOne... FAILED!");

                            TaskHelperServiceOne.TaskHelperServiceOneClient taskHelper = connObject as TaskHelperServiceOne.TaskHelperServiceOneClient;
                            taskHelper.Abort();

                        } break;
                    case 1:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceTwo");
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + " :: Connection to TaskHelperServiceTwo... FAILED!");

                            TaskHelperServiceTwo.TaskHelperServiceTwoClient taskHelper = connObject as TaskHelperServiceTwo.TaskHelperServiceTwoClient;
                            taskHelper.Abort();
                        } break;

                    case 2:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceThree");
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + " :: Connection to TaskHelperServiceThree... FAILED!");

                            TaskHelperServiceThree.TaskHelperServiceThreeClient taskHelper = connObject as TaskHelperServiceThree.TaskHelperServiceThreeClient;
                            taskHelper.Abort();

                        } break;

                    case 3:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceFour");
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + " :: Connection to TaskHelperServiceFour... FAILED!");

                            TaskHelperServiceFour.TaskHelperServiceFourClient taskHelper = connObject as TaskHelperServiceFour.TaskHelperServiceFourClient;
                            taskHelper.Abort();

                        } break;

                    case 4:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceFive");
                            LogNotifyer.SendMessage("FMDA :: " + name + " " + modeStr + " :: Connection to TaskHelperServiceFive... FAILED!");

                            TaskHelperServiceFive.TaskHelperServiceFiveClient taskHelper = connObject as TaskHelperServiceFive.TaskHelperServiceFiveClient;
                            taskHelper.Abort();
                        } break;
                }

            }
            return false;
        }

        public static Dictionary<String, String> GetNewPorts()
        {
            Dictionary<String, String> portLookUp = new Dictionary<String, String>();

            // Open the stream and read it back. 
            using (StreamReader sr = File.OpenText(System.IO.Directory.GetCurrentDirectory() + "\\" + "portFile.txt"))
            {
                int i = 1;
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                   switch(i)
                   {
                       case 1:
                           {
                               portLookUp.Add("TaskHelperServiceOne.ITaskHelperServiceOne", s);
                           } break;
                       case 2:
                           {
                               portLookUp.Add("TaskHelperServiceTwo.ITaskHelperServiceTwo", s);
                           } break;
                       case 3:
                           {
                               portLookUp.Add("TaskHelperServiceThree.ITaskHelperServiceThree", s);
                           } break;
                       case 4:
                           {
                               portLookUp.Add("TaskHelperServiceFour.ITaskHelperServiceFour", s);
                           } break;
                       case 5:
                           {
                               portLookUp.Add("TaskHelperServiceFive.ITaskHelperServiceFive", s);
                           } break;
                   }
                   i++;
                }
            }

            var portReturns = new Dictionary<string, string>();



            ClientSection clientSection =
            ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;

            ChannelEndpointElementCollection endpointCollection =
                clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;

            foreach (ChannelEndpointElement endpointElement in endpointCollection)
            {
                string portS = "";

                if (portLookUp.TryGetValue(endpointElement.Contract, out portS))
                {
                    String endpoint = endpointElement.Address.AbsoluteUri;

                    var baseaddress = endpoint.Split(':');
                    string text = baseaddress.LastOrDefault().Substring(4, baseaddress.LastOrDefault().Length - 4);
                    String endpointCreated = baseaddress[0] + ":" + baseaddress[1] + ":" + portS + text;

                    portReturns.Add(endpointElement.BindingConfiguration, endpointCreated);

                    Uri BaseAddress = new Uri(endpointCreated);
                }
            }
            return portReturns;
        }
    }
}
