﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace FMDA_Engine
{
    public class CommandListenner
    {
        static public void ListenForCommands()
        {

            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            String mode = System.Configuration.ConfigurationManager.AppSettings["MODE"];
            String key = mode +"_PORT";

            UdpClient udpClient = new UdpClient(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[key]), IPAddress.Parse(localIP).AddressFamily);//make listenning port dynamic


            while (true)
            {
                udpClient.EnableBroadcast = true;
                try
                {
                    //IPEndPoint object will allow us to read datagrams sent from any source.
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[key]));

                    Console.WriteLine("");
                    Console.WriteLine("[Waiting for Command..]");
                    Console.WriteLine("");


                    // Blocks until a message returns on this socket from a remote host.
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);


                    String dateStr = DateTime.Now.ToString();

                    Console.WriteLine("Service::" + returnData + "::" + dateStr);

                    String[] conditionArray = returnData.Split('_');

                    String machineName = Dns.GetHostName().ToUpper();

                    if ((conditionArray.FirstOrDefault() == "bRequestingInterrupt")
                    && (conditionArray.LastOrDefault() == "CLOSE"))
                    {
                        TaskHelperServiceOne.TaskHelperServiceOneClient processOne = new TaskHelperServiceOne.TaskHelperServiceOneClient();
                        processOne.CloseHelper();

                        TaskHelperServiceFive.TaskHelperServiceFiveClient processFive = new TaskHelperServiceFive.TaskHelperServiceFiveClient();
                        processFive.CloseHelper();

                        TaskHelperServiceFour.TaskHelperServiceFourClient processFour = new TaskHelperServiceFour.TaskHelperServiceFourClient();
                        processFour.CloseHelper();

                        TaskHelperServiceThree.TaskHelperServiceThreeClient processThree = new TaskHelperServiceThree.TaskHelperServiceThreeClient();
                        processThree.CloseHelper();

                        TaskHelperServiceTwo.TaskHelperServiceTwoClient processTwo = new TaskHelperServiceTwo.TaskHelperServiceTwoClient();
                        processTwo.CloseHelper();


                        Environment.Exit(0);

                        //Process.GetProcessesByName("whatever.exe");

                        //Process process = new Process();
                        //process.Kill();

                    }                    
                    //udpClient.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }
            udpClient.Close();
        }

    }
}
