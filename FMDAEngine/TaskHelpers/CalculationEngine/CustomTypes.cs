﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

namespace CalculationEngine
{
    public enum NumberErrors 
    {
        NaN = 999999999
    }

    [DataContract]
    public class TemporaryResult
    {
        [DataMember]
        public Double distance = Double.MaxValue;

        [DataMember]
        public List<DateTime> dates = new List<DateTime>();

        [DataMember]
        public List<Double> values = new List<double>();
        //public String symbol = "";
    }

    [DataContract]
    public class CorrelationEntity
    {
        [DataMember]
        public String SymbolID;

        [DataMember]        
        public String CorrelatingSymbolID;

        [DataMember]         
        public DateTime StartDate;

        [DataMember]         
        public DateTime EndDate;

        [DataMember]         
        public Double Distance;
        
        [DataMember] 
        public Double CorrelationCoefficient;
        
        [DataMember] 
        public TradeEvent Event;

        [DataMember]
        public String TimeFrame;

        [DataMember]
        public String Type;
    }

    [DataContract]
    public enum TradeEvent
    {
        [EnumMember]
        AdjustmentClose = 0,

        [EnumMember]
        Close = 1,

        [EnumMember]
        Open = 2,

        [EnumMember]
        High = 3,

        [EnumMember]
        Low = 4,

        [EnumMember]
        Volume = 5
    }

    public class CustomType
    {
        public static String TradeEventToString(TradeEvent tradeEvent)
        {
            String type = "";
            switch (tradeEvent)
            {
                case TradeEvent.AdjustmentClose:
                    {
                        type = "AdjClose";
                    }
                    break;

                case TradeEvent.Close:
                    {
                        type = "Close";
                    }break;

                case TradeEvent.Open:
                    {
                        type = "Open";
                    }
                    break;

                case TradeEvent.High:
                    {
                        type = "High";
                    }
                    break;

                case TradeEvent.Low:
                    {
                        type = "Low";
                    }
                    break;


                case TradeEvent.Volume:
                    {
                        type = "Volume";
                    }
                    break;
            }
            return type;
        }
    }



}
