﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace CalculationEngine
{
    public class TicketProcessor
    {
        public TicketProcessor()
        {
            Logger.Setup();

            LogHelper.Info("Log Folder Created!");
        }

        public void Process(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles, String timeFrame, String jobType)
        {
            switch (jobType)
            {
                case "HC":
                case "RC":
                    {
                        CorrelationProcessorHelper correlationProcesor = new CorrelationProcessorHelper();
                        correlationProcesor.ExecuteHistoricalCorrelationAnalysis(jobId, SymbolID, StockExchange, startDate, endDate, compatibles, timeFrame, jobType);

                    }
                    break;

                //case "RC":
                //    {

                //    }
                //    break;
            }
        }

    }

    public static class Tester
    {
        public static void ConnectionTest(String appName)
        {
            String name = Dns.GetHostName().ToUpper();

            try
            {
                using (DatabaseService.DatabaseWcfServiceClient tempClient =
                    new DatabaseService.DatabaseWcfServiceClient())
                {
                    tempClient.Open();

                    tempClient.GetDatabaseData("");
                    LogNotifyer.SendMessage(appName + " :: " + name + " Connection to Raw Data Service " + " :: ...OK!");

                }
            }
            catch (Exception ex)
            {
                LogNotifyer.SendMessage(appName + " :: " + name + " Connection to Raw Data Service " + " :: ...FAILED!");
                Console.WriteLine("Connection FAILED - RawDataService");
            }
        }
    }

}
