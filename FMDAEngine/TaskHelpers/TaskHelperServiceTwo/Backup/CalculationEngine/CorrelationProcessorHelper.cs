﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using LogSys;

namespace CalculationEngine
{


    public class CorrelationProcessorHelper
    {
        private Database _dataModel = new Database();


        public List<CorrelationEntity> ExecuteHistoricalCorrelationAnalysis(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles)
        {
            //Prepare for HC analysis
            Console.WriteLine("Historical Correlation Analysis...");
            Console.WriteLine("JobId: {0} SymbolID: {1} Compatibles: {2}", jobId, SymbolID, compatibles.Count);

            List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = GetDayTradeSummaries(jobId, SymbolID, StockExchange, startDate, endDate, compatibles);
            
            List<CorrelationEntity> correaltionEntityList = new List<CorrelationEntity>();

            if (dayTradeSummaries != null)
            {
                if (dayTradeSummaries.Count > 0)
                {
                    int criteriaLength = dayTradeSummaries.FirstOrDefault().Value.Count;

                    int positionWithinSymbolList = -1;

                        ++positionWithinSymbolList;

                        String currentSymbolCriteria = SymbolID;

                        for (int tradeEvent = (int)TradeEvent.AdjustmentClose; tradeEvent <= (int)TradeEvent.Volume; tradeEvent++)
                        {
                            try
                            {
                                List<Double> criteria = new List<double>();

                                if (positionWithinSymbolList >= 0)
                                {
                                    switch ((TradeEvent)tradeEvent)
                                    {
                                        case TradeEvent.AdjustmentClose:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.AdjustmentClose);
                                                }
                                            } break;

                                        case TradeEvent.Close:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Close);
                                                }
                                            } break;

                                        case TradeEvent.Open:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Open);
                                                }
                                            } break;

                                        case TradeEvent.High:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.High);
                                                }
                                            } break;


                                        case TradeEvent.Low:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Low);
                                                }
                                            } break;

                                        case TradeEvent.Volume:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Volume);
                                                }
                                            } break;
                                    }

                                    //*********


                                    DateTime present = DateTime.Now;
                                    System.Diagnostics.Debug.WriteLine(String.Format("\n\n SearchEngineDD algorithm start:: {0} \n\n", present.ToString()));


                                    for (int symbols = 0; symbols < dayTradeSummaries.Count; symbols++)
                                    {
                                        try
                                        {

                                            String symbolTask = dayTradeSummaries[symbols].Key;

                                            if (positionWithinSymbolList != symbols && currentSymbolCriteria != symbolTask)
                                            {
                                                int noItemsPerJob = dayTradeSummaries[symbols].Value.Count;
                                                List<DayTradeSummary> dtsList = dayTradeSummaries[symbols].Value;

                                                List<KeyValuePair<Double, DateTime>> comparer = new List<KeyValuePair<Double, DateTime>>();


                                                switch ((TradeEvent)tradeEvent)
                                                {
                                                    case TradeEvent.AdjustmentClose:
                                                        {
                                                            foreach (var item in dtsList) //fix casting issue
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.AdjustmentClose, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.Close:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Close, item.Date));
                                                            }
                                                        } break;


                                                    case TradeEvent.Open:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Open, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.High:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.High, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.Low:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Low, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.Volume:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Volume, item.Date));
                                                            }
                                                        } break;
                                                }

                                                //----------------------------------------------//


                                                CorrelationEntity correlationEntity = new CorrelationEntity();


                                                Double accumulatedValue = 0.0;

                                                Double xSum = 0.0;
                                                Double ySum = 0.0;
                                                Double xSquaredSum = 0.0;
                                                Double ySquaredSum = 0.0;
                                                Double xySum = 0.0;

                                                for (int coordinate = 0; coordinate < criteriaLength; coordinate++)
                                                {
                                                    Double value = comparer[coordinate].Key - criteria[coordinate];
                                                    Double squaredValue = value * value;

                                                    accumulatedValue = accumulatedValue + squaredValue;


                                                    //----PMCC----//
                                                    Double xSquared = criteria[coordinate] * criteria[coordinate];
                                                    Double ySquared = comparer[coordinate].Key * comparer[coordinate].Key;

                                                    Double xy = criteria[coordinate] * comparer[coordinate].Key;

                                                    xSum = xSum + criteria[coordinate];
                                                    ySum = ySum + comparer[coordinate].Key;

                                                    xSquaredSum = xSquaredSum + xSquared;
                                                    ySquaredSum = ySquaredSum + ySquared;
                                                    xySum = xySum + xy;

                                                    //---------------------------------------------------------//

                                                }
                                                Double distance = System.Math.Sqrt(accumulatedValue);

                                                //Correlation Ratio - Product moment correlation coefficient
                                                int totalNoDays = dtsList.Count;

                                                Double Sxx = xSquaredSum - ((xSum * xSum) / totalNoDays);
                                                Double Syy = ySquaredSum - ((ySum * ySum) / totalNoDays); ;
                                                Double Sxy = xySum - ((xSum * ySum) / totalNoDays);
                                                
                                                Double tempValue = System.Math.Sqrt(Sxx * Syy);

                                                Double correlationCoefficient = Convert.ToDouble(NumberErrors.NaN);

                                                if (tempValue > 0)
                                                {
                                                    correlationCoefficient = Sxy / tempValue;
                                                }

                                                //---------------------------------------------------------//


                                                correlationEntity.CorrelationCoefficient = correlationCoefficient;
                                                correlationEntity.Event = (TradeEvent)tradeEvent;
                                                correlationEntity.Distance = distance;

                                                correlationEntity.SymbolID = currentSymbolCriteria; 

                                                correlationEntity.CorrelatingSymbolID = symbolTask;
                                                correlationEntity.StartDate = startDate;
                                                correlationEntity.EndDate = endDate;

                                                //---------------------------------------------------//

                                                correaltionEntityList.Add(correlationEntity);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            String extraMsg = String.Format("indexpos: {0}, totaldays: {1}, SymbolID: {2}, currentSymbolCriteria: {3}", positionWithinSymbolList, dayTradeSummaries.Count, SymbolID, currentSymbolCriteria);
                                            String thread = System.Threading.Thread.CurrentThread.ToString();
                                            String level = "Error";
                                            String application = Console.Title;
                                            String selectorX = "JobsManager";

                                            switch (Database.Mode)
                                            {
                                                case MODE.Test:
                                                    {
                                                        selectorX += "_TEST";
                                                    }
                                                    break;

                                                case MODE.Live:
                                                    {
                                                        selectorX += "_LIVE";
                                                    }
                                                    break;

                                                case MODE.LiveTest:
                                                    {
                                                        selectorX += "_LIVE-TEST";
                                                    }
                                                    break;
                                            }
                                            ApplicationErrorLogger.Log(thread, level, application, "ExecuteHistoricalCorrelationAnalysis() :: " + extraMsg, ex.ToString(), selectorX);

                                            Console.WriteLine("inside loop - " + extraMsg  + " :: " + ex.Message);
                                            Console.WriteLine();
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Error -:: " + positionWithinSymbolList);
                                }

                            }
                            catch (Exception ex)
                            {
                                String extraMsg = String.Format("indexpos: {0}, totaldays: {1}, SymbolID: {2}, currentSymbolCriteria: {3}", positionWithinSymbolList, dayTradeSummaries.Count, SymbolID, currentSymbolCriteria);
                                String thread = System.Threading.Thread.CurrentThread.ToString();
                                String level = "Error";
                                String application = Console.Title;
                                String selectorX = "JobsManager";

                                switch (Database.Mode)
                                {
                                    case MODE.Test:
                                        {
                                            selectorX += "_TEST";
                                        }
                                        break;

                                    case MODE.Live:
                                        {
                                            selectorX += "_LIVE";
                                        }
                                        break;

                                    case MODE.LiveTest:
                                        {
                                            selectorX += "_LIVE-TEST";
                                        }
                                        break;
                                }
                                ApplicationErrorLogger.Log(thread, level, application, "ExecuteHistoricalCorrelationAnalysis() :: " + extraMsg, ex.ToString(), selectorX);

                                Console.WriteLine("inside loop - " + extraMsg + " :: " + ex.Message);
                                Console.WriteLine();
                            }
                        }
                        Console.WriteLine("[Chunk Completed]");
                        Console.WriteLine("");
                        Console.WriteLine("");
                }
            }
            
            return correaltionEntityList;
        }

        private List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummaries(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles)
        {
            //List<String> temp =  _dataModel.GetCompatibleListDataFromDB(StockExchange, SymbolID);
            List<String> symbolsTaskList = new List<string>();
            symbolsTaskList.Add(SymbolID);

            symbolsTaskList.AddRange(compatibles);

            List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = _dataModel.GetDayTradeSummariesDbService(symbolsTaskList, startDate, endDate, StockExchange);

            return dayTradeSummaries;
        }
    }
}
