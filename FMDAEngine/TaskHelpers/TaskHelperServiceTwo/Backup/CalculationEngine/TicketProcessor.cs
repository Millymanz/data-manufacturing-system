﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculationEngine
{
    public class TicketProcessor
    {
        public void Process(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles, String jobType)
        {
            switch (jobType)
            {
                case "HC":
                    {
                        CorrelationProcessorHelper correlationProcesor = new CorrelationProcessorHelper();
                        correlationProcesor.ExecuteHistoricalCorrelationAnalysis(jobId, SymbolID, StockExchange, startDate, endDate, compatibles);

                    }
                    break;

                case "RC":
                    {

                    }
                    break;
            }
        }

    }
}
