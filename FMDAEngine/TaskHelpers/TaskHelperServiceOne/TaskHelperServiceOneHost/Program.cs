﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Description;
using TaskHelperServiceOne;
using System.IO;
using System.Threading;
using System.Net;
using CalculationEngine;

namespace TaskHelperServiceOneHost
{
    static public class RunApp
    {
        static public void Run(bool bLive)
        {
            Console.Title = "TaskHelperServiceOne - 1";

            Tester.ConnectionTest(Console.Title);

            String modeType = bLive ? "LIVE" : "TEST";

            if (bLive)
            {
                modeType = "LIVE";
            }
            else
            {
                var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];
                modeType = modeStr;

                if (modeStr == "LIVE") modeType = "TEST";
            }

            if (modeType == "UI")
            {
                try
                {
                    Console.WriteLine("Select Runtime Mode - (1)TEST Or (2)LIVE-TEST");
                    String readAns = Console.ReadLine().ToLower();

                    var selection = (CalculationEngine.MODE)Convert.ToInt32(readAns);
                    CalculationEngine.Database.Mode = (MODE)selection;

                    if ((MODE)selection == MODE.Live) CalculationEngine.Database.Mode = CalculationEngine.MODE.Test;    


                    ModeTitle();

                    StartService();
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR starting WCF service. Exception - " + e.Message);
                    Console.WriteLine("Press Enter To terminate.");
                    Console.ReadLine();
                }
            }
            else
            {
                if (modeType == "LIVE")
                {
                    CalculationEngine.Database.Mode = (CalculationEngine.MODE)0;
                }
                else if (modeType == "TEST")
                {
                    CalculationEngine.Database.Mode = (CalculationEngine.MODE)1;
                }
                else if (modeType == "LIVE-TEST")
                {
                    CalculationEngine.Database.Mode = (CalculationEngine.MODE)2;
                }

                ModeTitle();

                StartService();

            }
        }

        static public void ModeTitle()
        {
            switch (CalculationEngine.Database.Mode)
            {
                case CalculationEngine.MODE.Test:
                    {
                        Console.Title += " :: TEST-MODE";
                    }
                    break;

                case CalculationEngine.MODE.Live:
                    {
                        Console.Title += " :: LIVE-MODE";
                    }
                    break;

                case CalculationEngine.MODE.LiveTest:
                    {
                        Console.Title += " :: LIVE-TEST-MODE";
                    }
                    break;
            }
        }

        static public Uri ConfigAddress()
        {
            String endpoint = System.Configuration.ConfigurationManager.AppSettings["TEMPBASEADDRESS"].ToString();

            if (File.Exists(System.IO.Directory.GetCurrentDirectory() + "\\" + "portFile.txt"))
            {
                string portS = "";
                // Open the stream and read it back. 
                using (StreamReader sr = File.OpenText(System.IO.Directory.GetCurrentDirectory() + "\\" + "portFile.txt"))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        portS = s;
                    }
                }

                var baseaddress = endpoint.Split(':');

                string text = baseaddress.LastOrDefault().Substring(4, baseaddress.LastOrDefault().Length - 4);

                String endpointCreated = baseaddress[0] + ":" + baseaddress[1] + ":" + portS + text;

                Uri BaseAddress = new Uri(endpointCreated);
                return BaseAddress;
            }

            Uri baseAddress = new Uri(endpoint);
            return baseAddress;
        }

        static public void StartService()
        {
            Uri BaseAddress = ConfigAddress();

            ServiceHost sv = new ServiceHost(typeof(TaskHelperServiceOne.TaskHelperServiceOne), BaseAddress);
            
            sv.Open();

            try
            {
                foreach (ServiceEndpoint se in sv.Description.Endpoints)
                    Console.WriteLine(se.Address.ToString());

                Console.WriteLine("");
                Console.WriteLine("Task Helper Service One");
                Console.WriteLine("");
            }
            catch (Exception e)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (CalculationEngine.Database.Mode)
                {
                    case CalculationEngine.MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case CalculationEngine.MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case CalculationEngine.MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }
                //ApplicationErrorLogger.Log(thread, level, application, "StartService()", e.ToString(), selector);
            }

            //InitiateDBS();

            Console.WriteLine("Press Enter To terminate.");
            Console.ReadLine();

            sv.Close();
        }

        //Unique to TH1 used to start DBS service so it can update itself it
        //hasnt already been started
        static public bool InitiateDBS()
        {
            try
            {
                CalculationEngine.DatabaseService.DatabaseWcfServiceClient dbs = new CalculationEngine.DatabaseService.DatabaseWcfServiceClient();

                dbs.Open();
                dbs.GetDatabaseData("start");
                dbs.Close();

                Console.WriteLine("Database Service Initiated");
            }
            catch (Exception ex)
            {

                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (CalculationEngine.Database.Mode)
                {
                    case CalculationEngine.MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case CalculationEngine.MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case CalculationEngine.MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }
                //ApplicationErrorLogger.Log(thread, level, application, "InitiateDBS()", ex.ToString(), selector);
                return false;
            }
            return true;
        }

    }



    class Program
    {
        static void Main(string[] args)
        {
            TaskHelperServiceOneHost.RunApp.Run(false);
        }
    }
}
