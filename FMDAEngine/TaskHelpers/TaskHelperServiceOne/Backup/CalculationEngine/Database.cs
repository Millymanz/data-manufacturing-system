﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;

//using TaskHelperServiceOne.DatabaseService;
using System.Data.DataSetExtensions;
using CalculationEngine.DatabaseService;
using LogSys;

namespace CalculationEngine
{
    static public class Exchanges
    {
        static public List<String> List = new List<string>();

        static public void InitialiseExchangeList()
        {
            string[] exchangesArray = System.Configuration.ConfigurationManager.AppSettings["EXCHANGES"].Split(';');
            Exchanges.List = exchangesArray.ToList();
        }
    }


    //Temp will be in its own file
    public class TradeDate
    {
        private DateTime date;
        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }
    }

    public enum MODE
    {
        Live = 0,
        Test = 1,
        LiveTest = 2
    }

    //temps

    public class DayTradeSummary
    {
        private DateTime date;
        private double open;
        private double high;
        private double low;
        private double close;
        private double adjustmentclose;
        private int volume;

        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public double Open
        {
            get { return open; }
            set { open = value; }
        }

        public double High
        {
            get { return high; }
            set { high = value; }
        }

        public double Low
        {
            get { return low; }
            set { low = value; }
        }

        public double Close
        {
            get { return close; }
            set { close = value; }
        }

        public int Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public double AdjustmentClose
        {
            get { return adjustmentclose; }
            set { adjustmentclose = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }


    }

    //-----------------------------------------------------------//
    /// <summary>
    /// Class to store one CSV row
    /// </summary>
    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }

    /// <summary>
    /// Class to write data to a CSV file
    /// </summary>
    public class CsvFileWriter : StreamWriter
    {
        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename, true)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            StringBuilder builder = new StringBuilder();
            bool firstColumn = true;
            foreach (string value in row)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    builder.Append(',');
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                    builder.Append(value);
                firstColumn = false;
            }
            row.LineText = builder.ToString();
            WriteLine(row.LineText);
        }
    }



    //---------------------------------------------------//


    public class Database
    {
        private List<KeyValuePair<String, String>> symbolLookUpTable = null;

        DataTable _dataTableHC = null;

        static public MODE Mode;

        public Database()
        {
            Exchanges.InitialiseExchangeList();
        }

        public List<String> GetHCSymbolsTaskList()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            List<DateTime> listLatestDayTradeDate = new List<DateTime>();


            //This code assumes that the index dbs are update with the latest day trade summaries
            //
            //
            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                DateTime latestDayTradeDate = new DateTime();
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        String commandTxt = "SELECT MAX(dbo.DayTradeSummaries.Date) AS MaxDate FROM dbo.DayTradeSummaries";
                        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                        sqlCommand.CommandTimeout = 0;

                        con.Open();

                        SqlDataReader rdr = sqlCommand.ExecuteReader();

                        while (rdr.Read())
                        {
                            latestDayTradeDate = Convert.ToDateTime(rdr["MaxDate"].ToString());
                        }
                    }
                    listLatestDayTradeDate.Add(latestDayTradeDate);
                }
                catch (Exception e)
                {
                    String thread = System.Threading.Thread.CurrentThread.ToString();
                    String level = "Error";
                    String application = Console.Title;

                    String selector = "JobsManager";

                    switch (Database.Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selector += "_LIVE-TEST";
                            }
                            break;
                    }
                    ApplicationErrorLogger.Log(thread, level, application, "GetHCSymbolsTaskList()", e.ToString(), selector);
                }
            }

            var ldTradeDate = (from dateItem in listLatestDayTradeDate select dateItem).Max();


            String dateCriteria = String.Format("{0}-{1}-{2}", ldTradeDate.Year, ldTradeDate.Month, ldTradeDate.Day);

            try
            {

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                {

                    String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate < '" + dateCriteria + "'";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol);
                    }
                }
            }
            catch (Exception e)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }
                ApplicationErrorLogger.Log(thread, level, application, "GetHCSymbolsTaskList()", e.ToString(), selector);
            }

            if (hcSymbolTaskList.Count == 0)//this condition is true for both uptodate and empty db table..?
            {
                foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
                {
                    String stockExchange = item.ToString();

                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                    try
                    {
                        using (SqlConnection con = new SqlConnection(settings.ToString()))
                        {
                            String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                            SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                            sqlCommand.CommandTimeout = 0;

                            con.Open();

                            SqlDataReader rdr = sqlCommand.ExecuteReader();

                            while (rdr.Read())
                            {
                                String symbol = rdr["SymbolID"].ToString();

                                hcSymbolTaskList.Add(symbol);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        String thread = System.Threading.Thread.CurrentThread.ToString();
                        String level = "Error";
                        String application = Console.Title;

                        String selector = "JobsManager";

                        switch (Database.Mode)
                        {
                            case MODE.Test:
                                {
                                    selector += "_TEST";
                                }
                                break;

                            case MODE.Live:
                                {
                                    selector += "_LIVE";
                                }
                                break;

                            case MODE.LiveTest:
                                {
                                    selector += "_LIVE-TEST";
                                }
                                break;
                        }
                        ApplicationErrorLogger.Log(thread, level, application, "GetHCSymbolsTaskList()", e.ToString(), selector);
                    }
                }
            }
            else
            {
                //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
                //exclude these symbols from the hcSymbolTaskList
                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate >= '" + dateCriteria + "'";

            }
            return hcSymbolTaskList;
        }

        public List<String> GetHCSymbolsTaskListX()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            List<DateTime> listLatestDayTradeDate = new List<DateTime>();


            //This code assumes that the index dbs are update with the latest day trade summaries
            //
            //
            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                DateTime latestDayTradeDate = new DateTime();
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT MAX(dbo.DayTradeSummaries.Date) AS MaxDate FROM dbo.DayTradeSummaries";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        latestDayTradeDate = Convert.ToDateTime(rdr["MaxDate"].ToString());
                    }
                }
                listLatestDayTradeDate.Add(latestDayTradeDate);
            }

            var ldTradeDate = (from dateItem in listLatestDayTradeDate select dateItem).Max();


            String dateCriteria = String.Format("{0}-{1}-{2}", ldTradeDate.Year, ldTradeDate.Month, ldTradeDate.Day);


            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //

            //this condition is true for both uptodate and empty db table..?

            List<String> uptoDateList = new List<string>();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {

                String commTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate >= '" + dateCriteria + "'";
                SqlCommand sqlCommand = new SqlCommand(commTxt, con);
                sqlCommand.CommandTimeout = 0;

                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    uptoDateList.Add(symbol.Trim());
                    //hcSymbolTaskList.Add(symbol);
                }
            }



            String criteriaStr = "";
            int itr = 0;
            foreach (var hg in uptoDateList)
            {
                if (itr == 0)
                {
                    criteriaStr = "'" + hg + "'";
                }
                else
                {
                    criteriaStr += " AND dbo.DayTradeSummaries.SymbolID = '" + hg + "'";
                }
                itr++;
            }

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries WHERE NOT dbo.DayTradeSummaries.SymbolID = " + criteriaStr + " ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol);
                    }
                }
            }
            //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
            //exclude these symbols from the hcSymbolTaskList


            //using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            //{

            //    String commTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate >= '" + dateCriteria + "'";
            //    SqlCommand sqlCommand = new SqlCommand(commTxt, con);
            //    con.Open();

            //    SqlDataReader rdr = sqlCommand.ExecuteReader();

            //    while (rdr.Read())
            //    {
            //        String symbol = rdr["SymbolID"].ToString();

            //        uptoDateList.Add(symbol);
            //        //hcSymbolTaskList.Add(symbol);
            //    }
            //}


            //if (hcSymbolTaskList.Count > 200)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 200);
            //}
            //else
            return hcSymbolTaskList;

        }

        public List<String> GetHistoricalCompatibleSymbolsList(string jobId)
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings["JobsManagerStaging"];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                String commandTxt = "SELECT * FROM dbo.HistoricallyCompatibleStocks WHERE dbo.HistoricallyCompatibleStocks.JobId='" + jobId + "'";

                //String commandTxt = "SELECT DISTINCT TOP 20 dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;

                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    hcSymbolTaskList.Add(symbol.Trim());
                }
            }
            return hcSymbolTaskList;
        }

        public List<String> GetCompatibleListDataFromDB(String stockExchange, String symbolID)
        {
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings["JobsManagerStaging"];

            Dictionary<String, KeyValuePair<DateTime, List<String>>> symbolList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            List<String> compList = new List<string>();

            DateTime dateTemp = new DateTime();

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //String commandTxt = "SELECT * FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "' ORDER BY dbo.HistoricallyCompatibleStocks.SymbolID ASC";
                String commandTxt = "SELECT dbo.HistoricallyCompatibleStocks.CompatibleSymbolID FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "' AND HistoricallyCompatibleStocks.CriteriaSymbolID ='" + symbolID + "'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;

                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String compatibleSymbolID = rdr["CompatibleSymbolID"].ToString();

                    /*String date = rdr["Date"].ToString();

                    dateTemp = DateTime.Parse(date);*/

                    compList.Add(compatibleSymbolID);
                    // symbolList.Add(symbol, new KeyValuePair<DateTime,List<string>>(dateItem, ));
                }
            }


            return compList;
        }



        public List<String> GetHCSymbolsTaskListY()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT TOP 20 dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        hcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }

            //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
            //exclude these symbols from the hcSymbolTaskList

            //if (hcSymbolTaskList.Count > 500)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 500);
            //}
            //else
            return hcSymbolTaskList;
        }

        public List<String> GetRCSymbolsTaskList()
        {
            //specify exchange
            List<String> rcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //assumes range correlation table is empty

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        rcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }

            //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
            //exclude these symbols from the hcSymbolTaskList

            //if (hcSymbolTaskList.Count > 500)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 500);
            //}
            //else
            return rcSymbolTaskList;
        }


        public Dictionary<String, List<DateTime>> GetTradeDates(List<String> symbolList)
        {
            String criteria = "";
            bool bfirstItem = true;
            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "'";
                }
            }



            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();


            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        //TradeDate tradeDate = new TradeDate();

                        //tradeDate.SymbolID = rdr["SymbolID"].ToString();
                        //tradeDate.Date = Convert.ToDateTime(rdr["Date"].ToString());

                        String sym = rdr["SymbolID"].ToString();
                        DateTime dateItem = Convert.ToDateTime(rdr["Date"].ToString());

                        if (!tradeDateList.ContainsKey(sym))
                        {
                            List<DateTime> dateList = new List<DateTime>();
                            dateList.Add(dateItem);

                            tradeDateList[sym] = dateList;

                        }
                        else
                        {
                            tradeDateList[sym].Add(dateItem);
                        }
                    }
                }
            }

            return tradeDateList;
        }

        public Dictionary<String, List<DateTime>> GetTradeDatesRange(List<String> symbolList, DateTime startDate, DateTime endDate)
        {
            String criteria = "";
            bool bfirstItem = true;

            String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'";
                }
            }

            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        //TradeDate tradeDate = new TradeDate();

                        //tradeDate.SymbolID = rdr["SymbolID"].ToString();
                        //tradeDate.Date = Convert.ToDateTime(rdr["Date"].ToString());

                        String sym = rdr["SymbolID"].ToString();
                        DateTime dateItem = Convert.ToDateTime(rdr["Date"].ToString());

                        if (!tradeDateList.ContainsKey(sym))
                        {
                            List<DateTime> dateList = new List<DateTime>();
                            dateList.Add(dateItem);

                            tradeDateList[sym] = dateList;

                        }
                        else
                        {
                            tradeDateList[sym].Add(dateItem);
                        }
                    }
                }
            }

            return tradeDateList;
        }

        public List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummaries(List<String> symbolList, DateTime startDate, DateTime endDate, String stockExchange)
        {

            List<KeyValuePair<String, List<DayTradeSummary>>> stockSymbolsHistory = new List<KeyValuePair<String, List<DayTradeSummary>>>();

            foreach (string symbol in symbolList)
            {
                List<DayTradeSummary> dataList = new List<DayTradeSummary>();

                List<Double> dataListTemp = new List<Double>();


                //String stockExchange = LookUpSymbolStockExchange(symbol);

                try
                {
                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange].ConnectionString))

                    //using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["AMEX"].ConnectionString))
                    {
                        conn.Open();

                        String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
                        String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

                        //SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);

                        SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);
                        sqlCommand.CommandTimeout = 0;

                        SqlDataReader reader = sqlCommand.ExecuteReader();


                        while (reader.Read())
                        {
                            String SymbolID = reader["SymbolID"].ToString();

                            //Double data = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            DayTradeSummary dayTrade = new DayTradeSummary();
                            dayTrade.SymbolID = reader["SymbolID"].ToString();
                            dayTrade.Date = Convert.ToDateTime(reader["Date"].ToString());

                            dayTrade.Open = Convert.ToDouble(reader["Open"].ToString());
                            dayTrade.High = Convert.ToDouble(reader["High"].ToString());
                            dayTrade.Low = Convert.ToDouble(reader["Low"].ToString());
                            dayTrade.Close = Convert.ToDouble(reader["Close"].ToString());
                            dayTrade.Volume = Convert.ToInt32(reader["Volume"].ToString());


                            dayTrade.AdjustmentClose = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            dataList.Add(dayTrade);

                            //dataListTemp.Add(dayTrade.AdjustmentClose);

                        }


                        stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(symbol, dataList));

                        //masterSymbolDataList.Add(new KeyValuePair<string, List<double>>(symbol, dataListTemp));

                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Write("Msg " + e.ToString());
                }
            }
            return stockSymbolsHistory;
        }

        public List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummariesDbService(List<String> symbolList, DateTime startDate, DateTime endDate, String stockExchange)
        {
            List<KeyValuePair<String, List<DayTradeSummary>>> stockSymbolsHistory = new List<KeyValuePair<String, List<DayTradeSummary>>>();

            DatabaseService.DatabaseWcfServiceClient dbService = new DatabaseWcfServiceClient();

            List<DatabaseService.DayTradeSummary> dtDaySummaries = new List<DatabaseService.DayTradeSummary>();

            TimeSpan span = endDate - startDate;

            //3000 days is limit estimate of 10years
            //Assumes data is available for all stock and each as 
            //3000 days available

            int daysLimit = 3000;

            TimeSpan spanTime = endDate - startDate;

            int remainingDays = 0;

            if (spanTime.Days > daysLimit)
            {
                remainingDays = spanTime.Days - daysLimit;
            }
            else
            {
                daysLimit = spanTime.Days;
            }


            for (int i = 0; i < symbolList.Count; i++)
            {
                String[] listArray = new string[1];
                listArray[0] = symbolList[i];

                List<DatabaseService.DayTradeSummary> temp = dbService.GetDayTradeSummaries(listArray, startDate, (startDate.AddDays(daysLimit)), stockExchange).ToList();

                if (temp.Count > 0)
                {
                    dtDaySummaries.AddRange(temp);
                }


                if (remainingDays > 0)
                {
                    List<DatabaseService.DayTradeSummary> tempList = dbService.GetDayTradeSummaries(listArray, (endDate.AddDays(-remainingDays)), endDate, stockExchange).ToList();

                    if (temp.Count > 0)
                    {
                        dtDaySummaries.AddRange(tempList);
                    }
                }
            }
            dbService.Close();


            if (dtDaySummaries.Count > 0)
            {
                DateTime st = dtDaySummaries.FirstOrDefault().Date;
                DateTime en = dtDaySummaries.LastOrDefault().Date;

                for (int i = 0; i < symbolList.Count; i++)
                {
                    List<DayTradeSummary> dataList = new List<DayTradeSummary>();

                    var tempa = from rows in dtDaySummaries
                                where rows.SymbolID == symbolList[i]
                                orderby rows.Date ascending
                                select rows;

                    foreach (var rowItem in tempa)
                    {
                        DayTradeSummary dayTrade = new DayTradeSummary();
                        dayTrade.SymbolID = rowItem.SymbolID;
                        dayTrade.Date = rowItem.Date;

                        dayTrade.Open = rowItem.Open;
                        dayTrade.High = rowItem.High;
                        dayTrade.Low = rowItem.Low;
                        dayTrade.Close = rowItem.Close;
                        dayTrade.Volume = rowItem.Volume;

                        dayTrade.AdjustmentClose = rowItem.AdjustmentClose;

                        dataList.Add(dayTrade);

                    }
                    stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(symbolList[i], dataList));
                }
            }



            //foreach (var item in symbolList)
            //{
            //    var tempRows = from rows in dtDaySummaries.AsEnumerable()
            //                   where rows.SymbolID == item
            //                   orderby rows.Date ascending
            //                   select rows;

            //    List<DayTradeSummary> dataList = new List<DayTradeSummary>();

            //    foreach (var rowItem in tempRows)
            //    {
            //        DayTradeSummary dayTrade = new DayTradeSummary();
            //        dayTrade.SymbolID = rowItem.SymbolID;
            //        dayTrade.Date = rowItem.Date;

            //        dayTrade.Open = rowItem.Open;
            //        dayTrade.High = rowItem.High;
            //        dayTrade.Low = rowItem.Low;
            //        dayTrade.Close = rowItem.Close;
            //        dayTrade.Volume = rowItem.Volume;

            //        dayTrade.AdjustmentClose = rowItem.AdjustmentClose;

            //        dataList.Add(dayTrade);

            //    }
            //    stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(item, dataList));

            //}



            return stockSymbolsHistory;
        }

        private void InitializeDataTables()
        {
            using (SqlConnection c = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                c.Open();
                using (SqlDataAdapter a = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.SymbolID='XXXER'", c))
                {
                    _dataTableHC = new DataTable();
                    a.SelectCommand.CommandTimeout = 0;
                    a.Fill(_dataTableHC);
                }
            }
        }

        private void PopulateSymbolLookUpTable()
        {
            symbolLookUpTable = new List<KeyValuePair<string, string>>();

            foreach (var item in Exchanges.List)
            //foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                String selector = stockExchange;
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                //var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();

                    SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID ASC", c);
                    sqlCommand.CommandTimeout = 0;


                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        String SymbolID = reader["SymbolID"].ToString();
                        symbolLookUpTable.Add(new KeyValuePair<string, string>(SymbolID, stockExchange));
                    }
                }

            
            }


            int hold = 8;
        }

        private String LookUpSymbolStockExchange(string sym)
        {
            var items = from kvp in symbolLookUpTable
                        where kvp.Key == sym
                        select kvp;

            return items.FirstOrDefault().Value;
        }
    }
}

