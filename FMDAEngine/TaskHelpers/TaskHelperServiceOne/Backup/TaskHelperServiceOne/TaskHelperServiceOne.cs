﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Net;
using CalculationEngine;

namespace TaskHelperServiceOne
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    public class TaskHelperServiceOne : ITaskHelperServiceOne
    {

        public TaskHelperServiceOne()
        {
            //List<String> listArray = new List<string>();
            ////listArray.AddRange(new List<String> {"BPI", "COP", "KMG"});
            //listArray.AddRange(new List<String> {"BPI", "COP"});

            //CalculationEngine.DatabaseService.DatabaseWcfServiceClient dbService = new CalculationEngine.DatabaseService.DatabaseWcfServiceClient();
            //dbService.Open();

            //List<CalculationEngine.DatabaseService.DayTradeSummary> tempList = dbService.GetDayTradeSummaries(listArray.ToArray(), new DateTime(2011, 05, 11), new DateTime(2012, 12, 26), "LSE").ToList();

            //int h = 8;
            //dbService.Close();


        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public void CloseHelper()
        {
            ThreadStart threadStart = new ThreadStart(CloseImmediate);
            Thread listenCommandThread = new System.Threading.Thread(threadStart);

            listenCommandThread.Start();        
        }

        private void CloseImmediate()
        {
            TimeSpan waitPeriod = TimeSpan.FromSeconds(1);
            System.Threading.Thread.Sleep(waitPeriod);

            Environment.Exit(0);
        }


        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public IList<CorrelationEntity> HistoricalCorrelationAnalysis(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles)
        {
            CorrelationProcessorHelper correlationProcesor = new CorrelationProcessorHelper();

            return correlationProcesor.ExecuteHistoricalCorrelationAnalysis(jobId, SymbolID, StockExchange, startDate, endDate, compatibles);
        }

        public void Process(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles, String jobType)
        {
            CalculationEngine.TicketProcessor tp = new CalculationEngine.TicketProcessor();

            tp.Process(jobId, SymbolID, StockExchange, startDate, endDate, compatibles, jobType);
        }

        public bool ModeCheck(int mode)
        {
            MODE modeItem = (MODE)mode;

            if (Database.Mode == modeItem)
            {
                return true;
            }
            return false;
        }

    }
}
