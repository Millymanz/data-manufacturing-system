﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Net;
using CalculationEngine;

namespace TaskHelperServiceOne
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class TaskHelperServiceOne : ITaskHelperServiceOne
    {
        public TaskHelperServiceOne()
        {
            Logger.Setup();
            LogHelper.Info("Log Folder Created!");
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public void CloseHelper()
        {
            ThreadStart threadStart = new ThreadStart(CloseImmediate);
            Thread listenCommandThread = new System.Threading.Thread(threadStart);

            listenCommandThread.Start();        
        }

        private void CloseImmediate()
        {
            TimeSpan waitPeriod = TimeSpan.FromSeconds(1);
            System.Threading.Thread.Sleep(waitPeriod);

            Environment.Exit(0);
        }


        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public IList<CorrelationEntity> HistoricalCorrelationAnalysis(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles, String timeFrame, String type)
        {
            CorrelationProcessorHelper correlationProcesor = new CorrelationProcessorHelper();

            return correlationProcesor.ExecuteHistoricalCorrelationAnalysis(jobId, SymbolID, StockExchange, startDate, endDate, compatibles, timeFrame, type);
        }

        public void Process(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles,  String timeFrame, String jobType)
        {
            CalculationEngine.TicketProcessor tp = new CalculationEngine.TicketProcessor();

            tp.Process(jobId, SymbolID, StockExchange, startDate, endDate, compatibles, timeFrame, jobType);
        }

        public bool ModeCheck(int mode)
        {
            MODE modeItem = (MODE)mode;

            if (Database.Mode == modeItem)
            {
                return true;
            }
            return false;
        }

    }
}
