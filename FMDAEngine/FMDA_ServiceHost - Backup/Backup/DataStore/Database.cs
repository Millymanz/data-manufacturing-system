﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;

using DataStore.DatabaseManagerService;
using DataStore.DatabaseService;

using System.Data.DataSetExtensions;
using System.Globalization;
using LogSys;

namespace DataStore
{
    static public class DateStamp
    {
        static public String DateID = "";
        static public String FMDA_EngineName = "";
        static public String FullName = "";
    }


    //Temp will be in its own file
    public class TradeDate
    {
        private DateTime date;
        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }
    }

    public enum MODE
    {
        Live = 0,
        Test = 1,
        LiveTest = 2
    }


    //temps

    public class DayTradeSummary
    {
        private DateTime date;
        private double open;
        private double high;
        private double low;
        private double close;
        private double adjustmentclose;
        private int volume;

        private String symbolID;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public double Open
        {
            get { return open; }
            set { open = value; }
        }

        public double High
        {
            get { return high; }
            set { high = value; }
        }

        public double Low
        {
            get { return low; }
            set { low = value; }
        }

        public double Close
        {
            get { return close; }
            set { close = value; }
        }

        public int Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public double AdjustmentClose
        {
            get { return adjustmentclose; }
            set { adjustmentclose = value; }
        }

        public String SymbolID
        {
            get { return symbolID; }
            set { symbolID = value; }
        }


    }

    //-----------------------------------------------------------//
    /// <summary>
    /// Class to store one CSV row
    /// </summary>
    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }

    /// <summary>
    /// Class to write data to a CSV file
    /// </summary>
    public class CsvFileWriter : StreamWriter
    {
        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename, true)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            String rowStr = "";
            try
            {
                StringBuilder builder = new StringBuilder();
                bool firstColumn = true;
                foreach (string value in row)
                {
                    // Add separator if this isn't the first value
                    if (!firstColumn)
                        builder.Append(',');
                    // Implement special handling for values that contain comma or quote
                    // Enclose in quotes and double up any double quotes
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                        builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                    else
                        builder.Append(value);
                    firstColumn = false;
                }
                
                row.LineText = builder.ToString();

                rowStr = row.LineText;

                WriteLine(row.LineText);
            }
            catch (Exception ex)
            {
                Console.WriteLine("WriteRow Execption - {0} - {1}" + rowStr + ex);
            }
        }
    }


    public class Database
    {
        private List<KeyValuePair<String, String>> symbolLookUpTable = null;

        DataTable _dataTableHC = null;

        public String _DirectoryOutput = "";

        static public MODE Mode;

        public Database()
        {
            String selectorOutput = "";

            switch (Mode)
            {
                case MODE.Test:
                    {
                        selectorOutput += "OUTPUTPATH_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selectorOutput += "OUTPUTPATH_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        selectorOutput += "OUTPUTPATH_LIVE-TEST";
                    }
                    break;
            }
            _DirectoryOutput = System.Configuration.ConfigurationManager.AppSettings[selectorOutput];
        }

        public String OutputControl(String calType)
        {
            _DirectoryOutput += DateStamp.DateID;
            if (Directory.Exists(_DirectoryOutput) == false)
            {
                Directory.CreateDirectory(_DirectoryOutput);
            }

            String compName = DateStamp.FMDA_EngineName.Replace("-", "");
            String name = calType + DateStamp.DateID + "_" + compName;

            DateStamp.FullName = name;

            String fileName = name + ".csv";
            if (!System.IO.File.Exists(fileName))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            }
            return _DirectoryOutput + "\\" + fileName;
        }

        public List<String> GetHCSymbolsTaskList()
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            List<DateTime> listLatestDayTradeDate = new List<DateTime>();


            //This code assumes that the index dbs are update with the latest day trade summaries
            //
            //
            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                DateTime latestDayTradeDate = new DateTime();
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT MAX(dbo.DayTradeSummaries.Date) AS MaxDate FROM dbo.DayTradeSummaries";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        latestDayTradeDate = Convert.ToDateTime(rdr["MaxDate"].ToString());
                    }
                }
                listLatestDayTradeDate.Add(latestDayTradeDate);
            }

            var ldTradeDate = (from dateItem in listLatestDayTradeDate select dateItem).Max();


            String dateCriteria = String.Format("{0}-{1}-{2}", ldTradeDate.Year, ldTradeDate.Month, ldTradeDate.Day);

            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {


                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate < '" + dateCriteria + "'";
                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;

                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    hcSymbolTaskList.Add(symbol);
                }
            }

            if (hcSymbolTaskList.Count == 0)//this condition is true for both uptodate and empty db table..?
            {
                foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
                {
                    String stockExchange = item.ToString();

                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                        SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                        sqlCommand.CommandTimeout = 0;

                        con.Open();

                        SqlDataReader rdr = sqlCommand.ExecuteReader();

                        while (rdr.Read())
                        {
                            String symbol = rdr["SymbolID"].ToString();

                            hcSymbolTaskList.Add(symbol);
                        }
                    }
                }
            }
            else
            {
                //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
                //exclude these symbols from the hcSymbolTaskList
                String commandTxt = "Select DISTINCT dbo.tblHistoricalCorrelations.SymbolID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.EndDate >= '" + dateCriteria + "'";

            }



            //if (hcSymbolTaskList.Count > 200)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 200);
            //}
            //else
            return hcSymbolTaskList;


        }

        public List<String> GetHistoricalCompatibleSymbolsList(string jobId)
        {
            //specify exchange
            List<String> hcSymbolTaskList = new List<String>();

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings["JobsManagerStaging"];

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                String commandTxt = "SELECT * FROM dbo.HistoricallyCompatibleStocks WHERE dbo.HistoricallyCompatibleStocks.JobId='" + jobId + "'";

                //String commandTxt = "SELECT DISTINCT TOP 20 dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;

                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String symbol = rdr["SymbolID"].ToString();

                    hcSymbolTaskList.Add(symbol.Trim());
                }
            }
            return hcSymbolTaskList;
        }

        public List<String> GetCompatibleListDataFromDB(String stockExchange, String symbolID)
        {
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings["JobsManagerStaging"];

            Dictionary<String, KeyValuePair<DateTime, List<String>>> symbolList = new Dictionary<String, KeyValuePair<DateTime, List<String>>>();

            List<String> compList = new List<string>();

            DateTime dateTemp = new DateTime();

            using (SqlConnection con = new SqlConnection(settings.ToString()))
            {
                //String commandTxt = "SELECT * FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "' ORDER BY dbo.HistoricallyCompatibleStocks.SymbolID ASC";
                String commandTxt = "SELECT dbo.HistoricallyCompatibleStocks.CompatibleSymbolID FROM dbo.HistoricallyCompatibleStocks WHERE HistoricallyCompatibleStocks.StockExchange ='" + stockExchange + "' AND HistoricallyCompatibleStocks.CriteriaSymbolID ='" + symbolID + "'";

                SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                sqlCommand.CommandTimeout = 0;

                con.Open();

                SqlDataReader rdr = sqlCommand.ExecuteReader();

                while (rdr.Read())
                {
                    String compatibleSymbolID = rdr["CompatibleSymbolID"].ToString();

                    /*String date = rdr["Date"].ToString();

                    dateTemp = DateTime.Parse(date);*/

                    compList.Add(compatibleSymbolID);
                    // symbolList.Add(symbol, new KeyValuePair<DateTime,List<string>>(dateItem, ));
                }
            }


            return compList;
        }

        public List<String> GetRCSymbolsTaskList()
        {
            //specify exchange
            List<String> rcSymbolTaskList = new List<String>();

            //This version is not for continuing from where the processing was last left of
            //each time this is run the application will start from scratch in order to populate the database
            //assumes range correlation table is empty

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID";
                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        String symbol = rdr["SymbolID"].ToString();

                        rcSymbolTaskList.Add(symbol.Trim());
                    }
                }
            }

            //using this approach means you dont need to use the check for enddate values which are less, this also  if (hcSymbolTaskList.Count == 0) problem
            //exclude these symbols from the hcSymbolTaskList

            //if (hcSymbolTaskList.Count > 500)//*temp debugging
            //{
            //    return hcSymbolTaskList.GetRange(0, 500);
            //}
            //else
            return rcSymbolTaskList;
        }


        public Dictionary<String, List<DateTime>> GetTradeDates(List<String> symbolList)
        {
            String criteria = "";
            bool bfirstItem = true;
            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "'";
                }
            }



            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();


            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        //TradeDate tradeDate = new TradeDate();

                        //tradeDate.SymbolID = rdr["SymbolID"].ToString();
                        //tradeDate.Date = Convert.ToDateTime(rdr["Date"].ToString());

                        String sym = rdr["SymbolID"].ToString();
                        DateTime dateItem = Convert.ToDateTime(rdr["Date"].ToString());

                        if (!tradeDateList.ContainsKey(sym))
                        {
                            List<DateTime> dateList = new List<DateTime>();
                            dateList.Add(dateItem);

                            tradeDateList[sym] = dateList;

                        }
                        else
                        {
                            tradeDateList[sym].Add(dateItem);
                        }
                    }
                }
            }

            return tradeDateList;
        }

        public Dictionary<String, List<DateTime>> GetTradeDatesRange(List<String> symbolList, DateTime startDate, DateTime endDate)
        {
            String criteria = "";
            bool bfirstItem = true;

            String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            foreach (var sym in symbolList)
            {
                if (bfirstItem) { criteria += "dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'"; bfirstItem = false; }
                else
                {
                    criteria += " OR dbo.DayTradeSummaries.SymbolID ='" + sym + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "'";
                }
            }

            Dictionary<String, List<DateTime>> tradeDateList = new Dictionary<string, List<DateTime>>();

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    String commandTxt = "SELECT dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date FROM dbo.DayTradeSummaries WHERE " + criteria + " ORDER BY dbo.DayTradeSummaries.SymbolID, dbo.DayTradeSummaries.Date ASC";

                    SqlCommand sqlCommand = new SqlCommand(commandTxt, con);
                    sqlCommand.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader rdr = sqlCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        //TradeDate tradeDate = new TradeDate();

                        //tradeDate.SymbolID = rdr["SymbolID"].ToString();
                        //tradeDate.Date = Convert.ToDateTime(rdr["Date"].ToString());

                        String sym = rdr["SymbolID"].ToString();
                        DateTime dateItem = Convert.ToDateTime(rdr["Date"].ToString());

                        if (!tradeDateList.ContainsKey(sym))
                        {
                            List<DateTime> dateList = new List<DateTime>();
                            dateList.Add(dateItem);

                            tradeDateList[sym] = dateList;

                        }
                        else
                        {
                            tradeDateList[sym].Add(dateItem);
                        }
                    }
                }
            }

            return tradeDateList;
        }

        public List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummaries(List<String> symbolList, DateTime startDate, DateTime endDate, String stockExchange)
        {

            List<KeyValuePair<String, List<DayTradeSummary>>> stockSymbolsHistory = new List<KeyValuePair<String, List<DayTradeSummary>>>();

            foreach (string symbol in symbolList)
            {
                List<DayTradeSummary> dataList = new List<DayTradeSummary>();

                List<Double> dataListTemp = new List<Double>();


                //String stockExchange = LookUpSymbolStockExchange(symbol);

                try
                {
                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange].ConnectionString))

                    //using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["AMEX"].ConnectionString))
                    {
                        conn.Open();

                        String stDate = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
                        String enDate = String.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

                        //SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);

                        SqlCommand sqlCommand = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID ='" + symbol + "' AND dbo.DayTradeSummaries.Date BETWEEN '" + stDate + "' AND '" + enDate + "' ORDER BY dbo.DayTradeSummaries.Date ASC", conn);
                        sqlCommand.CommandTimeout = 0;

                        SqlDataReader reader = sqlCommand.ExecuteReader();


                        while (reader.Read())
                        {
                            String SymbolID = reader["SymbolID"].ToString();

                            //Double data = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            DayTradeSummary dayTrade = new DayTradeSummary();
                            dayTrade.SymbolID = reader["SymbolID"].ToString();
                            dayTrade.Date = Convert.ToDateTime(reader["Date"].ToString());

                            dayTrade.Open = Convert.ToDouble(reader["Open"].ToString());
                            dayTrade.High = Convert.ToDouble(reader["High"].ToString());
                            dayTrade.Low = Convert.ToDouble(reader["Low"].ToString());
                            dayTrade.Close = Convert.ToDouble(reader["Close"].ToString());
                            dayTrade.Volume = Convert.ToInt32(reader["Volume"].ToString());


                            dayTrade.AdjustmentClose = Convert.ToDouble(reader["AdjustmentClose"].ToString());

                            dataList.Add(dayTrade);

                            //dataListTemp.Add(dayTrade.AdjustmentClose);

                        }


                        stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(symbol, dataList));

                        //masterSymbolDataList.Add(new KeyValuePair<string, List<double>>(symbol, dataListTemp));

                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Write("Msg " + e.ToString());
                }
            }
            return stockSymbolsHistory;
        }
     
        public List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummariesDbService(List<String> symbolList, DateTime startDate, DateTime endDate, String stockExchange)
        {
            List<KeyValuePair<String, List<DayTradeSummary>>> stockSymbolsHistory = new List<KeyValuePair<String, List<DayTradeSummary>>>();

            DatabaseService.DatabaseWcfServiceClient dbService = new DatabaseWcfServiceClient();
  
            List<DatabaseService.DayTradeSummary> dtDaySummaries = new List<DataStore.DatabaseService.DayTradeSummary>();

            TimeSpan span = endDate - startDate;

            //3000 days is limit estimate of 10years
            //Assumes data is available for all stock and each as 
            //3000 days available

            int daysLimit = 3000;

            TimeSpan spanTime = endDate - startDate;

            int remainingDays = 0;

            if (spanTime.Days > daysLimit)
            {
                remainingDays = spanTime.Days - daysLimit;
            }
            else
            {
                daysLimit = spanTime.Days;
            }


            try
            {
                for (int i = 0; i < symbolList.Count; i++)
                {
                    String[] listArray = new string[1];
                    listArray[0] = symbolList[i];

                    List<DatabaseService.DayTradeSummary> temp = dbService.GetDayTradeSummaries(listArray, startDate, (startDate.AddDays(daysLimit)), stockExchange).ToList();

                    if (temp.Count > 0)
                    {
                        dtDaySummaries.AddRange(temp);
                    }


                    if (remainingDays > 0)
                    {
                        List<DatabaseService.DayTradeSummary> tempList = dbService.GetDayTradeSummaries(listArray, (endDate.AddDays(-remainingDays)), endDate, stockExchange).ToList();

                        if (temp.Count > 0)
                        {
                            dtDaySummaries.AddRange(tempList);
                        }
                    }
                }

                dbService.Close();
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                ApplicationErrorLogger.Log(thread, level, application, "GetDayTradeSummariesDbService()", ex.ToString(), selectorX);
            }


            if (dtDaySummaries.Count > 0)
            {
                DateTime st = dtDaySummaries.FirstOrDefault().Date;
                DateTime en = dtDaySummaries.LastOrDefault().Date;


                for (int i = 0; i < symbolList.Count; i++)
                {
                    List<DayTradeSummary> dataList = new List<DayTradeSummary>();

                    var tempa = from rows in dtDaySummaries
                                where rows.SymbolID == symbolList[i]
                                orderby rows.Date ascending
                                select rows;

                    foreach (var rowItem in tempa)
                    {
                        DayTradeSummary dayTrade = new DayTradeSummary();
                        dayTrade.SymbolID = rowItem.SymbolID;
                        dayTrade.Date = rowItem.Date;

                        dayTrade.Open = rowItem.Open;
                        dayTrade.High = rowItem.High;
                        dayTrade.Low = rowItem.Low;
                        dayTrade.Close = rowItem.Close;
                        dayTrade.Volume = rowItem.Volume;

                        dayTrade.AdjustmentClose = rowItem.AdjustmentClose;

                        dataList.Add(dayTrade);

                    }
                    stockSymbolsHistory.Add(new KeyValuePair<String, List<DayTradeSummary>>(symbolList[i], dataList));
                }
            }
            return stockSymbolsHistory;
        }

        private void PopulateSymbolLookUpTable()
        {
            symbolLookUpTable = new List<KeyValuePair<string, string>>();

            foreach (var item in System.Configuration.ConfigurationManager.AppSettings)
            {
                String stockExchange = item.ToString();
                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[stockExchange];

                using (SqlConnection c = new SqlConnection(settings.ToString()))
                {
                    c.Open();

                    SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.DayTradeSummaries.SymbolID FROM dbo.DayTradeSummaries ORDER BY dbo.DayTradeSummaries.SymbolID ASC", c);
                    sqlCommand.CommandTimeout = 0;


                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        String SymbolID = reader["SymbolID"].ToString();
                        symbolLookUpTable.Add(new KeyValuePair<string, string>(SymbolID, stockExchange));
                    }
                }
            }
        }

        private String LookUpSymbolStockExchange(string sym)
        {
            var items = from kvp in symbolLookUpTable
                        where kvp.Key == sym
                        select kvp;

            return items.FirstOrDefault().Value;
        }

        public void SetHistoricalCorrelationsFile(List<CorrelationEntity> hcList, String symbolCriteria, String StockExchange)
        {
            var item = hcList.FirstOrDefault();
            String stockExchange = StockExchange; 
            String hc_id = stockExchange + "-" + symbolCriteria;

            String fileName = OutputControl("HC_");
            
            NumberStyles stylesCorrelationCoefficient = NumberStyles.AllowExponent | NumberStyles.Number;
            int decimalPlaces = 10;

            // Write sample data to CSV file
            using (CsvFileWriter writer = new CsvFileWriter(fileName))
            {
                foreach (var hcItem in hcList)
                {
                    String startDateStr = String.Format("{0}-{1}-{2}", hcItem.StartDate.Year, hcItem.StartDate.Month, hcItem.StartDate.Day);
                    String endDateStr = String.Format("{0}-{1}-{2}", hcItem.EndDate.Year, hcItem.EndDate.Month, hcItem.EndDate.Day);

                    CsvRow row = new CsvRow();
                    String dstExchange = stockExchange;

                    row.Add(hc_id);
                    row.Add(dstExchange + "-" + hcItem.CorrelatingSymbolID);

                    row.Add(stockExchange);
                    row.Add(dstExchange);

                    double roundedDistance = Math.Round(hcItem.Distance, decimalPlaces);
                    Decimal tempDistance = Decimal.Parse(roundedDistance.ToString(), System.Globalization.NumberStyles.Any);
                    double finalDistance = Convert.ToDouble(tempDistance);
                    row.Add(finalDistance.ToString());

                    double roundedCorrelationCo = Math.Round(hcItem.CorrelationCoefficient, decimalPlaces);
                    Decimal tempDec = Decimal.Parse(roundedCorrelationCo.ToString(), stylesCorrelationCoefficient);
                    double finalCorrelationCo = Convert.ToDouble(tempDec);
                    row.Add(finalCorrelationCo.ToString());

                    row.Add(startDateStr);
                    row.Add(endDateStr);
                    row.Add(symbolCriteria);

                    row.Add(CustomType.TradeEventToString(hcItem.Event));

                    writer.WriteRow(row);
                }
            }
        }

        public void SetHistoricalCorrelationsNew(List<CorrelationEntity> hcList, String symbolCriteria, String StockExchange)
        {
            var item = hcList.FirstOrDefault();
            String stockExchange = StockExchange; /*LookUpSymbolStockExchange(item.SymbolID);*/


            DataTable resultTable = new DataTable();
            resultTable = _dataTableHC.Clone();

            String hc_id = stockExchange + "-" + symbolCriteria;


            foreach (var hcItem in hcList)
            {
                String dstExchange = LookUpSymbolStockExchange(hcItem.SymbolID);

                DataRow row = resultTable.NewRow();
                row["CorrelatingEntityID"] = dstExchange + "-" + hcItem.CorrelatingSymbolID;
                row["SymbolID"] = symbolCriteria;
                row["StartDate"] = hcItem.StartDate;
                row["EndDate"] = hcItem.EndDate;
                row["Distance"] = hcItem.Distance;
                row["SourceStockExchange"] = stockExchange;
                row["DestinationStockExchange"] = dstExchange;

                row["CorrelationRatio"] = hcItem.CorrelationCoefficient;

                row["Event"] = CustomType.TradeEventToString(hcItem.Event);
                row["HC_ID"] = hc_id;

                resultTable.Rows.Add(row);
            }


            int noSuccessfulUpdates = 0;

            SqlDataAdapter dataAdapter = null;

            using (dataAdapter = new SqlDataAdapter())
            {
                SqlConnection conObj = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString);

                dataAdapter.InsertCommand = new SqlCommand("usp_UpdateHistoricalCorrelations", conObj);
                dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                dataAdapter.InsertCommand.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                dataAdapter.InsertCommand.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");    
                dataAdapter.InsertCommand.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                dataAdapter.InsertCommand.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                dataAdapter.InsertCommand.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                dataAdapter.InsertCommand.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                dataAdapter.InsertCommand.Parameters.Add("@StartDate", SqlDbType.DateTime, 18, "StartDate");
                dataAdapter.InsertCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 18, "EndDate");

                dataAdapter.InsertCommand.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                dataAdapter.InsertCommand.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");

                dataAdapter.InsertCommand.CommandTimeout = 0;

                noSuccessfulUpdates = dataAdapter.Update(resultTable);

            }

            if (noSuccessfulUpdates == 0)
            {
                SqlConnection conObj = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString);

                dataAdapter.InsertCommand = new SqlCommand("usp_InsertHistoricalCorrelations", conObj);
                dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                dataAdapter.Update(resultTable);
            }
        }

        public void SetHistoricalCorrelations(List<CorrelationEntity> hcList, String symbolCriteria, String StockExchange)
        {
            var item = hcList.FirstOrDefault();
            String stockExchange = StockExchange; /*LookUpSymbolStockExchange(item.SymbolID);*/

            DataTable dt = null;
            using (SqlConnection c = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                c.Open();
                using (SqlDataAdapter a = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.SymbolID='XXXER'", c))
                {
                    dt = new DataTable();
                    a.SelectCommand.CommandTimeout = 0;
                    a.Fill(dt);
                }
            }

            DataTable resultTable = new DataTable();
            resultTable = dt;

            String hc_id = stockExchange + "-" + symbolCriteria;


            foreach (var hcItem in hcList)
            {
                String dstExchange = LookUpSymbolStockExchange(hcItem.SymbolID);

                DataRow row = resultTable.NewRow();
                row["CorrelatingEntityID"] = dstExchange + "-" + hcItem.CorrelatingSymbolID;
                row["SymbolID"] = symbolCriteria;
                row["StartDate"] = hcItem.StartDate;
                row["EndDate"] = hcItem.EndDate;
                row["Distance"] = hcItem.Distance;
                row["SourceStockExchange"] = stockExchange;
                row["DestinationStockExchange"] = dstExchange;

                row["CorrelationRatio"] = hcItem.CorrelationCoefficient;

                row["Event"] = CustomType.TradeEventToString(hcItem.Event);
                row["HC_ID"] = hc_id;

                resultTable.Rows.Add(row);
            }



            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                conn.Open();


                SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.tblHistoricalCorrelations.HC_ID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.HC_ID='" + hc_id + "'", conn);
                sqlCommand.CommandTimeout = 0;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    //update
                    //Mass update
                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    {
                        SqlConnection conObj = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString);

                        dataAdapter.InsertCommand = new SqlCommand("usp_UpdateHistoricalCorrelations", conObj);
                        dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                        dataAdapter.InsertCommand.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                        dataAdapter.InsertCommand.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");
                        dataAdapter.InsertCommand.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                        dataAdapter.InsertCommand.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                        dataAdapter.InsertCommand.Parameters.Add("@StartDate", SqlDbType.DateTime, 18, "StartDate");
                        dataAdapter.InsertCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 18, "EndDate");

                        dataAdapter.InsertCommand.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                        dataAdapter.InsertCommand.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");

                        dataAdapter.InsertCommand.CommandTimeout = 0;

                        dataAdapter.Update(resultTable);

                    }

                }
                else
                {
                    //insert
                    /*SqlCommand sqlCommand = new SqlCommand("INSERT INTO dbo.tblHistoricalCorrelations (HC_ID, CorrelatingEntityID, SourceStockExchange, DestinationStockExchange, Distance, StartDate, EndDate,SymbolID, Event) 
                    VALUES ('', @Date, @Open, @High, @Low, @Close, @Volume, @AdjustmentClose)");*/

                    //using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    //{
                    //    SqlCommandBuilder cb = new SqlCommandBuilder(dataAdapter);
                    //    dataAdapter.InsertCommand = cb.GetInsertCommand();
                    //    dataAdapter.InsertCommand.CommandTimeout = 0;

                    //    dataAdapter.Update(resultTable);
                    //}


                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    {
                        SqlConnection conObj = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString);

                        dataAdapter.InsertCommand = new SqlCommand("usp_InsertHistoricalCorrelations", conObj);
                        dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                        dataAdapter.InsertCommand.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                        dataAdapter.InsertCommand.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");
                        dataAdapter.InsertCommand.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                        dataAdapter.InsertCommand.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                        dataAdapter.InsertCommand.Parameters.Add("@StartDate", SqlDbType.DateTime, 18, "StartDate");
                        dataAdapter.InsertCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 18, "EndDate");

                        dataAdapter.InsertCommand.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                        dataAdapter.InsertCommand.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");

                        dataAdapter.InsertCommand.CommandTimeout = 0;

                        dataAdapter.Update(resultTable);

                    }




                }
            }
        }

        public void SetHistoricalCorrelationsT(List<CorrelationEntity> hcList, String symbolCriteria)
        {
            var item = hcList.FirstOrDefault();
            String stockExchange = LookUpSymbolStockExchange(item.SymbolID);

            DataTable dt = null;
            using (SqlConnection c = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                c.Open();
                using (SqlDataAdapter a = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.SymbolID='XXXER'", c))
                {
                    dt = new DataTable();
                    a.Fill(dt);
                }
            }

            DataTable resultTable = new DataTable("tblHistoricalCorrelations");
            resultTable = dt;

            String hc_id = stockExchange + "-" + symbolCriteria;


            foreach (var hcItem in hcList)
            {
                String dstExchange = LookUpSymbolStockExchange(hcItem.SymbolID);

                DataRow row = resultTable.NewRow();
                row["CorrelatingEntityID"] = dstExchange + "-" + hcItem.CorrelatingSymbolID;
                row["SymbolID"] = symbolCriteria;
                row["StartDate"] = hcItem.StartDate;
                row["EndDate"] = hcItem.EndDate;
                row["Distance"] = hcItem.Distance;
                row["SourceStockExchange"] = stockExchange;
                row["DestinationStockExchange"] = dstExchange;
                row["CorrelationRatio"] = 0.0;

                row["Event"] = CustomType.TradeEventToString(hcItem.Event);
                row["HC_ID"] = hc_id;

                resultTable.Rows.Add(row);
            }



            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                conn.Open();


                SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.tblHistoricalCorrelations.HC_ID FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.HC_ID='" + hc_id + "'", conn);
                sqlCommand.CommandTimeout = 0;


                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    //update
                    //Mass update
                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    {

                        //dataAdapter.UpdateCommand = 

                        //SqlDataAdapter da = new SqlDataAdapter();
                        /*SqlCommand cmd;
                        
                        cmd = new SqlCommand("UPDATE dbo.tblHistoricalCorrelations SET dbo.tblHistoricalCorrelations.HC_ID = @HC_ID, dbo.tblHistoricalCorrelations.CorrelatingEntityID = @CorrelatingEntityID, " +
                                             "dbo.tblHistoricalCorrelations.SourceStockExchange = @SourceStockExchange, dbo.tblHistoricalCorrelations.DestinationStockExchange = @DestinationStockExchange, " +
                                             "dbo.tblHistoricalCorrelations.Distance = @Distance, dbo.tblHistoricalCorrelations.CorrelationRatio = @CorrelationRatio, " +
                                             "dbo.tblHistoricalCorrelations.StartDate = @StartDate, dbo.tblHistoricalCorrelations.EndDate = @EndDate, " +
                                             "dbo.tblHistoricalCorrelations.SymbolID = @SymbolID, dbo.tblHistoricalCorrelations.Event = @Event " +
                                             "WHERE dbo.tblHistoricalCorrelations.HC_ID = @HC_ID AND dbo.tblHistoricalCorrelations.CorrelatingEntityID = @CorrelatingEntityID", conn);
                          
                         */

                        SqlConnection conObj = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString);

                        dataAdapter.InsertCommand = new SqlCommand("usp_UpdateHistoricalCorrelations", conObj);
                        dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                        dataAdapter.InsertCommand.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                        dataAdapter.InsertCommand.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");
                        dataAdapter.InsertCommand.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                        dataAdapter.InsertCommand.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                        dataAdapter.InsertCommand.Parameters.Add("@StartDate", SqlDbType.DateTime, 18, "StartDate");
                        dataAdapter.InsertCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 18, "EndDate");

                        dataAdapter.InsertCommand.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                        dataAdapter.InsertCommand.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");





                        //dataAdapter.UpdateCommand = new SqlCommand("usp_UpdateHistoricalCorrelations", conn);
                        //dataAdapter.UpdateCommand.CommandType = CommandType.StoredProcedure;

                        //dataAdapter.UpdateCommand.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                        //dataAdapter.UpdateCommand.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");
                        //dataAdapter.UpdateCommand.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                        //dataAdapter.UpdateCommand.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                        //dataAdapter.UpdateCommand.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                        //dataAdapter.UpdateCommand.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                        //dataAdapter.UpdateCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                        //dataAdapter.UpdateCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);

                        //dataAdapter.UpdateCommand.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                        //dataAdapter.UpdateCommand.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");





                        //cmd.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                        //cmd.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");
                        //cmd.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                        //cmd.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                        //cmd.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                        //cmd.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                        //cmd.Parameters.Add("@StartDate", SqlDbType.DateTime);
                        //cmd.Parameters.Add("@EndDate", SqlDbType.DateTime);

                        //cmd.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                        //cmd.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");

                        //parm.SourceVersion = DataRowVersion.Original;

                        //dataAdapter.UpdateCommand = cmd;

                        //new SqlCommandBuilder(dataAdapter);

                        dataAdapter.Update(resultTable);

                    }

                }
                else
                {
                    //insert
                    /*SqlCommand sqlCommand = new SqlCommand("INSERT INTO dbo.tblHistoricalCorrelations (HC_ID, CorrelatingEntityID, SourceStockExchange, DestinationStockExchange, Distance, StartDate, EndDate,SymbolID, Event) 
                    VALUES ('', @Date, @Open, @High, @Low, @Close, @Volume, @AdjustmentClose)");*/

                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    {
                        new SqlCommandBuilder(dataAdapter);
                        dataAdapter.Update(resultTable);


                    }
                }
            }
        }

        public void SetHistoricalCorrelationsTest(List<CorrelationEntity> hcList, String symbolCriteria)
        {
            var item = hcList.FirstOrDefault();
            //String stockExchange = LookUpSymbolStockExchange(item.SymbolID);


            ////DataTable prepartion should be handled by the DatabaseManager and access via an operational contract
            ////
            ////But for now this should be left as is, current test is just to see if the databasemanager is doing what its supposed to be doing

            //DataTable dt = null;
            //using (SqlConnection c = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            //{
            //    c.Open();
            //    using (SqlDataAdapter a = new SqlDataAdapter("SELECT * FROM dbo.tblHistoricalCorrelations WHERE dbo.tblHistoricalCorrelations.SymbolID='XXXER'", c))
            //    {
            //        dt = new DataTable();
            //        a.Fill(dt);
            //    }
            //}

            //DataTable resultTable = new DataTable();
            //resultTable = dt;

            //resultTable.TableName = "tblHistoricalCorrelations";


            //String hc_id = stockExchange + "-" + symbolCriteria;


            //foreach (var hcItem in hcList)
            //{
            //    String dstExchange = LookUpSymbolStockExchange(hcItem.SymbolID);

            //    DataRow row = resultTable.NewRow();
            //    row["CorrelatingEntityID"] = dstExchange + "-" + hcItem.CorrelatingSymbolID;
            //    row["SymbolID"] = symbolCriteria;
            //    row["StartDate"] = hcItem.StartDate;
            //    row["EndDate"] = hcItem.EndDate;
            //    row["Distance"] = hcItem.Distance;
            //    row["SourceStockExchange"] = stockExchange;
            //    row["DestinationStockExchange"] = dstExchange;

            //    row["Event"] = CustomType.TradeEventToString(hcItem.Event);
            //    row["HC_ID"] = hc_id;

            //    resultTable.Rows.Add(row);
            //}

            ////Pass to databaseManager InsertMethod

            //DatabaseManagerService.DatabaseManagerServiceClient dbManger = new DatabaseManagerService.DatabaseManagerServiceClient();

            ////dbManger.InsertMethod(hc_id, resultTable);
            //DatabaseManagerService.ProcessedPackage processedPackage = new ProcessedPackage();
            //processedPackage.DT = resultTable;
            //processedPackage.SymbolKey = hc_id;


            //dbManger.InsertMethod(processedPackage);

        }


        public void SetRangeCorrelations(List<CorrelationEntity> rcList, String symbolCriteria)
        {
            var item = rcList.FirstOrDefault();
            String stockExchange = LookUpSymbolStockExchange(item.SymbolID);

            DataTable dt = null;
            using (SqlConnection c = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                c.Open();
                using (SqlDataAdapter a = new SqlDataAdapter("SELECT * FROM dbo.tblRangeCorrelations WHERE dbo.tblRangeCorrelations.RC_ID='XXXER'", c))
                {
                    dt = new DataTable();
                    a.Fill(dt);
                }
            }

            DataTable resultTable = new DataTable();
            resultTable = dt;

            DateTime startDate = item.StartDate;
            DateTime endDate = item.EndDate;

            String stDate = String.Format("{0}{1}{2}", startDate.Day, startDate.Month, startDate.Year);
            String enDate = String.Format("{0}{1}{2}", endDate.Day, endDate.Month, endDate.Year);


            String rc_id = stockExchange + "-" + symbolCriteria + "_" + stDate + "-" + enDate;

            foreach (var rcItem in rcList)
            {
                String dstExchange = LookUpSymbolStockExchange(rcItem.SymbolID);

                DataRow row = resultTable.NewRow();
                row["CorrelatingEntityID"] = dstExchange + "-" + rcItem.CorrelatingSymbolID + "_" + stDate + "-" + enDate;

                row["StartDate"] = rcItem.StartDate;
                row["EndDate"] = rcItem.EndDate;
                row["Distance"] = rcItem.Distance;
                row["SourceStockExchange"] = stockExchange;
                row["DestinationStockExchange"] = dstExchange;

                row["Event"] = CustomType.TradeEventToString(rcItem.Event);
                row["RC_ID"] = rc_id;

                resultTable.Rows.Add(row);
            }



            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
            {
                conn.Open();


                SqlCommand sqlCommand = new SqlCommand("SELECT DISTINCT dbo.tblRangeCorrelations.RC_ID FROM dbo.tblRangeCorrelations WHERE dbo.tblRangeCorrelations.RC_ID='" + rc_id + "'", conn);
                sqlCommand.CommandTimeout = 0;


                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    //update
                    //Mass update

                    /*using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblRangeCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    {
                        SqlConnection conObj = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString);

                        dataAdapter.InsertCommand = new SqlCommand("usp_UpdateHistoricalCorrelations", conObj);
                        dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;

                        dataAdapter.InsertCommand.Parameters.Add("@HC_ID", SqlDbType.NVarChar, 50, "HC_ID");

                        dataAdapter.InsertCommand.Parameters.Add("@CorrelatingEntityID", SqlDbType.NVarChar, 50, "CorrelatingEntityID");
                        dataAdapter.InsertCommand.Parameters.Add("@SourceStockExchange", SqlDbType.NChar, 10, "SourceStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@DestinationStockExchange", SqlDbType.NChar, 10, "DestinationStockExchange");
                        dataAdapter.InsertCommand.Parameters.Add("@Distance", SqlDbType.Decimal, 18, "Distance");
                        dataAdapter.InsertCommand.Parameters.Add("@CorrelationRatio", SqlDbType.Decimal, 18, "CorrelationRatio");

                        dataAdapter.InsertCommand.Parameters.Add("@StartDate", SqlDbType.DateTime, 18, "StartDate");
                        dataAdapter.InsertCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 18, "EndDate");

                        dataAdapter.InsertCommand.Parameters.Add("@SymbolID", SqlDbType.NChar, 10, "SymbolID");
                        dataAdapter.InsertCommand.Parameters.Add("@Event", SqlDbType.NChar, 10, "Event");

                        dataAdapter.Update(resultTable);

                    }*/

                }
                else
                {
                    //insert
                    /*SqlCommand sqlCommand = new SqlCommand("INSERT INTO dbo.tblHistoricalCorrelations (HC_ID, CorrelatingEntityID, SourceStockExchange, DestinationStockExchange, Distance, StartDate, EndDate,SymbolID, Event) 
                    VALUES ('', @Date, @Open, @High, @Low, @Close, @Volume, @AdjustmentClose)");*/

                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM dbo.tblRangeCorrelations", System.Configuration.ConfigurationManager.ConnectionStrings["FMDA_Result"].ConnectionString))
                    {
                        new SqlCommandBuilder(dataAdapter);
                        dataAdapter.Update(resultTable);


                    }
                }
            }
        }

    }
}

