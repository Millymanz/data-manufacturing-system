﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

namespace DataStore
{
    public enum NumberErrors 
    {
        NaN = 999999999
    }

    [DataContract]
    public class TemporaryResult
    {
        [DataMember]
        public Double distance = Double.MaxValue;

        [DataMember]
        public List<DateTime> dates = new List<DateTime>();

        [DataMember]
        public List<Double> values = new List<double>();
        //public String symbol = "";
    }


    public class CorrelationEntity
    {
        public String SymbolID;
        public String CorrelatingSymbolID;
        public DateTime StartDate;
        public DateTime EndDate;

        public Double Distance;
        public Double CorrelationCoefficient;

        public TradeEvent Event;
    }


    public enum TradeEvent
    {
        AdjustmentClose = 0,
        Close = 1,
        Open = 2,
        High = 3,
        Low = 4,
        Volume = 5
    }

    public class CustomType
    {
        public static String TradeEventToString(TradeEvent tradeEvent)
        {
            String type = "";
            switch (tradeEvent)
            {
                case TradeEvent.AdjustmentClose:
                    {
                        type = "AdjClose";
                    }
                    break;

                case TradeEvent.Close:
                    {
                        type = "Close";
                    }break;

                case TradeEvent.Open:
                    {
                        type = "Open";
                    }
                    break;

                case TradeEvent.High:
                    {
                        type = "High";
                    }
                    break;

                case TradeEvent.Low:
                    {
                        type = "Low";
                    }
                    break;


                case TradeEvent.Volume:
                    {
                        type = "Volume";
                    }
                    break;
            }
            return type;
        }
    }



}
