﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStore;
using System.Configuration;
using System.Net;
using LogSys;

namespace FMDA_Engine
{
    public enum TicketProtocol
    {
        JobId = 0,
        AllTimeTicketNo,
        TicketNo, 
        CalculationLevel, 
        TypeOfProcessing, 
        Symbol, 
        StockExchange, 
        TaskParameter 
    }

    public class JobsManagerClient
    {

        //Holde the method for translating the ticket protocol into the relevant method call from
        //the appropiate cclass

        private String _FMDA_EngineName = "";

        private KeyValuePair<String, List<String>> taskKVP = new KeyValuePair<string,List<string>>();

        public String FMDA_EngineName 
        {
            get { return _FMDA_EngineName; }
        }

        public DateTime _startTime = new DateTime();

        //must consume net tcp binding

        public JobsManagerClient(DateTime tm)
        {
           /* String directory = System.IO.Directory.GetCurrentDirectory();
            String [] directoryArray  = directory.Split('\\');

            _FMDA_EngineName = directoryArray[directoryArray.Count() - 3];*/
            _startTime = tm;

            //Console.Title = "FMDA :: " + _FMDA_EngineName;
        }

        public bool CheckJobsManagerConnection(bool bWriteFile)
        {
            try
            {
                bool matchingMode = false;
                using (JobsManagerService.JobsManagerServiceClient jbClient = new FMDA_Engine.JobsManagerService.JobsManagerServiceClient())
                {
                    jbClient.Open();

                    matchingMode = jbClient.ModeCheck((int)Database.Mode);
                    jbClient.Close();

                    String message = "";
 
                    if (matchingMode)
                    {
                        message = "Connection OK - JobsManagerService";
                    }
                    else
                    {
                        message = "Connection OK - JobsManagerService: Application MODES do not MATCH";
                    }

                    Console.WriteLine(message);
                    
                    if (bWriteFile)
                    {
                        String dir = System.Configuration.ConfigurationManager.AppSettings["COPYING_REPORT"];
                        String logFileName = dir + "\\" + Dns.GetHostName().ToUpper() + "_CopyingReport.txt";


                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileName, true))
                        {
                            file.WriteLine(message);
                        }
                    }

                    return matchingMode;
                }
            }
            catch (System.ServiceModel.CommunicationException commProblem)
            {
                Console.WriteLine("Connection FAILED - JobsManagerService");
            }
            return false;
        }

        public String GetJobTicket()
        {
            try
            {
               using (JobsManagerService.JobsManagerServiceClient jbClient = new FMDA_Engine.JobsManagerService.JobsManagerServiceClient())
               {
                   var temp = jbClient.GetTask(_FMDA_EngineName, (int)Database.Mode);

                   if (temp.Key != null)
                   {
                       taskKVP = new KeyValuePair<String, List<String>>(temp.Key, temp.Value.ToList());
                       return taskKVP.Key;           

                   }
               }
            }
            catch (Exception e)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }

                ApplicationErrorLogger.Log(thread, level, application, "GetJobTicket()", e.ToString(), selector);
            }
            return "";           
        }

        public void ProcessTicket(String ticket)
        {
            //[unique][AllTimeTicketNo][TicketNo][CalculationLevel][TypeOfProcessing][Symbol][StockExchange][TaskParameter]

            String[] decomposedTicket = ticket.Split(';');
            
            String[] dates = decomposedTicket[(int)TicketProtocol.TaskParameter].Split('@');
            
            DateTime startDate = DateTime.Parse(dates[0]);
            DateTime endDate = DateTime.Parse(dates[1]);


            switch (decomposedTicket[(int)TicketProtocol.TypeOfProcessing])
            {
                case "HC":
                    {
                        Console.WriteLine("[T- beginning HC]");

                        CorrelationProcessor correlationProcesor = new CorrelationProcessor();
                        //correlationProcesor.ExecuteHistoricalCorrelationAnalysis(decomposedTicket[(int)TicketProtocol.JobId], decomposedTicket[(int)TicketProtocol.Symbol], decomposedTicket[(int)TicketProtocol.StockExchange], startDate, endDate, taskKVP.Value);
                        correlationProcesor.HistoricalCorrelationAnalysisMapReducer(decomposedTicket[(int)TicketProtocol.JobId], decomposedTicket[(int)TicketProtocol.Symbol], decomposedTicket[(int)TicketProtocol.StockExchange], startDate, endDate, taskKVP.Value);
                        Console.WriteLine("[Ticket Completed]");
                        Console.WriteLine("");
                        Console.WriteLine("");

                    }
                    break;

                case "RC":
                    {

                    }
                    break;
            }

            Console.WriteLine("[Notify JobsManager]");

            try
            {
                using (JobsManagerService.JobsManagerServiceClient jbClient = new FMDA_Engine.JobsManagerService.JobsManagerServiceClient())
                {
                    jbClient.TaskCompleted(ticket, DateStamp.FullName, decomposedTicket[(int)TicketProtocol.TypeOfProcessing]);
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                ApplicationErrorLogger.Log(thread, level, application, "ProcessTicket()", ex.ToString(), selectorX);
            }
        }
    }

    public class TaskHelperClientManager
    {
        public bool CheckTaskHelperConnections(int i, bool bWritetoFile)
        {
            Object connObject = null;
            try
            {
                bool matchingMode = false;
                String message = "";

                switch (i)
                {
                    case 0:
                        {
                            TaskHelperServiceOne.TaskHelperServiceOneClient processObject = new TaskHelperServiceOne.TaskHelperServiceOneClient();
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceOne";
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceOne: Application MODES do not MATCH";
                            }
                        } break;
                    case 1:
                        {
                            TaskHelperServiceTwo.TaskHelperServiceTwoClient processObject = new TaskHelperServiceTwo.TaskHelperServiceTwoClient();
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceTwo";
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceTwo: Application MODES do not MATCH";
                            }

                        } break;

                    case 2:
                        {
                            TaskHelperServiceThree.TaskHelperServiceThreeClient processObject = new TaskHelperServiceThree.TaskHelperServiceThreeClient();
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            processObject.Close();

                            if (matchingMode)
                            {
                               message = "Connection OK - TaskHelperServiceThree";
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceThree: Application MODES do not MATCH";
                            }

                        } break;

                    case 3:
                        {
                            TaskHelperServiceFour.TaskHelperServiceFourClient processObject = new TaskHelperServiceFour.TaskHelperServiceFourClient();
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceFour";
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceFour: Application MODES do not MATCH";
                            }

                        } break;

                    case 4:
                        {
                            TaskHelperServiceFive.TaskHelperServiceFiveClient processObject = new TaskHelperServiceFive.TaskHelperServiceFiveClient();
                            connObject = processObject;

                            processObject.Open();
                            matchingMode = processObject.ModeCheck((int)Database.Mode);
                            processObject.Close();

                            if (matchingMode)
                            {
                                message = "Connection OK - TaskHelperServiceFive";
                            }
                            else
                            {
                                message = "Connection OK - TaskHelperServiceFive: Application MODES do not MATCH";
                            }

                        } break;
                }

                Console.WriteLine(message);

                if (bWritetoFile)
                {
                    String dir = System.Configuration.ConfigurationManager.AppSettings["COPYING_REPORT"];
                    String logFileName = dir + "\\" + Dns.GetHostName().ToUpper() + "_CopyingReport.txt";

                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileName, true))
                    {
                        file.WriteLine(message);
                    }
                }

                return matchingMode;

            }
            catch (System.ServiceModel.CommunicationException commProblem)
            {
                switch (i)
                {
                    case 0:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceOne");

                            TaskHelperServiceOne.TaskHelperServiceOneClient taskHelper = connObject as TaskHelperServiceOne.TaskHelperServiceOneClient;
                            taskHelper.Abort();

                        } break;
                    case 1:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceTwo");

                            TaskHelperServiceTwo.TaskHelperServiceTwoClient taskHelper = connObject as TaskHelperServiceTwo.TaskHelperServiceTwoClient;
                            taskHelper.Abort();


                        } break;

                    case 2:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceThree");

                            TaskHelperServiceThree.TaskHelperServiceThreeClient taskHelper = connObject as TaskHelperServiceThree.TaskHelperServiceThreeClient;
                            taskHelper.Abort();

                        } break;

                    case 3:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceFour");

                            TaskHelperServiceFour.TaskHelperServiceFourClient taskHelper = connObject as TaskHelperServiceFour.TaskHelperServiceFourClient;
                            taskHelper.Abort();

                        } break;

                    case 4:
                        {
                            Console.WriteLine("Connection FAILED - TaskHelperServiceFive");

                            TaskHelperServiceFive.TaskHelperServiceFiveClient taskHelper = connObject as TaskHelperServiceFive.TaskHelperServiceFiveClient;
                            taskHelper.Abort();
                        } break;
                }

            }
            return false;
        }
    }
}
