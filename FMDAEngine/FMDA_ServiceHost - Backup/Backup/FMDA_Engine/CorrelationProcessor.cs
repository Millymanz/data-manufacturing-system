﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStore;

using System.Threading;
using LogSys;

namespace FMDA_Engine
{
    public class TaskJob
    {
        public bool TaskCompleted = false;

        int ProcessType = -1;

        const int MaxArrayLimit = 8000;

        //--------------//
        String _JobId; 
        String _SymbolID;
        String _StockExchange;
        DateTime _StartDate;
        DateTime _EndDate;
        List<String> _Compatibles = new List<string>();
        int _Type = 0;
        //--------------//

        private List<CorrelationEntity> _CorrelationEntityList = new List<CorrelationEntity>();

        public List<CorrelationEntity> CorrelationEntityList
        {
            get { return _CorrelationEntityList; }
        }


        public TaskJob(String jobId, String symbolID, String stockExchange, DateTime startDate, DateTime endDate, List<String> compatibles, int type)
        {
            _JobId =  jobId;
            _SymbolID = symbolID;
            _StockExchange = stockExchange;
            _StartDate = startDate;
            _EndDate = endDate;
            _Compatibles = compatibles;
            _Type = type;
            ProcessType = _Type;

        }

        public void Process(Object processObject)
        {
            //expecting correlation entities as result
            // reserach returning unknown types to see if the data can be generic

            //make this class a base class
            try
            {

                switch (ProcessType)
                {
                    case 0:
                        {
                            TaskHelperServiceOne.TaskHelperServiceOneClient processOne = processObject as TaskHelperServiceOne.TaskHelperServiceOneClient;
                            if (processOne != null)
                            {
                                 List<TaskHelperServiceOne.CorrelationEntity> ceList = processOne.HistoricalCorrelationAnalysis(_JobId, _SymbolID, _StockExchange, _StartDate, _EndDate, _Compatibles.ToArray()).ToList();

                                 for (int i = 0; i < ceList.Count;  i++)
                                 {
                                     CorrelationEntity ce = new CorrelationEntity();

                                     ce.SymbolID = ceList[i].SymbolID;
                                     ce.CorrelatingSymbolID = ceList[i].CorrelatingSymbolID;
                                     ce.StartDate = ceList[i].StartDate;
                                     ce.EndDate = ceList[i].EndDate;
                                     ce.Distance = ceList[i].Distance;
                                     ce.CorrelationCoefficient = ceList[i].CorrelationCoefficient;
                                     ce.Event = (TradeEvent) ceList[i].Event;

                                     _CorrelationEntityList.Add(ce);
                                 }

                                 processOne.Close();
                            }

                        } break;
                    case 1:
                        {
                            TaskHelperServiceTwo.TaskHelperServiceTwoClient processTwo = processObject as TaskHelperServiceTwo.TaskHelperServiceTwoClient;
                            if (processTwo != null)
                            {
                                List<TaskHelperServiceTwo.CorrelationEntity> ceList = processTwo.HistoricalCorrelationAnalysis(_JobId, _SymbolID, _StockExchange, _StartDate, _EndDate, _Compatibles.ToArray()).ToList();

                                for (int i = 0; i < ceList.Count; i++)
                                {
                                    CorrelationEntity ce = new CorrelationEntity();

                                    ce.SymbolID = ceList[i].SymbolID;
                                    ce.CorrelatingSymbolID = ceList[i].CorrelatingSymbolID;
                                    ce.StartDate = ceList[i].StartDate;
                                    ce.EndDate = ceList[i].EndDate;
                                    ce.Distance = ceList[i].Distance;
                                    ce.CorrelationCoefficient = ceList[i].CorrelationCoefficient;
                                    ce.Event = (TradeEvent)ceList[i].Event;

                                    _CorrelationEntityList.Add(ce);
                                }
                                processTwo.Close();
                            }

                        } break;

                    case 2:
                        {
                            TaskHelperServiceThree.TaskHelperServiceThreeClient processThree = processObject as TaskHelperServiceThree.TaskHelperServiceThreeClient;
                            if (processThree != null)
                            {
                                List<TaskHelperServiceThree.CorrelationEntity> ceList = processThree.HistoricalCorrelationAnalysis(_JobId, _SymbolID, _StockExchange, _StartDate, _EndDate, _Compatibles.ToArray()).ToList();

                                for (int i = 0; i < ceList.Count; i++)
                                {
                                    CorrelationEntity ce = new CorrelationEntity();

                                    ce.SymbolID = ceList[i].SymbolID;
                                    ce.CorrelatingSymbolID = ceList[i].CorrelatingSymbolID;
                                    ce.StartDate = ceList[i].StartDate;
                                    ce.EndDate = ceList[i].EndDate;
                                    ce.Distance = ceList[i].Distance;
                                    ce.CorrelationCoefficient = ceList[i].CorrelationCoefficient;
                                    ce.Event = (TradeEvent)ceList[i].Event;

                                    _CorrelationEntityList.Add(ce);
                                }
                                processThree.Close();
                            }
                        } break;

                    case 3:
                        {
                            TaskHelperServiceFour.TaskHelperServiceFourClient processFour = processObject as TaskHelperServiceFour.TaskHelperServiceFourClient; 
                            if (processFour != null)
                            {
                                List<TaskHelperServiceFour.CorrelationEntity> ceList = processFour.HistoricalCorrelationAnalysis(_JobId, _SymbolID, _StockExchange, _StartDate, _EndDate, _Compatibles.ToArray()).ToList();

                                for (int i = 0; i < ceList.Count; i++)
                                {
                                    CorrelationEntity ce = new CorrelationEntity();

                                    ce.SymbolID = ceList[i].SymbolID;
                                    ce.CorrelatingSymbolID = ceList[i].CorrelatingSymbolID;
                                    ce.StartDate = ceList[i].StartDate;
                                    ce.EndDate = ceList[i].EndDate;
                                    ce.Distance = ceList[i].Distance;
                                    ce.CorrelationCoefficient = ceList[i].CorrelationCoefficient;
                                    ce.Event = (TradeEvent)ceList[i].Event;

                                    _CorrelationEntityList.Add(ce);
                                }
                                processFour.Close();
                            }

                        } break;

                    case 4:
                        {
                            TaskHelperServiceFive.TaskHelperServiceFiveClient processFive = processObject as TaskHelperServiceFive.TaskHelperServiceFiveClient; 
                            if (processFive != null)
                            {
                                List<TaskHelperServiceFive.CorrelationEntity> ceList = processFive.HistoricalCorrelationAnalysis(_JobId, _SymbolID, _StockExchange, _StartDate, _EndDate, _Compatibles.ToArray()).ToList();

                                for (int i = 0; i < ceList.Count; i++)
                                {
                                    CorrelationEntity ce = new CorrelationEntity();

                                    ce.SymbolID = ceList[i].SymbolID;
                                    ce.CorrelatingSymbolID = ceList[i].CorrelatingSymbolID;
                                    ce.StartDate = ceList[i].StartDate;
                                    ce.EndDate = ceList[i].EndDate;
                                    ce.Distance = ceList[i].Distance;
                                    ce.CorrelationCoefficient = ceList[i].CorrelationCoefficient;
                                    ce.Event = (TradeEvent)ceList[i].Event;

                                    _CorrelationEntityList.Add(ce);
                                }
                                processFive.Close();
                            }
                        } break;
                }
            }
            catch (Exception ex)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;
                String selectorX = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selectorX += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selectorX += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selectorX += "_LIVE-TEST";
                        }
                        break;
                }
                ApplicationErrorLogger.Log(thread, level, application, "Process()", ex.ToString(), selectorX);
            }
        }


        public void TaskJobSort()
        {
            Object processObject = null;

            switch (ProcessType)
            {
                case 0:
                    {
                        processObject = new TaskHelperServiceOne.TaskHelperServiceOneClient();

                    } break;
                case 1:
                    {
                        processObject = new TaskHelperServiceTwo.TaskHelperServiceTwoClient();

                    } break;

                case 2:
                    {
                        processObject = new TaskHelperServiceThree.TaskHelperServiceThreeClient();

                    } break;

                case 3:
                    {
                        processObject = new TaskHelperServiceFour.TaskHelperServiceFourClient(); ;

                    } break;

                case 4:
                    {
                        processObject = new TaskHelperServiceFive.TaskHelperServiceFiveClient();

                    } break;
            }

            Process(processObject);
        }
    }




    public class CorrelationProcessor
    {
        Database dataModel = new Database();

        private List<KeyValuePair<String, List<DayTradeSummary>>> GetDayTradeSummaries(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles)
        {
           //List<String> temp =  dataModel.GetCompatibleListDataFromDB(StockExchange, SymbolID);
           List<String> symbolsTaskList = new List<string>();
           symbolsTaskList.Add(SymbolID);

           symbolsTaskList.AddRange(compatibles);

           List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = dataModel.GetDayTradeSummariesDbService(symbolsTaskList, startDate, endDate, StockExchange);

            return dayTradeSummaries;
        }


        private List<KeyValuePair<String, List<DayTradeSummary>>> PrepareRangeCorrelationsAnalysis()
        {
            //this will get the task list and also workout what the date range 
            //for the tasks is or the date range will be passed in
            //all of this will be done on by the jobs manager engine

            //this implementation assumes that the db is empty and that work needs only to be carried out
            //for a date range consisting of the first 3 days of a stock's life
            List<String> symbolsTaskList = dataModel.GetRCSymbolsTaskList();

            //Dates will be known at this level

            DateTime startDate = new DateTime(2010, 7, 21);//temp
            DateTime endDate = new DateTime(2010, 7, 23);

            List<String> rcCompatiableList = RangeCorrelationCompatiabilityCheck(symbolsTaskList, startDate, endDate);

            //List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = dataModel.GetDayTradeSummaries(rcCompatiableList, startDate, endDate);

            List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = null;
            return dayTradeSummaries;
        }


        public void ExecuteHistoricalCorrelationAnalysisT()
        {
            List<CorrelationEntity> correaltionEntityList = new List<CorrelationEntity>();

            CorrelationEntity correlationEntity = new CorrelationEntity();

            Double distance = 0.52;

            correlationEntity.Event = TradeEvent.AdjustmentClose;
            correlationEntity.Distance = distance;

            correlationEntity.SymbolID = "Milly";
            correlationEntity.CorrelatingSymbolID = "Manz";
            correlationEntity.StartDate = new DateTime(2010, 2, 1);
            correlationEntity.EndDate = new DateTime(2011, 5, 5);
            
            correaltionEntityList.Add(correlationEntity);





            CorrelationEntity correlationEntityY = new CorrelationEntity();

            Double distanceY = 0.4475;

            correlationEntityY.Event = TradeEvent.AdjustmentClose;
            correlationEntityY.Distance = distanceY;

            correlationEntityY.SymbolID = "Milly";
            correlationEntityY.CorrelatingSymbolID = "Westend";
            correlationEntityY.StartDate = new DateTime(2010, 2, 1);
            correlationEntityY.EndDate = new DateTime(2011, 5, 5); ;

            correaltionEntityList.Add(correlationEntityY);

            //---------------------------------------------------//


            //dataModel.SetHistoricalCorrelationsT(correaltionEntityList, "Milly");

            dataModel.SetHistoricalCorrelationsTest(correaltionEntityList, "Milly");


        }

        public bool HistoricalCorrelationAnalysisMapReducer(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles)
        {
            //Prepare for HC analysis
            Console.WriteLine("Historical Correlation Map Reducer...");
            //List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = GetDayTradeSummaries(jobId, SymbolID, StockExchange, startDate, endDate, compatibles);
            //Not needed
            //--------------------------------------------------------------//
            var threadsListing = new List<Thread>();
            List<TaskJob> taskListings = new List<TaskJob>();


            int eventsPerSymbol = (int)TradeEvent.Volume + 1;

            int totalResultsExpected = eventsPerSymbol * compatibles.Count;

            Console.WriteLine("Total Records to be processed :: " + totalResultsExpected);



            int remainder = compatibles.Count % 4;//make dynamic

            int noSymbolsPerCycle = compatibles.Count / 4;

            int start = 0;

            for (int m = 0; m < 4; m++)
            {
                List<String> symbolDataList = new List<String>();

                //Memory management solution

                symbolDataList.AddRange(compatibles.GetRange(start, noSymbolsPerCycle));

                start = start + noSymbolsPerCycle;

                //------------------------------------------//
                TaskJob taskItem = new TaskJob(jobId, SymbolID, StockExchange, startDate, endDate, symbolDataList, m);

                Thread thread = new Thread(new ThreadStart(taskItem.TaskJobSort));

                //Thread thread = new Thread(new ParameterizedThreadStart()


                thread.Name = "Thread Task" + m.ToString();

                threadsListing.Add(thread);

                taskListings.Add(taskItem);

                thread.Start();

                //masterSymbolDataList.RemoveRange(start, noSymbolsPerCycle);

            }



            foreach (Thread t in threadsListing)
                t.Join();


            if (remainder > 0)
            {
                List<String> symbolDataList = new List<String>();

                symbolDataList.AddRange(compatibles.GetRange((compatibles.Count - remainder), remainder));


                TaskJob taskItem = new TaskJob(jobId, SymbolID, StockExchange, startDate, endDate, symbolDataList, 4);

                Thread thread = new Thread(new ThreadStart(taskItem.TaskJobSort));
                thread.Name = "Thread Task Last";

                threadsListing.Add(thread);
                taskListings.Add(taskItem);
                thread.Start();
                
                thread.Join();
            }






            List<CorrelationEntity> correlationEntityList = new List<CorrelationEntity>();

            for (int i = 0; i < taskListings.Count; i++)
            {
                correlationEntityList.AddRange(taskListings[i].CorrelationEntityList);
            }



            if (totalResultsExpected == correlationEntityList.Count)
            {

                Console.WriteLine("[Records Processed - Completed]");

                Console.WriteLine("Saving to File...");

                dataModel.SetHistoricalCorrelationsFile(correlationEntityList, SymbolID, StockExchange);

                Console.WriteLine("[Saving to File - Completed]");
                return true;
            }

            Console.WriteLine("[Incompleted]");

            return false;
        }


        public void ExecuteHistoricalCorrelationAnalysis(String jobId, String SymbolID, String StockExchange, DateTime startDate, DateTime endDate, List<String> compatibles)
        {
            //Prepare for HC analysis
            Console.WriteLine("Historical Correlation Analysis...");
            List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = GetDayTradeSummaries(jobId, SymbolID, StockExchange, startDate, endDate, compatibles);

            if (dayTradeSummaries != null)
            {
                if (dayTradeSummaries.Count > 0)
                {
                    int criteriaLength = dayTradeSummaries.FirstOrDefault().Value.Count;

                    int positionWithinSymbolList = -1;

                    //foreach (var symbolItem in dayTradeSummaries)
                    //{
                        ++positionWithinSymbolList;

                       // String currentSymbolCriteria = symbolItem.Key;

                        String currentSymbolCriteria = SymbolID;

                        List<CorrelationEntity> correaltionEntityList = new List<CorrelationEntity>();

                        for (int tradeEvent = (int)TradeEvent.AdjustmentClose; tradeEvent <= (int)TradeEvent.Volume; tradeEvent++)
                        {

                            try
                            {
                                List<Double> criteria = new List<double>();

                                if (positionWithinSymbolList >= 0)
                                {

                                    switch ((TradeEvent)tradeEvent)
                                    {
                                        case TradeEvent.AdjustmentClose:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    //criteria.Add(new KeyValuePair<Double, DateTime>(item.AdjustmentClose, item.Date));
                                                    criteria.Add(item.AdjustmentClose);
                                                }
                                            } break;

                                        case TradeEvent.Close:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Close);
                                                }
                                            } break;

                                        case TradeEvent.Open:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Open);
                                                }
                                            } break;

                                        case TradeEvent.High:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.High);
                                                }
                                            } break;


                                        case TradeEvent.Low:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Low);
                                                }
                                            } break;

                                        case TradeEvent.Volume:
                                            {
                                                foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                                {
                                                    criteria.Add(item.Volume);
                                                }
                                            } break;
                                    }

                                    //*********


                                    DateTime present = DateTime.Now;
                                    System.Diagnostics.Debug.WriteLine(String.Format("\n\n SearchEngineDD algorithm start:: {0} \n\n", present.ToString()));


                                    //int remainder = dayTradeSummaries.Count % 4;//make dynamic

                                    //int noSymbolsPerCycle = 0;
                                    //if (dayTradeSummaries.Count <= 4) noSymbolsPerCycle = dayTradeSummaries.Count;
                                    //else
                                    //    noSymbolsPerCycle = dayTradeSummaries.Count / 4;


                                    for (int symbols = 0; symbols < dayTradeSummaries.Count; symbols++)
                                    {

                                        try
                                        {

                                            String symbolTask = dayTradeSummaries[symbols].Key;

                                            if (positionWithinSymbolList != symbols && currentSymbolCriteria != symbolTask)
                                            {
                                                int noItemsPerJob = dayTradeSummaries[symbols].Value.Count;
                                                List<DayTradeSummary> dtsList = dayTradeSummaries[symbols].Value;

                                                List<KeyValuePair<Double, DateTime>> comparer = new List<KeyValuePair<Double, DateTime>>();


                                                switch ((TradeEvent)tradeEvent)
                                                {
                                                    case TradeEvent.AdjustmentClose:
                                                        {
                                                            foreach (var item in dtsList) //fix casting issue
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.AdjustmentClose, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.Close:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Close, item.Date));
                                                            }
                                                        } break;


                                                    case TradeEvent.Open:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Open, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.High:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.High, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.Low:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Low, item.Date));
                                                            }
                                                        } break;

                                                    case TradeEvent.Volume:
                                                        {
                                                            foreach (DayTradeSummary item in dtsList)
                                                            {
                                                                comparer.Add(new KeyValuePair<Double, DateTime>(item.Volume, item.Date));
                                                            }
                                                        } break;
                                                }

                                                //----------------------------------------------//


                                                CorrelationEntity correlationEntity = new CorrelationEntity();


                                                Double accumulatedValue = 0.0;

                                                Double xSum = 0.0;
                                                Double ySum = 0.0;
                                                Double xSquaredSum = 0.0;
                                                Double ySquaredSum = 0.0;
                                                Double xySum = 0.0;

                                                for (int coordinate = 0; coordinate < criteriaLength; coordinate++)
                                                {
                                                    Double value = comparer[coordinate].Key - criteria[coordinate];
                                                    Double squaredValue = value * value;

                                                    accumulatedValue = accumulatedValue + squaredValue;


                                                    //----PMCC----//
                                                    Double xSquared = criteria[coordinate] * criteria[coordinate];
                                                    Double ySquared = comparer[coordinate].Key * comparer[coordinate].Key;

                                                    Double xy = criteria[coordinate] * comparer[coordinate].Key;

                                                    xSum = xSum + criteria[coordinate];
                                                    ySum = ySum + comparer[coordinate].Key;

                                                    xSquaredSum = xSquaredSum + xSquared;
                                                    ySquaredSum = ySquaredSum + ySquared;
                                                    xySum = xySum + xy;

                                                    //---------------------------------------------------------//

                                                }
                                                Double distance = System.Math.Sqrt(accumulatedValue);

                                                //Correlation Ratio - Product moment correlation coefficient
                                                int totalNoDays = dtsList.Count;

                                                Double Sxx = xSquaredSum - ((xSum * xSum) / totalNoDays);
                                                Double Syy = ySquaredSum - ((ySum * ySum) / totalNoDays); ;
                                                Double Sxy = xySum - ((xSum * ySum) / totalNoDays);
                                                
                                                Double tempValue = System.Math.Sqrt(Sxx * Syy);

                                                Double correlationCoefficient = Convert.ToDouble(NumberErrors.NaN);

                                                if (tempValue > 0)
                                                {
                                                    correlationCoefficient = Sxy / tempValue;
                                                }

                                                //---------------------------------------------------------//


                                                correlationEntity.CorrelationCoefficient = correlationCoefficient;
                                                correlationEntity.Event = (TradeEvent)tradeEvent;
                                                correlationEntity.Distance = distance;

                                                correlationEntity.SymbolID = currentSymbolCriteria; 

                                                correlationEntity.CorrelatingSymbolID = symbolTask;
                                                correlationEntity.StartDate = startDate;
                                                correlationEntity.EndDate = endDate;

                                                //---------------------------------------------------//

                                                correaltionEntityList.Add(correlationEntity);
                                            }
                                        }

                                        catch (Exception e)
                                        {
                                            Console.WriteLine("inside loop - " + e.Message);
                                            Console.WriteLine(String.Format("indexpos: {0}, totaldays: {1}, SymbolID: {2}, currentSymbolCriteria: {3}", positionWithinSymbolList, dayTradeSummaries.Count, SymbolID, currentSymbolCriteria));
                                            Console.WriteLine("Press Enter To terminate.");
                                            Console.ReadLine();
                                        }
                                    }
                                }

                                else
                                {
                                    Console.WriteLine("Error -:: " + positionWithinSymbolList);
                                    int io = 3;
                                }

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("insode loop - " + e.Message);
                                Console.WriteLine("Press Enter To terminate.");
                                Console.ReadLine();
                            }
                        }

                        //dataModel.SetHistoricalCorrelations(correaltionEntityList, currentSymbolCriteria);
                        Console.WriteLine("[Completed]");

                        Console.WriteLine("Saving to Database...");
                        dataModel.SetHistoricalCorrelationsFile(correaltionEntityList, currentSymbolCriteria, StockExchange);
                        Console.WriteLine("[Completed]");


                    //}
                }
            }
        }

        private List<String> HistoricalCorrelationCompatiabilityCheck(List<String> symbolsTaskList, out DateTime startDate, out DateTime endDate)
        {
            List<String> hcCompatiableList = new List<string>();

            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDates(symbolsTaskList);

            String symTask = symbolsTaskList.FirstOrDefault();

            hcCompatiableList.Add(tradeDateList.FirstOrDefault().Key);

            startDate = tradeDateList.FirstOrDefault().Value.FirstOrDefault();
            endDate = tradeDateList.FirstOrDefault().Value.Last();

            foreach (KeyValuePair<string, List<DateTime>> item in tradeDateList)
            {
                if (symTask != item.Key)
                {
                    //if (tradeDateList[symTask].Count <= item.Value.Count)
                    //{
                    //    bool bMatch = true;
                    //    for (int i = 0; i < tradeDateList[symTask].Count; i++)
                    //    {
                    //        //if (tradeDateList[symTask].FirstOrDefault() >= item.Value[i])
                    //        if (item.Value[i] >= tradeDateList[symTask].FirstOrDefault())
                    //        {
                    //            if (tradeDateList[symTask][i] != item.Value[i])
                    //            {
                    //                bMatch = false;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    if (bMatch)
                    //    {
                    //        hcCompatiableList.Add(item.Key);
                    //    }
                    //}


                    //temp
                    //if (tradeDateList[symTask].Count <= item.Value.Count)
                    //{
                    //    bool bMatch = true;

                    //    int p = 0;
                    //    while (true)
                    //    {
                    //        if (p < item.Value.Count)
                    //        {
                    //            if (item.Value[p] >= tradeDateList[symTask].FirstOrDefault())
                    //            {
                    //                for (int i = 0; i < tradeDateList[symTask].Count; i++)
                    //                {
                    //                    if (tradeDateList[symTask][i] != item.Value[p + i])
                    //                    {
                    //                        bMatch = false;
                    //                        break;
                    //                    }
                    //                }
                    //                if (bMatch)
                    //                {
                    //                    hcCompatiableList.Add(item.Key);
                    //                }
                    //                else
                    //                {
                    //                    break;
                    //                }
                    //            }
                    //            p++;
                    //        }
                    //    }
                    //}


                    if (tradeDateList[symTask].Count <= item.Value.Count)
                    {
                        bool bMatch = true;

                        for (int p = 0; p < item.Value.Count; p++)
                        {

                            if (item.Value[p] >= tradeDateList[symTask].FirstOrDefault())
                            {
                                for (int i = 0; i < tradeDateList[symTask].Count; i++)
                                {
                                    int index = p + i;

                                    if (index < item.Value.Count)
                                    {
                                        if (tradeDateList[symTask][i] != item.Value[index])
                                        {
                                            bMatch = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        bMatch = false;
                                        break;
                                    }
                                }
                                if (bMatch)
                                {
                                    hcCompatiableList.Add(item.Key);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

            }

            return hcCompatiableList;
        }

        public void ExecuteRangeCorrelationAnalysis()
        {
            //Prepare for range correlation analysis

            //this will be changed to work on a task by task basis
            List<KeyValuePair<String, List<DayTradeSummary>>> dayTradeSummaries = PrepareRangeCorrelationsAnalysis();

            if (dayTradeSummaries != null)
            {
                if (dayTradeSummaries.Count > 0)
                {
                    int criteriaLength = dayTradeSummaries.FirstOrDefault().Value.Count;

                    DateTime startDate = dayTradeSummaries.FirstOrDefault().Value.FirstOrDefault().Date;

                    DateTime endDate = dayTradeSummaries.FirstOrDefault().Value.Last().Date;

                    int positionWithinSymbolList = -1;
                    //-------------------------------------------------------------------//

                    foreach (var symbolItem in dayTradeSummaries)
                    {
                        ++positionWithinSymbolList;

                        String currentSymbolCriteria = symbolItem.Key;

                        List<CorrelationEntity> correaltionEntityList = new List<CorrelationEntity>();


                        for (int tradeEvent = (int)TradeEvent.AdjustmentClose; tradeEvent <= (int)TradeEvent.Volume; tradeEvent++)
                        {

                            try
                            {
                                List<Double> criteria = new List<double>();

                                switch ((TradeEvent)tradeEvent)
                                {
                                    case TradeEvent.AdjustmentClose:
                                        {
                                            foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                            {
                                                //criteria.Add(new KeyValuePair<Double, DateTime>(item.AdjustmentClose, item.Date));
                                                criteria.Add(item.AdjustmentClose);
                                            }
                                        } break;

                                    case TradeEvent.Close:
                                        {
                                            foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                            {
                                                criteria.Add(item.Close);
                                            }
                                        } break;

                                    case TradeEvent.Open:
                                        {
                                            foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                            {
                                                criteria.Add(item.Open);
                                            }
                                        } break;

                                    case TradeEvent.High:
                                        {
                                            foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                            {
                                                criteria.Add(item.High);
                                            }
                                        } break;


                                    case TradeEvent.Low:
                                        {
                                            foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                            {
                                                criteria.Add(item.Low);
                                            }
                                        } break;

                                    case TradeEvent.Volume:
                                        {
                                            foreach (DayTradeSummary item in dayTradeSummaries[positionWithinSymbolList].Value)
                                            {
                                                criteria.Add(item.Volume);
                                            }
                                        } break;
                                }

                                //*********


                                DateTime present = DateTime.Now;
                                System.Diagnostics.Debug.WriteLine(String.Format("\n\n SearchEngineDD algorithm start:: {0} \n\n", present.ToString()));


                                //int remainder = dayTradeSummaries.Count % 4;//make dynamic

                                //int noSymbolsPerCycle = 0;
                                //if (dayTradeSummaries.Count <= 4) noSymbolsPerCycle = dayTradeSummaries.Count;
                                //else
                                //    noSymbolsPerCycle = dayTradeSummaries.Count / 4;


                                for (int symbols = 0; symbols < dayTradeSummaries.Count; symbols++)
                                {

                                    try
                                    {

                                        String symbolTask = dayTradeSummaries[symbols].Key;

                                        if (positionWithinSymbolList != symbols && currentSymbolCriteria != symbolTask)
                                        {
                                            int noItemsPerJob = dayTradeSummaries[symbols].Value.Count;
                                            List<DayTradeSummary> dtsList = dayTradeSummaries[symbols].Value;

                                            List<KeyValuePair<Double, DateTime>> comparer = new List<KeyValuePair<Double, DateTime>>();


                                            switch ((TradeEvent)tradeEvent)
                                            {
                                                case TradeEvent.AdjustmentClose:
                                                    {
                                                        foreach (var item in dtsList) //fix casting issue
                                                        {
                                                            comparer.Add(new KeyValuePair<Double, DateTime>(item.AdjustmentClose, item.Date));
                                                        }
                                                    } break;

                                                case TradeEvent.Close:
                                                    {
                                                        foreach (DayTradeSummary item in dtsList)
                                                        {
                                                            comparer.Add(new KeyValuePair<Double, DateTime>(item.Close, item.Date));
                                                        }
                                                    } break;


                                                case TradeEvent.Open:
                                                    {
                                                        foreach (DayTradeSummary item in dtsList)
                                                        {
                                                            comparer.Add(new KeyValuePair<Double, DateTime>(item.Open, item.Date));
                                                        }
                                                    } break;

                                                case TradeEvent.High:
                                                    {
                                                        foreach (DayTradeSummary item in dtsList)
                                                        {
                                                            comparer.Add(new KeyValuePair<Double, DateTime>(item.High, item.Date));
                                                        }
                                                    } break;

                                                case TradeEvent.Low:
                                                    {
                                                        foreach (DayTradeSummary item in dtsList)
                                                        {
                                                            comparer.Add(new KeyValuePair<Double, DateTime>(item.Low, item.Date));
                                                        }
                                                    } break;

                                                case TradeEvent.Volume:
                                                    {
                                                        foreach (DayTradeSummary item in dtsList)
                                                        {
                                                            comparer.Add(new KeyValuePair<Double, DateTime>(item.Volume, item.Date));
                                                        }
                                                    } break;
                                            }

                                            //----------------------------------------------//


                                            CorrelationEntity correlationEntity = new CorrelationEntity();


                                            Double accumulatedValue = 0.0;
                                            for (int coordinate = 0; coordinate < criteriaLength; coordinate++)
                                            {
                                                Double value = comparer[coordinate].Key - criteria[coordinate];
                                                Double squaredValue = value * value;

                                                accumulatedValue = accumulatedValue + squaredValue;
                                            }
                                            Double distance = System.Math.Sqrt(accumulatedValue);

                                            correlationEntity.Event = (TradeEvent)tradeEvent;
                                            correlationEntity.Distance = distance;

                                            correlationEntity.SymbolID = dayTradeSummaries[symbols].Key;
                                            correlationEntity.CorrelatingSymbolID = symbolTask;
                                            correlationEntity.StartDate = startDate;
                                            correlationEntity.EndDate = endDate;

                                            //---------------------------------------------------//

                                            correaltionEntityList.Add(correlationEntity);
                                        }
                                    }




                                    catch (Exception e)
                                    {
                                        Console.WriteLine("insode loop - " + e.Message);
                                        Console.WriteLine("Press Enter To terminate.");
                                        Console.ReadLine();
                                    }
                                }
                            }

                            catch (Exception e)
                            {
                                Console.WriteLine("insode loop - " + e.Message);
                                Console.WriteLine("Press Enter To terminate.");
                                Console.ReadLine();
                            }
                        }

                        dataModel.SetRangeCorrelations(correaltionEntityList, currentSymbolCriteria);
                    }
                }

            }
        }

        private List<String> RangeCorrelationCompatiabilityCheck(List<String> symbolsTaskList, DateTime startDate, DateTime endDate)
        {
            List<String> rcCompatiableList = new List<string>();

            Dictionary<String, List<DateTime>> tradeDateList = dataModel.GetTradeDatesRange(symbolsTaskList, startDate, endDate);

            String symTask = symbolsTaskList.FirstOrDefault();

            rcCompatiableList.Add(tradeDateList.FirstOrDefault().Key);

            startDate = tradeDateList.FirstOrDefault().Value.FirstOrDefault();
            endDate = tradeDateList.FirstOrDefault().Value.Last();
            //-------------------------------------------------------------//


            foreach (KeyValuePair<string, List<DateTime>> item in tradeDateList)
            {
                if (symTask != item.Key)
                {
                    if (tradeDateList[symTask].Count <= item.Value.Count)
                    {
                        bool bMatch = true;

                        for (int p = 0; p < item.Value.Count; p++)
                        {

                            if (item.Value[p] >= tradeDateList[symTask].FirstOrDefault())
                            {
                                for (int i = 0; i < tradeDateList[symTask].Count; i++)
                                {
                                    int index = p + i;

                                    if (index < item.Value.Count)
                                    {
                                        if (tradeDateList[symTask][i] != item.Value[index])
                                        {
                                            bMatch = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        bMatch = false;
                                        break;
                                    }
                                }
                                if (bMatch)
                                {
                                    rcCompatiableList.Add(item.Key);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return rcCompatiableList;
        }

    }
}
