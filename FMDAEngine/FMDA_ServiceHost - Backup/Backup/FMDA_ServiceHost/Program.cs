﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Description;
//temp
using FMDA_Engine;
using DataStore;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.Threading;
using System.Net;
using LogSys;

namespace FMDA_ServiceHost
{
    static public class RunApp
    {
        static public void Run(bool bLive)
        {
            ThreadStart threadStart = new ThreadStart(CommandListenner.ListenForCommands);
            Thread listenCommandThread = new System.Threading.Thread(threadStart);

            listenCommandThread.Start();


            String name = Dns.GetHostName().ToUpper();
            Console.Title = "FMDA :: " + name;
            DateStamp.FMDA_EngineName = name;

            String modeType = bLive ? "LIVE" : "TEST";

            if (bLive)
            {
                modeType = "LIVE";
            }
            else
            {
                var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];
                modeType = modeStr;

                if (modeStr == "LIVE") modeType = "TEST";
            }

            if (modeType == "UI")
            {
                //Configuration Display

                DisplaySettings();

                Console.WriteLine("Select Runtime Mode - (1)TEST Or (2)LIVE-TEST");
                String readAns = Console.ReadLine().ToLower();

                var selection = (MODE)Convert.ToInt32(readAns);
                Database.Mode = (MODE)selection;

                if ((MODE)selection == MODE.Live) Database.Mode = MODE.Test;               

                ModeTitle();


                Console.WriteLine("");
                Console.WriteLine("Perform Connection Checks? Y or N");
                String answer = Console.ReadLine().ToLower();

                bool bProceed = true;


                if (answer == "y")
                {
                    bProceed = ConnectionTest(false);
                }


                if (bProceed)
                {
                    //--------------------------------------------------------------//
                    Console.WriteLine("Press any key To Start Processing");
                    Console.ReadLine();

                    RunTicketProcessing();
                }
            }
            else
            {
                if (modeType == "LIVE")
                {
                    Database.Mode = (MODE)0;
                }
                else if (modeType == "TEST")
                {
                    Database.Mode = (MODE)1;
                }
                else if (modeType == "LIVE-TEST")
                {
                    Database.Mode = (MODE)2;
                }

                ModeTitle();

                DisplaySettings();
                System.Threading.Thread.Sleep(25000);

                ConnectionTest(true);

                RunTicketProcessing();

                Console.ReadLine();
            }
        }

        static public void ModeTitle()
        {
            switch (Database.Mode)
            {
                case MODE.Test:
                    {
                        Console.Title += " :: TEST-MODE";
                    }
                    break;

                case MODE.Live:
                    {
                        Console.Title += " :: LIVE-MODE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        Console.Title += " :: LIVE-TEST-MODE";
                    }
                    break;
            }
        }

        static public void DisplaySettings()
        {

            Console.WriteLine("Current Configuration Settings");
            Console.WriteLine("");

            ClientSection clientSection =
               ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;

            ChannelEndpointElementCollection endpointCollection =
                clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;
            List<string> endpointNames = new List<string>();
            foreach (ChannelEndpointElement endpointElement in endpointCollection)
            {
                Console.WriteLine(endpointElement.Address);
            }

        }

        static public bool ConnectionTest(bool bWriteFile)
        {
            bool[] connectionsStates = new bool[] { false, false, false, false, false, false };

            FMDA_Engine.TaskHelperClientManager taskHelperCM = new FMDA_Engine.TaskHelperClientManager();

            for (int i = 0; i < 5; i++)
            {
                connectionsStates[i] = taskHelperCM.CheckTaskHelperConnections(i, bWriteFile);
                Console.WriteLine("");
            }
            FMDA_Engine.JobsManagerClient jbC = new FMDA_Engine.JobsManagerClient(DateTime.Now);

            connectionsStates[5] = jbC.CheckJobsManagerConnection(bWriteFile);
            Console.WriteLine("");

            if (connectionsStates.Contains(false))
            {
                Console.WriteLine("Please check configuration file and the connecting applications and restart this application");
                Console.WriteLine("");
                Console.WriteLine("Press Enter To terminate.");
                Console.ReadLine();

                return false;
            }
            return true;

        }

        static public void RunTicketProcessing()
        {
            String tempTicket = "";
            try
            {
                FMDA_Engine.JobsManagerClient jobsManager = new FMDA_Engine.JobsManagerClient(DateTime.Now);
                Console.Title += jobsManager.FMDA_EngineName;


                int counter = 0;
                while (true)
                {
                    tempTicket = "";
                    if (Console.Title != "")
                    {
                        tempTicket = jobsManager.GetJobTicket();
                        String[] ticketArray = tempTicket.Split('*');
                        String ticket = ticketArray[0];

                        Console.WriteLine("Ticket No: {0} \n", ticket);

                        if (ticket == "")
                        {
                            counter++;

                            if (counter == 5)
                            {
                                Console.WriteLine("No Tasks Sleep for awhile \n");
                                counter = 0; //reset as ticket is not empty

                                if (Database.Mode == MODE.Test)
                                {
                                    System.Threading.Thread.Sleep(10000);
                                }
                                else
                                {
                                    System.Threading.Thread.Sleep(300000);//Sleep for long period //5mins
                                }
                            }
                        }
                        else
                        {
                            counter = 0; //reset as ticket is not empty
                            //Process ticket

                            Console.WriteLine("[ProcessTicket]");

                            DateStamp.DateID = ticketArray[1];
                            jobsManager.ProcessTicket(ticket);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                String thread = System.Threading.Thread.CurrentThread.ToString();
                String level = "Error";
                String application = Console.Title;

                String selector = "JobsManager";

                switch (Database.Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            selector += "_LIVE-TEST";
                        }
                        break;
                }
                ApplicationErrorLogger.Log(thread, level, application, "RunTicketProcessing() :: " + tempTicket, e.ToString(), selector);
            }
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            RunApp.Run(false);
        }


    }
}
