USE [JobsManager]
GO
/****** Object:  StoredProcedure [dbo].[proc_CheckAllJobCompletions]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_CheckAllJobCompletions]
@DayStartDatetime datetime,
@DayEndDatetime datetime
AS

BEGIN
	/*SELECT [JobsManager].[dbo].[Jobs].JobStartTime, [JobsManager].[dbo].[Jobs].JobEndTime, [JobsManager].[dbo].[Jobs].Completed FROM [JobsManager].[dbo].[Jobs] 
	WHERE [JobsManager].[dbo].[Jobs].JobStartTime BETWEEN @DayStartDatetime AND @DayEndDatetime AND [JobsManager].[dbo].[Jobs].Completed = 0
	*/
	SELECT COUNT(*) AS Total FROM [JobsManager].[dbo].[Jobs] 
	WHERE [JobsManager].[dbo].[Jobs].JobStartTime BETWEEN @DayStartDatetime AND @DayEndDatetime AND [JobsManager].[dbo].[Jobs].Completed = 0
END






GO
/****** Object:  StoredProcedure [dbo].[proc_CheckRawDataFileRegisteration]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_CheckRawDataFileRegisteration]
@FileName nvarchar(20),
@Count int output
AS


SELECT @Count = COUNT(*) FROM [dbo].[FileImports] WHERE [dbo].[FileImports].[FileName] = @FileName

GO
/****** Object:  StoredProcedure [dbo].[proc_DataConsumptionCompletionUpdate]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[proc_DataConsumptionCompletionUpdate]
@FileName nvarchar(20)
AS

DECLARE @ID nvarchar(100)

SELECT @ID = [ID] FROM dbo.FileImports D WHERE D.FileName = @FileName

UPDATE dbo.DataConsumer SET dbo.DataConsumer.DownloadCompleted = '1' WHERE dbo.DataConsumer.FileImportID = @ID




GO
/****** Object:  StoredProcedure [dbo].[proc_DeleteAllCompatiblityCheckers_Admin]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[proc_DeleteAllCompatiblityCheckers_Admin]
AS

DELETE FROM [dbo].HistoricallyCompatibleStocks
DELETE FROM [dbo].NonCompatibleStocks



GO
/****** Object:  StoredProcedure [dbo].[proc_DeleteAllFileImportLogs_Admin]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[proc_DeleteAllFileImportLogs_Admin]
AS

DELETE FROM [dbo].[FileImports]
DELETE FROM [dbo].[ImportLog]
DELETE FROM [dbo].[JobsWithinFile]


GO
/****** Object:  StoredProcedure [dbo].[proc_DeleteAllJobs_Admin]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DeleteAllJobs_Admin]
AS

DELETE FROM [dbo].[Jobs]


GO
/****** Object:  StoredProcedure [dbo].[proc_GetImportReadyFiles]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[proc_GetImportReadyFiles]
AS
SELECT dbo.[FileImports].[FileName] FROM dbo.[FileImports] 
JOIN dbo.DataConsumer ON dbo.[FileImports].ID = dbo.DataConsumer.FileImportID
--JOIN [dbo].[ImportLog] ON [dbo].[ImportLog].FileImportID = [dbo].[FileImports].ID
WHERE dbo.[FileImports].ImportCompleted ='0' AND dbo.[FileImports].DataType ='Raw'

--Oriignal hold
--SELECT dbo.[FileImports].[FileName] FROM dbo.[FileImports] 
--JOIN dbo.DataConsumer ON dbo.[FileImports].ID = dbo.DataConsumer.FileImportID
--WHERE dbo.[FileImports].ImportCompleted ='0' AND dbo.[FileImports].DataType ='Raw' AND
--dbo.DataConsumer.[DownloadCompleted] = '1' 



--SELECT dbo.[FileImports].[FileName] FROM dbo.[FileImports] 
--JOIN dbo.DataConsumer ON dbo.[FileImports].ID = dbo.DataConsumer.FileImportID
----JOIN [dbo].[ImportLog] ON [dbo].[ImportLog].FileImportID = [dbo].[FileImports].ID
--WHERE dbo.[FileImports].ImportCompleted ='0' AND dbo.[FileImports].DataType ='Raw'


GO
/****** Object:  StoredProcedure [dbo].[proc_ImportLogging]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ImportLogging]
@FileName nvarchar(100),
@ErrorMsg nvarchar(MAX),
@CSVRecordCount int,
@ImportedRecordCount int,
@PercentageSuccess float,
@PercentageFailure float,
@ImportStartTime datetime,
@ImportEndTime datetime,
@SuccessThreshold float
AS

DECLARE @ID nvarchar(100)
DECLARE @FileImportID nvarchar(100)

SET @ID = NEWID()

/* We dont want to update as it is a log database we want to see a history of activity */

SELECT @FileImportID = [JobsManager].[dbo].FileImports.ID FROM [JobsManager].[dbo].FileImports 
WHERE [JobsManager].[dbo].FileImports.[FileName] = @FileName

INSERT INTO [JobsManager].[dbo].ImportLog (ID, FileImportID, ErrorMsg, CSVRecordCount, ImportedRecordCount, PercentageSuccess, PercentageFailure, ImportStartTime, ImportEndTime)
VALUES (@ID, @FileImportID, @ErrorMsg, @CSVRecordCount, @ImportedRecordCount, @PercentageSuccess, @PercentageFailure, @ImportStartTime, @ImportEndTime)

IF @PercentageSuccess > @SuccessThreshold
	BEGIN
		UPDATE [JobsManager].[dbo].FileImports SET [JobsManager].[dbo].FileImports.ImportCompleted = 1
		WHERE [JobsManager].[dbo].FileImports.ID = @FileImportID
	END
ELSE
	BEGIN
		UPDATE [JobsManager].[dbo].FileImports SET [JobsManager].[dbo].FileImports.ImportCompleted = 0
		WHERE [JobsManager].[dbo].FileImports.ID = @FileImportID
	END

GO
/****** Object:  StoredProcedure [dbo].[proc_InsertJobTickets]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[proc_InsertJobTickets]
@JobTicket nvarchar(MAX),
@JobId nvarchar(200),
@SymbolID nvarchar(20),
@ProcessType nvarchar(20),
@Exchange nvarchar(20),
@CompatibilityStartDate date,
@EndDate date,
@JobTicketCreationDate date
AS

INSERT INTO [dbo].Jobs (ID, JobTicket, JobId, SymbolID, ProcessType, StockExchange, CompatibilityStartDate, EndDate, JobTicketCreationDate)
VALUES (NEWID(), @JobTicket, @JobId, @SymbolID, @ProcessType, @Exchange, @CompatibilityStartDate, @EndDate, @JobTicketCreationDate)



GO
/****** Object:  StoredProcedure [dbo].[proc_InsertNonCompatibleStocksAndDates]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[proc_InsertNonCompatibleStocksAndDates]
@ID nvarchar(100),
@CriteriaSymbolID nvarchar(20),
@StockExchange nchar(20),
@NonMatchingSymbolID nvarchar(20),
@NonMatchingStockExchange nchar(20),
@RefID nvarchar(100),
@Date date

AS

BEGIN
	INSERT INTO dbo.NonCompatibleStocks_Diagnostics (ID, CriteriaSymbolID, StockExchange, NonMatchingSymbolID, NonMatchingStockExchange) 
	VALUES(@ID, @CriteriaSymbolID, @StockExchange, @NonMatchingSymbolID, @NonMatchingStockExchange)

	INSERT INTO dbo.NonCompatibleDates_Diagnostics (RefID, [Date]) VALUES(@RefID, @Date)
END


GO
/****** Object:  StoredProcedure [dbo].[proc_JobTicketCompleted]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_JobTicketCompleted]
@JobTicket nvarchar(MAX),
@EndTime datetime,
@FileNameOutput nvarchar(100),
@CalculationType nvarchar(20)
AS


UPDATE [dbo].Jobs SET [dbo].Jobs.JobEndTime = @EndTime, [dbo].Jobs.Completed = 1, 
[dbo].Jobs.TotalDuration = datediff(s,[dbo].Jobs.JobStartTime, @EndTime) 
WHERE [dbo].Jobs.JobTicket = @JobTicket

/*SELECT ([dbo].Jobs.JobEndTime - [dbo].Jobs.JobStartTime)
*/

DECLARE @FileNameID nvarchar(MAX)
DECLARE @ID nvarchar(100)


IF NOT EXISTS (SELECT 1 FROM [dbo].FileImports WHERE [dbo].FileImports.[FileName] = @FileNameOutput)
	BEGIN
		SET @FileNameID = NEWID()
		INSERT INTO [dbo].FileImports (ID, [FileName], [DataType], [CalculationType])
		VALUES (@FileNameID, @FileNameOutput, 'Computed', @CalculationType)

		SELECT @ID = [dbo].Jobs.ID FROM [dbo].Jobs WHERE [dbo].Jobs.JobTicket = @JobTicket 

		INSERT INTO [dbo].JobsWithinFile ([FileNameID], JobTicketID) VALUES (@FileNameID, @ID)
	END

ELSE
	BEGIN
		SELECT @FileNameID = [dbo].FileImports.[FileName] FROM [dbo].FileImports WHERE [dbo].FileImports.[FileName] = @FileNameOutput 

		SELECT @ID = [dbo].Jobs.ID FROM [dbo].Jobs WHERE [dbo].Jobs.JobTicket = @JobTicket 

		INSERT INTO [dbo].JobsWithinFile ([FileNameID], JobTicketID)
		VALUES (@FileNameID, @ID)
	END	




GO
/****** Object:  StoredProcedure [dbo].[proc_LogError]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LogError]
@Thread varchar(255),
@Level varchar(50),
@Application varchar(255),
@Msg varchar(4000),
@Exception varchar(2000),
@MachineName varchar(50)
AS

BEGIN
	INSERT INTO dbo.ApplicationErrorLogs ([Date], Thread, [Level], Logger, [Message], Exception, MachineName)
	VALUES (GETDATE(), @Thread, @Level, @Application, @Msg, @Exception, @MachineName)
END 

GO
/****** Object:  StoredProcedure [dbo].[proc_RegisterProcessingEngine]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[proc_RegisterProcessingEngine]
@Ticket nvarchar(MAX),
@MachineProcessedOn nvarchar(20),
@FMDA_EngineName nvarchar(20),
@JobStartTime datetime

AS

UPDATE [dbo].Jobs SET [dbo].Jobs.MachineProcessedOn = @MachineProcessedOn, 
[dbo].Jobs.FMDA_EngineName = @FMDA_EngineName,
[dbo].Jobs.JobStartTime = @JobStartTime
WHERE [dbo].Jobs.JobTicket = @Ticket



GO
/****** Object:  StoredProcedure [dbo].[proc_RegisterRawDataFile]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_RegisterRawDataFile]
@FileName nvarchar(100)
AS

DECLARE @ID nvarchar(100)
DECLARE @DCPrimaryID nvarchar(100)

SET @ID = NewID()
SET @DCPrimaryID = NewID()

INSERT INTO [dbo].FileImports (ID, [FileName], ImportCompleted, DataType, [CalculationType])
VALUES (@ID, @FileName, '0', 'Raw', 'None')

INSERT INTO [dbo].DataConsumer (ID, [FileImportID], [DownloadCompleted])
VALUES (@DCPrimaryID, @ID,'0')



GO
/****** Object:  StoredProcedure [dbo].[proc_ViewComputedFileImports]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[proc_ViewComputedFileImports]
@ImportStartTime date,
@ImportEndTime date
AS
SELECT [dbo].[FileImports].[ID]
      ,[dbo].[ImportLog].[FileImportID]
      ,[ErrorMsg]
      ,[CSVRecordCount]
      ,[ImportedRecordCount]
      ,[PercentageSuccess]
      ,[PercentageFailure]
      ,[ImportStartTime]
      ,[ImportEndTime]
	  ,[FileName]
      ,[ImportCompleted]
      ,[DataType]
	  ,[CalculationType]
  FROM [dbo].[ImportLog]
  JOIN [dbo].[FileImports] ON [dbo].[FileImports].ID = [dbo].[ImportLog].FileImportID
  WHERE [dbo].[ImportLog].ImportStartTime BETWEEN  @ImportStartTime AND @ImportEndTime
  AND [dbo].[FileImports].[DataType] = 'Computed'
  ORDER BY [JobsManager].[dbo].[ImportLog].ImportStartTime DESC


  --WHERE [dbo].[ImportLog].ImportStartTime > @ImportStartTime 




GO
/****** Object:  StoredProcedure [dbo].[proc_ViewFileImports]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[proc_ViewFileImports]
@ImportStartTime date,
@ImportEndTime date
AS
SELECT [dbo].[FileImports].[ID]
      ,[dbo].[ImportLog].[FileImportID]
      ,[ErrorMsg]
      ,[CSVRecordCount]
      ,[ImportedRecordCount]
      ,[PercentageSuccess]
      ,[PercentageFailure]
      ,[ImportStartTime]
      ,[ImportEndTime]
	  ,[FileName]
      ,[ImportCompleted]
      ,[DataType]
	  ,[DownloadCompleted]
  FROM [dbo].[ImportLog]
  JOIN [dbo].[FileImports] ON [dbo].[FileImports].ID = [dbo].[ImportLog].FileImportID
  JOIN [dbo].[DataConsumer] ON [dbo].[FileImports].ID = [dbo].[DataConsumer].FileImportID
  WHERE [dbo].[ImportLog].ImportStartTime BETWEEN  @ImportStartTime AND @ImportEndTime
  ORDER BY [JobsManager].[dbo].[ImportLog].ImportStartTime DESC


  --WHERE [dbo].[ImportLog].ImportStartTime > @ImportStartTime 


GO
/****** Object:  Table [dbo].[ApplicationErrorLogs]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicationErrorLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](2000) NULL,
	[MachineName] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DataConsumer]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataConsumer](
	[ID] [nvarchar](100) NOT NULL,
	[FileImportID] [nvarchar](100) NOT NULL,
	[DownloadCompleted] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileImports]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileImports](
	[ID] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](100) NULL,
	[ImportCompleted] [bit] NOT NULL,
	[DataType] [nvarchar](20) NULL,
	[CalculationType] [nvarchar](20) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HistoricallyCompatibleStocks]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoricallyCompatibleStocks](
	[ID] [nvarchar](100) NOT NULL,
	[CriteriaSymbolID] [nvarchar](20) NULL,
	[StockExchange] [nchar](20) NULL,
	[CompatibleSymbolID] [nvarchar](20) NULL,
	[CompatibleStockExchange] [nvarchar](20) NULL,
	[Date] [date] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ImportLog]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportLog](
	[ID] [nvarchar](100) NOT NULL,
	[FileImportID] [nvarchar](100) NOT NULL,
	[ErrorMsg] [nvarchar](max) NULL,
	[CSVRecordCount] [int] NULL,
	[ImportedRecordCount] [int] NULL,
	[PercentageSuccess] [float] NULL,
	[PercentageFailure] [float] NULL,
	[ImportStartTime] [datetime] NULL,
	[ImportEndTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jobs](
	[ID] [nvarchar](100) NOT NULL,
	[JobTicket] [nvarchar](max) NULL,
	[JobId] [nvarchar](200) NULL,
	[SymbolID] [nvarchar](20) NULL,
	[StockExchange] [nvarchar](20) NULL,
	[CompatibilityStartDate] [date] NULL,
	[EndDate] [date] NULL,
	[JobStartTime] [datetime] NULL,
	[JobEndTime] [datetime] NULL,
	[TotalDuration] [decimal](18, 4) NULL,
	[JobTicketCreationDate] [date] NULL,
	[MachineProcessedOn] [nvarchar](20) NULL,
	[FMDA_EngineName] [nvarchar](20) NULL,
	[ProcessType] [nvarchar](20) NULL,
	[Completed] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[JobsWithinFile]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobsWithinFile](
	[FileNameID] [nvarchar](100) NULL,
	[JobTicketID] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NonCompatibleDates_Diagnostics]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NonCompatibleDates_Diagnostics](
	[RefID] [nvarchar](100) NOT NULL,
	[Date] [date] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NonCompatibleStocks]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NonCompatibleStocks](
	[ID] [nvarchar](100) NOT NULL,
	[SymbolID] [nvarchar](20) NULL,
	[StockExchange] [nchar](20) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NonCompatibleStocks_Diagnostics]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NonCompatibleStocks_Diagnostics](
	[ID] [nvarchar](100) NOT NULL,
	[CriteriaSymbolID] [nvarchar](20) NULL,
	[StockExchange] [nchar](20) NULL,
	[NonMatchingSymbolID] [nvarchar](20) NULL,
	[NonMatchingStockExchange] [nchar](20) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notify]    Script Date: 11/02/2015 20:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notify](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Exchange] [nvarchar](50) NULL,
	[Notify] [bit] NOT NULL,
 CONSTRAINT [PK_Notify] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[FileImports] ADD  CONSTRAINT [DF_FileImports_ImportCompleted]  DEFAULT ((0)) FOR [ImportCompleted]
GO
ALTER TABLE [dbo].[Jobs] ADD  CONSTRAINT [DF_Jobs_Completed]  DEFAULT ((0)) FOR [Completed]
GO
