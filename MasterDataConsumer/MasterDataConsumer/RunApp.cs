﻿using System;
using System.Linq;
using ConsumerLibrary;

namespace MasterDataConsumer
{
    static public class RunApp
    {
        static DataConsumer _dataConsumerRun = null;

        static public void FreeResources()
        {
            if (_dataConsumerRun != null)
            {
                Library.WriteErrorLog(DateTime.Now + " :: FreeResources");
                _dataConsumerRun.Dispose();
                Library.WriteErrorLog(DateTime.Now + " :: APP CLOSED");

            }
        }

        static public void RunListenner()
        {
            var performance = System.Configuration.ConfigurationManager.AppSettings["PERFORMANCETEST"];
            if (performance != "NOOUTPUT")
            {
                var runasScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCRAPPER"].ToString());
                if (runasScrapper == false)
                {
                    CommandListenner cmdList = new CommandListenner();
                    cmdList.ListenForCommands();
                }
            }
        }

        static public void Run(bool bLive)
        {
            ConsumerLibrary.Logger.Setup("MasterDataConsumer.exe.config");

            //var startTh = new System.Threading.ThreadStart(RunListenner);
            //var thread = new System.Threading.Thread(startTh);
            //thread.Start();

            String modeType = bLive ? "LIVE" : "TEST";

            if (bLive)
            {
                modeType = "LIVE";
            }
            else
            {
                var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];
                modeType = modeStr;

                if (modeStr == "LIVE") modeType = "TEST";
            }

            Console.Title = "MasterDataConsumer";

            if (modeType == "UI")
            {
                _dataConsumerRun = new DataConsumer(false);
                
                Console.WriteLine("Select Runtime Mode - (1)TEST Or (2)LIVE-TEST");
                String readAns = Console.ReadLine().ToLower();

                MODE modeItem;
                var selection = (MODE)Convert.ToInt32(readAns);
                modeItem = (MODE)selection;
                if ((MODE)selection == MODE.Live) modeItem = MODE.Test;

                switch (modeItem)
                {
                    case MODE.Test:
                    {
                        DataConsumer.Mode = MODE.Test;
                        Console.Title += " :: TEST-MODE";
                    }
                        break;

                    case MODE.Live:
                    {
                        DataConsumer.Mode = MODE.Live;
                        Console.Title += " :: LIVE-MODE";
                    }
                        break;

                    case MODE.LiveTest:
                    {
                        DataConsumer.Mode = MODE.LiveTest;
                        Console.Title += " :: LIVE-TEST-MODE";
                    }
                        break;
                }



                Console.WriteLine("");
                Console.WriteLine("Perform Connection Checks? Y or N");

                bool[] connectionsStates = new bool[] { false };
                bool bProceed = true;

                String res = Console.ReadLine().ToLower();

                if (res == "y")
                {
                    connectionsStates[0] = DataConsumer.CheckDatabaseManagerConnection();
                    Console.WriteLine("");


                    if (connectionsStates.Contains(false))
                    {
                        bProceed = false;
                        Console.WriteLine("Please check configuration file and the connecting applications and restart this application");
                        Console.WriteLine("");
                        Console.WriteLine("Press Enter To terminate.");
                        Console.ReadLine();
                    }
                }


                if (bProceed)
                {
                    Console.WriteLine("Do you want to run the first time download? (Y)Yes (N)No \n");
                    String answer = Console.ReadLine().ToLower(); 

                    Console.WriteLine("\n");

                    if (answer == "y")
                    {
                        Console.WriteLine("Run Download For Price Action Data(1) Or Fundamental Data(2)? \n");
                        var input = Console.ReadLine().ToLower(); 

                        Console.WriteLine("\n");

                        if (input == "1")
                        {
                            _dataConsumerRun.ConsumeHistoricalDataCSVQueueBased();
                        }
                        else if (input == "2")
                        {
                            var scrap = new Scraper();                         
                            //scrap.Scrap();
                            scrap.ManualPageByPageScrap();
                        }
                    }
                    if (answer == "n")
                    {
                        Console.WriteLine("Do you want to run the Data Filler Manually? (Y)Yes (N)No \n");
                        var answerTemp = Console.ReadLine().ToLower();

                        if (answerTemp == "y")
                        {
                            Console.WriteLine("Run from scratch? (Y)Yes (N)No \n");
                            var answerTempScratch = Console.ReadLine().ToLower();

                            DataConsumer.SkipGenerateArtificalDateTimes
                                = answerTempScratch == "n" ? true : false;

                            _dataConsumerRun.DataFiller();
                        }
                        else
                        {
                            Console.WriteLine("Do you want to run the Generate Weekly and Monthly Data Manually? (Y)Yes (N)No \n");
                            var answerTempRes = Console.ReadLine().ToLower();
                            if (answerTempRes == "y")
                            {
                                _dataConsumerRun.GenerateWeeklyAndMonthly();
                            }

                        }
                        Console.WriteLine("\n");
                    }
                }

                //Dataconsumption triggered by the scheduler after initial first time download
            }
            else if (modeType == "LIVE")
            {
                DataConsumer.Mode = MODE.Live;
                Console.Title += " :: LIVE-MODE";
            }
            else
            {
                DataConsumer.Mode = MODE.Test;
                Console.Title += " :: TEST-MODE";
            }

            //DataConsumerScheduling dataConsumerSch = new DataConsumerScheduling();
            //dataConsumerSch.StartScheduling();


            var runasScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCRAPPER"].ToString());
            var runasScheduleScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCHEDULED_SCRAPPER"].ToString());

            if (runasScrapper)
            {
                Console.Title += " :: SCRAPPER";
                //These changes added for running with Windows Schedule Tasks
                var scrapRun = new Scraper();
                scrapRun.ManualPageByPageScrap();
            }
            else if (runasScheduleScrapper)
            {
                var scrapperSch = new ScrapperScheduler();
                scrapperSch.StartScheduling();
            }
            else
            {
                _dataConsumerRun = new DataConsumer(false);
                _dataConsumerRun.ConsumeHistoricalDataCSVQueueBased();
            }



            //read appconfig and  generate threads for each excahnge

            //Console.WriteLine("Master Data Consumer Running...\n");
            //Console.ReadLine();
            Console.ReadLine();

        }
    }
}