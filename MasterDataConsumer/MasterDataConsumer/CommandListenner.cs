﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Timers;
using ConsumerLibrary;

namespace MasterDataConsumer
{
    public class CommandListenner
    {
        static bool _allowErrorCall = true;
        static Timer _timer;


        private String _machine = "";
        private ServerType _serverType = null;

        public CommandListenner()
        {
            _timer = new Timer(120000); //2min
            _timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
            _timer.Enabled = true; // Enable it
        }

        static public void RunFaultHandlingInstance()
        {
            Console.WriteLine("Master Data Consumer Running Fault Handled..\n");

            System.Threading.Thread.Sleep(120000); //wait 2 minutes

            DataConsumer dataConsumer = new DataConsumer(false);
            dataConsumer.DataFiller();

            dataConsumer.ConsumeHistoricalDataCSVQueueBased();


            DataConsumerScheduling dataConsumerSch = new DataConsumerScheduling();
            dataConsumerSch.StartScheduling();
        }


        public void ListenForCommands()
        {
            // PopulateMachinePairing();

            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            UdpClient udpClient = new UdpClient(19074);//make listenning port dynamic //10198

            while (true)
            {
                udpClient.EnableBroadcast = true;
                String errorKey = "";
                try
                {
                    //IPEndPoint object will allow us to read datagrams sent from any source.
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), 0);

                    Console.WriteLine("");
                    Console.WriteLine("[Listenning for Fault Issues ..]");
                    Console.WriteLine("");


                    // Blocks until a message returns on this socket from a remote host.
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);

                    if (returnData == "IDF_Fault_OnDemand" && _allowErrorCall)
                    {
                        _allowErrorCall = false;
                        var startTh = new System.Threading.ThreadStart(RunFaultHandlingInstance);
                        var thread = new System.Threading.Thread(startTh);
                        thread.Start();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error Occurred With Key : - " + errorKey);
                    Console.WriteLine(e.ToString());
                }
            }
            udpClient.Close();
        }

        static void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _allowErrorCall = true;
        }

        public void SendMessage(String msg)
        {
            UdpClient udp = new UdpClient();

            IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, 19414); //restart dependpant apps

            byte[] sendBytes4 = Encoding.ASCII.GetBytes(msg);
            udp.Send(sendBytes4, sendBytes4.Length, groupEP);
        }

        public void SendMessage(String msg, int port)
        {
            UdpClient udp = new UdpClient();

            IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, port); //make dynamic

            byte[] sendBytes4 = Encoding.ASCII.GetBytes(msg);
            udp.Send(sendBytes4, sendBytes4.Length, groupEP);
        }

    }
}