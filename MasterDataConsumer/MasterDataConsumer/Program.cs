﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Diagnostics;
using System.IO;
using ConsumerLibrary;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using ConsumerLibrary.Api;
using ConsumerLibrary.Import;
using Microsoft.Practices.Unity;
using Unity;

namespace MasterDataConsumer
{
    class Program
    {
        private static Thread cryptoImportProcess;
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);
        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Library.WriteErrorLog(DateTime.Now + " :: CLOSING APP");
            RunApp.FreeResources();
            return false;
        }

        static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            var container = UnityBootStrapper.GetContainer();

            var task = new List<Task>();
            //task.Add(Task.Run(() =>
            //{
            //    RunApp.Run(false);
            //}));
            task.Add(Task.Run(() =>
            {
                RegisterProcessAsThread(container.Resolve<IImportProcess>(), out cryptoImportProcess);
            }));

           Task.WhenAll(task).GetAwaiter().GetResult();
            
            
        }

        private static void RegisterProcessAsThread(IImportProcess process, out Thread thread)
        {
            thread =new Thread(process.RunAsThread);
            thread.Start();
            Library.WriteErrorLog(process.GetType().Name + " : " + DateTime.Now + " :: started  APP");
        }
    }
}
