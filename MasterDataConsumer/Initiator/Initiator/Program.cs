﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;

namespace Initiator
{

    public class SymbolInfo
    {
        public String Symbol;
        public String Exchange;
    }

    public class exchangeFileInfo
    {
        public String exchangeSymbol;
        public String filePath;
    }


    class Program
    {
        static void Main(string[] args)
        {
            GetLatestSymbolListQueueBased();

            foreach (var gh in _SymbolCollection)
            {
                using (DataConsumerWcfService.DataConsumerWcfServiceClient denTr = new DataConsumerWcfService.DataConsumerWcfServiceClient())
                {
                    denTr.Open();
                    denTr.GetData(gh.Symbol);

                   // denTr.GetData("AUDNZD.FXCM");
                }
                System.Threading.Thread.Sleep(10000);
            }
        }

        public static Queue<SymbolInfo> _SymbolCollection = new Queue<SymbolInfo>();
        public static String _selectedExchange = "";
        public static exchangeFileInfo EFileInfo = null;
        public static List<exchangeFileInfo> _exchangeInfoList = new List<exchangeFileInfo>();



        static public bool GetLatestSymbolListQueueBased()
        {
            bool bSuccess = true;
            _SymbolCollection.Clear();

            if (_selectedExchange != "")
            {
                exchangeFileInfo eFileInfo = new exchangeFileInfo();
                eFileInfo.exchangeSymbol = _selectedExchange;
                //eFileInfo.filePath = _selectedExchange + ".txt";

                eFileInfo.filePath = Directory.GetCurrentDirectory() + "\\" + _selectedExchange + ".txt";
                _exchangeInfoList.Add(eFileInfo);
            }
            else
            {
                String[] tempAPP = System.Configuration.ConfigurationManager.AppSettings["DATALIST"].Split('|');

                foreach (var item in tempAPP)
                {
                    exchangeFileInfo eFileInfo = new exchangeFileInfo();
                    eFileInfo.exchangeSymbol = item.Split('.').FirstOrDefault();
                    //eFileInfo.filePath = item;
                    eFileInfo.filePath = Directory.GetCurrentDirectory() + "\\" + item;
                    _exchangeInfoList.Add(eFileInfo);
                }
            }

            //if one of the files cant be found the program will not continue, improve code

            //file read
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.

                foreach (var item in _exchangeInfoList)
                {
                    //temp file location in future will be with exe
                    using (StreamReader sr = new StreamReader(item.filePath))
                    {
                        string line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.

                        //Queue<String> symbols = new Queue<String>();
                        while ((line = sr.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            if (String.IsNullOrEmpty(line) == false)
                            {
                                //symbols.Enqueue(line);
                                SymbolInfo symInfo = new SymbolInfo();
                                symInfo.Symbol = line;
                                symInfo.Exchange = item.exchangeSymbol;

                                _SymbolCollection.Enqueue(symInfo);
                            }
                        }

                        //_SymbolCollection.Add(item.exchangeSymbol, symbols);
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                bSuccess = false;
            }
            return bSuccess;
        }

    }
}
