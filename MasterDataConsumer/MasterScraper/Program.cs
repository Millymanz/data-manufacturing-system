﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;

using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Timers;
using ConsumerLibrary;
using System.Runtime.InteropServices;

namespace MasterScraper
{
    static public class RunApp
    {
        static DataConsumer _dataConsumerRun = null;

        static public void FreeResources()
        {
            if (_dataConsumerRun != null)
            {
                Library.WriteErrorLog(DateTime.Now + " :: FreeResources");
                _dataConsumerRun.Dispose();
                Library.WriteErrorLog(DateTime.Now + " :: APP CLOSED");

            }
        }

        static public void RunListenner()
        {
            var performance = System.Configuration.ConfigurationManager.AppSettings["PERFORMANCETEST"];
            if (performance != "NOOUTPUT")
            {
                var runasScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCRAPPER"].ToString());
                if (runasScrapper == false)
                {
                    CommandListenner cmdList = new CommandListenner();
                    cmdList.ListenForCommands();
                }
            }
        }

        static public void Run(bool bLive)
        {
            ConsumerLibrary.Logger.Setup("MasterScraper.exe.config");

            //var startTh = new System.Threading.ThreadStart(RunListenner);
            //var thread = new System.Threading.Thread(startTh);
            //thread.Start();

            String modeType = bLive ? "LIVE" : "TEST";

            if (bLive)
            {
                modeType = "LIVE";
            }
            else
            {
                var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];
                modeType = modeStr;

                if (modeStr == "LIVE") modeType = "TEST";
            }

            Console.Title = "MasterScraper";

            if (modeType == "UI")
            {
                _dataConsumerRun = new DataConsumer(false);

                Console.WriteLine("Select Runtime Mode - (1)TEST Or (2)LIVE-TEST");
                String readAns = Console.ReadLine().ToLower();

                MODE modeItem;
                var selection = (MODE)Convert.ToInt32(readAns);
                modeItem = (MODE)selection;
                if ((MODE)selection == MODE.Live) modeItem = MODE.Test;

                switch (modeItem)
                {
                    case MODE.Test:
                        {
                            DataConsumer.Mode = MODE.Test;
                            Console.Title += " :: TEST-MODE";
                        }
                        break;

                    case MODE.Live:
                        {
                            DataConsumer.Mode = MODE.Live;
                            Console.Title += " :: LIVE-MODE";
                        }
                        break;

                    case MODE.LiveTest:
                        {
                            DataConsumer.Mode = MODE.LiveTest;
                            Console.Title += " :: LIVE-TEST-MODE";
                        }
                        break;
                }



                Console.WriteLine("");
                Console.WriteLine("Perform Connection Checks? Y or N");

                bool[] connectionsStates = new bool[] { false };
                bool bProceed = true;

                String res = Console.ReadLine().ToLower();

                if (res == "y")
                {
                    connectionsStates[0] = DataConsumer.CheckDatabaseManagerConnection();
                    Console.WriteLine("");


                    if (connectionsStates.Contains(false))
                    {
                        bProceed = false;
                        Console.WriteLine("Please check configuration file and the connecting applications and restart this application");
                        Console.WriteLine("");
                        Console.WriteLine("Press Enter To terminate.");
                        Console.ReadLine();
                    }
                }


                if (bProceed)
                {
                    Console.WriteLine("Do you want to run the first time download? (Y)Yes (N)No \n");
                    String answer = Console.ReadLine().ToLower();

                    Console.WriteLine("\n");

                    if (answer == "y")
                    {
                        Console.WriteLine("Run Download For Price Action Data(1) Or Fundamental Data(2)? \n");
                        var input = Console.ReadLine().ToLower();

                        Console.WriteLine("\n");

                        if (input == "1")
                        {
                            _dataConsumerRun.ConsumeHistoricalDataCSVQueueBased();
                        }
                        else if (input == "2")
                        {
                            var scrap = new Scraper();
                            //scrap.Scrap();
                            scrap.ManualPageByPageScrap();
                        }
                    }
                    if (answer == "n")
                    {
                        Console.WriteLine("Do you want to run the Data Filler Manually? (Y)Yes (N)No \n");
                        var answerTemp = Console.ReadLine().ToLower();

                        if (answerTemp == "y")
                        {
                            Console.WriteLine("Run from scratch? (Y)Yes (N)No \n");
                            var answerTempScratch = Console.ReadLine().ToLower();

                            DataConsumer.SkipGenerateArtificalDateTimes
                                = answerTempScratch == "n" ? true : false;

                            _dataConsumerRun.DataFiller();
                        }
                        else
                        {
                            Console.WriteLine("Do you want to run the Generate Weekly and Monthly Data Manually? (Y)Yes (N)No \n");
                            var answerTempRes = Console.ReadLine().ToLower();
                            if (answerTempRes == "y")
                            {
                                _dataConsumerRun.GenerateWeeklyAndMonthly();
                            }

                        }
                        Console.WriteLine("\n");
                    }
                }

                //Dataconsumption triggered by the scheduler after initial first time download
            }
            else if (modeType == "LIVE")
            {
                DataConsumer.Mode = MODE.Live;
                Console.Title += " :: LIVE-MODE";
            }
            else
            {
                DataConsumer.Mode = MODE.Test;
                Console.Title += " :: TEST-MODE";
            }

            //DataConsumerScheduling dataConsumerSch = new DataConsumerScheduling();
            //dataConsumerSch.StartScheduling();


            var runasScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCRAPPER"].ToString());
            var runasScheduleScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCHEDULED_SCRAPPER"].ToString());

            if (runasScrapper)
            {
                Console.Title += " :: SCRAPPER";
                //These changes added for running with Windows Schedule Tasks
                var scrapRun = new Scraper();
                scrapRun.ManualPageByPageScrap();
            }
            else if (runasScheduleScrapper)
            {
                Console.Title += " :: SCHEDULED SCRAPPER";

                var scrapperSch = new ScrapperScheduler();
                scrapperSch.StartScheduling();
            }
            Console.ReadLine();

        }
    }

    public class ServerType
    {
        public String Type = "ALL";
        public int WaitingPeriod = 0;
    }

    public class CommandListenner
    {
        static bool _allowErrorCall = true;
        static Timer _timer;


        private String _machine = "";
        private ServerType _serverType = null;

        public CommandListenner()
        {
            _timer = new Timer(120000); //2min
            _timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
            _timer.Enabled = true; // Enable it
        }

        static public void RunFaultHandlingInstance()
        {
            Console.WriteLine("Master Data Consumer Running Fault Handled..\n");

            System.Threading.Thread.Sleep(120000); //wait 2 minutes

            DataConsumer dataConsumer = new DataConsumer(false);
            dataConsumer.DataFiller();

            dataConsumer.ConsumeHistoricalDataCSVQueueBased();


            DataConsumerScheduling dataConsumerSch = new DataConsumerScheduling();
            dataConsumerSch.StartScheduling();
        }


        public void ListenForCommands()
        {
            // PopulateMachinePairing();

            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            UdpClient udpClient = new UdpClient(19074);//make listenning port dynamic //10198

            while (true)
            {
                udpClient.EnableBroadcast = true;
                String errorKey = "";
                try
                {
                    //IPEndPoint object will allow us to read datagrams sent from any source.
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(localIP), 0);

                    Console.WriteLine("");
                    Console.WriteLine("[Listenning for Fault Issues ..]");
                    Console.WriteLine("");


                    // Blocks until a message returns on this socket from a remote host.
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);

                    if (returnData == "IDF_Fault_OnDemand" && _allowErrorCall)
                    {
                        _allowErrorCall = false;
                        var startTh = new System.Threading.ThreadStart(RunFaultHandlingInstance);
                        var thread = new System.Threading.Thread(startTh);
                        thread.Start();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error Occurred With Key : - " + errorKey);
                    Console.WriteLine(e.ToString());
                }
            }
            udpClient.Close();
        }

        static void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _allowErrorCall = true;
        }

        public void SendMessage(String msg)
        {
            UdpClient udp = new UdpClient();

            IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, 19414); //restart dependpant apps

            byte[] sendBytes4 = Encoding.ASCII.GetBytes(msg);
            udp.Send(sendBytes4, sendBytes4.Length, groupEP);
        }

        public void SendMessage(String msg, int port)
        {
            UdpClient udp = new UdpClient();

            IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, port); //make dynamic

            byte[] sendBytes4 = Encoding.ASCII.GetBytes(msg);
            udp.Send(sendBytes4, sendBytes4.Length, groupEP);
        }

    }

    class Program
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);
        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Library.WriteErrorLog(DateTime.Now + " :: CLOSING APP");
            RunApp.FreeResources();
            return false;
        }

        static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            RunApp.Run(false);
        }
    }
}
