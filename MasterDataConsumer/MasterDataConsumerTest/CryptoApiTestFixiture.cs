﻿using ConsumerLibrary;
using ConsumerLibrary.Api;
using CsvHelper;
using NUnit.Framework;
using System;
namespace MasterDataConsumerTest
{
    [TestFixture]
    public class CryptoApiTestFixiture
    {
        private IConfigHelper configHelper;
        private ICryptoApi cryptoApi;
        [SetUp]
        public void SetUp()
        {
            configHelper = new ConfigHelper();
            cryptoApi = new CryptoApi(configHelper);
        }
        [Test]
        public void Test_Api_DownLoad()
        {
            var exchanges = cryptoApi.Metadata_list_exchanges();
            Assert.IsNotNull(exchanges);
        }
        [Test,Ignore]
        public void Api_Symbol_Call()
        {
            var symbols = cryptoApi.Metadata_list_symbols();
            //Temp code to dump to csv
            using (var writer = new System.IO.StreamWriter("new.csv"))
            {
                using (var csvWriter = new CsvWriter(writer))
                {
                    // You can turn AutoFlush on and handle it that way,
                    // but it will flush on every write this way, which might not be ideal.
                    //writer.AutoFlush = true;

                    // You can change configuration here.
                    csvWriter.Configuration.QuoteAllFields = true;

                    csvWriter.WriteRecords(symbols);
                    csvWriter.Configuration.AutoMap<Symbol>();
                    // You can manually flush.
                    writer.Flush();
                } // 
            }
        }
    }
}
