﻿Feature: Scraper
	Check the scraper behaves as expected for the various scenarios	
	#Assumption here is that the code will automatically work when the clocks are moved back or forwards
	#daylightsavings

@mytag
Scenario: Scheduled scraper fires at the different scheduled times
	Given I have launched the scheduled scraper it schedules its launch at different times
	When  The scraper is run I wait 7 minutes for it to complete its session
	Then the result should be 4 economic events in the files


@mytag
Scenario: Scheduled scraper fires at the same scheduled times
	Given I have launched the scheduled scraper it schedules its launches for the same time
	When  The scraper is run I wait 7 minutes for it to complete its session
	Then the result should be 4 economic events in the files


@mytag
Scenario: Scheduled scraper fires with a mixture of many scheduled events
	Given I have launched the scheduled scraper it schedules its launches for a mixture of many scheduled events
	When  The scraper is run I wait 8 minutes for it to complete its session
	Then the result should be 17 economic events in the files
