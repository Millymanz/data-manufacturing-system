﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using ConsumerLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConsumptionTests.Step_Definition
{
    [Binding]
    public class Scraper
    {
        ScrapperScheduler _scrapperScheduling = null;
        List<string> _eventsToLookFor = new List<string>();
        List<bool> _eventsFound = new List<bool>();

        [Given(@"I have launched the scheduled scraper it schedules its launch at different times")]
        public void GivenIHaveLaunchedTheScheduledScraperItSchedulesItsLaunchAtDifferentTimes()
        {
            //delete all the files in the output folder
            var path = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"].ToString();

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
            foreach (System.IO.FileInfo file in di.GetFiles())
            {
                file.Delete();
            } 
            var connectionName = "JobsManager_TEST";
            DatabaseUtility.DeleteFileRegisteration(connectionName);


            _scrapperScheduling = new ScrapperScheduler();

            var dateTime = DateTime.Now;

            var usaTimeZoneCurrentTemp = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            var hours = dateTime.Hour + usaTimeZoneCurrentTemp.BaseUtcOffset.Hours;
            var minutes = dateTime.Minute + 2;
            if (minutes > 59)
            {
                 hours = hours + 1;
                 minutes = minutes - 60;
            }

            var minutesFirst = dateTime.Minute + 3;
            if (minutesFirst > 59)
            {
                hours = hours + 1;
                minutesFirst = minutesFirst - 60;
            }

            var minutesSecond = dateTime.Minute + 4;
            if (minutesSecond > 59)
            {
                hours = hours + 1;
                minutesSecond = minutesSecond - 60;
            }

            string minStr = minutes.ToString();
            string minFirstStr = minutesFirst.ToString();
            string minSecondStr = minutesSecond.ToString();

            if (minutes < 10) { minStr = "0" + minutes; }
            if (minutesFirst < 10) { minFirstStr = "0" + minutesFirst; }
            if (minutesSecond < 10) { minSecondStr = "0" + minutesSecond; }


            string ke = hours + ":" + minStr;
            string first = hours + ":" + minFirstStr;
            string second = hours + ":" + minSecondStr;
            string third = hours + ":" + minSecondStr;

            var timeTable = new Dictionary<string, string>();
            timeTable.Add("http://www.investing.com/economic-calendar/core-retail-sales-857", ke);
            timeTable.Add("http://www.investing.com/economic-calendar/core-retail-sales-63", first);
            timeTable.Add("http://www.investing.com/economic-calendar/unemployment-rate-299", second);
            timeTable.Add("http://www.investing.com/economic-calendar/adp-nonfarm-employment-change-1", third);

            _eventsToLookFor.Add("core retail sales yoy");
            _eventsToLookFor.Add("core retail sales mom");
            _eventsToLookFor.Add("unemployment rate");
            _eventsToLookFor.Add("adp national employment report");

            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);

            _scrapperScheduling.TimeTable = timeTable;

            Assert.AreEqual(4, _eventsFound.Count);
            Assert.AreEqual(4, timeTable.Count);
        }

        [Given(@"I have launched the scheduled scraper it schedules its launches for a mixture of many scheduled events")]
        public void GivenIHaveLaunchedTheScheduledScraperItSchedulesItsLaunchesForAMixtureOfManyScheduledEvents()
        {
            //delete all the files in the output folder
            var path = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"].ToString();

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
            foreach (System.IO.FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            var connectionName = "JobsManager_TEST";
            DatabaseUtility.DeleteFileRegisteration(connectionName);


            _scrapperScheduling = new ScrapperScheduler();

            var dateTime = DateTime.Now;

            var usaTimeZoneCurrentTemp = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            var hours = dateTime.Hour + usaTimeZoneCurrentTemp.BaseUtcOffset.Hours;
            var minutes = dateTime.Minute + 2;
            if (minutes > 59)
            {
                hours = hours + 1;
                minutes = minutes - 60;
            }

            var minutesFirst = dateTime.Minute + 3;
            if (minutesFirst > 59)
            {
                hours = hours + 1;
                minutesFirst = minutesFirst - 60;
            }

            var minutesSecond = dateTime.Minute + 4;
            if (minutesSecond > 59)
            {
                hours = hours + 1;
                minutesSecond = minutesSecond - 60;
            }

            string minStr = minutes.ToString();
            string minFirstStr = minutesFirst.ToString();
            string minSecondStr = minutesSecond.ToString();

            if (minutes < 10) { minStr = "0" + minutes; }
            if (minutesFirst < 10) { minFirstStr = "0" + minutesFirst; }
            if (minutesSecond < 10) { minSecondStr = "0" + minutesSecond; }


            string ke = hours + ":" + minStr;
            string first = hours + ":" + minFirstStr;
            string second = hours + ":" + minSecondStr;
            string third = hours + ":" + minSecondStr;

            string expired = hours + ":00";

            var timeTable = new Dictionary<string, string>();
            timeTable.Add("http://www.investing.com/economic-calendar/core-retail-sales-857", ke);
            timeTable.Add("http://www.investing.com/economic-calendar/core-retail-sales-63", first);
            timeTable.Add("http://www.investing.com/economic-calendar/unemployment-rate-299", second);
            timeTable.Add("http://www.investing.com/economic-calendar/adp-nonfarm-employment-change-1", third);
            timeTable.Add("http://www.investing.com/economic-calendar/real-consumer-spending-914", third);
            timeTable.Add("http://www.investing.com/economic-calendar/nonfarm-payrolls-227", third);

            timeTable.Add("http://www.investing.com/economic-calendar/house-price-index-313", third);
            timeTable.Add("http://www.investing.com/economic-calendar/new-home-sales-896", third);

            timeTable.Add("http://www.investing.com/economic-calendar/german-retail-sales-138", third);
            timeTable.Add("http://www.investing.com/economic-calendar/national-cpi-992", third);
            timeTable.Add("http://www.investing.com/economic-calendar/gdp-125", third);
            timeTable.Add("http://www.investing.com/economic-calendar/boe-qe-total-665", third);
            timeTable.Add("http://www.investing.com/economic-calendar/interest-rate-decision-166", third);
            timeTable.Add("http://www.investing.com/economic-calendar/unit-labor-costs-303", third);
            timeTable.Add("http://www.investing.com/economic-calendar/services-pmi-1062", third);
            timeTable.Add("http://www.investing.com/economic-calendar/eia-weekly-cushing-oil-inventories-1657", third);
            timeTable.Add("http://www.investing.com/economic-calendar/deposit-facility-rate-1655", third);
            timeTable.Add("http://www.investing.com/economic-calendar/housing-starts-898", expired);
           
            //timeTable.Add("http://www.investing.com/economic-calendar/housing-starts-151", expired);


            _eventsToLookFor.Add("core retail sales yoy");
            _eventsToLookFor.Add("core retail sales mom");
            _eventsToLookFor.Add("unemployment rate");
            _eventsToLookFor.Add("adp national employment report");
            _eventsToLookFor.Add("real consumer spending");
            _eventsToLookFor.Add("nonfarm payrolls");

            _eventsToLookFor.Add("house price index yoy");
            _eventsToLookFor.Add("new home sales mom");

            _eventsToLookFor.Add("retail sales mom");
            _eventsToLookFor.Add("national consumer price index yoy");
            _eventsToLookFor.Add("gross domestic product qoq");
            _eventsToLookFor.Add("quantitative easing");
            _eventsToLookFor.Add("vote on interest rate");
            _eventsToLookFor.Add("unit labor costs qoq");
            _eventsToLookFor.Add("services pmi");
            _eventsToLookFor.Add("cushing crude oil inventories");
            _eventsToLookFor.Add("facility rate deposit");
            _eventsToLookFor.Add("housing starts mom");
           // _eventsToLookFor.Add("housing starts");

            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
           
            //_eventsFound.Add(false);

            _scrapperScheduling.TimeTable = timeTable;

            Assert.AreEqual(18, _eventsFound.Count);
            Assert.AreEqual(18, timeTable.Count);
        }

        [Given(@"I have launched the scheduled scraper it schedules its launches for the same time")]
        public void GivenIHaveLaunchedTheScheduledScraperItSchedulesItsLaunchesForTheSameTime()
        {
            //delete all the files in the output folder
            var path = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"].ToString();

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
            foreach (System.IO.FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            var connectionName = "JobsManager_TEST";
            DatabaseUtility.DeleteFileRegisteration(connectionName);


            _scrapperScheduling = new ScrapperScheduler();

            var dateTime = DateTime.Now;

            var usaTimeZoneCurrentTemp = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            var hours = dateTime.Hour + usaTimeZoneCurrentTemp.BaseUtcOffset.Hours;
            var minutes = dateTime.Minute + 2;
            if (minutes > 59)
            {
                hours = hours + 1;
                minutes = minutes - 60;
            }
            
            string minStr = minutes.ToString();
            if (minutes < 10) { minStr = "0" + minutes; }

            string ke = hours + ":" + minStr;

            var timeTable = new Dictionary<string, string>();
            timeTable.Add("http://www.investing.com/economic-calendar/core-retail-sales-857", ke);
            timeTable.Add("http://www.investing.com/economic-calendar/core-retail-sales-63", ke);
            timeTable.Add("http://www.investing.com/economic-calendar/unemployment-rate-299", ke);
            timeTable.Add("http://www.investing.com/economic-calendar/adp-nonfarm-employment-change-1", ke);

            _eventsToLookFor.Add("core retail sales yoy");
            _eventsToLookFor.Add("core retail sales mom");
            _eventsToLookFor.Add("unemployment rate");
            _eventsToLookFor.Add("adp national employment report");

            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);
            _eventsFound.Add(false);

            _scrapperScheduling.TimeTable = timeTable;

            Assert.AreEqual(4, _eventsFound.Count);
            Assert.AreEqual(4, timeTable.Count);
        }

        [When(@"The scraper is run I wait (.*) minutes for it to complete its session")]
        public void WhenTheScraperIsRunIWaitMinutesForItToCompleteItsSession(int p0)
        {
            var waitPeriod = p0 * 60000;

            _scrapperScheduling.StartSchedulingManualTimeTable();

            System.Threading.Thread.Sleep(waitPeriod);
        }

        [Then(@"the result should be (.*) economic events in the files")]
        public void ThenTheResultShouldBeEconomicEventsInTheFiles(int p0)
        {
             var path = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"].ToString();

             string[] filePaths = System.IO.Directory.GetFiles(path, "*.csv");

             var dataExists = 0;
             
             foreach (var item in filePaths)
             {
                 string[] lines = System.IO.File.ReadAllLines(item);
                 
                 foreach (var it in _eventsToLookFor)
                 {
                    if (lines.Any(m => m.IndexOf(it) > -1))
                    {
                        dataExists++;
                    }
                 }
             }
             Assert.AreEqual(p0, dataExists);
        }
    }
}
