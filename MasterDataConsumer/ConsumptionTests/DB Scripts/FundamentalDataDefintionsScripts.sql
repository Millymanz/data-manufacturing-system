USE [master]
GO

/****** Object:  Database [FundamentalDataDefinitions]    Script Date: 14/10/2016 12:09:15 ******/
CREATE DATABASE [FundamentalDataDefinitions]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FundamentalDataDefinitions', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQL2012\MSSQL\DATA\FundamentalDataDefinitions.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FundamentalDataDefinitions_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQL2012\MSSQL\DATA\FundamentalDataDefinitions_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [FundamentalDataDefinitions] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FundamentalDataDefinitions].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [FundamentalDataDefinitions] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET ARITHABORT OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET  DISABLE_BROKER 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET  MULTI_USER 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [FundamentalDataDefinitions] SET DB_CHAINING OFF 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [FundamentalDataDefinitions] SET  READ_WRITE 
GO












USE [FundamentalDataDefinitions]
GO
/****** Object:  StoredProcedure [dbo].[Admin_NOT_Failed_Scrap_FundamentalKeywordDefinitions]    Script Date: 14/10/2016 12:08:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Admin_NOT_Failed_Scrap_FundamentalKeywordDefinitions]
AS
	BEGIN
		SELECT DISTINCT FF.MatchWords, FF.EventType, EE.Country FROM [FundamentalDataDefinitions].[dbo].[tblFundamentalKeywordDefinitions] FF
		LEFT JOIN [FundamentalMarketData].[dbo].[tblEconomicEvents] EE ON FF.EventType = EE.EventType
		WHERE EE.InstitutionBody IS NULL
	END





GO
/****** Object:  StoredProcedure [dbo].[Admin_WorkingFundamentalKeywordDefinitionsInAction]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Admin_WorkingFundamentalKeywordDefinitionsInAction]
AS
	BEGIN
		SELECT DISTINCT FF.MatchWords, FF.EventType, EE.Country FROM [FundamentalDataDefinitions].[dbo].[tblFundamentalKeywordDefinitions] FF
		LEFT JOIN [FundamentalMarketData].[dbo].[tblEconomicEvents] EE ON FF.EventType = EE.EventType
		WHERE FF.InstitutionBody = EE.InstitutionBody
	END





GO
/****** Object:  StoredProcedure [dbo].[GetFundamentalKeywordDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetFundamentalKeywordDefinitions]
AS
	BEGIN
		SELECT
		 [MatchWords]
		,[Category]
		,[InstitutionBody]
		,[EventType]
		,[Link]
		,GG.FirstName
		,GG.LastName
		,GG.[Role]
		FROM [dbo].[tblFundamentalKeywordDefinitions] DD
		LEFT JOIN [dbo].tblPersonnelAssociation TT ON DD.ID = TT.AssociationID
		LEFT JOIN [dbo].tblKeyPersonnelDefinitions GG ON TT.KeyPersonnelID = GG.ID
	END




GO
/****** Object:  StoredProcedure [dbo].[InsertFundamentalKeywordDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[InsertFundamentalKeywordDefinitions]
@MatchWords nvarchar(max),
@Category nvarchar(50),
@InstitutionBody nvarchar(50),
@EventType nvarchar(200),
@Link nvarchar(300)
AS

	BEGIN
		INSERT INTO [dbo].[tblFundamentalKeywordDefinitions]
				   ([ID]
				   ,[MatchWords]
				   ,[Category]
				   ,[InstitutionBody]
				   ,[EventType]
				   ,[Link])
			 VALUES
				   (NEWID()
				   ,@MatchWords
				   ,@Category
				   ,@InstitutionBody
				   ,@EventType
				   ,@Link)
	END
	 







GO
/****** Object:  StoredProcedure [dbo].[InsertKeyPersonnelDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[InsertKeyPersonnelDefinitions]
@FirstName nvarchar(50),
@LastName nvarchar(50),
@Role nvarchar(50)
AS


	BEGIN
		INSERT INTO [dbo].[tblKeyPersonnelDefinitions]
				   ([ID]
				   ,[FirstName]
				   ,[LastName]
				   ,[Role])
			 VALUES
				   (NEWID()
				   ,@FirstName
				   ,@LastName
				   ,@Role)
	END

	 







GO
/****** Object:  StoredProcedure [dbo].[InsertPersonnelAssociation]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[InsertPersonnelAssociation]
@InstitutionBody nvarchar(50),
@FirstName nvarchar(50),
@LastName nvarchar(50)
AS

DECLARE @InstitutionBodyID uniqueidentifier
DECLARE @PersonnelID uniqueidentifier


	
		SELECT @InstitutionBodyID = [ID] FROM dbo.[tblFundamentalKeywordDefinitions] DD
		WHERE DD.InstitutionBody = @InstitutionBody
	
		SELECT @PersonnelID = [ID] FROM dbo.tblKeyPersonnelDefinitions DD
		WHERE DD.FirstName = @FirstName AND DD.LastName = @LastName
	
		IF NOT EXISTS(SELECT * FROM dbo.[tblPersonnelAssociation] DD 
		WHERE DD.AssociationID = @InstitutionBodyID AND DD.KeyPersonnelID = @PersonnelID)
		BEGIN
			INSERT dbo.tblPersonnelAssociation (AssociationID, KeyPersonnelID)
			SELECT ID, @PersonnelID FROM dbo.tblFundamentalKeywordDefinitions 
			WHERE dbo.tblFundamentalKeywordDefinitions.InstitutionBody = @InstitutionBody

		END

	
	


	--BEGIN
	--	IF EXISTS(SELECT * FROM dbo.[tblFundamentalKeywordDefinitions] DD
	--	WHERE DD.InstitutionBody = @InstitutionBody)
	
	--		IF EXISTS(SELECT * FROM dbo.[tblFundamentalKeywordDefinitions] DD
	--		WHERE DD.InstitutionBody = @InstitutionBody)

	--		BEGIN
	--			IF EXISTS(SELECT * FROM dbo.[tblFundamentalKeywordDefinitions] DD
	--			WHERE DD.InstitutionBody = @InstitutionBody)
	--		END
	--	END
	--	IF EXISTS(SELECT * FROM dbo.[tblFundamentalKeywordDefinitions] DD
	--	WHERE DD.InstitutionBody = @InstitutionBody)
	--END


	 







GO
/****** Object:  StoredProcedure [dbo].[proc_Admin_SearchAndViewKeywords_EventOrInstitutionOrLink]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[proc_Admin_SearchAndViewKeywords_EventOrInstitutionOrLink]
@keywordvar nvarchar(50)
AS

BEGIN
	 SELECT [MatchWords]
		  ,[Category]
		  ,[InstitutionBody]
		  ,[EventType]
		  ,[Link]
	  FROM [dbo].[tblFundamentalKeywordDefinitions] DD
	  WHERE DD.[EventType] like '%'+@keywordvar+'%' Or DD.[InstitutionBody] like '%'+@keywordvar+'%'
	  Or DD.[Link] like '%'+@keywordvar+'%'
END




GO
/****** Object:  StoredProcedure [dbo].[proc_BulkInsertFundamentalKeywordDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/**This proc is run manually and not by an application*/






/*************************************************************************/

CREATE PROCEDURE [dbo].[proc_BulkInsertFundamentalKeywordDefinitions]
@ROWSAFFECTED int output
AS

DELETE FROM [dbo].tblSTAGING_FundamentalKeywordDefinitions

IF OBJECT_ID('StageFundamentalKeywordDefinitions') IS NOT NULL
BEGIN
	DROP TABLE StageFundamentalKeywordDefinitions
END


BULK
INSERT [dbo].tblSTAGING_FundamentalKeywordDefinitions
FROM 'C:\Users\Public\Downloads\Output\Test\RawData\Staging\Temp.csv'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n', --Hex equivalent of \n   0x0a
--ERRORFILE = 'C:\Users\Public\Downloads\Output\Test\RawData\Staging\myRubbishData.log', 
MAXERRORS = 99999999
)

DECLARE @GUID [nvarchar](MAX)

SET @ROWSAFFECTED = 0

--SELECT @ROWSAFFECTED = COUNT(*) FROM [dbo].tblSTAGING_Trades


--/****** Object:  Index [ix_tblTrades]    Script Date: 08/02/2014 09:01:26 ******/
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Covering_SymbolID') 
--    DROP INDEX [ix_tblTrades_Covering_SymbolID] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Covering_SymbolID_TimeFrame') 
--    DROP INDEX [ix_tblTrades_Covering_SymbolID_TimeFrame] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Covering_TimeFrame') 
--    DROP INDEX [ix_tblTrades_Covering_TimeFrame] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Covering_MaxDateTimeCheck') 
--    DROP INDEX [ix_tblTrades_Covering_MaxDateTimeCheck] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Covering_DateTime') 
--    DROP INDEX [ix_tblTrades_Covering_DateTime] ON [dbo].[tblTrades]; 

-------------------------- Filtered ----------------------------
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_1min') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_1min] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_2min') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_2min] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_5min') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_5min] ON [dbo].[tblTrades]; 
	
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_10min') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_10min] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_15min') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_15min] ON [dbo].[tblTrades]; 
	
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_30min') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_30min] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_1hour') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_1hour] ON [dbo].[tblTrades]; 
	
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_2hour') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_2hour] ON [dbo].[tblTrades]; 
		
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_4hour') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_4hour] ON [dbo].[tblTrades]; 

--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_EndOfDay') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_EndOfDay] ON [dbo].[tblTrades]; 
			
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_Weekly') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_Weekly] ON [dbo].[tblTrades]; 
				
--IF EXISTS (SELECT name FROM sys.indexes
--            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_Monthly') 
--    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_Monthly] ON [dbo].[tblTrades]; 



----------------------------------------------------------------------------------------

BEGIN

SELECT NEWID() AS ID,
[dbo].tblSTAGING_FundamentalKeywordDefinitions.[MatchWords],
[dbo].tblSTAGING_FundamentalKeywordDefinitions.[Category],
[dbo].tblSTAGING_FundamentalKeywordDefinitions.[EventType],
[dbo].tblSTAGING_FundamentalKeywordDefinitions.[InstitutionBody],
[dbo].tblSTAGING_FundamentalKeywordDefinitions.[Link] INTO StageFundamentalKeywordDefinitions
FROM [dbo].tblSTAGING_FundamentalKeywordDefinitions;


/*Ensuring data set (grouped data) is DISITINCT */
WITH DistinctSet AS
(
  SELECT row_number() OVER(PARTITION BY [Link] ORDER BY [Link] DESC) AS rn
  FROM [dbo].StageFundamentalKeywordDefinitions
)
DELETE FROM DistinctSet
WHERE rn > 1 


/*Ensure that there is only one batch of date with a given enddate no duplicates for when this sp is run several times within the day */
BEGIN
	DECLARE @Link [nvarchar](300)
	SELECT TOP 1  @Link = [Link] FROM StageFundamentalKeywordDefinitions
	DELETE FROM [dbo].tblFundamentalKeywordDefinitions WHERE [dbo].tblFundamentalKeywordDefinitions.[Link] = @Link
END



INSERT INTO [dbo].tblFundamentalKeywordDefinitions (
	   [ID]
      ,[MatchWords]
      ,[Category]
      ,[InstitutionBody]
      ,[EventType]
      ,[Link])
SELECT DISTINCT 
	   [ID]
      ,[MatchWords]
      ,[Category]
      ,[InstitutionBody]
      ,[EventType]
      ,[Link] FROM StageFundamentalKeywordDefinitions

DROP TABLE StageFundamentalKeywordDefinitions

BEGIN
	/*Delete any duplicates within the main database table*/
	WITH DistinctSet AS
	(
	  SELECT row_number() OVER(PARTITION BY [Link] ORDER BY [Link] DESC) AS rn
	  FROM [dbo].tblFundamentalKeywordDefinitions
	)
	DELETE FROM DistinctSet
	WHERE rn > 1 
END

--BEGIN
	/****************** Adding Indicies ******************/
	-- Create a nonclustered index
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_SymbolID] ON [dbo].[tblTrades]
--	(
--		[SymbolID] ASC
--	)
--	INCLUDE ([Open],
--		[High],
--		[Low],
--		[Close],
--		[Volume],
--		[AdjustmentClose],
--		[TimeFrame],
--		[DateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
--		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
----END
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_SymbolID_TimeFrame] ON [dbo].[tblTrades]
--	(
--		[SymbolID] ASC, [TimeFrame]
--	)
--	INCLUDE ([Open],
--		[High],
--		[Low],
--		[Close],
--		[Volume],
--		[AdjustmentClose],
--		[DateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
--		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_TimeFrame] ON [dbo].[tblTrades]
--	(
--		[TimeFrame]
--	)
--	INCLUDE (
--		[SymbolID],
--		[Open],
--		[High],
--		[Low],
--		[Close],
--		[Volume],
--		[AdjustmentClose],
--		[DateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
--		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_MaxDateTimeCheck] ON [dbo].[tblTrades]
--	(
--		[TimeFrame]
--	)
--	INCLUDE (
--		[SymbolID],
--		[DateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
--		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_DateTime] ON [dbo].[tblTrades]
--	(
--		[DateTime]
--	)
--	INCLUDE (
--		[SymbolID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
--		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--	------- Filtered ---------
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_1min] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '1min'

--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_2min] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '2min'

--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_5min] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '5min'
	
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_10min] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '10min'
		
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_15min] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '15min'
		
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_30min] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '30min'
			
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_1hour] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '1hour'
				
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_2hour] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '2hour'
					
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_4hour] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = '4hour'

--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_EndOfDay] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = 'EndOfDay'
	
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_Weekly] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = 'Weekly'
		
--	-- Create a nonclustered index 
--	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_Monthly] ON [dbo].[tblTrades]
--	(
--		[SymbolID],
--		[DateTime] ASC
--	)
--	WHERE [TimeFrame] = 'Monthly'



-------Last Step------

END




GO
/****** Object:  StoredProcedure [dbo].[UpdateFundamentalKeywordDefinitions_Link]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[UpdateFundamentalKeywordDefinitions_Link]
@ID uniqueidentifier,
@Link nvarchar(300)
AS

	BEGIN
		UPDATE [dbo].[tblFundamentalKeywordDefinitions] SET [dbo].[tblFundamentalKeywordDefinitions].Link = @Link
		WHERE [dbo].[tblFundamentalKeywordDefinitions].ID = @ID	
	END
	 



GO
/****** Object:  Table [dbo].[tblFundamentalKeywordDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFundamentalKeywordDefinitions](
	[ID] [uniqueidentifier] NOT NULL,
	[MatchWords] [nvarchar](max) NULL,
	[Category] [nvarchar](50) NULL,
	[InstitutionBody] [nvarchar](50) NULL,
	[EventType] [nvarchar](200) NULL,
	[Link] [nvarchar](300) NULL,
 CONSTRAINT [PK_tblFundamentalKeywordDefinitions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblKeyPersonnelDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblKeyPersonnelDefinitions](
	[ID] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Role] [nvarchar](70) NULL,
 CONSTRAINT [PK_KeyPersonnelDefinitions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblPersonnelAssociation]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPersonnelAssociation](
	[AssociationID] [uniqueidentifier] NULL,
	[KeyPersonnelID] [uniqueidentifier] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSTAGING_FundamentalKeywordDefinitions]    Script Date: 14/10/2016 12:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSTAGING_FundamentalKeywordDefinitions](
	[MatchWords] [nvarchar](max) NULL,
	[Category] [nvarchar](50) NULL,
	[InstitutionBody] [nvarchar](50) NULL,
	[EventType] [nvarchar](200) NULL,
	[Link] [nvarchar](300) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'399a9e4e-b03a-4196-9ffd-00105efc74a3', N'australia participation rate', N'employment', N'australian bureau of statistics', N'participation rate', N'http://www.investing.com/economic-calendar/participation-rate-1051')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6c5a26c8-ff32-4db5-8d58-001564ca06d6', N'australia anz job advertisements mom', N'employment', N'austrailia and new zealand banking group', N'job advertisements mom', N'http://www.investing.com/economic-calendar/anz-job-advertisements-385')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f088f3ad-7c0f-43ec-b87f-007278d320f3', N'u.k. business investment qoq', N'economic activity', N'office for national statistics', N'business investment qoq', N'http://www.investing.com/economic-calendar/business-investment-30')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c9fb0b63-08f3-40bf-9289-00ae314a36de', N'bank of japan press conference', N'central bank', N'boj', N'press conference', N'http://www.investing.com/economic-calendar/boj-press-conference-370')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ba1ca644-fc86-4b7c-abe0-00da95b09ab9', N'u.s. michigan consumer expectations', N'economic activity', N'university of michigan', N'michigan sentiment index', N'http://www.investing.com/economic-calendar/michigan-consumer-expectations-900')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd1217e6a-5437-4b7c-a4ca-015153af779a', N'boe mpc vote unchanged', N'central bank', N'boe', N'monetary policy vote unchanged', N'http://www.investing.com/economic-calendar/boe-mpc-vote-unchanged-848')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b5d3900e-930b-4c88-b729-0183763e1b04', N'china gross domestic product (gdp) yoy', N'economic activity', N'national bureau of statistics', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/chinese-gdp-461')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e6e6dc65-1cd8-423f-88ff-01a66ab4ce81', N'eurozone unemployment rate', N'employment', N'eurostat', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-299')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5babbcb4-bded-4eeb-ad66-01be78c93f95', N'japan industrial production mom', N'economic activity', N'ministry of economy', N'industrial production mom', N'http://www.investing.com/economic-calendar/industrial-production-159')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6d1b6984-8c89-4b26-a068-027c4814084f', N'eurozone producer price index (ppi) mom', N'inflation', N'eurostat', N'producer price index input mom', N'http://www.investing.com/economic-calendar/ppi-237')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ad5a192b-6b4d-44f3-84f8-028056fbe71f', N'canada raw materials price index (rmpi) mom', N'inflation', N'statistics canada', N'raw materials price index mom', N'http://www.investing.com/economic-calendar/rmpi-266')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'06e3d4f0-886c-4083-a8f8-037d7e110186', N'brexit referendum', N'economic activity', N'uk government', N'brexit referendum', N'http://www.investing.com/economic-calendar/brexit-referendum-1671')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ae52e8b6-1344-42c0-8b86-043d25a30497', N'germany gross domestic product (gdp) yoy', N'economic activity', N'destatis', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/german-gdp-738')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f6d0d5d0-8053-4cc8-9828-07c9f39d231e', N'u.s. building permits mom', N'economic activity', N'us department of commerce', N'building permits mom', N'http://www.investing.com/economic-calendar/building-permits-885')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'daaf44ea-2a3a-4ec0-8037-0936d7789791', N'japan machine tool orders yoy', N'economic activity', N'japan machine tool builders', N'machine tool orders yoy', N'http://www.investing.com/economic-calendar/machine-tool-orders-200')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9702ad5f-240c-444d-891f-099f26184c3a', N'new zealand unemployment rate', N'employment', N'statistics new zealand', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-295')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'cb8aaa73-3341-449d-b829-0a390f00acea', N'japan gross domestic product (gdp) qoq', N'economic activity', N'cabinet office', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/gdp-119')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a4ad41e0-fb57-4019-a511-0a797b0f12b0', N'u.s. s&p/cs hpi composite - 20 n.s.a. yoy', N'economic activity', N'standard and poor''s', N's&p house price index', N'http://www.investing.com/economic-calendar/s-p-cs-hpi-composite-20-n.s.a.-329')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'88c3e9c9-94a3-417b-8290-0ac0cbc43fd0', N'new zealand consumer price index (cpi) qoq', N'inflation', N'statistics new zealand', N'consumer price index qoq', N'http://www.investing.com/economic-calendar/cpi-72')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'aeb79ca6-7087-45c7-a63d-0b1d98283082', N'brazil gross domestic product (gdp) yoy', N'economic activity', N'ibge', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/brazilian-gdp-413')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'351cad4f-fad9-4729-b173-0b25ef1ab01c', N'initial jobless claims', N'employment', N'department of labor', N'initial jobless claims', N'http://www.investing.com/economic-calendar/initial-jobless-claims-294')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f6732cd0-ba4b-48f2-aa38-0dcb1b82134b', N'u.s. richmond manufacturing index', N'economic activity', N'federal reserve bank of richmond', N'richmond manufacturing index', N'http://www.investing.com/economic-calendar/richmond-manufacturing-index-263')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9c7c6280-9676-4ce4-95a5-0f10532c7c1b', N'u.s. small business health index', N'economic activity', N'fit small business', N'small business health index', N'http://www.investing.com/economic-calendar/u.s.-small-business-health-index-1584')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4b8f7535-00f2-4a24-828e-117d2c7a01dd', N'new zealand participation rate', N'employment', N'statistics new zealand', N'participation rate', N'http://www.investing.com/economic-calendar/participation-rate-1532')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fbad59d6-0ee1-47cd-acb9-11e9bec482ad', N'reserve bank of australia rate statement', N'central bank', N'reserve bank of australia', N'reserve bank of australia statement', N'http://www.investing.com/economic-calendar/rba-rate-statement-387')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0fb03e7d-7215-40e3-873c-131a11350a47', N'u.k. halifax house price index yoy', N'economic activity', N'bank of scotland', N'halifax house price index yoy', N'http://www.investing.com/economic-calendar/halifax-house-price-index-844')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'83885a41-224c-4160-a437-1584b8edc6f9', N'u.s. real consumer spending', N'economic activity', N'us department of commerce', N'real consumer spending', N'http://www.investing.com/economic-calendar/real-consumer-spending-914')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'31652b80-2dab-489b-8556-16dad857557c', N'nonfarm payrolls', N'employment', N'bureau of labor statistics', N'nonfarm payrolls', N'http://www.investing.com/economic-calendar/nonfarm-payrolls-227')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b4a39167-d4ce-4bcc-a40f-1756a693d8ff', N'u.k. house price index yoy', N'economic activity', N'office for national statistics', N'house price index yoy', N'http://www.investing.com/economic-calendar/house-price-index-313')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'28609eeb-c604-41cc-974e-17a6127085e9', N'u.s. new home sales mom', N'economic activity', N'us department of commerce', N'new home sales mom', N'http://www.investing.com/economic-calendar/new-home-sales-896')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'685285ca-317b-4210-ab7a-1855b533dee0', N'canada core retail sales mom', N'economic activity', N'statistics canada', N'core retail sales mom', N'http://www.investing.com/economic-calendar/core-retail-sales-65')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ec83136a-40b1-49b5-b3af-189e5fd96a1a', N'adp national employment report', N'employment', N'adp research institute', N'adp national employment report', N'http://www.investing.com/economic-calendar/adp-nonfarm-employment-change-1')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f5881ba4-861b-4f9f-969f-1975f20ded3b', N'canada core consumer price index (cpi) mom', N'inflation', N'statistics canada', N'core consumer price index mom', N'http://www.investing.com/economic-calendar/core-cpi-57')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd78e20a4-035f-4b7e-9dfe-19a1403c25d6', N'u.s. existing home sales', N'economic activity', N'national association of realtors', N'existing home sales', N'http://www.investing.com/economic-calendar/existing-home-sales-99')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f4939359-8ae3-4b8a-916a-1aaa4ec51360', N'australia full employment change', N'employment', N'australian bureau of statistics', N'full employment change', N'http://www.investing.com/economic-calendar/full-employment-change-1052')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b80666ad-e9df-4f64-a0b0-1af4682d2e78', N'china caixin manufacturing pmi', N'economic activity', N'markit', N'hsbc manufacturing pmi', N'http://www.investing.com/economic-calendar/chinese-caixin-manufacturing-pmi-753')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'34db6771-f27e-4193-ae1e-1c2a5008acd7', N'germany retail sales mom', N'economic activity', N'destatis', N'retail sales mom', N'http://www.investing.com/economic-calendar/german-retail-sales-138')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fa78f35a-a42d-4158-86ee-1da1b338ebe6', N'australia wage price index yoy', N'inflation', N'australian bureau of statistics', N'wage price index yoy', N'http://www.investing.com/economic-calendar/wage-price-index-1019')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ab43eb17-7d1f-460a-9bb5-1de3ee4b1f5a', N'u.k. consumer price index (cpi) yoy', N'confidence index', N'office for national statistics', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-67')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e3e3beb7-e419-4d06-9f83-1e265e78db73', N'japan tankan large manufacturers index', N'economic activity', N'bank of japan', N'tankan large manufacturers index', N'http://www.investing.com/economic-calendar/tankan-large-manufacturers-index-279')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8c969b4e-1ac5-481f-b991-1eed17738132', N'bank of canada monetary policy report', N'central bank', N'boc', N'bank of canada monetary policy report', N'http://www.investing.com/economic-calendar/boc-monetary-policy-report-326')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'18e066bb-44fe-4397-bfb3-1f1c824bf112', N'canada foreign securities purchases', N'central bank', N'statistics canada', N'foreign securities purchases', N'http://www.investing.com/economic-calendar/foreign-securities-purchases-109')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'96037471-806b-4691-bde5-1f1f6c33cc2c', N'canada labor productivity qoq', N'economic activity', N'statistics canada', N'labor productivity qoq', N'http://www.investing.com/economic-calendar/labor-productivity-189')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'29038879-5fe4-4110-9169-1f75fe47acd8', N'u.s. house price index mom', N'inflation', N'ofheo', N'house price index mom', N'http://www.investing.com/economic-calendar/house-price-index-327')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'53fc2ba0-7188-4189-ad10-1fd9852c911b', N'speaks', N'none', N'none', N'speech', N'')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4102a99e-ce24-4592-bf99-2109b12351db', N'france jobseekers total', N'employment', N'minist�re du travail et de..', N'jobseekers total', N'http://www.investing.com/economic-calendar/france-jobseekers-total-1623')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f744a118-93a2-4c17-94b8-21317f03c6ba', N'eurogroup meetings', N'central bank', N'eurogroup', N'eurogroup meeting', N'http://www.investing.com/economic-calendar/eurogroup-meetings-1643')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2eff6972-830c-4315-a980-21db6c558bac', N'new zealand gross domestic product (gdp) annual average', N'economic activity', N'statistics new zealand', N'gross domestic product annual average', N'http://www.investing.com/economic-calendar/gdp---annual-average-1458')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eb4f023c-b58f-44b6-b7e9-22650d27a124', N'china consumer price index (cpi) yoy', N'inflation', N'national bureau of statistics of china', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/chinese-cpi-459')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9f0f8fc1-a528-45ab-b021-22af37f9ce1c', N'u.s. producer price index (ppi) mom', N'inflation', N'bureau of labor statistics', N'producer price index mom', N'http://www.investing.com/economic-calendar/ppi-238')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'78208827-819f-4ae4-93f4-22cb98c65b1b', N'eurozone m3 money supply yoy', N'central bank', N'ecb', N'm3 monetary aggregates yoy', N'http://www.investing.com/economic-calendar/m3-money-supply-198')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a2d4a182-bc36-462b-9e1a-232ca9cb75f1', N'new zealand gross domestic product (gdp) yoy', N'economic activity', N'statistics new zealand', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/gdp---annual-1457')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2cce64fb-19b9-4e42-93c3-24a10cd4a86e', N'japan all industries activity index mom', N'economic activity', N'ministry of economy', N'all industries activity index mom', N'http://www.investing.com/economic-calendar/all-industries-activity-index-4')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9b1328cc-3606-46c7-ac92-26a0b161fd84', N'u.s. ism manufacturing pmi', N'economic activity', N'ism institute for supply management', N'ism manufacturing pmi', N'http://www.investing.com/economic-calendar/ism-manufacturing-pmi-173')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'565e19cf-4f05-4f8f-9b08-26b9b0e4cabb', N'germany retail sales yoy', N'economic activity', N'destatis', N'retail sales yoy', N'http://www.investing.com/economic-calendar/german-retail-sales-740')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2f7f68b7-f723-412c-89b8-26c31a34b284', N'australia aig services index', N'economic activity', N'australian industry group', N'aig services index', N'http://www.investing.com/economic-calendar/aig-services-index-347')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'bcec9de3-cefb-4e46-849b-26d006274b73', N'u.s. ism non-manufacturing employment', N'employment', N'institute of supply management', N'ism non-manufacturing pmi', N'http://www.investing.com/economic-calendar/ism-non-manufacturing-employment-1048')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9ebdebe2-ed49-44e1-b64d-26f68f6a03db', N'germany factory orders mom', N'economic activity', N'bundesministerium', N'factory orders mom', N'http://www.investing.com/economic-calendar/german-factory-orders-130')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eea54926-52be-406c-a9cd-26f8134aa556', N'u.s. ny empire state manufacturing index', N'economic activity', N'federal reserve bank of new york', N'empire state manufacturing index', N'http://www.investing.com/economic-calendar/ny-empire-state-manufacturing-index-323')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'cc076ce6-3b7b-4696-b1e3-27d3dd2593da', N'australia gross domestic product (gdp) capital expenditure', N'economic activity', N'australian bureau of statistics', N'gross domestic product capital expenditure', N'http://www.investing.com/economic-calendar/gdp-capital-expenditure-1460')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c2463efb-293d-4989-b9a4-2a2f6ce3bac4', N'new zealand electronic card retail sales mom', N'economic activity', N'statistics new zealand', N'electronic card retail sales mom', N'http://www.investing.com/economic-calendar/electronic-card-retail-sales-1058')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'711674b4-f9ec-42a2-986e-2a453c69bf86', N'u.k. manufacturing production yoy', N'economic activity', N'office for national statistics', N'manufacturing production yoy', N'http://www.investing.com/economic-calendar/manufacturing-production-845')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'48e7e387-c309-4256-b83b-2acec720c825', N'u.s. existing home sales mom', N'economic activity', N'national association of realtors', N'existing home sales mom', N'http://www.investing.com/economic-calendar/existing-home-sales-891')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b820d867-d752-4d7c-a213-2b7b2518cd67', N'china imports yoy', N'economic activity', N'national bureau of statistics', N'imports yoy', N'http://www.investing.com/economic-calendar/chinese-imports-867')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'25b32593-7dc5-41c2-8431-2dc61eb87471', N'australia producer price index (ppi) qoq', N'inflation', N'australian bureau of statistics', N'producer price index input qoq', N'http://www.investing.com/economic-calendar/ppi-239')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'39e1a2d4-6870-445b-8de8-2e0b313a68cb', N'u.s. chicago fed national activity', N'central bank', N'fomc', N'chicago fed national activity', N'http://www.investing.com/economic-calendar/chicago-fed-national-activity-523')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'352b08cf-be0a-4714-be8e-2f1aaa159f79', N'japan imports yoy', N'economic activity', N'ministry of finance', N'imports yoy', N'http://www.investing.com/economic-calendar/imports-996')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'22ee48c3-de77-49bf-bd65-33e35c30e38c', N'japan national consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'national consumer price index yoy', N'http://www.investing.com/economic-calendar/national-cpi-992')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4bcffa9d-6fa3-4931-a981-34120d9a417e', N'new zealand gross domestic product (gdp) qoq', N'economic activity', N'statistics new zealand', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/gdp-125')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b364453b-ae40-4801-90ad-34a8f00122ac', N'bank of japan quarterly outlook report', N'economic activity', N'bank of japan', N'bank of japan quarterly outlook report', N'http://www.investing.com/economic-calendar/boj-outlook-report-1668')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'662923a3-f93a-42c8-abbd-351048f66520', N'french non-farm payrolls', N'employment', N'insee', N'nonfarm payrolls', N'http://www.investing.com/economic-calendar/french-non-farm-payrolls-339')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ab4223fd-b214-4a58-af58-39325e186c85', N'u.k. boe qe total', N'central bank', N'boe', N'quantitative easing', N'http://www.investing.com/economic-calendar/boe-qe-total-665')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'50a5cc3f-f220-48ec-bfad-39759fde8db4', N'canada full employment change', N'employment', N'statictics canada', N'full employment change', N'http://www.investing.com/economic-calendar/full-employment-change-1021')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e66964ad-1126-4485-8dd9-3a77c5ae2e09', N'u.s. redbook mom', N'economic activity', N'redbook research ', N'redbook index mom', N'http://www.investing.com/economic-calendar/redbook-655')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd769d12d-c880-4823-959e-3a8815c27884', N'average hourly earnings mom', N'employment', N'bureau of labor statistics', N'average hourly earnings mom', N'http://www.investing.com/economic-calendar/average-hourly-earnings-8')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e1c9cc29-a183-4435-9c7f-3c1f710217ce', N'germany industrial production mom', N'economic activity', N'destatis', N'industrial production mom', N'http://www.investing.com/economic-calendar/german-industrial-production-135')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'49617d0d-e9ab-420e-be4d-3d9dd208dc2b', N'australia hia new home sales mom', N'economic activity', N'housing industry association', N'hia new home sales mom', N'http://www.investing.com/economic-calendar/hia-new-home-sales-377')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'be8598c8-b506-49ea-8cc4-3ef9f4caefe5', N'australia anz internet job ads mom', N'employment', N'austrailia and new zealand banking group', N'internet job adverts mom', N'http://www.investing.com/economic-calendar/anz-internet-job-ads-1520')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'16bb5511-b06e-4192-b3b0-3fac5cd7c9f1', N'u.s. house price index yoy', N'inflation', N'ofheo', N'house price index yoy', N'http://www.investing.com/economic-calendar/house-price-index-897')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f6547a49-6145-498c-866b-3faeb3d007a3', N'eurozone core consumer price index (cpi) mom', N'inflation', N'eurostat', N'core consumer price index mom', N'http://www.investing.com/economic-calendar/core-cpi-922')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'94a160e1-5f92-4f65-a216-3fb7dba54edb', N'eurozone selling price expectations', N'inflation', N'eurostat', N'selling price expectations', N'http://www.investing.com/economic-calendar/european-selling-price-expectations-1572')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a4d06452-3f87-4958-97d8-41db6bcfe300', N'canada industrial product price index (ippi) mom', N'inflation', N'statistics canada', N'industrial product price index mom', N'http://www.investing.com/economic-calendar/ippi-172')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'162e396f-eaf0-4513-85da-4246ae4f14e5', N'people''s bank of china reserve requirement ratio', N'central bank', N'pboc', N'requirement ratio', N'http://www.investing.com/economic-calendar/pboc-reserve-requirement-ratio-1084')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'29163b59-adf1-4564-9eb1-44a74f1d4a5b', N'china non-manufacturing pmi', N'economic activity', N'china federation of logistics', N'non manufacturing pmi', N'http://www.investing.com/economic-calendar/chinese-non-manufacturing-pmi-831')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7df1c3e3-0339-42c2-9d9f-45eb08fcf716', N'australia wage price index qoq', N'inflation', N'australian bureau of statistics', N'wage price index qoq', N'http://www.investing.com/economic-calendar/wage-price-index-1020')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ffe35656-47ca-441c-917d-45fd61f811d5', N'u.k. industrial production mom', N'economic activity', N'office for national statistics', N'industrial production mom', N'http://www.investing.com/economic-calendar/industrial-production-158')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ea21f28b-1e26-4599-9252-462d1778e4f9', N'canada interest rate decision', N'central bank', N'boc', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-166')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2f358bdc-20cc-4845-a506-4675c97ce6c1', N'unit labor costs qoq', N'employment', N'bureau of labor statistics of the us department of', N'unit labor costs qoq', N'http://www.investing.com/economic-calendar/unit-labor-costs-303')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fbf4d816-5823-4889-8997-46cd8982cccd', N'japan tertiary industry activity index mom', N'economic activity', N'meti', N'tertiary industry activity index mom', N'http://www.investing.com/economic-calendar/tertiary-industry-activity-index-282')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c3ef0a9c-0d45-4892-8142-47b2a2fd904f', N'australia construction work done qoq', N'economic activity', N'australian bureau of statistics', N'construction work done qoq', N'http://www.investing.com/economic-calendar/construction-work-done-46')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'02c50ab7-9a54-44cd-8691-493166d7fd5c', N'u.k. consumer price index (cpi) mom', N'confidence index', N'office for national statistics', N'consumer price index mom', N'http://www.investing.com/economic-calendar/cpi-727')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5f5b6a2d-fe95-40a6-a873-49792b9f781d', N'u.k. gross domestic product (gdp) yoy', N'economic activity', N'office for national statistics', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/gdp-728')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'federal open market committee (fomc) meeting minutes', N'central bank', N'fomc', N'meeting minutes', N'http://www.investing.com/economic-calendar/fomc-meeting-minutes-108')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8beffdd0-6d7b-48c0-9a63-4c85453b0cf1', N'u.k. core rpi mom', N'economic activity', N'office for national statistics', N'core retail price index mom', N'http://www.investing.com/economic-calendar/core-rpi-854')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b1a68cd7-a426-42fe-bbec-4e97b32050dc', N'new zealand inflation expectations qoq', N'inflation', N'statistics new zealand', N'inflation expectations qoq', N'http://www.investing.com/economic-calendar/inflation-expectations-342')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'95e052e6-d098-4b77-b34d-4f592c0213ee', N'claimant count change', N'employment', N'uk office of national statistics', N'claimant count change', N'http://www.investing.com/economic-calendar/claimant-count-change-39')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'28c3aea9-3630-4d0e-9171-505c81440728', N'u.s. services pmi', N'economic activity', N'markit', N'services pmi', N'http://www.investing.com/economic-calendar/services-pmi-1062')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'04f77357-d988-4485-ac92-50e492003990', N'brazil auto production mom', N'economic activity', N'anfavea', N'auto production mom', N'http://www.investing.com/economic-calendar/brazilian-auto-output-1438')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5255491b-d28c-4407-b7bf-520345b6585d', N'new zealand manufacturing sales volume qoq', N'economic activity', N'statistics new zealand', N'manufacturing sales volume qoq', N'http://www.investing.com/economic-calendar/manufacturing-sales-206')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f8b2b1c6-ad67-4840-b5b9-52441fe5a945', N'u.s. cushing crude oil inventories', N'economic activity', N'energy information administration', N'cushing crude oil inventories', N'http://www.investing.com/economic-calendar/eia-weekly-cushing-oil-inventories-1657')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e7d840dd-1099-4e9d-9a90-530044e9ed9e', N'average earnings index', N'employment', N'uk office of national statistics', N'average earnings index', N'http://www.investing.com/economic-calendar/average-earnings-index-bonus-7')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5028acbc-fa5f-44f6-8843-53ab610afcb9', N'france unemployment rate', N'employment', N'unknown', N'unemployment rate', N'http://www.investing.com/economic-calendar/french-unemployment-rate-397')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd8e688bf-69a0-4a71-9681-5447f07b1bcf', N'u.s. api weekly distillates stocks', N'economic activity', N'american petroleum institute', N'crude oil storage', N'http://www.investing.com/economic-calendar/api-weekly-distillates-stocks-1035')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eccc94af-ffb4-4c76-8780-564c9b3cde2c', N'eurozone markit composite pmi', N'economic activity', N'markit', N'composite pmi', N'http://www.investing.com/economic-calendar/markit-composite-pmi-1491')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b84d51cf-75af-4d97-82b9-583675756008', N'u.s. cb leading index mom', N'economic activity', N'the conference board', N'cb leading index mom', N'http://www.investing.com/economic-calendar/cb-leading-index-194')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'20ed98e3-c761-40bd-8f4d-5866d56d1fec', N'china gross domestic product (gdp) qoq', N'economic activity', N'national bureau of statistics', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/chinese-gdp-868')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8d63b4a9-22e8-481a-8359-5a52d7c6c1ad', N'germany manufacturing pmi', N'economic activity', N'markit', N'manufacturing pmi', N'http://www.investing.com/economic-calendar/german-manufacturing-pmi-136')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'81ddd8f8-6050-4e93-abc6-5ab83af485bb', N'u.s. chain store sales yoy', N'economic activity', N'international council of shopping centers', N'chain store sales yoy', N'http://www.investing.com/economic-calendar/chain-store-sales-482')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'78f62fb0-ffc4-4b57-9a00-5b7c54595422', N'deposit facility rate', N'central bank', N'ecb', N'facility rate deposit', N'http://www.investing.com/economic-calendar/deposit-facility-rate-1655')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e36259b6-c0d7-4b0f-977a-5c5d2042481c', N'u.s. chain store sales wow', N'economic activity', N'international council of shopping centers', N'chain store sales wow', N'http://www.investing.com/economic-calendar/chain-store-sales-915')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'bceb6d81-5ae0-49c6-8cc3-5ccf533aa79f', N'germany unemployment rate', N'employment', N'german federal statistical office', N'unemployment rate', N'http://www.investing.com/economic-calendar/german-unemployment-rate-142')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'755e940f-13df-4ca3-b003-5d5bace439e4', N'brazil consumer price index (cpi) yoy', N'inflation', N'ibge', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/brazilian-cpi-410')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'da96b696-9637-4c19-a3c4-5decca60ce32', N'germany producer price index (ppi) mom', N'inflation', N'destatis', N'producer price index input mom', N'http://www.investing.com/economic-calendar/german-ppi-137')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'3b4ce17c-e54f-44ac-9a98-5f8b32a7d40c', N'u.s. housing starts mom', N'economic activity', N'census bureau', N'housing starts mom', N'http://www.investing.com/economic-calendar/housing-starts-898')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'838116a8-bfef-4edd-a9ac-5fce901232c3', N'new zealand business nz pmi', N'economic activity', N'business nz', N'business nz pmi', N'http://www.investing.com/economic-calendar/business-nz-pmi-338')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e0e7e770-3e1b-4800-967e-5fe3e84ec125', N'u.s. philadelphia fed business conditions', N'economic activity', N'federal reserve bank of philadelphia', N'philadelphia fed business conditions', N'http://www.investing.com/economic-calendar/philly-fed-business-conditions-907')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8c4cf687-07df-43b8-89be-625c47b34372', N'australia new motor vehicle sales mom', N'economic activity', N'australian bureau of statistics', N'new motor vehicle sales mom', N'http://www.investing.com/economic-calendar/new-motor-vehicle-sales-225')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fd9aa7d9-c4f7-4abb-8f69-669fb93d497e', N'japan monetary base yoy', N'central bank', N'boj', N'monetary base yoy', N'http://www.investing.com/economic-calendar/monetary-base-209')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'83c9c44b-303b-40e2-b84b-67ec957ad3e9', N'u.s. housing starts', N'economic activity', N'census bureau', N'housing starts', N'http://www.investing.com/economic-calendar/housing-starts-151')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c66e2bd3-eaab-45bf-bed7-68061f9a96b3', N'reserve bank of australia bulletin', N'central bank', N'reserve bank of australia', N'reserve bank of australia bulletin', N'http://www.investing.com/economic-calendar/rba-bulletin-254')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'96e47c1b-241e-4c81-bc37-680f31b5fea0', N'brazil ipc-fipe inflation index mom', N'inflation', N'fipe', N'fipe inflation index mom', N'http://www.investing.com/economic-calendar/brazilian-ipc-fipe-inflation-index-1562')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'139c5f89-32cd-4296-b87a-69e0249c3479', N'canada wholesale sales mom', N'economic activity', N'statistics canada', N'wholesale sales mom', N'http://www.investing.com/economic-calendar/wholesale-sales-306')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6c43fb42-433e-40f2-acd1-6a7657f41154', N'china producer price index (ppi) yoy', N'inflation', N'national bureau of statistics of china', N'producer price index yoy', N'http://www.investing.com/economic-calendar/chinese-ppi-464')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'533276fe-2899-453c-8817-6abd3f7a4a75', N'australia house price index (hpi) qoq', N'economic activity', N'australian bureau of statistics', N'house price index qoq', N'http://www.investing.com/economic-calendar/house-price-index-147')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8702716e-45d6-4c0d-9c94-6b0b8326c86d', N'japan interest rate decision', N'central bank', N'boj', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-165')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0fdf10c8-382b-47f3-95cc-6bd66ea13aca', N'u.k. core retail sales yoy', N'economic activity', N'office for national statistics', N'core retail sales yoy', N'http://www.investing.com/economic-calendar/core-retail-sales-857')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'cf0cfa69-0f5c-4ab0-ad06-6c9f6cc4348d', N'china fixed asset investment yoy', N'economic activity', N'national bureau of statistics', N'fixed asset investment yoy', N'http://www.investing.com/economic-calendar/chinese-fixed-asset-investment-460')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7ad1dbb3-525b-4d2f-8047-6f04117eb413', N'u.s. core retail sales mom', N'economic activity', N'census bureau', N'core retail sales mom', N'http://www.investing.com/economic-calendar/core-retail-sales-63')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8ac95f43-c6e8-43cb-8a6f-6f32734c2b5b', N'australia mi leading index mom', N'economic activity', N'australian industry group', N'mi leading index mom', N'http://www.investing.com/economic-calendar/mi-leading-index-308')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4ac9ff21-0a5e-4d00-ae3f-6fd372a89554', N'u.k. m4 money supply mom', N'central bank', N'boe', N'm4 money supply mom', N'http://www.investing.com/economic-calendar/m4-money-supply-199')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1bc71423-3950-4ab6-a39d-7013edc1c384', N'japan capacity utilization mom', N'economic activity', N'ministry of economy', N'capacity utilization mom', N'http://www.investing.com/economic-calendar/capacity-utilization-989')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ce90cebd-e2b8-4d3d-be91-701a564a9c3a', N'u.s. producer price index (ppi) yoy', N'inflation', N'bureau of labor statistics', N'producer price index yoy', N'http://www.investing.com/economic-calendar/ppi-734')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd2c0343a-d6d1-46a6-9d8b-740047ecc415', N'continuing jobless claims', N'employment', N'united states department of labor', N'continuing jobless claims', N'http://www.investing.com/economic-calendar/continuing-jobless-claims-522')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eb3c3024-ffe3-4346-9f15-775e75d4758a', N'bank of japan monthly report', N'central bank', N'boj', N'monthly report', N'http://www.investing.com/economic-calendar/boj-monthly-report-319')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eb38296d-d259-4cca-b315-775f1cf945bd', N'reserve bank of australia financial stability review', N'central bank', N'reserve bank of australia', N'reserve bank of australia financial stability review', N'http://www.investing.com/economic-calendar/rba-financial-stability-review-358')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'533481db-edbf-4268-a7ff-775f41fdf8c5', N'u.k. gross domestic product (gdp) qoq', N'economic activity', N'office for national statistics', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/gdp-121')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b8b6148d-0af3-4fab-8279-784ecbec973c', N'canada gross domestic product (gdp) annualized qoq', N'economic activity', N'statistics canada', N'gross domestic product annualized qoq', N'http://www.investing.com/economic-calendar/gdp-annualized-1025')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8a266789-0d8f-46f7-8b61-79bac99c9a57', N'canada unemployment rate', N'employment', N'statictics canada', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-301')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'23b24108-571e-4455-a68b-79da073d4e53', N'canada gross domestic product (gdp) qoq', N'economic activity', N'statistics canada', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/gdp-1024')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c16756dd-91a0-4ce6-9ebe-7a4cd2822196', N'canada consumer price index (cpi) mom', N'inflation', N'statistics canada', N'consumer price index mom', N'http://www.investing.com/economic-calendar/cpi-70')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'698a344a-05e9-4701-b0bf-7a670771c916', N'australia consumer price index (cpi) yoy', N'inflation', N'australian bureau of statistics', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-1011')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'3dd0cc44-9d77-4661-9bb5-7b0087a8755d', N'u.s. real personal consumption mom', N'economic activity', N'us department of commerce', N'real personal consumption mom', N'http://www.investing.com/economic-calendar/real-personal-consumption-895')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2b11c657-94a9-4df9-bcba-7cbbd69e457f', N'reserve bank of australia meeting minutes', N'central bank', N'reserve bank of australia', N'meeting minutes', N'http://www.investing.com/economic-calendar/monetary-policy-meeting-minutes-391')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'547c9219-3870-4718-afbc-7d8d591f9fd9', N'u.s. industrial production mom', N'economic activity', N'federal reserve ', N'industrial production mom', N'http://www.investing.com/economic-calendar/industrial-production-161')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd62fc6de-6b98-4926-9a40-7dfde1c16a99', N'u.k. core producer price index (ppi) output yoy', N'inflation', N'office for national statistics', N'core producer price index output yoy', N'http://www.investing.com/economic-calendar/core-ppi-output-852')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c3bee6e4-cfb5-4712-8822-7e47f7636d01', N'u.k. cbi industrial trends orders', N'economic activity', N'confederation of british industry', N'cbi industrial trends orders', N'http://www.investing.com/economic-calendar/cbi-industrial-trends-orders-34')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5848d645-d029-4ae3-9dee-7e5738163b82', N'u.s. producer price index (ppi) input mom', N'inflation', N'bureau of labor statistics', N'producer price index input mom', N'http://www.investing.com/economic-calendar/core-ppi-62')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'3b775fcf-a36d-4cea-84af-7e6a4bd76be1', N'new zealand interest rate decision', N'central bank', N'rbnz', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-167')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'772bbdc3-df37-4c15-bd5e-7f4c4eae3e7d', N'eurozone consumer price index (cpi) yoy', N'inflation', N'eurostat', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-68')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4a0c0095-313e-49ae-bf67-7f54993bdb6c', N'u.s. factory orders mom', N'economic activity', N'census bureau', N'factory orders mom', N'http://www.investing.com/economic-calendar/factory-orders-100')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8856236b-5755-44c9-b6a8-808fe22b842a', N'ecb publishes account of monetary policy meeting', N'central bank', N'ecb', N'monetary policy meeting publication', N'http://www.investing.com/economic-calendar/ecb-publishes-account-of-monetary-policy-meeting-1610')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'72871929-1845-431c-a8d6-81cbd18eac6c', N'eurozone money-m3 3-month-moving average', N'central bank', N'ecb', N'm3 monetary aggregates', N'http://www.investing.com/economic-calendar/money-m3-3-month-moving-average-1401')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'273cb74f-9897-4448-a50c-831a67df5484', N'u.s. federal reserve''s federal open market committee (fomc) statement', N'central bank', N'fomc', N'fomc statement', N'http://www.investing.com/economic-calendar/fomc-statement-398')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1ee19e13-5e91-4750-9cf7-836b0614fe57', N'australia aig manufacturing index', N'economic activity', N'australian industry group', N'aig manufacturing index', N'http://www.investing.com/economic-calendar/aig-manufacturing-index-203')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f898a05a-0cd7-4e89-9950-8453dc2c987c', N'canada rbc manufacturing pmi', N'economic activity', N'rbc', N'rbc manufacturing pmi', N'http://www.investing.com/economic-calendar/rbc-manufacturing-pmi-1029')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ff48a987-c699-4ff7-9284-84557b29bef2', N'brazil fgv consumer confidence', N'economic activity', N'fgv', N'consumer confidence', N'http://www.investing.com/economic-calendar/fgv-consumer-confidence-860')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'258d07b6-c3b6-4db8-b168-847ad1d0c9fe', N'new zealand imports', N'economic activity', N'statistics new zealand', N'imports', N'http://www.investing.com/economic-calendar/imports-1066')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ae263d42-5923-4c22-b487-88499b6b8915', N'japan national core consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'national core consumer price index yoy', N'http://www.investing.com/economic-calendar/national-core-cpi-344')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1298dda8-25e8-4ce8-a96d-8907711c3aea', N'canada industrial product price index (ippi) yoy', N'inflation', N'statistics canada', N'industrial product price index yoy', N'http://www.investing.com/economic-calendar/ippi-742')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'03c445f9-ea3f-4026-b718-89f805996307', N'eurozone business climate', N'economic activity', N'economic & financial affair', N'business climate', N'http://www.investing.com/economic-calendar/business-climate-918')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6dbcf645-c4b9-4ee8-9ef7-8a6d852028ad', N'u.k. cb leading index mom', N'economic activity', N'the conference board', N'cb leading index mom', N'http://www.investing.com/economic-calendar/leading-index-192')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'80d3fb72-f06a-444c-bd50-8b2729e22056', N'u.s. core consumer price index (cpi) index', N'confidence index', N'bureau of labor statistics', N'consumer price index', N'http://www.investing.com/economic-calendar/core-cpi-index-1226')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e6f3c172-a8f1-46f9-84f3-8b789bc9524b', N'canada capacity utilization rate', N'economic activity', N'statistics canada', N'capacity utilization rate', N'http://www.investing.com/economic-calendar/capacity-utilization-rate-376')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2e1430d3-3410-4d1c-a73d-8bd92348e738', N'u.s. philadelphia fed manufacturing index', N'economic activity', N'federal reserve bank of philadelphia', N'philadelphia federal reserve manufacturing index', N'http://www.investing.com/economic-calendar/philadelphia-fed-manufacturing-index-236')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f5c0d90b-a1da-41c2-9af2-8d830b2e8dd6', N'new zealand consumer price index (cpi) yoy', N'inflation', N'statistics new zealand', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-1063')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'97e64b92-31db-4711-8a78-8da09a158cda', N'japan construction orders yoy', N'economic activity', N'ministry of land', N'construction orders yoy', N'http://www.investing.com/economic-calendar/construction-orders-991')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'68b2fbb7-85e2-4fa4-984a-8dab2396b071', N'u.s. building permits', N'economic activity', N'census bureau', N'building permits', N'http://www.investing.com/economic-calendar/building-permits-25')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'646e336d-f329-45df-b0c2-8dd2c39241df', N'japan capital spending yoy', N'economic activity', N'ministry of finance', N'capital spending yoy', N'http://www.investing.com/economic-calendar/capital-spending-32')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'21d5eb94-1a59-43cd-8969-8f0ecb5748d8', N'japan average cash earnings yoy', N'employment', N'ministry of welfare', N'average cash earnings yoy', N'http://www.investing.com/economic-calendar/average-cash-earnings-6')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8caa9dc1-989d-4153-80fc-90018af24d07', N'canada part time employment change', N'employment', N'statictics canada', N'part time employment change', N'http://www.investing.com/economic-calendar/part-time-employment-change-1022')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2a46dd49-6e00-4b32-884f-90932a08a844', N'u.s. quantitative easing (qe) total', N'central bank', N'fomc', N'quantitative easing total', N'http://www.investing.com/economic-calendar/qe-total-1405')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'63ed024a-aaab-4eb1-8092-913b0a740863', N'germany import price index qoq', N'inflation', N'destatis', N'import price index mom', N'http://www.investing.com/economic-calendar/german-import-price-index-134')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'01da4bfd-f38f-4ad3-bc61-9250cfbefb16', N'germany gross domestic product (gdp) qoq', N'economic activity', N'destatis', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/german-gdp-131')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'433d1994-8a77-4488-b0ca-92d71c04effa', N'eurozone gross domestic product (gdp) qoq', N'economic activity', N'eurostat', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/gdp-120')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6ff2306f-30bc-4214-b5e1-92eac8b706c9', N'canada new housing price index mom', N'inflation', N'statistics canada', N'new housing price index mom', N'http://www.investing.com/economic-calendar/new-housing-price-index-223')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e1aecb6a-6b1e-4ff8-96f5-92eaddde2cfa', N'china caixin services pmi', N'economic activity', N'markit', N'hsbc services pmi', N'http://www.investing.com/economic-calendar/chinese-caixin-services-pmi-596')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ca389442-ec4b-4a15-b32d-930738d97383', N'u.s. retail sales mom', N'economic activity', N'census bureau', N'retail sales mom', N'http://www.investing.com/economic-calendar/retail-sales-256')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7506491b-314e-4986-a9f6-95052bcce786', N'australia employment change', N'employment', N'australian bureau of statistics', N'employment change', N'http://www.investing.com/economic-calendar/employment-change-94')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6de1400b-4aff-468f-9144-95288a5c8043', N'new zealand globaldairytrade price index', N'inflation', N'globaldairytrade', N'globaldairytrade price index', N'http://www.investing.com/economic-calendar/globaldairytrade-price-index-1654')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2b9ebc57-b1cb-4805-a163-96a10a3348c2', N'japan gross domestic product (gdp) yoy', N'economic activity', N'cabinet office', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/gdp-1053')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2b669912-3fdb-428f-ae7b-9702775396c7', N'brazil consumer price index (cpi) mom', N'inflation', N'ibge', N'consumer price index mom', N'http://www.investing.com/economic-calendar/brazilian-cpi-1165')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f80b0d76-5607-4356-8653-97670d6047ae', N'challenger job cuts, released', N'employment', N'challenger gray & christmas', N'challenger job cuts', N'http://www.investing.com/economic-calendar/challenger-job-cuts-888')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ad8d2475-e1de-471f-868e-97a6adf161fa', N'brazil bank lending mom', N'economic activity', N'ibge', N'bank lending mom', N'http://www.investing.com/economic-calendar/brazilian-bank-lending-761')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4c69182c-8c7e-451f-9b56-97ce0a570fb6', N'japan corporate goods price index (cgpi) mom', N'inflation', N'bank of japan', N'corporate goods price index mom', N'http://www.investing.com/economic-calendar/cgpi-990')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'76e839e3-0e00-4fed-a461-99188005d223', N'u.s. producer price index (ppi) input yoy', N'inflation', N'bureau of labor statistics', N'producer price index input yoy', N'http://www.investing.com/economic-calendar/core-ppi-735')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'cd9d6e42-6794-4988-826e-994209d0276c', N'brazil markit services pmi', N'economic activity', N'markit', N'services pmi', N'http://www.investing.com/economic-calendar/brazilian-markit-services-pmi-1477')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'057cd4af-0a6a-46d5-af88-99f8e8278ddf', N'u.s. redbook yoy', N'economic activity', N'redbook research ', N'redbook index yoy', N'http://www.investing.com/economic-calendar/redbook-911')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'bed520c2-5b46-449a-be61-9a5416f777cb', N'germany unemployment', N'employment', N'german federal statistical office', N'unemployment rate', N'http://www.investing.com/economic-calendar/german-unemployment-total-1527')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'870decc6-81d9-4b7c-928f-9bce60a44f52', N'u.k. rics house price balance', N'economic activity', N'rics', N'rics house price balance', N'http://www.investing.com/economic-calendar/rics-house-price-balance-264')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f1f6d230-7a9d-4e2a-a110-9d0d9c0eb472', N'australia gross domestic product (gdp) qoq', N'economic activity', N'australian bureau of statistics', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/gdp-124')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c036a165-e5e3-4d6b-b0d5-9d85cd6bd543', N'canada building permits mom', N'economic activity', N'statistics canada', N'building permits mom', N'http://www.investing.com/economic-calendar/building-permits-24')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1a2f0275-4083-4dd5-8ac3-9eb8afbe16a4', N'u.k. producer price index (ppi) input yoy', N'inflation', N'office for national statistics', N'producer price index input yoy', N'http://www.investing.com/economic-calendar/ppi-input-729')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4998e15a-d479-4435-a843-9f2e3dcb071f', N'china consumer price index (cpi) mom', N'inflation', N'national bureau of statistics of china', N'consumer price index mom', N'http://www.investing.com/economic-calendar/chinese-cpi-743')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7c552a7b-55d9-4ca4-843b-9f70af7b05ac', N'u.s. durable goods orders mom', N'economic activity', N'census bureau', N'durable goods orders mom', N'http://www.investing.com/economic-calendar/durable-goods-orders-86')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0f427574-dbab-47bd-82ad-a02936c99651', N'eurozone interest rate decision', N'central bank', N'ecb', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-164')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4d25cfc8-1552-4780-83b3-a04f7a4f15dc', N'australia trimmed mean consumer price index (cpi) qoq', N'inflation', N'australian bureau of statistics', N'trimmed mean consumer price index qoq', N'http://www.investing.com/economic-calendar/trimmed-mean-cpi-293')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4593f190-8952-4e84-9b6d-a08c0693ef8f', N'new zealand retail sales qoq', N'economic activity', N'statistics new zealand', N'retail sales qoq', N'http://www.investing.com/economic-calendar/retail-sales-257')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'05a6be50-5be3-464f-8b12-a0d56c56d6f9', N'u.k. core rpi yoy', N'economic activity', N'office for national statistics', N'core retail price index yoy', N'http://www.investing.com/economic-calendar/core-rpi-855')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c0bc252e-d0b4-44c5-8d07-a1717091c0ae', N'japan tankan large non-manufacturers index', N'economic activity', N'bank of japan', N'tankan large non manufacturers index', N'http://www.investing.com/economic-calendar/tankan-large-non-manufacturers-index-280')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'78686c48-ecbd-4f80-a09e-a188a88a973d', N'u.s. chicago pmi', N'economic activity', N'kingsbury international', N'chicago pmi', N'http://www.investing.com/economic-calendar/chicago-pmi-38')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6eba46f3-4d1b-4383-a43e-a1a62db62f60', N'u.s. markit composite pmi', N'economic activity', N'markit', N'composite pmi', N'http://www.investing.com/economic-calendar/markit-composite-pmi-1492')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'cf7fbf8c-35cf-457b-a3a2-a3581cb781e7', N'australia gross domestic product (gdp) yoy', N'economic activity', N'australian statistician', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/gdp-1013')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0f983d60-4ee2-4ebf-8ee1-a3597035da73', N'u.s. ism non-manufacturing pmi', N'economic activity', N'ism institute for supply management', N'ism non manufacturing pmi', N'http://www.investing.com/economic-calendar/ism-non-manufacturing-pmi-176')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'997e906a-4555-4346-8d7b-a3affbac3892', N'u.s. import price index mom', N'inflation', N'us department of labor', N'import price index mom', N'http://www.investing.com/economic-calendar/import-price-index-153')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'589d1c80-9c9e-4e69-9caf-a4a33e2617e6', N'u.s. core durable goods orders mom', N'economic activity', N'census bureau', N'core durable goods orders mom', N'http://www.investing.com/economic-calendar/core-durable-goods-orders-59')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'33b3ceb4-ca31-4ec5-a4a7-a528e2ad5193', N'people''s bank of china deposit rate', N'central bank', N'pboc', N'deposit rate', N'http://www.investing.com/economic-calendar/pboc-deposit-rate-1082')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ac762fc6-86b9-4819-9b4c-a5cba7df39c9', N'australia private new capital expenditure qoq', N'economic activity', N'australian bureau of statistics', N'private new capital expenditure qoq', N'http://www.investing.com/economic-calendar/private-new-capital-expenditure-252')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'26c9a0f2-5527-4468-997d-a5d1970baf20', N'u.s. new home sales', N'economic activity', N'us department of commerce', N'new home sales', N'http://www.investing.com/economic-calendar/new-home-sales-222')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'39c0e283-8a27-4c3d-8dae-a5eb9c7fbcab', N'japan foreign bonds buying', N'economic activity', N'ministry of finance', N'foreign bonds buying numbers', N'http://www.investing.com/economic-calendar/foreign-bonds-buying-987')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'90ac5a48-ca9e-47b7-b111-a6eb4d770e86', N'china manufacturing pmi', N'economic activity', N'china logistics', N'manufacturing pmi', N'http://www.investing.com/economic-calendar/chinese-manufacturing-pmi-594')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fd4ea5d8-e29a-4b4f-ba9a-a6f37b05477d', N'eurozone services pmi', N'economic activity', N'markit', N'services pmi', N'http://www.investing.com/economic-calendar/services-pmi-272')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b9460c58-084d-48e5-bba1-a6f922c1962d', N'new zealand core retail sales qoq', N'economic activity', N'statistics new zealand', N'core retail sales qoq', N'http://www.investing.com/economic-calendar/core-retail-sales-64')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'81a3e98f-b787-482a-90c7-a784aa4f948a', N'bank of england inflation report', N'inflation', N'bank of england', N'inflation report', N'http://www.investing.com/economic-calendar/boe-inflation-report-15')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c36728ff-08dc-4b02-8fe2-a7fa1781107a', N'eurozone gross domestic product (gdp) yoy', N'economic activity', N'eurostat', N'gross domestic product yoy', N'http://www.investing.com/economic-calendar/gdp-925')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9920fd92-65a8-41fe-9604-a853da4f9c00', N'japan coincident indicator mom', N'economic activity', N'cabinet office', N'coincident indicator mom', N'http://www.investing.com/economic-calendar/coincident-indicator-1441')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f0524154-23df-4ee9-9f4e-a936ded488fb', N'canada participation rate', N'employment', N'statictics canada', N'participation change', N'http://www.investing.com/economic-calendar/participation-rate-670')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'08ac26aa-7e95-4751-82ab-a94a5e81bea1', N'australia gross domestic product (gdp) chain price index', N'economic activity', N'australian bureau of statistics', N'gross domestic product chain price index', N'http://www.investing.com/economic-calendar/gdp-chain-price-index-1461')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'014489fb-6068-47af-ae47-a9fb80747823', N'new zealand building consents mom', N'economic activity', N'statistics new zealand', N'building consents mom', N'http://www.investing.com/economic-calendar/building-consents-23')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6192d99d-c47d-4820-9879-aa37c9c1a604', N'japan core machinery orders yoy', N'economic activity', N'cabinet office', N'core machinery orders yoy', N'http://www.investing.com/economic-calendar/core-machinery-orders-1003')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1265ada3-808f-42d6-b534-aa5a875c7ba7', N'japan household spending mom', N'economic activity', N'statistics bureau', N'household spending mom', N'http://www.investing.com/economic-calendar/household-spending-1191')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f337afe4-e227-4bb9-99df-aaee302425d7', N'u.k. halifax house price index mom', N'economic activity', N'bank of scotland', N'halifax house price index mom', N'http://www.investing.com/economic-calendar/halifax-house-price-index-145')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4959002f-ae02-48eb-bcc0-ab4746ec6fb1', N'u.s. capacity utilization rate', N'economic activity', N'federal reserve ', N'capacity utilization rate', N'http://www.investing.com/economic-calendar/capacity-utilization-rate-31')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'faef10f1-5052-413e-826a-ace8b63c3743', N'brazil gross domestic product (gdp) qoq', N'economic activity', N'ibge', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/brazilian-gdp-858')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a65623c2-e2bd-4577-9dd8-ad5f715a8179', N'u.k. index of services', N'economic activity', N'office for national statistics', N'index of services', N'http://www.investing.com/economic-calendar/index-of-services-155')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4e1b9802-d404-4bbf-b7c2-ad86195bceba', N'japan household spending yoy', N'economic activity', N'statistics bureau', N'household spending yoy', N'http://www.investing.com/economic-calendar/household-spending-361')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'316a91c4-b1f5-4bee-ba49-ae4ffa2c255b', N'brazil caged net payroll jobs', N'employment', N'ibge', N'caged net payroll jobs', N'http://www.investing.com/economic-calendar/brazilian-caged-net-payroll-jobs-1523')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'09868f60-21b2-4a33-aa47-ae7abd8e1346', N'canada ivey pmi', N'economic activity', N'richard ivey school of business', N'ivey pmi', N'http://www.investing.com/economic-calendar/ivey-pmi-185')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a069e67f-f370-48c0-a3da-b1fd8d5db8bb', N'new zealand producer price index (ppi) output qoq', N'inflation', N'statistics new zealand', N'producer price index output qoq', N'http://www.investing.com/economic-calendar/ppi-output-247')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'74468230-0980-4ee2-9785-b289a26ac6bc', N'australia aig construction index', N'economic activity', N'australian industry group', N'aig construction index', N'http://www.investing.com/economic-calendar/aig-construction-index-349')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8d598f7f-05a3-429e-b428-b39837ef0a25', N'australia consumer price index (cpi) index number', N'inflation', N'australian bureau of statistics', N'consumer price index index number', N'http://www.investing.com/economic-calendar/cpi-index-number-1010')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e3ecc58a-293f-438e-85e8-b456336a8cec', N'eurozone industrial sentiment', N'economic activity', N'economic & financial affair', N'industrial sentiment', N'http://www.investing.com/economic-calendar/industrial-sentiment-919')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f1c6ae93-54ed-4bba-9304-b503bf569c07', N'brazil industrial production yoy', N'economic activity', N'ibge', N'industrial production yoy', N'http://www.investing.com/economic-calendar/brazilian-industrial-production-414')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'00c5ce8b-99c1-43c2-86aa-b639296c6181', N'japan gross domestic product (gdp) price index yoy', N'inflation', N'cabinet office', N'gross domestic product price index yoy', N'http://www.investing.com/economic-calendar/gdp-price-index-126')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'64686db8-cfe4-4597-b512-b6f267443c7e', N'reserve bank of new zealand offshore holdings', N'central bank', N'rbnz', N'offshore holdings', N'http://www.investing.com/economic-calendar/rbnz-offshore-holdings-1407')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'3a561940-2d74-43a7-9843-b7b795c93741', N'australia trimmed mean consumer price index (cpi) yoy', N'inflation', N'australian bureau of statistics', N'trimmed mean consumer price index yoy', N'http://www.investing.com/economic-calendar/trimmed-mean-cpi-1017')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1bfa1da1-fb83-40bd-9367-b832ddc893c6', N'brazil interest rate decision', N'central bank', N'banco central do brasil', N'vote on interest rate', N'http://www.investing.com/economic-calendar/brazilian-interest-rate-decision-415')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b9b753e3-7a93-43e3-a61d-b963e2a92547', N'canada retail sales mom', N'economic activity', N'statistics canada', N'retail sales mom', N'http://www.investing.com/economic-calendar/retail-sales-260')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'061b6a0e-5982-4f5e-a977-b96f2a35ec72', N'u.s. quantitative easing (qe) mbs', N'central bank', N'fomc', N'quantitative easing mbs', N'http://www.investing.com/economic-calendar/qe-mbs-1404')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6b3e2364-6769-4b49-9c02-ba4b6338e087', N'brazil foreign exchange flows', N'central bank', N'banco central do brasil', N'foreign exchange flows', N'http://www.investing.com/economic-calendar/brazilian-foreign-exchange-flows-764')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'29eb7bdb-a901-41a4-aba9-bade21df2e6a', N'australia retail sales mom', N'economic activity', N'australian bureau of statistics', N'retail sales mom', N'http://www.investing.com/economic-calendar/retail-sales-262')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8469f95b-9ac0-40d5-b635-bb168354873e', N'u.k. producer price index (ppi) output mom', N'inflation', N'office for national statistics', N'producer price index output mom', N'http://www.investing.com/economic-calendar/ppi-output-246')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'3990232c-7a15-43ce-b42a-bc99db82d31a', N'european central bank economic bulletin', N'central bank', N'ecb', N'economic bulletin', N'http://www.investing.com/economic-calendar/ecb-economic-bulletin-318')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8fffaeea-03d0-441b-99e0-bcc327679fb7', N'u.k. boe mpc vote hike', N'central bank', N'boe', N'monetary policy vote hike', N'http://www.investing.com/economic-calendar/boe-mpc-vote-hike-847')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eaff5321-243a-46bd-b250-bcefba7546b1', N'bank of england (boe) monetary policy committee (mpc) meeting minutes', N'central bank', N'boe', N'monetary policy meeting minutes', N'http://www.investing.com/economic-calendar/mpc-meeting-minutes-212')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6c431a57-35a4-4403-948b-bd74e8f53892', N'japan jobs/applications ratio', N'employment', N'the japan institute for labour', N'jobs applications ratio', N'http://www.investing.com/economic-calendar/jobs-applications-ratio-1001')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2d084082-7012-4b8f-a0e5-be92ac59b1c2', N'u.s. ism manufacturing new orders index', N'economic activity', N'ism institute for supply management', N'ism manufacturing new orders index', N'http://www.investing.com/economic-calendar/ism-manufacturing-new-orders-index-1483')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'72c2b429-f3e2-497b-911e-c0883d05e1f9', N'u.s. kc fed composite index', N'economic activity', N'federal reserve bank of kansas', N'kc fed composite index', N'http://www.investing.com/economic-calendar/kc-fed-composite-index-662')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'3826bf32-6050-41c9-b5f2-c1f210521400', N'japan tankan all big industry capex', N'economic activity', N'bank of japan', N'capex', N'http://www.investing.com/economic-calendar/tankan-all-big-industry-capex-1515')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b4073136-2d81-4ee4-85f7-c2141d69b95c', N'china foreign direct investment (fdi)', N'economic activity', N'ministry of commerce', N'foreign direct investment', N'http://www.investing.com/economic-calendar/chinese-fdi-588')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'd192e127-e5ab-4636-8f81-c2cd2846a221', N'australia interest rate decision', N'central bank', N'reserve bank of australia', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-171')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'54cbffec-7d17-4e7c-b0db-c4d7fbcb8034', N'australia building approvals mom', N'economic activity', N'australian bureau of statistics', N'building approvals mom', N'http://www.investing.com/economic-calendar/building-approvals-22')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2f1d078a-2a30-4399-8b90-c532a372dfc9', N'canada housing starts', N'economic activity', N'canada mortgage and housing', N'housing starts', N'http://www.investing.com/economic-calendar/housing-starts-150')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'74a0352c-2417-4981-a897-c5a5174d3730', N'australia gross domestic product (gdp) final consumption', N'economic activity', N'australian bureau of statistics', N'gross domestic product final consumption', N'http://www.investing.com/economic-calendar/gdp-final-consumption-1464')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1de4f5b9-9609-4e18-9f07-c5f03a184473', N'new zealand producer price index (ppi) qoq', N'inflation', N'statistics new zealand', N'producer price index input qoq', N'http://www.investing.com/economic-calendar/ppi-input-243')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'25fd6b93-5a39-4fd7-ad5c-c78cc575df7b', N'germany producer price index (ppi) yoy', N'inflation', N'destatis', N'producer price index yoy', N'http://www.investing.com/economic-calendar/german-ppi-739')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9d7c100a-cc51-4e1f-b086-c80c3a42cc15', N'canada imports', N'economic activity', N'statistics canada', N'imports', N'http://www.investing.com/economic-calendar/imports-1026')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'61be3243-0b7f-4e78-9135-c827cc25946b', N'japan overtime pay yoy', N'economic activity', N'ministry of health', N'overtime pay yoy', N'http://www.investing.com/economic-calendar/overtime-pay-1499')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fce9d494-368c-4257-b6e4-c83ab859ba20', N'australia cb leading index mom', N'economic activity', N'the conference board', N'cb leading index mom', N'http://www.investing.com/economic-calendar/cb-leading-index-193')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e1a0a09b-b1c7-43c7-8d48-ca495803eb77', N'germany wholesale price index (wpi) mom', N'inflation', N'destatis', N'wholesale price index mom', N'http://www.investing.com/economic-calendar/german-wpi-143')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fd541b80-6c84-46ef-9351-cb330b0be2cb', N'japan tokyo core consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'core consumer price index yoy', N'http://www.investing.com/economic-calendar/tokyo-core-cpi-328')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f203f92a-3bab-4729-a75e-cbc917c85d83', N'u.k. interest rate decision', N'central bank', N'boe', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-170')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ecf19fb7-ecdf-4add-96f9-cd23103a1ce6', N'brazil unemployment rate', N'employment', N'ibge', N'unemployment rate', N'http://www.investing.com/economic-calendar/brazilian-unemployment-rate-411')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e2e9b204-6883-4ba7-8875-cdcb38f72351', N'china retail sales yoy', N'economic activity', N'national bureau of statistics', N'retail sales yoy', N'http://www.investing.com/economic-calendar/chinese-retail-sales-465')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a1134f44-6dc5-42d1-8801-cdd2a80d3c7e', N'u.s. federal open market committee (fomc) economic projections', N'central bank', N'fomc', N'economic projections', N'http://www.investing.com/economic-calendar/fomc-economic-projections-1061')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'71c50204-fcdb-4f3f-a74e-ce22bf074056', N'japan unemployment rate', N'employment', N'the japan institute for labour', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-298')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1f31e1fa-57ea-417f-8616-ce38fd3ab361', N'u.k. producer price index (ppi) input mom', N'inflation', N'office for national statistics', N'producer price index input mom', N'http://www.investing.com/economic-calendar/ppi-input-242')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'651b6813-61cc-4882-9dc2-ce7565dea1a1', N'beige book', N'central bank', N'fomc', N'beige book', N'http://www.investing.com/economic-calendar/beige-book-10')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ee475b59-87ef-4f64-acd7-ce9f6949a64b', N'u.k. retail sales mom', N'economic activity', N'office for national statistics', N'retail sales mom', N'http://www.investing.com/economic-calendar/retail-sales-258')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'409a67ad-38e1-4520-a2b9-cf1180bc2d3b', N'germany services pmi', N'economic activity', N'markit', N'services pmi', N'http://www.investing.com/economic-calendar/german-services-pmi-140')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4821a679-78cf-4f2b-9db0-cfc12cbf83b4', N'australia unemployment rate', N'employment', N'australian bureau of statistics', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-302')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'75d55b47-5167-4de4-a8dc-d00cf17b648d', N'eurozone industrial production mom', N'economic activity', N'eurostat', N'industrial production mom', N'http://www.investing.com/economic-calendar/industrial-production-160')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'47e0dcc4-8a46-44f0-9276-d02e9d765f27', N'u.s. gross domestic product (gdp) qoq', N'economic activity', N'bureau of economic analysis', N'gross domestic product qoq', N'http://www.investing.com/economic-calendar/gdp-375')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9951ba14-04e9-4aac-8ca0-d0df75bf8f9b', N'eurozone labor cost index yoy', N'inflation', N'eurostat', N'labor cost index yoy', N'http://www.investing.com/economic-calendar/labor-cost-index-187')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ed9184f3-ab4a-4008-a636-d2414ba9c086', N'canada employment change', N'employment', N'statictics canada', N'employment change', N'http://www.investing.com/economic-calendar/employment-change-95')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'b2a436f5-748b-4827-bdb2-d2d3f85aa793', N'canada raw materials price index (rmpi) yoy', N'inflation', N'statistics canada', N'raw materials price index yoy', N'http://www.investing.com/economic-calendar/rmpi-1030')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f5941906-7d6e-4e1f-bc40-d34122bdc998', N'australia consumer price index (cpi) qoq', N'inflation', N'australian bureau of statistics', N'consumer price index qoq', N'http://www.investing.com/economic-calendar/cpi-73')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8d2b292d-e766-44b3-b4f9-d35a03492fa5', N'eurozone manufacturing pmi', N'economic activity', N'markit', N'manufacturing pmi', N'http://www.investing.com/economic-calendar/manufacturing-pmi-201')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fb36f509-c4d6-46c9-95d7-d3fb063fbf61', N'australia import price index qoq', N'inflation', N'australian bureau of statistics', N'import price index qoq', N'http://www.investing.com/economic-calendar/import-price-index-154')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'58db47cb-5846-404d-90da-d4228bdefc68', N'eurozone consumer price index (cpi) mom', N'inflation', N'eurostat', N'consumer price index mom', N'http://www.investing.com/economic-calendar/cpi-928')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'975a2a22-a1ea-41d4-a609-d4b85832286e', N'canada gross domestic product (gdp) mom', N'economic activity', N'statistics canada', N'gross domestic product mom', N'http://www.investing.com/economic-calendar/gdp-123')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ffe39d5b-9282-4d9f-8ff9-d5cc8af05480', N'u.k. retail price index (rpi) yoy', N'inflation', N'office for national statistics', N'retail price index yoy', N'http://www.investing.com/economic-calendar/rpi-267')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'571db485-adf4-495a-a0a1-d79bcf723b6b', N'japan core machinery orders mom', N'economic activity', N'cabinet office', N'core machinery orders mom', N'http://www.investing.com/economic-calendar/core-machinery-orders-60')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0e6fa9ea-1768-40bb-820d-d86ad4764110', N'eurozone consumer inflation expectation', N'inflation', N'eurostat', N'consumer inflation expectation', N'http://www.investing.com/economic-calendar/consumer-inflation-expectation-920')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'22ed89e5-acd0-4618-bab3-d8a370153aff', N'brazil retail sales yoy', N'economic activity', N'ibge', N'retail sales yoy', N'http://www.investing.com/economic-calendar/brazilian-retail-sales-416')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4ac285ae-4468-4684-aae4-d8d6651bccde', N'japan retail sales yoy', N'economic activity', N'ministry of economy', N'retail sales yoy', N'http://www.investing.com/economic-calendar/retail-sales-261')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a96e2137-c775-4777-b83e-d901fdd44d0d', N'china house prices yoy', N'economic activity', N'national bureau of statistics', N'house prices yoy', N'http://www.investing.com/economic-calendar/china-house-prices-752')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4f0dcc6d-5857-41ad-90c5-da560fa084f8', N'u.k. industrial production yoy', N'economic activity', N'office for national statistics', N'industrial production yoy', N'http://www.investing.com/economic-calendar/industrial-production-732')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'cbb95552-1a7d-433b-bc68-dae6ec7f2c26', N'japan exports yoy', N'economic activity', N'ministry of finance', N'exports yoy', N'http://www.investing.com/economic-calendar/exports-995')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'636a89fc-504d-4255-823a-dc56a4c65766', N'u.s. unemployment rate', N'employment', N'bureau of labor statistics', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-300')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c47f5dbc-8798-4eb3-b9f1-dcc0c448dfcd', N'u.k. nationwide hpi mom', N'inflation', N'nationwide building society', N'nationwide hpi mom', N'http://www.investing.com/economic-calendar/nationwide-hpi-363')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'c02b1bbb-e1ee-4415-a3bc-dcca0af48626', N'germany consumer price index (cpi) yoy', N'inflation', N'destatis', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/german-cpi-737')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'47d70ec7-4177-486e-b4e5-dd7cfb0ea5f4', N'u.s. goods orders non defense ex air mom', N'economic activity', N'census bureau', N'non defense goods orders mom', N'http://www.investing.com/economic-calendar/goods-orders-non-defense-ex-air-1047')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ea48cf9f-d440-43e4-aa57-de7cacce5121', N'japan tokoyo consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/tokyo-cpi-993')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6ab71618-2ec2-4fc7-a356-de89c27d5fc1', N'eurozone employment change qoq', N'employment', N'eurostat', N'employment change', N'http://www.investing.com/economic-calendar/employment-change-96')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'24bd6024-f403-430a-b444-de9d947aa2b9', N'new zealand gross domestic product (gdp) expenditure qoq', N'economic activity', N'statistics new zealand', N'gross domestic product expenditure qoq', N'http://www.investing.com/economic-calendar/gdp-expenditure-1462')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'4fad3140-4454-4181-b735-deb1bb4b769d', N'eurozone retail sales mom', N'economic activity', N'eurostat', N'retail sales mom', N'http://www.investing.com/economic-calendar/retail-sales-255')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e6825dd2-08d4-42b9-8c67-e08ea266af4c', N'u.s. manufacturing pmi', N'economic activity', N'markit', N'manufacturing pmi', N'http://www.investing.com/economic-calendar/manufacturing-pmi-829')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'dab448a0-74da-4167-998b-e097243a01e6', N'u.k. construction pmi', N'economic activity', N'cips', N'construction pmi', N'http://www.investing.com/economic-calendar/construction-pmi-44')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e1da9024-4481-4960-8612-e0d898d8fed8', N'u.s. quantitative easing (qe) treasuries', N'central bank', N'fomc', N'quantitative easing treasuries', N'http://www.investing.com/economic-calendar/qe-treasuries-1406')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2bc761c8-6cbd-4291-b436-e0f25be745e2', N'brazil debt-to-gdp ratio', N'economic activity', N'ibge', N'debt to gdp ratio', N'http://www.investing.com/economic-calendar/brazil-debt-to-gdp-ratio-763')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'91914b45-defc-47a0-9435-e13c781840c5', N'u.k. retail sales yoy', N'economic activity', N'office for national statistics', N'retail sales yoy', N'http://www.investing.com/economic-calendar/retail-sales-731')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'1dceb79c-3deb-4f27-bc4b-e18b9668ac93', N'canada core consumer price index (cpi) yoy', N'inflation', N'statistics canada', N'core consumer price index yoy', N'http://www.investing.com/economic-calendar/core-cpi-1020')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7e19efed-0fa5-4a84-9f0d-e378b8e6d8c2', N'japan leading index mom', N'economic activity', N'cabinet office', N'leading index mom', N'http://www.investing.com/economic-calendar/leading-index-1582')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ac281341-0133-4f36-8380-e434c264fe3b', N'eurozone producer price index (ppi) yoy', N'inflation', N'eurostat', N'producer price index yoy', N'http://www.investing.com/economic-calendar/ppi-935')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0d248aea-2b03-461d-9db1-e531f9bdffd1', N'u.k. retail price index (rpi) mom', N'economic activity', N'office for national statistics', N'retail price index mom', N'http://www.investing.com/economic-calendar/rpi-853')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f4497132-7004-4b97-ab3b-e554d2298b58', N'u.k. manufacturing pmi', N'economic activity', N'markit', N'manufacturing pmi', N'http://www.investing.com/economic-calendar/manufacturing-pmi-204')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a6c23f3d-6d1a-4160-a4e2-e61316448619', N'u.k. core producer price index (ppi) output mom', N'inflation', N'office for national statistics', N'core producer price index output mom', N'http://www.investing.com/economic-calendar/core-ppi-output-851')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'60bbb691-b959-4665-b371-e97f977fa4fe', N'germany consumer price index (cpi) mom', N'inflation', N'destatis', N'consumer price index mom', N'http://www.investing.com/economic-calendar/german-cpi-128')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'fe82097e-0019-4f25-974d-ea1967315921', N'bank of canada business outlook survey', N'central bank', N'boc', N'business outlook survey', N'http://www.investing.com/economic-calendar/boc-business-outlook-survey-1153')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'a59c9bdc-8225-4c2d-8047-ebae25cf6d5b', N'japan tankan big manufacturing outlook index', N'economic activity', N'bank of japan', N'tankan big manufacturing outlook index', N'http://www.investing.com/economic-calendar/tankan-big-manufacturing-outlook-index-1517')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'130d39dc-0cff-4c29-870b-ebe3d451f0f1', N'u.k. services pmi', N'economic activity', N'cips', N'services pmi', N'http://www.investing.com/economic-calendar/services-pmi-274')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2fb52b75-c96a-434e-9cbf-ec0b3da4a39a', N'canada consumer price index (cpi) yoy', N'inflation', N'statistics canada', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-741')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5ace2d5f-ba7b-47aa-ba15-ec2ae4c57c18', N'eurozone retail sales yoy', N'economic activity', N'eurostat', N'retail sales yoy', N'http://www.investing.com/economic-calendar/retail-sales-936')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7ba2da6b-823c-4a38-8e16-ec6ed53ddd71', N'u.s. michigan current conditions', N'economic activity', N'university of michigan', N'michigan current conditions', N'http://www.investing.com/economic-calendar/michigan-current-conditions-901')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'faf4a152-c539-4d6c-aec0-ed21b522202e', N'u.s. nonfarm productivity qoq', N'economic activity', N'bureau of labor statistics of the us department', N'nonfarm productivity qoq', N'http://www.investing.com/economic-calendar/nonfarm-productivity-228')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'e8ad06a5-e134-41fa-9c52-edfc2c65a74d', N'brazil long term interest rate tjlp', N'central bank', N'banco central do brasil', N'long term interest rate', N'http://www.investing.com/economic-calendar/brazil-long-term-interest-rate-tjlp-1409')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ee9da06e-948c-4769-9a04-f0452cce01a5', N'new zealand food price index (fpi) mom', N'inflation', N'statistics new zealand', N'food price index mom', N'http://www.investing.com/economic-calendar/fpi-110')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8497bd34-4556-4cb7-9607-f15d0512be28', N'australia commodity prices yoy', N'inflation', N'reserve bank of australia', N'commodity prices yoy', N'http://www.investing.com/economic-calendar/commodity-prices-346')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ee1c9cf1-415f-484b-84c6-f15ee1cce888', N'u.k. boe mpc vote cut', N'central bank', N'boe', N'monetary policy vote cut', N'http://www.investing.com/economic-calendar/boe-mpc-vote-cut-846')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'0e66c06a-4b72-4cf4-a3bc-f1abab16bc47', N'new zealand visitor arrivals mom', N'economic activity', N'statistics new zealand', N'visitor arrivals mom', N'http://www.investing.com/economic-calendar/visitor-arrivals-304')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'abe5ba8a-26d4-49f2-b8bd-f22bd67a2b21', N'germany unemployment change', N'employment', N'destatis', N'unemployment change', N'http://www.investing.com/economic-calendar/german-unemployment-change-332')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'90122bd0-7c89-46c0-87a7-f337d5548a18', N'brazil retail sales mom', N'economic activity', N'ibge', N'retail sales mom', N'http://www.investing.com/economic-calendar/brazilian-retail-sales-861')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6f7dfa81-650f-48ce-b81e-f5090b5f7e2d', N'u.s. personal spending mom', N'economic activity', N'bureau of economic analysis', N'personal spending mom', N'http://www.investing.com/economic-calendar/personal-spending-235')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'f1e967a7-56c8-4cec-b58f-f5f6986d7fb8', N'eurozone retail pmi', N'economic activity', N'markit', N'retail pmi', N'http://www.investing.com/economic-calendar/retail-pmi-828')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ee6224e7-0faa-486f-a5c6-f63367db76a6', N'japan bsi large manufacturing conditions', N'economic activity', N'ministry of finance', N'business sentiment index', N'http://www.investing.com/economic-calendar/bsi-large-manufacturing-conditions-21')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5d037b2b-b146-4b1c-b8c1-f7104af46059', N'china industrial production yoy', N'economic activity', N'national bureau of statistics', N'industrial production yoy', N'http://www.investing.com/economic-calendar/chinese-industrial-production-462')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'9ee8554c-1360-4af7-9074-f82f073879de', N'eurozone industrial production yoy', N'economic activity', N'eurostat', N'industrial production yoy', N'http://www.investing.com/economic-calendar/industrial-production-931')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'2e253a2d-14c9-405b-91fa-f8e13f9dae8c', N'u.k. trade balance non-eu', N'economic activity', N'office for national statistics', N'trade balance non-eu', N'http://www.investing.com/economic-calendar/trade-balance-non-eu-849')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5cfead71-2d73-494e-9627-f9ca87584884', N'foreign securities purchases by canadians', N'economic activity', N'statistics canada', N'foreign securities purchases by canadians', N'http://www.investing.com/economic-calendar/foreign-securities-purchases-by-canadians-1028')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'5c215ae9-5a9f-4a47-b29f-faaac009f1ee', N'germany house price index (hpi) mom', N'economic activity', N'hypoprt', N'house price index mom', N'http://www.investing.com/economic-calendar/german-house-price-index-545')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'478cc885-df78-4d7d-8fb0-fafc4e1926c4', N'brazil foreign direct investment (usd)', N'economic activity', N'fundo monetario', N'foreign direct investment', N'http://www.investing.com/economic-calendar/foreign-direct-investment-(usd)-862')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'272402cd-59f9-40e3-a407-fb6b0614e65f', N'japan manufacturing pmi', N'economic activity', N'nomura', N'manufacturing pmi', N'http://www.investing.com/economic-calendar/manufacturing-pmi-202')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'837c74dd-b947-4589-806b-fb88aa3fe04c', N'brazil cni factory utilization', N'economic activity', N'cni', N'capacity utilization', N'http://www.investing.com/economic-calendar/brazilian-cni-factory-utilization-762')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'6d9e027f-3272-4728-a9ad-fc058e7b492f', N'canada manufacturing sales mom', N'economic activity', N'statistics canada', N'manufacturing sales mom', N'http://www.investing.com/economic-calendar/manufacturing-sales-207')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'federal open market committee (fomc) members vote on where to set the rate', N'central bank', N'fomc', N'vote on interest rate', N'http://www.investing.com/economic-calendar/interest-rate-decision-168')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'11231029-ba21-4d8a-b19a-fcadfdd0f51f', N'u.k. producer price index (ppi) output yoy', N'inflation', N'office for national statistics', N'producer price index output yoy', N'http://www.investing.com/economic-calendar/ppi-output-730')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'7c91b6d1-56d5-4900-b220-fd7d921d4839', N'u.k. nationwide hpi yoy', N'inflation', N'nationwide building society', N'nationwide hpi yoy', N'http://www.investing.com/economic-calendar/nationwide-hpi-850')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'132b0514-852a-4d69-872f-fe7c72cb4d13', N'u.s. pending home sales mom', N'economic activity', N'national association of realtors', N'pending home sales mom', N'http://www.investing.com/economic-calendar/pending-home-sales-232')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'8ff6ed11-e60c-4a51-9efb-ff1e706d66f2', N'germany composite pmi', N'economic activity', N'destatis', N'composite pmi', N'http://www.investing.com/economic-calendar/german-composite-pmi-1469')
GO
INSERT [dbo].[tblFundamentalKeywordDefinitions] ([ID], [MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'ce6da862-f453-46fc-9e1d-ffea3c36dd90', N'new zealand employment change qoq', N'employment', N'statistics new zealand', N'employment change qoq', N'http://www.investing.com/economic-calendar/employment-change-93')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'f52fd2f3-03e7-4557-bfdc-0afb13be61ea', N'narayana', N'kocherlakota', N'federal open market committee (fomc) voting member')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'756ba549-b1af-41ec-b341-0ff3702a6ba0', N'timothy', N'geithner', N'u.s. treasury secretary')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'807cc905-107d-41d8-b699-12f836f0c8ee', N'jannet', N'yellen', N'federal reserve chair')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'1e4f05c5-2358-4ddd-a392-3f50255aa3c1', N'charles', N'evans', N'federal open market committee (fomc) voting member')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'dab4f342-ca90-4a79-9014-446a592ce5db', N'john', N'williams', N'federal open market committee (fomc) voting member')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'0aa63b48-c833-4fd6-b467-47d59a4fd98e', N'eric', N'rosengren', N'federal open market committee (fomc) voting member')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'3152e790-0b4b-47cf-ac0c-543295edc98a', N'richard', N'fisher', N'federal open market committee (fomc) voting member')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'624f2937-2951-4fb3-b5db-5b12460f654b', N'mervyn', N'king', N'bank of england (boe) governor')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'0a9ea244-efff-43f5-be75-60ac1cf68fdb', N'mario', N'draghi', N'president of the european central bank')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'5c7f4705-7487-4a41-926a-8aa08fc4988c', N'jens', N'weidmann', N'deutsche bundesbank (buba) president')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'9b5d718e-4521-45e6-ae2e-ac27f55e9130', N'jack', N'lew', N'u.s. treasury secretary')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'a7f63de1-7867-46e5-9d0e-c1ea4552362c', N'thomas', N'jordan', N'swiss national bank (snb) chairman')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'494d0d42-fd59-4d25-a524-c4d664ffdc61', N'dennis', N'lockhart', N'federal open market committee (fomc) voting member')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'6922207d-a019-4452-9c35-c590e14a6575', N'ben', N'bernanke', N'federal reserve chairman')
GO
INSERT [dbo].[tblKeyPersonnelDefinitions] ([ID], [FirstName], [LastName], [Role]) VALUES (N'20ca4bb4-4640-4e51-9250-dea9dcb8ba2d', N'mark', N'carney', N'bank of england (boe) governor')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'807cc905-107d-41d8-b699-12f836f0c8ee')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'807cc905-107d-41d8-b699-12f836f0c8ee')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'6922207d-a019-4452-9c35-c590e14a6575')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'6922207d-a019-4452-9c35-c590e14a6575')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'1e4f05c5-2358-4ddd-a392-3f50255aa3c1')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'1e4f05c5-2358-4ddd-a392-3f50255aa3c1')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'dab4f342-ca90-4a79-9014-446a592ce5db')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'dab4f342-ca90-4a79-9014-446a592ce5db')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'0aa63b48-c833-4fd6-b467-47d59a4fd98e')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'0aa63b48-c833-4fd6-b467-47d59a4fd98e')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'3152e790-0b4b-47cf-ac0c-543295edc98a')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'3152e790-0b4b-47cf-ac0c-543295edc98a')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'494d0d42-fd59-4d25-a524-c4d664ffdc61')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'494d0d42-fd59-4d25-a524-c4d664ffdc61')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'fe71db5f-2687-4de8-897c-4c794aa4bb19', N'f52fd2f3-03e7-4557-bfdc-0afb13be61ea')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'8e61562d-f71a-4c26-9704-fc8d9322bdbb', N'f52fd2f3-03e7-4557-bfdc-0afb13be61ea')
GO
INSERT [dbo].[tblPersonnelAssociation] ([AssociationID], [KeyPersonnelID]) VALUES (N'ce5a4680-e49c-4f5f-bc65-4786f0f088bc', N'9b5d718e-4521-45e6-ae2e-ac27f55e9130')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. producer price index (ppi) input yoy', N'inflation', N'office for national statistics', N'producer price index input yoy', N'http://www.investing.com/economic-calendar/ppi-input-729')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. producer price index (ppi) output yoy', N'inflation', N'office for national statistics', N'producer price index output yoy', N'http://www.investing.com/economic-calendar/ppi-output-730')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. producer price index (ppi) output mom', N'inflation', N'office for national statistics', N'producer price index output mom', N'http://www.investing.com/economic-calendar/ppi-output-246')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. retail price index (rpi) yoy', N'inflation', N'office for national statistics', N'retail price index yoy', N'http://www.investing.com/economic-calendar/rpi-267')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. nationwide hpi yoy', N'inflation', N'nationwide building society', N'nationwide hpi yoy', N'http://www.investing.com/economic-calendar/nationwide-hpi-850')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. nationwide hpi mom', N'inflation', N'nationwide building society', N'nationwide hpi mom', N'http://www.investing.com/economic-calendar/nationwide-hpi-363')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'bank of england inflation report', N'inflation', N'bank of england', N'inflation report', N'http://www.investing.com/economic-calendar/boe-inflation-report-15')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. import price index mom', N'inflation', N'us department of labor', N'import price index mom', N'http://www.investing.com/economic-calendar/import-price-index-153')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. producer price index (ppi) input yoy', N'inflation', N'bureau of labor statistics', N'producer price index input yoy', N'http://www.investing.com/economic-calendar/core-ppi-735')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. producer price index (ppi) input mom', N'inflation', N'bureau of labor statistics', N'producer price index input mom', N'http://www.investing.com/economic-calendar/core-ppi-62')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. producer price index (ppi) mom', N'inflation', N'bureau of labor statistics', N'producer price index mom', N'http://www.investing.com/economic-calendar/ppi-238')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. producer price index (ppi) yoy', N'inflation', N'bureau of labor statistics', N'producer price index yoy', N'http://www.investing.com/economic-calendar/ppi-734')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. house price index mom', N'inflation', N'ofheo', N'house price index mom', N'http://www.investing.com/economic-calendar/house-price-index-327')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.s. house price index yoy', N'inflation', N'ofheo', N'house price index yoy', N'http://www.investing.com/economic-calendar/house-price-index-897')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone core consumer price index (cpi) mom', N'inflation', N'eurostat', N'core consumer price index mom', N'http://www.investing.com/economic-calendar/core-cpi-922')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone consumer price index (cpi) mom', N'inflation', N'eurostat', N'consumer price index mom', N'http://www.investing.com/economic-calendar/cpi-928')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone consumer price index (cpi) yoy', N'inflation', N'eurostat', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-68')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone producer price index (ppi) yoy', N'inflation', N'eurostat', N'producer price index yoy', N'http://www.investing.com/economic-calendar/ppi-935')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone producer price index (ppi) mom', N'inflation', N'eurostat', N'producer price index input mom', N'http://www.investing.com/economic-calendar/ppi-237')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone consumer inflation expectation', N'inflation', N'eurostat', N'consumer inflation expectation', N'http://www.investing.com/economic-calendar/consumer-inflation-expectation-920')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone labor cost index yoy', N'inflation', N'eurostat', N'labor cost index yoy', N'http://www.investing.com/economic-calendar/labor-cost-index-187')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone selling price expectations', N'inflation', N'eurostat', N'selling price expectations', N'http://www.investing.com/economic-calendar/european-selling-price-expectations-1572')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada new housing price index mom', N'inflation', N'statistics canada', N'new housing price index mom', N'http://www.investing.com/economic-calendar/new-housing-price-index-223')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada raw materials price index (rmpi) mom', N'inflation', N'statistics canada', N'raw materials price index mom', N'http://www.investing.com/economic-calendar/rmpi-266')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada raw materials price index (rmpi) yoy', N'inflation', N'statistics canada', N'raw materials price index yoy', N'http://www.investing.com/economic-calendar/rmpi-1030')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada industrial product price index (ippi) mom', N'inflation', N'statistics canada', N'industrial product price index mom', N'http://www.investing.com/economic-calendar/ippi-172')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada industrial product price index (ippi) yoy', N'inflation', N'statistics canada', N'industrial product price index yoy', N'http://www.investing.com/economic-calendar/ippi-742')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada consumer price index (cpi) mom', N'inflation', N'statistics canada', N'consumer price index mom', N'http://www.investing.com/economic-calendar/cpi-70')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada consumer price index (cpi) yoy', N'inflation', N'statistics canada', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-741')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada core consumer price index (cpi) yoy', N'inflation', N'statistics canada', N'core consumer price index yoy', N'http://www.investing.com/economic-calendar/core-cpi-1020')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada core consumer price index (cpi) mom', N'inflation', N'statistics canada', N'core consumer price index mom', N'http://www.investing.com/economic-calendar/core-cpi-57')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'china consumer price index (cpi) mom', N'inflation', N'national bureau of statistics of china', N'consumer price index mom', N'http://www.investing.com/economic-calendar/chinese-cpi-743')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'china consumer price index (cpi) yoy', N'inflation', N'national bureau of statistics of china', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/chinese-cpi-459')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'china producer price index (ppi) yoy', N'inflation', N'national bureau of statistics of china', N'producer price index yoy', N'http://www.investing.com/economic-calendar/chinese-ppi-464')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia consumer price index (cpi) qoq', N'inflation', N'australian bureau of statistics', N'consumer price index qoq', N'http://www.investing.com/economic-calendar/cpi-73')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia consumer price index (cpi) yoy', N'inflation', N'australian bureau of statistics', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-1011')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia consumer price index (cpi) index number', N'inflation', N'australian bureau of statistics', N'consumer price index index number', N'http://www.investing.com/economic-calendar/cpi-index-number-1010')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia trimmed mean consumer price index (cpi) yoy', N'inflation', N'australian bureau of statistics', N'trimmed mean consumer price index yoy', N'http://www.investing.com/economic-calendar/trimmed-mean-cpi-1017')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia trimmed mean consumer price index (cpi) qoq', N'inflation', N'australian bureau of statistics', N'trimmed mean consumer price index qoq', N'http://www.investing.com/economic-calendar/trimmed-mean-cpi-293')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia import price index qoq', N'inflation', N'australian bureau of statistics', N'import price index qoq', N'http://www.investing.com/economic-calendar/import-price-index-154')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia producer price index (ppi) qoq', N'inflation', N'australian bureau of statistics', N'producer price index input qoq', N'http://www.investing.com/economic-calendar/ppi-239')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia commodity prices yoy', N'inflation', N'reserve bank of australia', N'commodity prices yoy', N'http://www.investing.com/economic-calendar/commodity-prices-346')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia wage price index yoy', N'inflation', N'australian bureau of statistics', N'wage price index yoy', N'http://www.investing.com/economic-calendar/wage-price-index-1019')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia wage price index qoq', N'inflation', N'australian bureau of statistics', N'wage price index qoq', N'http://www.investing.com/economic-calendar/wage-price-index-1020')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand food price index (fpi) mom', N'inflation', N'statistics new zealand', N'food price index mom', N'http://www.investing.com/economic-calendar/fpi-110')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand producer price index (ppi) qoq', N'inflation', N'statistics new zealand', N'producer price index input qoq', N'http://www.investing.com/economic-calendar/ppi-input-243')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand producer price index (ppi) output qoq', N'inflation', N'statistics new zealand', N'producer price index output qoq', N'http://www.investing.com/economic-calendar/ppi-output-247')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand consumer price index (cpi) yoy', N'inflation', N'statistics new zealand', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/cpi-1063')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand consumer price index (cpi) qoq', N'inflation', N'statistics new zealand', N'consumer price index qoq', N'http://www.investing.com/economic-calendar/cpi-72')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand inflation expectations qoq', N'inflation', N'statistics new zealand', N'inflation expectations qoq', N'http://www.investing.com/economic-calendar/inflation-expectations-342')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand globaldairytrade price index', N'inflation', N'globaldairytrade', N'globaldairytrade price index', N'http://www.investing.com/economic-calendar/globaldairytrade-price-index-1654')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan corporate goods price index (cgpi) mom', N'inflation', N'bank of japan', N'corporate goods price index mom', N'http://www.investing.com/economic-calendar/cgpi-990')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan national core consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'national core consumer price index yoy', N'http://www.investing.com/economic-calendar/national-core-cpi-344')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan national consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'national consumer price index yoy', N'http://www.investing.com/economic-calendar/national-cpi-992')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan tokyo core consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'core consumer price index yoy', N'http://www.investing.com/economic-calendar/tokyo-core-cpi-328')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan tokoyo consumer price index (cpi) yoy', N'inflation', N'statistics bureau', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/tokyo-cpi-993')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan gross domestic product (gdp) price index yoy', N'inflation', N'cabinet office', N'gross domestic product price index yoy', N'http://www.investing.com/economic-calendar/gdp-price-index-126')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'germany wholesale price index (wpi) mom', N'inflation', N'destatis', N'wholesale price index mom', N'http://www.investing.com/economic-calendar/german-wpi-143')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'germany consumer price index (cpi) mom', N'inflation', N'destatis', N'consumer price index mom', N'http://www.investing.com/economic-calendar/german-cpi-128')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'germany consumer price index (cpi) yoy', N'inflation', N'destatis', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/german-cpi-737')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'germany producer price index (ppi) mom', N'inflation', N'destatis', N'producer price index input mom', N'http://www.investing.com/economic-calendar/german-ppi-137')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'germany producer price index (ppi) yoy', N'inflation', N'destatis', N'producer price index yoy', N'http://www.investing.com/economic-calendar/german-ppi-739')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'germany import price index qoq', N'inflation', N'destatis', N'import price index mom', N'http://www.investing.com/economic-calendar/german-import-price-index-134')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'brazil consumer price index (cpi) mom', N'inflation', N'ibge', N'consumer price index mom', N'http://www.investing.com/economic-calendar/brazilian-cpi-1165')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'brazil consumer price index (cpi) yoy', N'inflation', N'ibge', N'consumer price index yoy', N'http://www.investing.com/economic-calendar/brazilian-cpi-410')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'brazil ipc-fipe inflation index mom', N'inflation', N'fipe', N'fipe inflation index mom', N'http://www.investing.com/economic-calendar/brazilian-ipc-fipe-inflation-index-1562')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone unemployment rate', N'employment', N'eurostat', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-299')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'eurozone employment change qoq', N'employment', N'eurostat', N'employment change', N'http://www.investing.com/economic-calendar/employment-change-96')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada unemployment rate', N'employment', N'statictics canada', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-301')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada employment change', N'employment', N'statictics canada', N'employment change', N'http://www.investing.com/economic-calendar/employment-change-95')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada full employment change', N'employment', N'statictics canada', N'full employment change', N'http://www.investing.com/economic-calendar/full-employment-change-1021')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada part time employment change', N'employment', N'statictics canada', N'part time employment change', N'http://www.investing.com/economic-calendar/part-time-employment-change-1022')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'canada participation rate', N'employment', N'statictics canada', N'participation change', N'http://www.investing.com/economic-calendar/participation-rate-670')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'brazil unemployment rate', N'employment', N'ibge', N'unemployment rate', N'http://www.investing.com/economic-calendar/brazilian-unemployment-rate-411')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'brazil caged net payroll jobs', N'employment', N'ibge', N'caged net payroll jobs', N'http://www.investing.com/economic-calendar/brazilian-caged-net-payroll-jobs-1523')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan unemployment rate', N'employment', N'the japan institute for labour', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-298')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan jobs/applications ratio', N'employment', N'the japan institute for labour', N'jobs applications ratio', N'http://www.investing.com/economic-calendar/jobs-applications-ratio-1001')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'japan average cash earnings yoy', N'employment', N'ministry of welfare', N'average cash earnings yoy', N'http://www.investing.com/economic-calendar/average-cash-earnings-6')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand unemployment rate', N'employment', N'statistics new zealand', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-295')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand employment change qoq', N'employment', N'statistics new zealand', N'employment change qoq', N'http://www.investing.com/economic-calendar/employment-change-93')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'new zealand participation rate', N'employment', N'statistics new zealand', N'participation rate', N'http://www.investing.com/economic-calendar/participation-rate-1532')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia anz internet job ads mom', N'employment', N'austrailia and new zealand banking group', N'internet job adverts mom', N'http://www.investing.com/economic-calendar/anz-internet-job-ads-1520')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia anz job advertisements mom', N'employment', N'austrailia and new zealand banking group', N'job advertisements mom', N'http://www.investing.com/economic-calendar/anz-job-advertisements-385')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia employment change', N'employment', N'australian bureau of statistics', N'employment change', N'http://www.investing.com/economic-calendar/employment-change-94')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia full employment change', N'employment', N'australian bureau of statistics', N'full employment change', N'http://www.investing.com/economic-calendar/full-employment-change-1052')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia participation rate', N'employment', N'australian bureau of statistics', N'participation rate', N'http://www.investing.com/economic-calendar/participation-rate-1051')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'australia unemployment rate', N'employment', N'australian bureau of statistics', N'unemployment rate', N'http://www.investing.com/economic-calendar/unemployment-rate-302')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. core producer price index (ppi) output yoy', N'inflation', N'office for national statistics', N'core producer price index output yoy', N'http://www.investing.com/economic-calendar/core-ppi-output-852')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. core producer price index (ppi) output mom', N'inflation', N'office for national statistics', N'core producer price index output mom', N'http://www.investing.com/economic-calendar/core-ppi-output-851')
GO
INSERT [dbo].[tblSTAGING_FundamentalKeywordDefinitions] ([MatchWords], [Category], [InstitutionBody], [EventType], [Link]) VALUES (N'u.k. producer price index (ppi) input mom', N'inflation', N'office for national statistics', N'producer price index input mom', N'http://www.investing.com/economic-calendar/ppi-input-242')
GO
