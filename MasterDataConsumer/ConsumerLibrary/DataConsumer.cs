﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.IO;
using System.Net;

using System.Data.SqlClient;
using System.Data;
using System.Xml;

using System.Timers;
using System.Text.RegularExpressions;

// added for access to RegistryKey
using Microsoft.Win32;
// added for access to socket classes
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using ConsumerLibrary;

namespace ConsumerLibrary
{
    //struct exchangeFileInfo
    //{
    //    public String exchangeSymbol;
    //    public String filePath;
    //}

    public class exchangeFileInfo
    {
        public String exchangeSymbol;
        public String filePath;
    }

    public class SymbolInfo
    {
        public String Symbol;
        public String Exchange;
    }

    public enum MODE
    {
        Live = 0,
        Test = 1,
        LiveTest = 2
    }

    public class DataConsumerScheduling
    {
        private List<DataConsumer> _dataConsumerList = new List<DataConsumer>();

        public void StartScheduling()
        {
            String[] timeArray = System.Configuration.ConfigurationManager.AppSettings["TIMELIST"].Split('|');

            foreach (var item in timeArray)
            {
                var timeItem = item.Split('@');

                DataConsumer dataConsumer = new DataConsumer(timeItem.LastOrDefault(), timeItem.FirstOrDefault());
                _dataConsumerList.Add(dataConsumer);

                ThreadStart threadStart = new ThreadStart(dataConsumer.ApplyScheduler);
                Thread thread = new Thread(threadStart);
                thread.Start();
            }

            //DataConsumer dataConsumer = new DataConsumer("18:28", "NYSE");
            //_dataConsumerList.Add(dataConsumer);

            //ThreadStart threadStart = new ThreadStart(dataConsumer.ApplyScheduler);
            //Thread thread = new Thread(threadStart);
            //thread.Start();

        }
    }

    public delegate bool RunDownload(String timeFrame, String exchange);

    public delegate void TradeSummaryDelegate(List<DataFeedService.TradeSummary> tradeSummary);


    public class DataConsumer : IDisposable
    {
        private static TradeSummaryDelegate _updateTradeSummaryHandler = null;
        private static DataFeedService.InternalDataFeedServiceClient _client;
        public static String RecipientAppID = "DataConsumer" +
            String.Format("{0}{1}{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

        private Dictionary<String, List<String>> symbolList = new Dictionary<String, List<String>>();

        private static object locker = new object();

        public static Queue<String> _timeFrameListQueue = new Queue<String>();
        public static String _currentTimeFrame = "";

        public static Queue<SymbolInfo> _SymbolCollection = new Queue<SymbolInfo>();
        public static RunDownload ConsumeData = new RunDownload(GetDownloadData);
        private static IQFeedHistoricDownloader _iqFeedHistoricalDownloaderT = new IQFeedHistoricDownloader();
        private static String _currentDownloadFileT = "";
        private static String _currentSelectorT = "";

        public static List<string> _DirectSymbolCollection = new List<string>();
        public static bool _bWorkCompleteFlagDataFiller = false;
        public static int _WorkCompleteFlagDataFillerThreshold = 0;
        public static bool _bDataFillerMood = false;
        public static bool SkipGenerateArtificalDateTimes = false;

        public static ConcurrentDictionary<string, bool> _dataFillerLookUpComplete = new ConcurrentDictionary<string, bool>();

        System.Timers.Timer _dataFillerTimer;

        System.Timers.Timer _workCompletedTimer;


        System.Timers.Timer actionTimer;
        private String _triggerTime;

        private String _fileName = "";


        static public MODE Mode = MODE.Test;

        public exchangeFileInfo EFileInfo = null;

        private IQFeedHistoricDownloader _iqFeedHistoricalDownloader = new IQFeedHistoricDownloader();

        private List<exchangeFileInfo> _exchangeInfoList = new List<exchangeFileInfo>();
        //private String _selectedExchange = "";

        public static String _selectedExchange = "";

        private String _currentDownloadFile = "";
        private String _currentSelector = "";

        private List<string> _timeframeList = new List<string>();

        private Queue<string> _taskWork = new Queue<string>();


        public DataConsumer(String triggerTimeParameter, String exchange)
        {
            _selectedExchange = exchange;
            _triggerTime = triggerTimeParameter;
            // Initial(triggerTimeParameter);

            TradeSummaryDelegate tradeSummaryDelegate = new TradeSummaryDelegate(SetTradeSummaryHandler);
            _updateTradeSummaryHandler = tradeSummaryDelegate;
            Register();
        }

        public void Dispose()
        {
            BroadcastorCallback cb = new BroadcastorCallback();
            cb.SetHandler(DataConsumer.HandleBroadcast);

            try
            {
                System.ServiceModel.InstanceContext context =
                   new System.ServiceModel.InstanceContext(cb);

                using (DataFeedService.InternalDataFeedServiceClient internalFeedServiceProxy = new DataFeedService.InternalDataFeedServiceClient(context))
                {
                    internalFeedServiceProxy.WorkCompleteFreeResources(RecipientAppID);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }

        //~DataConsumer()
        //{
        //    BroadcastorCallback cb = new BroadcastorCallback();
        //    cb.SetHandler(DataConsumer.HandleBroadcast);

        //    try
        //    {
        //        System.ServiceModel.InstanceContext context =
        //           new System.ServiceModel.InstanceContext(cb);

        //        using (DataFeedService.InternalDataFeedServiceClient internalFeedServiceProxy = new DataFeedService.InternalDataFeedServiceClient(context))
        //        {
        //            internalFeedServiceProxy.WorkCompleteFreeResources(RecipientAppID);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //        Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
        //    }
        //}

        public DataConsumer(String triggerTimeParameter)
        {
            Initial(triggerTimeParameter);

            TradeSummaryDelegate tradeSummaryDelegate = new TradeSummaryDelegate(SetTradeSummaryHandler);
            _updateTradeSummaryHandler = tradeSummaryDelegate;

            Register();
        }

        public DataConsumer(bool bApplyScheduler)
        {
            TradeSummaryDelegate tradeSummaryDelegate = new TradeSummaryDelegate(SetTradeSummaryHandler);

            if (bApplyScheduler)
            {
                GetScheduleTriggerTime();

                SystemEvents.TimeChanged += new EventHandler(SystemEvents_TimeChanged);

                actionTimer = new System.Timers.Timer();

                actionTimer.Elapsed += new ElapsedEventHandler(Task);

                actionTimer.Interval = CalculateInterval();
                actionTimer.AutoReset = true;

                actionTimer.Start();
                
            }
            _updateTradeSummaryHandler = tradeSummaryDelegate;

            Register();
        }

        public void WorkCompletedTimeoutInit()
        {
            _workCompletedTimer = new System.Timers.Timer();

            _workCompletedTimer.Elapsed += new ElapsedEventHandler(WorkCompleteTimeoutHandler);

            //Wait 40mins
            _workCompletedTimer.Interval = 2400000;
            //_workCompletedTimer.Interval = 1575525;
            _workCompletedTimer.AutoReset = true;
            _workCompletedTimer.Start();
        }

        public void WorkCompleteTimeoutHandler(object sender, EventArgs e)
        {
            Library.WriteErrorLog(DateTime.Now + " :: WORK COMPLETED TIMEOUT FIRED_REQUEST PRCOESS TAKEN TOO LONG!!");

            WorkComplete();

            _workCompletedTimer.AutoReset = false;
            _workCompletedTimer.Stop();
            _workCompletedTimer.Close();            
        }

        private void Register()
        {
            String name = Dns.GetHostName().ToUpper();
            var modeStr = System.Configuration.ConfigurationManager.AppSettings["MODE"];

            try
            {
                if ((_client != null))
                {
                    _client.Abort();
                    _client = null;
                }

                BroadcastorCallback cb = new BroadcastorCallback();
                cb.SetHandler(HandleBroadcast);

                System.ServiceModel.InstanceContext context =
                    new System.ServiceModel.InstanceContext(cb);
                _client =
                    new DataFeedService.InternalDataFeedServiceClient(context);

                _client.RegisterClient(RecipientAppID);

                LogNotifyer.SendMessage(RecipientAppID + " :: " + name + " " + modeStr + " :: Connection to Internal Feed" + "...Started OK");
            }
            catch (Exception ex)
            {
                LogNotifyer.SendMessage(RecipientAppID + " :: " + name + " " + modeStr + " :: Connection to Internal Feed...FAILED!");

                Console.WriteLine(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }

            //--------------------------------------------------------------------------------//            
            try
            {
                using (DatabaseManagerServiceImporter.DatabaseManagerServiceClient tempClient =
                    new DatabaseManagerServiceImporter.DatabaseManagerServiceClient())
                {
                    tempClient.Open();

                    tempClient.RetrieveFiles("", 1);
                    LogNotifyer.SendMessage(RecipientAppID + " :: " + name + " Connection to Database Manager Service " + " :: ...OK!");

                }
            }
            catch (Exception ex)
            {
                LogNotifyer.SendMessage(RecipientAppID + " :: " + name + " Connection to Database Manager Service " + " :: ...FAILED!");
                Console.WriteLine("Connection FAILED - DatabaseManager");
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
            //--------------------------------------------------------------------------------//

        }

        private void Initial(String triggerTimeParameter)
        {
            SystemEvents.TimeChanged += new EventHandler(SystemEvents_TimeChanged);

            _triggerTime = triggerTimeParameter;
            actionTimer = new System.Timers.Timer();

            actionTimer.Elapsed += new ElapsedEventHandler(Task);

            actionTimer.Interval = CalculateInterval();
            actionTimer.AutoReset = true;

            actionTimer.Start();
        }

        public void ApplyScheduler()
        {
            //GetScheduleTriggerTime();

            SystemEvents.TimeChanged += new EventHandler(SystemEvents_TimeChanged);

            actionTimer = new System.Timers.Timer();

            actionTimer.Elapsed += new ElapsedEventHandler(Task);

            actionTimer.Interval = CalculateInterval();
            actionTimer.AutoReset = true;

            actionTimer.Start();
        }

        public void CheckDatabaseSize()
        {
            String connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString;
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[1];

            int databaseSize = 0;

            using (SqlConnection myConn = new SqlConnection(connectionStr))
            {
                myConn.Open();

                String databaseConString = System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString;

                String[] strArray = databaseConString.Split(';');
                String initialCatalog = strArray[1];

                String[] tempStrArray = initialCatalog.Split('=');
                String databaseName = tempStrArray[1];

                SqlCommand sqlCommand = new SqlCommand("SELECT S.name, S.size FROM master.dbo.sysaltfiles S WHERE S.name ='" + databaseName + "'", myConn);

                sqlCommand.CommandType = CommandType.Text;
                SqlDataReader reader = sqlCommand.ExecuteReader();

                int sizeItem = reader.GetOrdinal("size"); //file size in KB

                while (reader.Read())
                {
                    databaseSize = reader.GetInt32(sizeItem);
                }
            }

            if (databaseSize >= 3500000) // about 3.33Gigbytes
            {
                DateTime date = DateTime.Now.AddDays(-300);

                String dateStr = String.Format("{0}-{1}-{2}", date.Year, date.Month, date.Day);

                using (SqlConnection myConn = new SqlConnection(connectionStr))
                {
                    myConn.Open();
                    SqlCommand sqlCommand = new SqlCommand("DELETE FROM dbo.DayTradeSummaries WHERE dbo.DayTradeSummaries.Date < '" + dateStr + "'", myConn);
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void LogToCSVFile(List<DateTime> dateTimeListing, string symbol, string timeFrame)
        {
            string logDatav = "Log To File :" + symbol + " : DataPoints To Log : " + dateTimeListing.Count() 
                + " TimeFrame : " + timeFrame;
            Console.WriteLine(logDatav);
            Logger.log.Info(logDatav);
            
            var fileName = System.Configuration.ConfigurationManager.AppSettings["LOGTOCSV"];
            if (!System.IO.File.Exists(fileName))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            }
            else
            {
                File.Delete(fileName);
                using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            }



            lock (locker)
            {
                using (CsvFileWriter writer = new CsvFileWriter(fileName))
                {
                    foreach (var rt in dateTimeListing) //result.Items
                    {
                        CsvRow csvRow = new CsvRow();

                        var sqlFormattedDate = rt.ToString("yyyy-MM-dd HH:mm:ss");  
                        csvRow.Add(symbol);
                        csvRow.Add(timeFrame);
                        csvRow.Add(sqlFormattedDate);
                        csvRow.Add("0");
                        csvRow.Add("0");
                        writer.WriteRow(csvRow);
                    }
                }
            }
        }

        private void UpdateLogToCSVFile(List<DataFeedService.TradeSummary> tradeSumamaryListing)
        {
            string logDatav = "Updates to Log File"; 
            Console.WriteLine(logDatav);
            Logger.log.Info(logDatav);

            var fileName = System.Configuration.ConfigurationManager.AppSettings["LOGTOCSV_UPDATE"];
            //if (!System.IO.File.Exists(fileName))
            //{
            //    //crude approach
            //    using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            //}
            //else
            //{
            //    File.Delete(fileName);
            //    using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            //}



            lock (locker)
            {
                using (CsvFileWriter writer = new CsvFileWriter(fileName))
                {
                    foreach (var rt in tradeSumamaryListing) //result.Items
                    {
                        CsvRow csvRow = new CsvRow();

                        var sqlFormattedDate = rt.DateTime.ToString("yyyy-MM-dd HH:mm:ss");
                        csvRow.Add(rt.SymbolID);
                        csvRow.Add(rt.TimeFrame);
                        csvRow.Add(sqlFormattedDate);
                        csvRow.Add("1");
                        csvRow.Add("1");
                        writer.WriteRow(csvRow);
                    }
                }
            }
        }


        private void WriteToCSVFile(List<DataFeedService.TradeSummary> tradeSummary)
        {
            var performance = System.Configuration.ConfigurationManager.AppSettings["PERFORMANCETEST"];

            if (performance != "NOOUTPUT")
            {
                lock (locker)
                {
                    //var fileName = OutputControlT("Forex", 0);

                    var fileName = OutputControlT(tradeSummary.FirstOrDefault().Exchange, 0);
                    try
                    {
                        using (CsvFileWriter writer = new CsvFileWriter(fileName))
                        {
                            foreach (var rt in tradeSummary) //result.Items
                            {
                                CsvRow csvRow = new CsvRow();

                                var sqlFormattedDate = rt.DateTime.ToString("yyyy-MM-dd HH:mm:ss");
                                csvRow.Add(sqlFormattedDate);

                                csvRow.Add(rt.Open.ToString());
                                csvRow.Add(rt.High.ToString());

                                csvRow.Add(rt.Low.ToString());
                                csvRow.Add(rt.Close.ToString());

                                csvRow.Add(rt.Volume.ToString());
                                csvRow.Add(rt.AdjustmentClose.ToString());
                                csvRow.Add(rt.SymbolID.Trim());
                                csvRow.Add(rt.TimeFrame.Trim());

                                if (rt.Exchange != "FOREX")
                                {
                                    csvRow.Add(rt.Exchange);
                                }
                                writer.WriteRow(csvRow);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log.Error(ex);
                        Console.WriteLine(ex.ToString());
                        Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                    }
                }
            }
        }

        private void SetTradeSummaryHandler(List<DataFeedService.TradeSummary> tradeSummary)
        {
            if (tradeSummary.Count > 0)
            {
                //I know a bit of a hack!..DOA
                if (_bDataFillerMood == false && tradeSummary.Count > 0 || _bDataFillerMood == false && tradeSummary.FirstOrDefault().Exchange == "SynchComplete") //skip realtime data
                {
                    if (tradeSummary.FirstOrDefault().Exchange != "SynchComplete")
                    {
                        WriteToCSVFile(tradeSummary); 
                    }
                    else
                    {
                        if (_SymbolCollection.Any() == false)
                        {
                            if (_timeFrameListQueue.Any() || _timeFrameListQueue.Any() == false && _taskWork.Any())
                            {
                                GetLatestSymbolListQueueBased();

                                _currentTimeFrame = _timeFrameListQueue.Dequeue();
                                GetDownloadData(_currentTimeFrame, _selectedExchange);
                            }
                            else
                            {
                                WorkComplete();
                            }
                        }
                        else
                        {
                            //Job done get the other symbol data
                            GetDownloadData(_currentTimeFrame, _selectedExchange);
                        }
                    }
                }
                else
                {
                    if (_bDataFillerMood) //single data point here
                    {
                        if (tradeSummary.FirstOrDefault().DateTime != DateTime.MinValue)
                        {

                            var groupings = tradeSummary.GroupBy(a => new { a.SymbolID, a.TimeFrame });
                            var currentGroupList = groupings.ToList();
                            foreach (var item in currentGroupList.ToList())
                            {
                                foreach (var ad in item.ToList())
                                {
                                    var selectingkey = ad.SymbolID + ad.TimeFrame;
                                    bool retrieveBool = false;
                                    if (_dataFillerLookUpComplete.TryGetValue(selectingkey, out retrieveBool))
                                    {
                                        _dataFillerLookUpComplete[selectingkey] = true;
                                    }
                                }
                            }
                            //write to file
                            WriteToCSVFile(tradeSummary);

                            //log to db as checked and found
                            //on seperate thread
                            //CheckedAndFoundDateTimes(tradeSummary);

                            UpdateLogToCSVFile(tradeSummary);
                            _WorkCompleteFlagDataFillerThreshold--;


                            bool notCompleted = _dataFillerLookUpComplete.Select(d => d.Value).ToList().Exists(w => w == false);

                            if (notCompleted == false && _bWorkCompleteFlagDataFiller == false)
                            {
                                _bWorkCompleteFlagDataFiller = true;
                                Console.WriteLine("Finalising Received Data...");
                                Logger.log.Info("Finalising Received Data...");

                                Thread.Sleep(120000);
                                CheckedAndFoundDateTimes();

                                MarkedAsCheckedForUnFound();
                                _bDataFillerMood = false;
                                Logger.log.Info("DataFiller Flag False");
                                Console.WriteLine("DataFiller Flag False");

                                Console.WriteLine("WorkComplete");
                                WorkComplete();
                            }
                            else
                            {
                                Console.WriteLine("Received Data..." + tradeSummary.Count);

                                Logger.log.Info("Not Completed More Data Yet to be receieved");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Unexpected");
                        }

                        if (_dataFillerTimer != null)
                        {
                            _dataFillerTimer.Stop();
                            _dataFillerTimer.Close();
                        }

                        _dataFillerTimer = new System.Timers.Timer(300000); //5min wait
                        _dataFillerTimer.Elapsed += new ElapsedEventHandler(DataFillerTask);
                        _dataFillerTimer.AutoReset = false;
                        _dataFillerTimer.Start();
                    }
                    else
                    {
                        Console.WriteLine("Failed To Write To Output CSV");
                        Logger.log.Info("Failed To Write To Output CSV");
                        Library.WriteErrorLog("Failed To Write To Output CSV");
                    }
                }
            }
        }

        public bool ConsumeHistoricalDataCSV()
        {
            return DownloadData("EndOfDay");
        }

        public bool ConsumeHistoricalTickDataCSV()
        {
            return DownloadData("Tick");
        }

        private bool DownloadData(String timeFrame)
        {
            DateTime startTime = DateTime.Now;

            String triggerlogFileName = "TriggerLog.txt";
            if (!System.IO.File.Exists(triggerlogFileName))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(triggerlogFileName)) { }
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
            {
                file.WriteLine(String.Format("StartTime  {0}  {1}", DateTime.Now.TimeOfDay, DateTime.Now));
                //file.WriteLine("");
            }

            //-------------------------------------------------------------------//

            if (GetLatestSymbolList())
            {
                foreach (var exchangeInfo in _exchangeInfoList)
                {

                    EFileInfo = exchangeInfo;
                    //_iqFeedHistoricalDownloader.OutputFileName = OutputControl(EFileInfo.exchangeSymbol);
                    var path = OutputControl(EFileInfo.exchangeSymbol);

                    //String selector = exchangeInfo.exchangeSymbol;

                    String selector = "TRADES";
                    switch (Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;

                        case MODE.LiveTest:
                            {
                                selector += "_LIVE-TEST";
                            }
                            break;
                    }

                    if (exchangeInfo.exchangeSymbol == "Forex") selector += "_" + exchangeInfo.exchangeSymbol;

                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                    String stockExchange = EFileInfo.exchangeSymbol;

                    List<String> symbols = symbolList[stockExchange];

                    String[] dateArray = System.Configuration.ConfigurationManager.AppSettings["EARLIEST_DATE"].Split(';');

                    DateTime dateStart = new DateTime(Convert.ToInt32(dateArray[0]), Convert.ToInt32(dateArray[1]), Convert.ToInt32(dateArray[2]));

                    foreach (var itemSym in symbols)
                    {
                        try
                        {
                            using (SqlConnection con = new SqlConnection(settings.ToString()))
                            {
                                SqlCommand command = null;

                                // sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", "OXM", "", "", 10, "", "", "");
                                if (timeFrame == "Tick")
                                {
                                    command = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID='" + itemSym + "' AND dbo.DayTradeSummaries.Date=(SELECT MAX(dbo.DayTradeSummaries.Date) FROM dbo.DayTradeSummaries WHERE dbo.DayTradeSummaries.SymbolID='" + itemSym + "')", con);
                                }
                                else
                                {
                                    //command = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID='" + itemSym + "' AND dbo.DayTradeSummaries.Date=(SELECT MAX(dbo.DayTradeSummaries.Date) FROM dbo.DayTradeSummaries WHERE dbo.DayTradeSummaries.SymbolID='" + itemSym + "')", con);

                                    command = new SqlCommand("SELECT * FROM dbo.tblTrades where dbo.tblTrades.SymbolID='" + itemSym + "' AND dbo.tblTrades.DateTime=(SELECT MAX(dbo.tblTrades.DateTime) FROM dbo.tblTrades WHERE dbo.tblTrades.SymbolID='" + itemSym + "')", con);
                                }
                                command.CommandTimeout = 0;

                                con.Open();

                                SqlDataReader dataReader = command.ExecuteReader();

                                DateTime startDateRange = dateStart;//earliest date

                                DateTime endDateRange = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                                switch (DateTime.Now.DayOfWeek)
                                {
                                    case DayOfWeek.Saturday:
                                        {
                                            endDateRange = endDateRange.AddDays(-1);
                                        }
                                        break;

                                    case DayOfWeek.Sunday:
                                        {
                                            endDateRange = endDateRange.AddDays(-2);
                                        }
                                        break;
                                }


                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {
                                        startDateRange = Convert.ToDateTime(dataReader["DateTime"]);
                                    }

                                    startDateRange = startDateRange.AddDays(1);
                                    switch (startDateRange.DayOfWeek)
                                    {
                                        case DayOfWeek.Saturday:
                                            {
                                                startDateRange = startDateRange.AddDays(2);
                                            }
                                            break;

                                        case DayOfWeek.Sunday:
                                            {
                                                startDateRange = startDateRange.AddDays(1);
                                            }
                                            break;
                                    }
                                }

                                if (startDateRange <= endDateRange)
                                {
                                    TimeSpan tspan = endDateRange - startDateRange;

                                    String dateStr = startDateRange.ToString("yyyyMMdd");

                                    string sRequest = "";
                                    // HDT,SYMBOL,BEGINDATE,ENDDATE,MAXDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                                    // sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", "OXM", "", "", 10, "", "", "");
                                    if (timeFrame == "Tick")
                                    {
                                    }
                                    else
                                    {
                                        sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", itemSym, dateStr, "", "", "", "", "");
                                    }



                                    // send it to the feed via the socket
                                    String exchange = exchangeInfo.exchangeSymbol != "Forex" ? exchangeInfo.exchangeSymbol : "Forex";
                                    _iqFeedHistoricalDownloader.Run(sRequest, itemSym, exchange, timeFrame, path);

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Generic Exception for " + itemSym + ":: " + ex.Message);
                            Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                        }
                    }
                }

                // Collect all generations of memory.
                GC.Collect();
            }
            //handle failure at this level?


            //---------------------------------------------------//
            //end time
            TimeSpan span = DateTime.Now - startTime;

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
            {
                file.WriteLine(String.Format("EndTime  {0}  {1} Total Duration::{2}", DateTime.Now.TimeOfDay, DateTime.Now, span));
                file.WriteLine("");
            }

            //DataConsumptionCompleted
            DataConsumptionCompleted(_currentDownloadFile, _currentSelector);

            System.Threading.Thread.Sleep(30000);

            using (DatabaseManagerServiceImporter.DatabaseManagerServiceClient dbmClient = new DatabaseManagerServiceImporter.DatabaseManagerServiceClient())
            {
                dbmClient.Open();//_fileName
                dbmClient.RetrieveRawDataFiles("", (int)Mode);

            }
            return false;
        }

        private void EmailNotificationCheck()
        {
            //var message = new StringBuilder();
            //message.AppendLine("Trade Data currently in being consummed/imported to central database.\n\n");
            //message.AppendLine("Date :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            //message.AppendLine("Data Manufacturing Process\n");

            //new EmailManager().Send(null, "STAGE (1) Raw Trade Data Importation In Progress", message.ToString());


            var message = new StringBuilder();
            message.AppendLine("Trade Data currently in being downloaded and readied for central database.\n\n");
            message.AppendLine("Date :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            message.AppendLine("Data Manufacturing Process\n");

            new EmailManager().Send(null, "STAGE (1) Raw Trade Data Consumption In Progress", message.ToString());
        }

        public bool ConsumeHistoricalDataCSVQueueBased()
        {
            //get latest items
            GetTaskQueueBased();           

            var timeframeList = System.Configuration.ConfigurationManager.AppSettings["TimeFrameList"].Split(',').ToList();
            foreach (var item in timeframeList)
            {
                _timeFrameListQueue.Enqueue(item);
            }

            
            if (GetLatestSymbolListQueueBased())
            {
                //_taskWork.Dequeue();

                Library.WriteErrorLog(DateTime.Now + " :: Begin Data Request");

                WorkCompletedTimeoutInit();

                EmailNotificationCheck();

                //-------------------------------------------------------------------//
                _selectedExchange = _SymbolCollection.FirstOrDefault().Exchange;

                _currentTimeFrame = _timeFrameListQueue.Dequeue();

                Library.WriteErrorLog(DateTime.Now + " :: Symbol Combinations " + _SymbolCollection.Count);

                GetDownloadData(_currentTimeFrame, _selectedExchange);

                //Dispose();
                Library.WriteErrorLog(DateTime.Now + " :: Data Request Scope End");

                //--------------------------------------------------//
                Console.ReadLine();//
            }
            return false;
        }

        public bool DataFiller()
        {
            var fileName = System.Configuration.ConfigurationManager.AppSettings["LOGTOCSV_UPDATE"];
            if (!System.IO.File.Exists(fileName))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            }
            else
            {
                File.Delete(fileName);
                using (System.IO.FileStream fs = System.IO.File.Create(fileName)) { }
            }



            Logger.log.Info("Running DataFiller");
            Console.WriteLine("Running DataFiller");


            _bDataFillerMood = true;
            Logger.log.Info("DataFiller Flag True");
            Console.WriteLine("DataFiller Flag True");

            _bWorkCompleteFlagDataFiller = false;

            if (SkipGenerateArtificalDateTimes == false)
            {
                GenerateArtificalDateTimes();
            }
            else
            {
                GetLatestSymbolsList();
                _timeframeList = System.Configuration.ConfigurationManager.AppSettings["TimeFrameList"].Split(',').ToList();
                _timeframeList = _timeframeList.Select(m => m.Trim()).ToList();
            }

            var kvp = GetDateTimeMaxMin();

            DateTime startDateTime = new DateTime(kvp.Key.Year, kvp.Key.Month, kvp.Key.Day);
            DateTime endDateTimeExtents = new DateTime(kvp.Value.Year, kvp.Value.Month, kvp.Value.Day);

            var timeSpanSlices = endDateTimeExtents - startDateTime;
            var count = Convert.ToInt32(timeSpanSlices.TotalDays);

            Console.WriteLine("Cycle Rounds : " + count);
            Logger.log.Info("Cycle Rounds : " + count);

            for (int dayCycleItem = 1; dayCycleItem < count; dayCycleItem++)
            {
                DateTime endDateTime = startDateTime.Add(new TimeSpan(dayCycleItem, 0, 0, 0));

                if (endDateTime <= endDateTimeExtents)
                {
                    var totalExpected = GetUncheckedMissingDataPointsCountDates(startDateTime, endDateTime);
                    Console.WriteLine("TradeSummary Items Expected : " + totalExpected);
                    Logger.log.Info("TradeSummary Items Expected : " + totalExpected);

                    //Get all unchecked data  points
                    var tradeListing = GetUncheckedMissingDataPoints(startDateTime, endDateTime);

                    //request from data feed
                    if (tradeListing != null)
                    {
                        //_WorkCompleteFlagDataFillerThreshold = tradeListing.Count;
                        Console.WriteLine("Cycle Extents : " + startDateTime.ToString() + " : " + endDateTime.ToString());
                        Console.WriteLine("Requesting Missing DataPoints From DataFeed : Count => " + tradeListing.Count);

                        Logger.log.Info("Cycle Extents : " + startDateTime.ToString() + " : " + endDateTime.ToString());
                        Logger.log.Info("Requesting Missing DataPoints From DataFeed : Count => " + tradeListing.Count);

                        var groupings = tradeListing.GroupBy(a => new { a.SymbolID, a.TimeFrame });
                        var currentGroupList = groupings.ToList();
                        _WorkCompleteFlagDataFillerThreshold = currentGroupList.Count;

                        Console.WriteLine("Grouped Symbol and TimeFrame : Count => " + currentGroupList.Count);
                        Logger.log.Info("Grouped Symbol and TimeFrame : Count => " + currentGroupList.Count);


                        foreach (var item in currentGroupList.ToList())
                        {
                            foreach (var ad in item.ToList())
                            {
                                var selectingkey = ad.SymbolID + ad.TimeFrame;
                                if (DoesSymbolExist(ad.SymbolID))
                                {
                                    _dataFillerLookUpComplete.TryAdd(selectingkey, false);
                                }
                                else
                                {
                                    Logger.log.Info("SymbolID Not Exist " + selectingkey);
                                    Console.WriteLine("SymbolID Not Exist " + selectingkey);
                                }
                            }
                        }

                        if (_WorkCompleteFlagDataFillerThreshold > 0)
                        {
                            foreach (var item in _DirectSymbolCollection)
                            {
                                foreach (var timeFrame in _timeframeList)
                                {
                                    if (currentGroupList != null)
                                    {
                                        if (currentGroupList.Any())
                                        {
                                            var selectItems = currentGroupList.Where(m => m.Any(j => j.TimeFrame == timeFrame && j.SymbolID == item));
                                            if (selectItems != null)
                                            {
                                                if (selectItems.Any())
                                                {
                                                    var earliest = selectItems.OrderBy(u => u.FirstOrDefault().DateTime).FirstOrDefault().FirstOrDefault();

                                                    GetIntradayDataAsynch(item, timeFrame, "Forex", earliest.DateTime);
                                                    Thread.Sleep(2000); //wait 2seconds
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("WorkCompleteFlagDataFillerThreshold None = 0");
                            Logger.log.Info("WorkCompleteFlagDataFillerThreshold None = 0");
                        }
                    }
                    else
                    {
                        Logger.log.Info("No Data Filling Required");
                        Console.WriteLine("No Data Filling Required");
                    }

                    Logger.log.Info("DataFiller Requesting Phase Done");
                    Console.WriteLine("DataFiller Requesting Phase Done");
                }
            }

            return false;
        }

        private List<DateTime> CreateDateTimes(TimeSpan span, DateTime startDateTime, DateTime endDateTime)
        {
            List<DateTime> dateTimeList = new List<DateTime>();

            if (span.TotalMilliseconds > 0.0)
            {
                bool continueLoop = true;

                while (continueLoop)
                {
                    startDateTime = startDateTime + span;
                    if (startDateTime <= endDateTime)
                    {
                        dateTimeList.Add(startDateTime);
                    }
                    else
                    {
                        continueLoop = false;
                    }
                }
            }
            else
            {
                int nin = 0;
            }
            return dateTimeList;
        }

        private bool DoesSymbolExist(string symbolID)
        {
            bool bExist = false;
            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand command = new SqlCommand("proc_DoesSymbolExist", con);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 200;
                    con.Open();

                    command.Parameters.AddWithValue("@SymbolID", symbolID);

                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            if (String.IsNullOrEmpty(dataReader["SymbolID"].ToString()) == false)
                            {
                                bExist = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.log.Error(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
            return bExist;
        }


        private void CheckDateTimePointsAgainstDBVersion(List<DateTime> dateTimeList, string timeFrame, DateTime startDateTime, DateTime endDateTime)
        {
            //Get DB equvaluent

            foreach (var dateItem in _DirectSymbolCollection)
            {
                var currentDateTimeList = new List<DateTime>();

                var exchangeInUse = "Forex";

                String selector = "TRADES";
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                }
                String stockExchange = exchangeInUse;

                if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
                {
                    selector += "_" + exchangeInUse;
                }


                bool continueFetchingData = true;
                while (continueFetchingData)
                {
                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                    try
                    {
                        using (SqlConnection con = new SqlConnection(settings.ToString()))
                        {
                            //SqlCommand command = new SqlCommand("SELECT [DateTime] FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + dateItem +
                            //"' AND DD.[TimeFrame] = '" +timeFrame+ "' ORDER BY [DateTime] ASC", con);

                            //SqlCommand command = new SqlCommand("SELECT [DateTime] FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + dateItem +
                            //"' AND DD.[TimeFrame] = '" + timeFrame + "' ORDER BY [DateTime] ASC", con);


                            SqlCommand command = new SqlCommand("SELECT [DateTime] FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + dateItem
                              + "' AND DD.[TimeFrame] = '" + timeFrame + "' AND DD.[DateTime] BETWEEN '" + startDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "' AND '"
                              + endDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY [DateTime] ASC", con);

                            //command.CommandType = CommandType.StoredProcedure;
                            //command.CommandTimeout = 200;
                            command.CommandTimeout = 0; //5mins

                            con.Open();

                            SqlDataReader dataReader = command.ExecuteReader();

                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                   
                                    currentDateTimeList.Add(Convert.ToDateTime(dataReader["DateTime"]));
                                }
                            }
                            continueFetchingData = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        Logger.log.Error(ex.ToString());
                        Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                    }
                }

                string logData = dateItem + " : Current DB List : " + currentDateTimeList.Count;
                Console.WriteLine(logData);
                Logger.log.Info(logData);


                var dontExist = dateTimeList.Where(m => currentDateTimeList.Any(o => o != m));
                if (dontExist.Any())
                {
                    string logDatav = dateItem + " : DatePoints Dont Exist In DB : " + dontExist.Count();
                    Console.WriteLine(logDatav);
                    Logger.log.Info(logDatav);

                    LogNonExistingDateTimes(dontExist.ToList(), dateItem, timeFrame);
                }
            }
        }

        private void CheckedAndFoundDateTimesOld(List<DataFeedService.TradeSummary> tradeSumamaryListing)
        {
            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                foreach (var datetimeItem in tradeSumamaryListing)
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand sqlCommand = new SqlCommand("proc_UpdateMissingDateTimePoints", con);
                        sqlCommand.CommandTimeout = 0;
                        con.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.Parameters.AddWithValue("@SymbolID", datetimeItem.SymbolID);
                        sqlCommand.Parameters.AddWithValue("@TimeFrame", datetimeItem.TimeFrame);
                        sqlCommand.Parameters.AddWithValue("@DateTime", datetimeItem.DateTime);
                        sqlCommand.Parameters.AddWithValue("@Checked", true);
                        sqlCommand.Parameters.AddWithValue("@Found", true);

                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }


        private void CheckedAndFoundDateTimes()
        {
            //Thread.Sleep(12000); //12seconds

            //UpdateLogToCSVFile(tradeSumamaryListing);
            
            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_BulkUpdateMissingDateTimePoints", con);
                    sqlCommand.CommandTimeout = 0;
                    con.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.log.Error(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }

        private void MarkedAsCheckedForUnFound()
        {
            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand sqlCommand = new SqlCommand("proc_MarkedAsCheckedForUnFound", con);
                        sqlCommand.CommandTimeout = 0;
                        con.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.log.Error(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }

        private void LogNonExistingDateTimesOld(List<DateTime> dateTimeListing, string symbol, string timeFrame)
        {
            LogToCSVFile(dateTimeListing, symbol, timeFrame);

            string logDatav = symbol + " : DataPoints To Log : " + dateTimeListing.Count() + " TimeFrame : " + timeFrame;
            Console.WriteLine(logDatav);
            Logger.log.Info(logDatav);

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                foreach (var datetimeItem in dateTimeListing)
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand sqlCommand = new SqlCommand("proc_LogMissingDateTimePoints", con);
                        sqlCommand.CommandTimeout = 0;
                        con.Open();

                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@SymbolID", symbol);
                        sqlCommand.Parameters.AddWithValue("@TimeFrame", timeFrame);
                        sqlCommand.Parameters.AddWithValue("@DateTime", datetimeItem);

                        sqlCommand.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }


        private void LogNonExistingDateTimes(List<DateTime> dateTimeListing, string symbol, string timeFrame)
        {
            LogToCSVFile(dateTimeListing, symbol, timeFrame);

            string logDatav = symbol + " : DataPoints To Log : " + dateTimeListing.Count() + " TimeFrame : " + timeFrame;
            Console.WriteLine(logDatav);
            Logger.log.Info(logDatav);

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {

                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand sqlCommand = new SqlCommand("proc_BulkLogMissingDateTimePoints", con);
                        sqlCommand.CommandTimeout = 0;
                        con.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        sqlCommand.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.log.Error(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }

        private void GenerateArtificalDateTimes()
        {
            Console.WriteLine("GenerateArtificalDateTimes()");

            var kvp = GetDateTimeMaxMin();

            _timeframeList = System.Configuration.ConfigurationManager.AppSettings["TimeFrameList"].Split(',').ToList();
            _timeframeList = _timeframeList.Select(m => m.Trim()).ToList();

            DateTime startDateTime = new DateTime(kvp.Key.Year, kvp.Key.Month, kvp.Key.Day);

            DateTime endDateTimeExtents = new DateTime(kvp.Value.Year, kvp.Value.Month, kvp.Value.Day);

            var timeSpanSlices = endDateTimeExtents - startDateTime;
            var count = Convert.ToInt32(timeSpanSlices.TotalDays);

            //for (int dayCycleItem = 1; dayCycleItem < count;  dayCycleItem++)
            {
             
               // DateTime endDateTime = startDateTime.Add(new TimeSpan(dayCycleItem, 0, 0, 0));
                DateTime endDateTime = endDateTimeExtents;

                //if (endDateTime <= endDateTimeExtents)
                {
                    GetLatestSymbolsList();

                    //_timeframeList.Clear();
                    //_timeframeList = new List<string>() { "15min", "EndOfDay" };

                    Console.WriteLine("Generating DateList to check against DB");
                    Console.WriteLine("Range : " + startDateTime.ToString() + " :: " + endDateTime.ToString());


                    foreach (var item in _timeframeList)
                    {
                        TimeSpan span = new TimeSpan();

                        switch (item)
                        {
                            case "1min":
                                {
                                    span = new TimeSpan(0, 1, 0);
                                } break;

                            case "2min":
                                {
                                    span = new TimeSpan(0, 5, 0);
                                } break;

                            case "3min":
                                {
                                    span = new TimeSpan(0, 3, 0);
                                } break;

                            case "4min":
                                {
                                    span = new TimeSpan(0, 4, 0);
                                } break;

                            case "5min":
                                {
                                    span = new TimeSpan(0, 5, 0);
                                } break;

                            case "10min":
                                {
                                    span = new TimeSpan(0, 10, 0);
                                } break;

                            case "15min":
                                {
                                    span = new TimeSpan(0, 15, 0);
                                } break;

                            case "30min":
                                {
                                    span = new TimeSpan(0, 30, 0);
                                } break;

                            case "1hour":
                                {
                                    span = new TimeSpan(1, 0, 0);
                                } break;

                            case "2hour":
                                {
                                    span = new TimeSpan(2, 0, 0);
                                } break;

                            case "3hour":
                                {
                                    span = new TimeSpan(3, 0, 0);
                                } break;

                            case "4hour":
                                {
                                    span = new TimeSpan(4, 0, 0);
                                } break;

                            case "EndOfDay":
                                {
                                    span = new TimeSpan(1, 0, 0, 0);
                                } break;
                        }
                        var dateList = CreateDateTimes(span, startDateTime, endDateTime);

                        string logData = item + " :: DatePoints :" + dateList.Count + " Checking DB Collection ";
                        Console.WriteLine(logData);
                        Logger.log.Info(logData);

                        CheckDateTimePointsAgainstDBVersion(dateList, item, startDateTime, endDateTime);
                    }
                }

                //startDateTime = endDateTime;
            }
        }

        private KeyValuePair<DateTime, DateTime> GetDateTimeMaxMin()
        {
            Console.WriteLine("Getting DateTimeMaxMin...");
            bool bkeepTrying = true;

            KeyValuePair<DateTime, DateTime> kvp = new KeyValuePair<DateTime, DateTime>();
            while (bkeepTrying)
            {
                var exchangeInUse = "Forex";

                String selector = "TRADES";
                switch (Mode)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;

                }
                String stockExchange = exchangeInUse;

                if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
                {
                    selector += "_" + exchangeInUse;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand command = null;

                        command = new SqlCommand("proc_GetDateMaxMin", con);
                        command.CommandType = CommandType.StoredProcedure;
                       // command.CommandTimeout = 120; //2minutes
                        command.CommandTimeout = 600;

                        con.Open();

                        SqlDataReader dataReader = command.ExecuteReader();

                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                kvp = new KeyValuePair<DateTime, DateTime>(Convert.ToDateTime(dataReader["Earliest"]),
                                    Convert.ToDateTime(dataReader["Latest"]));
                                bkeepTrying = false;

                                var stritem = "StartDateTime : " + kvp.Key + " EndDateTime : " + kvp.Value;
                                Console.WriteLine(stritem);
                                Logger.log.Info(stritem);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
            }
            return kvp;
        }

        private int GetUncheckedMissingDataPointsCount()
        {
            int dataCount = 0;
            Console.WriteLine("Getting UncheckedMissingDataPoints COUNT...");

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }


            bool continueFetchingData = true;
            while (continueFetchingData)
            {
                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand command = null;

                        command = new SqlCommand("proc_GetUncheckedMissingDateTimePointsCount", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        con.Open();

                        SqlDataReader dataReader = command.ExecuteReader();

                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                dataCount = Convert.ToInt32(dataReader["Count"].ToString());
                            }
                        }
                        continueFetchingData = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
            }
            return dataCount;
        }

        private int GetUncheckedMissingDataPointsCountDates(DateTime startDateTime, DateTime endDateTime)
        {
            int dataCount = 0;
            Console.WriteLine("Getting UncheckedMissingDataPoints Within Dates COUNT...");

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }


            Console.WriteLine("Date Range Missing Points : " + startDateTime.ToString("yyyy-MM-dd")
                + " to " + endDateTime.ToString("yyyy-MM-dd"));
            Logger.log.Info("Date Range Missing Points : " + startDateTime.ToString("yyyy-MM-dd")
                + " to " + endDateTime.ToString("yyyy-MM-dd"));


            bool continueFetchingData = true;
            while (continueFetchingData)
            {
                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand command = null;

                        command = new SqlCommand("proc_GetUncheckedMissingDateTimePointsDatesParaWithDates", con);
                        command.Parameters.AddWithValue("@startDateTime", startDateTime.ToString("yyyy-MM-dd"));
                        command.Parameters.AddWithValue("@endDateTime", endDateTime.ToString("yyyy-MM-dd"));
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;


                        con.Open();

                        SqlDataReader dataReader = command.ExecuteReader();

                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                dataCount = Convert.ToInt32(dataReader["Count"].ToString());
                            }
                        }
                        continueFetchingData = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
            }
            return dataCount;
        }


        private List<DataFeedService.TradeSummary> GetUncheckedMissingDataPoints(DateTime startDateTime, DateTime endDateTime)
        {
            Console.WriteLine("Getting UncheckedMissingDataPoints...");
            List<DataFeedService.TradeSummary> tradeSummary = new List<DataFeedService.TradeSummary>();

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }


            bool continueFetchingData = true;
            while (continueFetchingData)
            {
                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand command = null;

                        command = new SqlCommand("proc_GetUncheckedMissingDateTimePointsDatesPara", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;
                        command.Parameters.AddWithValue("@startDateTime", startDateTime.ToString("yyyy-MM-dd"));
                        command.Parameters.AddWithValue("@endDateTime", endDateTime.ToString("yyyy-MM-dd"));

                        con.Open();

                        SqlDataReader dataReader = command.ExecuteReader();

                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                               
                                DataFeedService.TradeSummary tradeSum = new DataFeedService.TradeSummary
                                {
                                    SymbolID = dataReader["SymbolID"].ToString(),
                                    DateTime = Convert.ToDateTime(dataReader["DateTime"]),
                                    TimeFrame = dataReader["TimeFrame"].ToString()
                                };
                                tradeSummary.Add(tradeSum);
                            }
                        }
                        continueFetchingData = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
            }
            return tradeSummary;
        }

        private List<DataFeedService.TradeSummary> GetUncheckedMissingDataPoints()
        {
            Console.WriteLine("Getting UncheckedMissingDataPoints...");
            List<DataFeedService.TradeSummary> tradeSummary = new List<DataFeedService.TradeSummary>();

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }


            bool continueFetchingData = true;
            while (continueFetchingData)
            {
                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand command = null;

                        command = new SqlCommand("proc_GetUncheckedMissingDateTimePoints", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 0;

                        con.Open();

                        SqlDataReader dataReader = command.ExecuteReader();

                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                               
                                DataFeedService.TradeSummary tradeSum = new DataFeedService.TradeSummary
                                {
                                    SymbolID = dataReader["Count"].ToString(),
                                    DateTime = Convert.ToDateTime(dataReader["DateTime"]),
                                    TimeFrame = dataReader["TimeFrame"].ToString()
                                };
                                tradeSummary.Add(tradeSum);
                            }
                        }
                        continueFetchingData = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
            }
            return tradeSummary;
        }

        public bool ConsumeHistoricalDataCSVBased(String symbolID)
        {
            if (symbolID != "")
            {
                DateTime startTime = DateTime.Now;

                String triggerlogFileName = "TriggerLog.txt";
                if (!System.IO.File.Exists(triggerlogFileName))
                {
                    //crude approach
                    using (System.IO.FileStream fs = System.IO.File.Create(triggerlogFileName)) { }
                }

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
                {
                    file.WriteLine(String.Format("StartTime  {0}  {1}", DateTime.Now.TimeOfDay, DateTime.Now));
                    //file.WriteLine("");
                }

                //-------------------------------------------------------------------//
                _selectedExchange = "Forex";
                GetDownloadData("1min", _selectedExchange);

                //--------------------------------------------------//
               

                //---------------------------------------------------//
                //end time
                TimeSpan span = DateTime.Now - startTime;

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(triggerlogFileName, true))
                {
                    file.WriteLine(String.Format("EndTime  {0}  {1} Total Duration::{2}", DateTime.Now.TimeOfDay, DateTime.Now, span));
                    file.WriteLine("");
                }

            }
            return false;
        }

        public static bool GetSingleDownloadData(String timeFrame, String exchangeInUse, String symbolID)
        {
            String symbol = symbolID;

            if (String.IsNullOrEmpty(symbol) == false)
            {
                var path = OutputControlT(exchangeInUse, 0);

                String selector = "TRADES";
                switch (MODE.Test)
                {
                    case MODE.Test:
                        {
                            selector += "_TEST";
                        }
                        break;

                    case MODE.Live:
                        {
                            selector += "_LIVE";
                        }
                        break;
                }
                String stockExchange = exchangeInUse;
                String itemSym = symbol;

                if (exchangeInUse == "Forex")
                {
                    var splitArray = Regex.Split(symbol, ".FXCM");
                    itemSym = splitArray.FirstOrDefault();

                    selector += "_" + exchangeInUse;
                }

                var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];



                String[] dateArray = System.Configuration.ConfigurationManager.AppSettings["EARLIEST_DATE"].Split(';');

                DateTime dateStart = new DateTime(Convert.ToInt32(dateArray[0]), Convert.ToInt32(dateArray[1]), Convert.ToInt32(dateArray[2]));
                try
                {
                    using (SqlConnection con = new SqlConnection(settings.ToString()))
                    {
                        SqlCommand command = null;
                        // sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", "OXM", "", "", 10, "", "", "");
                        if (timeFrame == "Tick")
                        {
                            command = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID='" + itemSym + "' AND dbo.DayTradeSummaries.Date=(SELECT MAX(dbo.DayTradeSummaries.Date) FROM dbo.DayTradeSummaries WHERE dbo.DayTradeSummaries.SymbolID='" + itemSym + "')", con);
                        }
                        else
                        {
                            //command = new SqlCommand("SELECT * FROM dbo.DayTradeSummaries where dbo.DayTradeSummaries.SymbolID='" + itemSym + "' AND dbo.DayTradeSummaries.Date=(SELECT MAX(dbo.DayTradeSummaries.Date) FROM dbo.DayTradeSummaries WHERE dbo.DayTradeSummaries.SymbolID='" + itemSym + "')", con);
                            command = new SqlCommand("SELECT * FROM dbo.tblTrades where dbo.tblTrades.SymbolID='" + itemSym + "' AND dbo.tblTrades.DateTime=(SELECT MAX(dbo.tblTrades.DateTime) FROM dbo.tblTrades WHERE dbo.tblTrades.SymbolID='" + itemSym + "')", con);
                        }
                        command.CommandTimeout = 0;

                        con.Open();

                        SqlDataReader dataReader = command.ExecuteReader();

                        DateTime startDateRange = dateStart;//earliest date

                        DateTime endDateRange = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                        switch (DateTime.Now.DayOfWeek)
                        {
                            case DayOfWeek.Saturday:
                                {
                                    endDateRange = endDateRange.AddDays(-1);
                                }
                                break;

                            case DayOfWeek.Sunday:
                                {
                                    endDateRange = endDateRange.AddDays(-2);
                                }
                                break;
                        }


                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                startDateRange = Convert.ToDateTime(dataReader["DateTime"]);
                            }

                            startDateRange = startDateRange.AddDays(1);
                            switch (startDateRange.DayOfWeek)
                            {
                                case DayOfWeek.Saturday:
                                    {
                                        startDateRange = startDateRange.AddDays(2);
                                    }
                                    break;

                                case DayOfWeek.Sunday:
                                    {
                                        startDateRange = startDateRange.AddDays(1);
                                    }
                                    break;
                            }
                        }

                        if (startDateRange <= endDateRange)
                        {
                            TimeSpan tspan = endDateRange - startDateRange;
                            int totalDayCount = tspan.Days;
                            int totalWeekendCount = 0;
                            
                            for (int i = 0; i < totalDayCount; i++)
                            {
                                DateTime testDate = startDateRange.AddDays(i);
                                if (testDate.DayOfWeek == DayOfWeek.Saturday || testDate.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    totalWeekendCount++;
                                }
                            }

                            //int maxPoints = totalDayCount - totalWeekendCount;
                            int maxPoints = totalDayCount;

                            if (maxPoints < 2500)
                            {
                                String dateStr = startDateRange.ToString("yyyyMMdd");

                                string sRequest = "";
                                // HDT,SYMBOL,BEGINDATE,ENDDATE,MAXDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                                // sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", "OXM", "", "", 10, "", "", "");
                                if (timeFrame == "Tick")
                                {
                                }
                                else
                                {
                                    //sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", symbol, dateStr, "", "5000", "", "", "");
                                    sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", symbol, dateStr, "", maxPoints, "1", "", "");
                                    //sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", symbol, "", "", "", "", "", "");
                                }

                                // send it to the feed via the socket
                                String exchange = exchangeInUse != "Forex" ? exchangeInUse : "Forex";

                                _iqFeedHistoricalDownloaderT.Run(sRequest, itemSym, exchange, timeFrame, path);
                            }
                            else
                            {
                                int remainder = maxPoints  % 2500;
                                int count = maxPoints - remainder;
                                int trips = count / 2500;

                               DateTime tempDate = startDateRange;
                               String startDateStr = tempDate.ToString("yyyyMMdd");
                               String endDateStr = tempDate.AddDays(2500).ToString("yyyyMMdd");

                                for (int i = 0; i < trips; i++)
                                {
                                    String sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", symbol, startDateStr, endDateStr, 2500, "1", "", "");
                                    String exchange = exchangeInUse != "Forex" ? exchangeInUse : "Forex";

                                    _iqFeedHistoricalDownloaderT.Run(sRequest, itemSym, exchange, timeFrame, path);

                                    startDateStr = endDateStr;
                                    endDateStr = tempDate.AddDays(2500).ToString("yyyyMMdd");

                                    System.Threading.Thread.Sleep(800);
                                }

                                if (remainder > 0)
                                {
                                    //tempDate = tempDate.AddDays(remainder);
                                    //dateStr = tempDate.ToString("yyyyMMdd");

                                    String sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", symbol, "", "", remainder, "1", "", "");
                                    String exchange = exchangeInUse != "Forex" ? exchangeInUse : "Forex";

                                    _iqFeedHistoricalDownloaderT.Run(sRequest, itemSym, exchange, timeFrame, path);                                    
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Generic Exception for " + itemSym + ":: " + ex.Message);
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
                // Collect all generations of memory.
                GC.Collect();
                //handle failure at this level?
            }
            return false;
        }

        public static bool GetDownloadData(String timeFrame, String exchangeInUse)
        {
            if (_SymbolCollection.Any())
            {
                var symbolInfo = _SymbolCollection.Dequeue();

                String symbol = symbolInfo.Symbol;
                _selectedExchange = symbolInfo.Exchange;
                exchangeInUse = _selectedExchange;

                if (String.IsNullOrEmpty(symbol) == false)
                {
                    var path = OutputControlT(exchangeInUse, 0);

                    String selector = "TRADES";
                    switch (Mode)
                    {
                        case MODE.Test:
                            {
                                selector += "_TEST";
                            }
                            break;

                        case MODE.Live:
                            {
                                selector += "_LIVE";
                            }
                            break;
                    }
                    String stockExchange = exchangeInUse;
                    String itemSym = symbol;

                    if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
                    {
                        var splitArray = Regex.Split(symbol, ".FXCM");
                        itemSym = splitArray.FirstOrDefault();
                        selector += "_" + exchangeInUse;
                    }
                    else if (exchangeInUse == "NYMEX")
                    {
                        selector += "_" + exchangeInUse;
                    }
                    

                    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

                    String[] dateArray = System.Configuration.ConfigurationManager.AppSettings["EARLIEST_DATE"].Split(';');

                    DateTime dateStart = new DateTime(Convert.ToInt32(dateArray[0]), Convert.ToInt32(dateArray[1]), Convert.ToInt32(dateArray[2]));
                    try
                    {
                        using (SqlConnection con = new SqlConnection(settings.ToString()))
                        {
                            SqlCommand command = null;

                            command = new SqlCommand("SELECT * FROM dbo.tblTrades where dbo.tblTrades.SymbolID='" + itemSym + "' AND dbo.tblTrades.TimeFrame = '" + timeFrame 
                                + "' AND dbo.tblTrades.DateTime=(SELECT MAX(dbo.tblTrades.DateTime) FROM dbo.tblTrades WHERE dbo.tblTrades.SymbolID='" + itemSym + "' AND dbo.tblTrades.TimeFrame = '"+ timeFrame +"')", con);

                            command.CommandTimeout = 0;

                            con.Open();

                            SqlDataReader dataReader = command.ExecuteReader();

                            DateTime startDateRange = dateStart;//earliest date

                            DateTime endDateRange = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                            switch (DateTime.Now.DayOfWeek)
                            {
                                case DayOfWeek.Saturday:
                                    {
                                        endDateRange = endDateRange.AddDays(-1);
                                    }
                                    break;

                                case DayOfWeek.Sunday:
                                    {
                                        endDateRange = endDateRange.AddDays(-2);
                                    }
                                    break;
                            }


                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    startDateRange = Convert.ToDateTime(dataReader["DateTime"]);
                                    startDateRange = new DateTime(startDateRange.Year, startDateRange.Month, startDateRange.Day, 0, 0, 0);
                                }

                                //Appending day
                                startDateRange = startDateRange.AddDays(1);
                                switch (startDateRange.DayOfWeek)
                                {
                                    case DayOfWeek.Saturday:
                                        {
                                            startDateRange = startDateRange.AddDays(2);
                                        }
                                        break;

                                    case DayOfWeek.Sunday:
                                        {
                                            startDateRange = startDateRange.AddDays(1);
                                        }
                                        break;
                                }
                            }

                            if (startDateRange <= endDateRange)
                            {
                                TimeSpan tspan = endDateRange - startDateRange;
                                String dateStr = startDateRange.ToString("yyyyMMdd");

                                // send it to the feed via the socket
                                String exchange = exchangeInUse != "Forex" ? exchangeInUse : "Forex";

                                //create a look up for the timeframe
                                Library.WriteErrorLog(DateTime.Now + " :: " + itemSym + " : " + timeFrame + " : " + exchange + " : " + startDateRange + " Symbols Left : " 
                                    + _SymbolCollection.Count + " Timeframe : " + _timeFrameListQueue.Count);

                                GetIntradayDataAsynch(itemSym, timeFrame, exchange, startDateRange);

                                Thread.Sleep(5000); //wait 5 seconds between calls
                                //help to prevent crashing off iqfeed
                            }
                            else
                            {
                                if (_timeFrameListQueue.Any())
                                {
                                    _currentTimeFrame = _timeFrameListQueue.Dequeue();
                                    Library.WriteErrorLog(DateTime.Now + " :: Process Next Time Frame :- " + _currentTimeFrame);

                                    GetDownloadData(_currentTimeFrame, _selectedExchange);
                                }
                                else
                                {
                                    var dataConsumerRun = new DataConsumer(false);
                                    dataConsumerRun.WorkComplete();

                                    Library.WriteErrorLog(DateTime.Now + " :: All Time Frames Data Request Done");                                  
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Generic Exception for " + itemSym + ":: " + ex.Message);
                        Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                    }
                    // Collect all generations of memory.
                    GC.Collect();
                    //handle failure at this level?
                }
            }
            return false;
        }

        public void GenerateWeeklyAndMonthly()
        {
            Logger.log.Info("Running GenerateWeeklyAndMonthly");
            Console.WriteLine("Running GenerateWeeklyAndMonthly");

            GetLatestSymbolsList();

            _timeframeList = System.Configuration.ConfigurationManager
                .AppSettings["TimeFrameList"].Split(',').ToList();


            //get week and monthly data
            string[] periodTimeFrame = new string[] { "Weekly", "Monthly" };

            //string[] periodTimeFrame = new string[] { "Monthly" };
            foreach (var periodtype in periodTimeFrame)
            {
                var periodGen = new PeriodGenerator(Mode, periodtype);

                foreach (var sym in _DirectSymbolCollection)
                {
                    string selectTimeFrame = periodtype == "Weekly" ? "EndOfDay" : "Weekly";

                    var returnPeriods = periodGen.GeneratePeriodData(sym, selectTimeFrame, periodtype);

                    if (returnPeriods.Any())
                    {
                        string msg = sym + " : " + periodtype + " : " + returnPeriods.Count;
                        Logger.log.Info(msg);
                        Console.WriteLine(msg);

                        WriteToCSVFile(returnPeriods);                        
                    }
                }
            }

            Logger.log.Info("GenerateWeeklyAndMonthly Completed");
            Console.WriteLine("GenerateWeeklyAndMonthly Completed");

        }

        public void WorkComplete()
        {
            Library.WriteErrorLog(DateTime.Now + " :: " + "WorkComplete");

            GenerateWeeklyAndMonthly();

            System.Threading.Thread.Sleep(30000);

            DataConsumptionCompleted(_currentDownloadFileT, _currentSelectorT);

            System.Threading.Thread.Sleep(30000);

            using (DatabaseManagerServiceImporter.DatabaseManagerServiceClient dbmClient = new DatabaseManagerServiceImporter.DatabaseManagerServiceClient())
            {
                dbmClient.Open();//_fileName
                dbmClient.RetrieveRawDataFiles("", (int)Mode);
                Library.WriteErrorLog(DateTime.Now + " :: " + "Notified Importer To import latest files");
            }
        }

        public static void GetSingleDataAsynch(String symbol, String timeFrame, String exchange, DateTime beginDate)
        {
            int interval = GetInterval(timeFrame);

            BroadcastorCallback cb = new BroadcastorCallback();
            cb.SetHandler(DataConsumer.HandleBroadcast);

            List<DataFeedService.TradeSummary> tradeSummaryListing = new List<DataFeedService.TradeSummary>();

            Console.WriteLine("Requesting Data :: {0} {1} {2} {3}", symbol, interval, exchange, beginDate.ToString());
            Library.WriteErrorLog(DateTime.Now + " :: " + symbol + " : " + interval + " : " + exchange + " : " + beginDate.ToString());

            try
            {
                System.ServiceModel.InstanceContext context =
                   new System.ServiceModel.InstanceContext(cb);

                using (DataFeedService.InternalDataFeedServiceClient internalFeedServiceProxy = new DataFeedService.InternalDataFeedServiceClient(context))
                {
                    //internalFeedServiceProxy.GetTradeSummaryDataAsynch(RecipientAppID, symbol, interval, exchange, beginDate);

                    internalFeedServiceProxy.GetTradeSummaryDataSingleDataPointAsynch(RecipientAppID, symbol, interval, exchange, beginDate);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Request Failure :: {0}", symbol);
                
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }


        public static void GetIntradayDataAsynch(String symbol, String timeFrame, String exchange, DateTime beginDate)
        {
            int interval = GetInterval(timeFrame);

            BroadcastorCallback cb = new BroadcastorCallback();
            cb.SetHandler(DataConsumer.HandleBroadcast);

            List<DataFeedService.TradeSummary> tradeSummaryListing = new List<DataFeedService.TradeSummary>();

            Console.WriteLine("Requesting Data :: {0} {1} {2} {3}", symbol, interval, exchange, beginDate.ToString());

            try
            {
                System.ServiceModel.InstanceContext context =
                   new System.ServiceModel.InstanceContext(cb);

                using (DataFeedService.InternalDataFeedServiceClient internalFeedServiceProxy = new DataFeedService.InternalDataFeedServiceClient(context))
                {
                    internalFeedServiceProxy.GetTradeSummaryDataAsynch(RecipientAppID, symbol, interval, exchange, beginDate);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Request Failure :: {0}", symbol);
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }

        private static int GetInterval(String timeFrame)
        {
            int intervalReturn = 0;

            switch (timeFrame)
            {
                case "1min":
                    {
                        intervalReturn = 60;
                    } break;

                case "2min":
                    {
                        intervalReturn = 120;
                    } break;

                case "3min":
                    {
                        intervalReturn = 180;
                    } break;

                case "4min":
                    {
                        intervalReturn = 240;
                    } break;

                case "5min":
                    {
                        intervalReturn = 300;
                    } break;

                case "6min":
                    {
                        intervalReturn = 360;
                    } break;

                case "10min":
                    {
                        intervalReturn = 600;
                    } break;

                case "15min":
                    {
                        intervalReturn = 900;
                    } break;

                case "30min":
                    {
                        intervalReturn = 1800;
                    } break;

                case "1hour":
                    {
                        intervalReturn = 3600;
                    } break;

                case "2hour":
                    {
                        intervalReturn = 7200;
                    } break;

                case "3hour":
                    {
                        intervalReturn = 10800;
                    } break;

                case "4hour":
                    {
                        intervalReturn = 14400;
                    } break;

                case "EndOfDay":
                    {
                        intervalReturn = 86400;
                    } break;
            }
            return intervalReturn;

        }

        #region "callback services"
        private delegate void HandleBroadcastCallback(object sender, EventArgs e);

        //public static void HandleBroadcast(object sender, EventArgs e)
        public static void HandleBroadcast(object sender, EventArgs e)
        {
            try
            {
                //see if object can be set or created as AnswerPackage
                var eventData = (DataFeedService.EventDataTypeCollection)sender;

                if (_updateTradeSummaryHandler != null)
                {
                    _updateTradeSummaryHandler.Invoke(eventData.EventMessage.ToList());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                //Logger.log.Error("HandleBroadcast Failure  :: " + ex.ToString());
            }
        }
        #endregion

        private void DataFillerTask(object sender, ElapsedEventArgs e)
        {
            _dataFillerTimer.Stop();
            _dataFillerTimer.Enabled = false;

            Console.WriteLine("Finalising Received Data...");
            Logger.log.Info("Finalising Received Data...");

            CheckedAndFoundDateTimes();

            MarkedAsCheckedForUnFound();
            _bDataFillerMood = false;
            Logger.log.Info("DataFiller Flag False");
            Console.WriteLine("DataFiller Flag False");

            Console.WriteLine("WorkComplete");
            WorkComplete();

            _dataFillerTimer.Dispose();
            _dataFillerTimer = null;
        }



        private void Task(object sender, ElapsedEventArgs e)
        {
            actionTimer.Stop();
            actionTimer.Enabled = false;

            //time consuming process
           // ConsumeHistoricalDataCSV();
            ConsumeHistoricalDataCSVQueueBased();

            //Tell DBM to start import
            actionTimer.Interval = CalculateInterval();
            actionTimer.Enabled = true;
            actionTimer.Start();
        }

        public void SystemEvents_TimeChanged(object sender, EventArgs e)
        {
            actionTimer.Stop();
            actionTimer.Enabled = false;

            actionTimer.Interval = CalculateInterval();

            actionTimer.Enabled = true;
            actionTimer.Start();
        }

        private double CalculateInterval()
        {

            int hours = Convert.ToInt32(_triggerTime.Substring(0, 2));
            int minutes = Convert.ToInt32(_triggerTime.Substring(3, 2));

            DateTime dayDueTime = DateTime.Now;

            DateTime testDateTime = new DateTime(dayDueTime.Year, dayDueTime.Month, dayDueTime.Day,
                hours, minutes, 0);

            if (DateTime.Now > testDateTime)
            {
                dayDueTime = DateTime.Now.AddDays(1);
            }

            DateTime criteriaDateTime = new DateTime(dayDueTime.Year, dayDueTime.Month, dayDueTime.Day,
                hours, minutes, 0);

            long ticks = criteriaDateTime.Ticks - DateTime.Now.Ticks;

            TimeSpan tms = new TimeSpan(ticks);

            return tms.TotalMilliseconds;
        }

        private bool GetLatestSymbolListOld()
        {
            bool bSuccess = true;
            symbolList.Clear();

            List<exchangeFileInfo> exchangeInfoList = new List<exchangeFileInfo>();

            //read exchange list file location from xml file
            //This code has no validation on it

            bool bDirectoryRead = false;

            Console.WriteLine("file path::" + Directory.GetCurrentDirectory() + "\\StockExchangeListing.xml");
            XmlTextReader xmlReader = new XmlTextReader(Directory.GetCurrentDirectory() + "\\StockExchangeListing.xml");
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.

                        if (xmlReader.Name != "ScheduleTime")
                        {
                            Console.Write("<" + xmlReader.Name);
                            Console.WriteLine(">");

                            if (xmlReader.Name == "Name")
                            {
                                exchangeFileInfo eFileInfo = new exchangeFileInfo();
                                EFileInfo = eFileInfo;
                                exchangeInfoList.Add(eFileInfo);
                            }
                        }
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Console.WriteLine(xmlReader.Value);

                        if (!bDirectoryRead)
                        {
                            if (!String.IsNullOrEmpty(xmlReader.Value))
                            {
                                var obj = exchangeInfoList.Last();

                                if (String.IsNullOrEmpty(obj.exchangeSymbol))
                                {
                                    obj.exchangeSymbol = xmlReader.Value;
                                }
                                else
                                {
                                    obj.filePath = xmlReader.Value;
                                    bDirectoryRead = true;
                                }
                            }
                        }

                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.

                        if (xmlReader.Name != "ScheduleTime")
                        {
                            Console.Write("</" + xmlReader.Name);
                            Console.WriteLine(">");
                        }
                        break;
                }
            }
            //Console.ReadLine();

            //if one of the files cant be found the program will not continue, improve code

            //file read
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.

                foreach (var item in exchangeInfoList)
                {
                    //temp file location in future will be with exe
                    using (StreamReader sr = new StreamReader(item.filePath))
                    {
                        string line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.

                        List<String> symbols = new List<string>();
                        while ((line = sr.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            if (String.IsNullOrEmpty(line) == false)
                            {
                                symbols.Add(line);
                            }
                        }
                        symbolList.Add(item.exchangeSymbol, symbols);
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                bSuccess = false;
            }
            return bSuccess;
        }

        private bool GetLatestSymbolList()
        {
            bool bSuccess = true;
            symbolList.Clear();

            if (_selectedExchange != "")
            {
                exchangeFileInfo eFileInfo = new exchangeFileInfo();
                eFileInfo.exchangeSymbol = _selectedExchange;
                //eFileInfo.filePath = _selectedExchange + ".txt";

                eFileInfo.filePath = Directory.GetCurrentDirectory() + "\\" + _selectedExchange + ".txt";
                _exchangeInfoList.Add(eFileInfo);
            }
            else
            {
                String[] tempAPP = System.Configuration.ConfigurationManager.AppSettings["DATALIST"].Split('|');

                foreach (var item in tempAPP)
                {
                    //exchangeFileInfo eFileInfo = new exchangeFileInfo();
                    //eFileInfo.exchangeSymbol = item.Split('.').FirstOrDefault();
                    //eFileInfo.filePath = Directory.GetCurrentDirectory() + "\\" + item;
                    //_exchangeInfoList.Add(eFileInfo);

                    exchangeFileInfo eFileInfo = new exchangeFileInfo();
                    eFileInfo.exchangeSymbol = "FOREX";
                    eFileInfo.filePath = item;
                    _exchangeInfoList.Add(eFileInfo);
                }
            }

            //if one of the files cant be found the program will not continue, improve code

            //file read
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.

                foreach (var item in _exchangeInfoList)
                {
                    //temp file location in future will be with exe
                    using (StreamReader sr = new StreamReader(item.filePath))
                    {
                        string line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.

                        List<String> symbols = new List<string>();
                        while ((line = sr.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            if (String.IsNullOrEmpty(line) == false)
                            {
                                symbols.Add(line);
                            }
                        }
                        symbolList.Add(item.exchangeSymbol, symbols);
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                bSuccess = false;
            }
            return bSuccess;
        }

        private bool GetLatestSymbolsList()
        {
            _DirectSymbolCollection.Clear();

            bool bSuccess = true;

            String[] tempAPP = System.Configuration.ConfigurationManager.AppSettings["DATALIST"].Split('|');

            foreach (var item in tempAPP)
            {
                exchangeFileInfo eFileInfo = new exchangeFileInfo();
                eFileInfo.exchangeSymbol = "FOREX";
                eFileInfo.filePath = item;
                _exchangeInfoList.Add(eFileInfo);
            }


            //if one of the files cant be found the program will not continue, improve code

            //file read
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.

                foreach (var item in _exchangeInfoList)
                {
                    //temp file location in future will be with exe
                    using (StreamReader sr = new StreamReader(item.filePath))
                    {
                        string line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.

                        //Queue<String> symbols = new Queue<String>();
                        while ((line = sr.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            if (String.IsNullOrEmpty(line) == false)
                            {
                                var splitArray = Regex.Split(line, ".FXCM");

                                _DirectSymbolCollection.Add(splitArray.FirstOrDefault());
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                bSuccess = false;
            }

           // _DirectSymbolCollection = new List<string>() { "EURUSD", "EURAUD", "CHFJPY" };
           // _DirectSymbolCollection = new List<string>() { "AUDCAD", "EURUSD" };
            return bSuccess;
        }

        private void GetTaskQueueBased()
        {
            String[] tempAPP = System.Configuration.ConfigurationManager.AppSettings["DATALIST"].Split('|');
            foreach (var item in tempAPP)
            {
                _taskWork.Enqueue(item);
            }
        }

        private bool GetLatestSymbolListQueueBased()
        {
            bool bSuccess = true;
            _SymbolCollection.Clear();
            _exchangeInfoList.Clear();


            //String[] tempAPP = System.Configuration.ConfigurationManager.AppSettings["DATALIST"].Split('|');
            string currentExchange = "";

            if (_timeFrameListQueue.Any() == false)
            {
                _taskWork.Dequeue();

                var timeframeList = System.Configuration.ConfigurationManager.AppSettings["TimeFrameList"].Split(',').ToList();
                foreach (var item in timeframeList)
                {
                    _timeFrameListQueue.Enqueue(item);
                }
            }

            //foreach (var item in tempAPP)
            if (_taskWork.Any())
            {
                currentExchange = _taskWork.ElementAt(0);

                var fileInfo = new FileInfo(currentExchange);
                var exchange =fileInfo.Name.Substring(0, fileInfo.Name.Length - 4);

                exchangeFileInfo eFileInfo = new exchangeFileInfo();
                eFileInfo.exchangeSymbol = exchange;
                eFileInfo.filePath = currentExchange;
                _exchangeInfoList.Add(eFileInfo);
            }


            //if one of the files cant be found the program will not continue, improve code

            //file read
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.

                foreach (var item in _exchangeInfoList)
                {
                    //temp file location in future will be with exe
                    using (StreamReader sr = new StreamReader(item.filePath))
                    {
                        string line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.

                        //Queue<String> symbols = new Queue<String>();
                        while ((line = sr.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            if (String.IsNullOrEmpty(line) == false)
                            {
                                //symbols.Enqueue(line);
                                SymbolInfo symInfo = new SymbolInfo();
                                symInfo.Symbol = line;
                                symInfo.Exchange = item.exchangeSymbol;

                                _SymbolCollection.Enqueue(symInfo);
                            }
                        }

                        //_SymbolCollection.Add(item.exchangeSymbol, symbols);
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                bSuccess = false;
            }
            return bSuccess;
        }


        private String GetScheduleTriggerTime()
        {
            Console.WriteLine("file path::" + Directory.GetCurrentDirectory() + "\\StockExchangeListing.xml");

            XmlTextReader xmlReader = new XmlTextReader(Directory.GetCurrentDirectory() + "\\StockExchangeListing.xml");

            bool bTime = false;

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.

                        if (xmlReader.Name == "ScheduleTime")
                        {
                            Console.Write("<" + xmlReader.Name);
                            Console.WriteLine(">");
                            bTime = true;
                        }
                        break;

                    case XmlNodeType.Text: //Display the text in each element.

                        if (bTime)
                        {
                            Console.WriteLine(xmlReader.Value);

                            _triggerTime = xmlReader.Value;
                        }
                        break;

                    case XmlNodeType.EndElement: //Display the end of the element.
                        if (bTime)
                        {
                            bTime = false;
                            Console.Write("</" + xmlReader.Name);
                            Console.WriteLine(">");

                            Console.WriteLine("\n");
                        }
                        break;
                }
            }


            return "";
        }

        DateTime IncrementDate(DateTime start, DateTime end)
        {
            start = start.AddDays(1);
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    {
                        start = DateTime.Now.AddDays(2);
                    }
                    break;

                case DayOfWeek.Sunday:
                    {
                        start = DateTime.Now.AddDays(1);
                    }
                    break;
            }
            return start;
        }

        public String OutputControl()
        {
            String directoryOutput = "";
            String selector = "JobsManager";

            switch (Mode)
            {
                case MODE.Test:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"];
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVE"];
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVETEST"];
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            //String exchangeName = System.Configuration.ConfigurationManager.ConnectionStrings[1].Name;

            String exchangeName = EFileInfo.exchangeSymbol;

            directoryOutput += "\\" + exchangeName;
            if (Directory.Exists(directoryOutput) == false)
            {
                Directory.CreateDirectory(directoryOutput);
            }
            String dateStr = String.Format("{0}{1}{2}_{3}{4}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute);

            String tempFileName = exchangeName + "_" + dateStr;
            String fileName = tempFileName + ".csv";

            String fullPath = directoryOutput + "\\" + fileName;
            if (!System.IO.File.Exists(fullPath))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fullPath)) { }

                RegisterFile(tempFileName, selector, 0);
            }
            else
            {
                int rowsAffected = 0;
                //Double check the file is in the database
                SqlParameter outputIdParam = new SqlParameter("@Count", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_CheckRawDataFileRegisteration", con);
                    con.Open();

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@FileName", tempFileName);
                    sqlCommand.Parameters.Add(outputIdParam);

                    sqlCommand.CommandTimeout = 0;
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }

                if (Convert.ToInt32(outputIdParam.Value) == 0)
                {
                    RegisterFile(tempFileName, selector, 0);
                }

            }
            return directoryOutput + "\\" + fileName;
        }

        public static String OutputControlT(String exchange, int filenum)
        {
            String directoryOutput = "";
            String selector = "JobsManager";
            String directoryOutputTemp = "";

            switch (Mode)
            {
                case MODE.Test:
                    {
                        directoryOutputTemp = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"];
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"];
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        directoryOutputTemp = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVE"];
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVE"];
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        directoryOutputTemp = System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVETEST"];
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVETEST"];
                        selector += "_LIVE-TEST";
                    }
                    break;
            }


            var timeframeList = System.Configuration.ConfigurationManager.AppSettings["TimeFrameList"].Split(',').ToList();

            bool minuteOnly = false;
            bool tenMin15minOnly = false;
            string timeFrameStamp = "";
            if (timeframeList.Any(d => d == "1min" || d == "5min") && timeframeList.Count == 4)
            {
                minuteOnly = true;
                timeFrameStamp = "_1min5min";
            }
            if (timeframeList.Any(d => d == "10min" || d == "15min") && timeframeList.Count == 4)
            {
                tenMin15minOnly = true;
                timeFrameStamp = "_10min15min";
            }


            var split = exchange.Split('_');
            var folderName = split.Count() > 0 ? split.FirstOrDefault() : exchange;

            String exchangeName = exchange;

            directoryOutput += "\\" + folderName;

            try
            {
                if (Directory.Exists(directoryOutput) == false)
                {
                    Directory.CreateDirectory(directoryOutput);
                }
            }
            catch (Exception ex)
            {
                directoryOutput = directoryOutputTemp;
                if (Directory.Exists(directoryOutput) == false)
                {
                    Directory.CreateDirectory(directoryOutput);
                }
            }

            //String dateStr = String.Format("{0}{1}{2}_{3}{4}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute);
            String dateStr = String.Format("{0}{1}{2}_{3}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour);

            String tempFileName = exchangeName + "_" + dateStr + timeFrameStamp;
            String fileName = tempFileName + ".csv";

            String fullPath = directoryOutput + "\\" + fileName;
            if (!System.IO.File.Exists(fullPath))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fullPath)) { }

                RegisterFile(tempFileName, selector, filenum);
            }
            else
            {
                int rowsAffected = 0;
                //Double check the file is in the database
                SqlParameter outputIdParam = new SqlParameter("@Count", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_CheckRawDataFileRegisteration", con);
                    sqlCommand.CommandTimeout = 0;
                    con.Open();

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@FileName", tempFileName);
                    sqlCommand.Parameters.Add(outputIdParam);
                   
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }

                if (Convert.ToInt32(outputIdParam.Value) == 0)
                {
                    RegisterFile(tempFileName, selector, filenum);
                }
            }

            _currentDownloadFileT = tempFileName;
            _currentSelectorT = selector;

            return directoryOutput + "\\" + fileName;
        }

        public String OutputControl(String exchange)
        {
            String directoryOutput = "";
            String selector = "JobsManager";

            switch (Mode)
            {
                case MODE.Test:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"];
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVE"];
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVETEST"];
                        selector += "_LIVE-TEST";
                    }
                    break;
            }
            //String exchangeName = System.Configuration.ConfigurationManager.ConnectionStrings[1].Name;

            String exchangeName = exchange;

            directoryOutput += "\\" + exchangeName;
            if (Directory.Exists(directoryOutput) == false)
            {
                Directory.CreateDirectory(directoryOutput);
            }
            String dateStr = String.Format("{0}{1}{2}_{3}{4}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute);

            String tempFileName = exchangeName + "_" + dateStr;
            String fileName = tempFileName + ".csv";

            String fullPath = directoryOutput + "\\" + fileName;
            if (!System.IO.File.Exists(fullPath))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fullPath)) { }

                RegisterFile(tempFileName, selector, 0);
            }
            else
            {
                int rowsAffected = 0;
                //Double check the file is in the database
                SqlParameter outputIdParam = new SqlParameter("@Count", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_CheckRawDataFileRegisteration", con);
                    con.Open();

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@FileName", tempFileName);
                    sqlCommand.Parameters.Add(outputIdParam);

                    sqlCommand.CommandTimeout = 0;
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }

                if (Convert.ToInt32(outputIdParam.Value) == 0)
                {
                    RegisterFile(tempFileName, selector, 0);
                }
            }

            _currentDownloadFile = tempFileName;
            _currentSelector = selector;

            return directoryOutput + "\\" + fileName;
        }

        public static void RegisterFile(String fileName, String selector, int filenum)
        {
             var performance = System.Configuration.ConfigurationManager.AppSettings["PERFORMANCETEST"];

             if (performance != "NOOUTPUT")
             {
                 using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
                 {
                     SqlCommand sqlCommand = new SqlCommand("proc_RegisterRawDataFile", con);
                     con.Open();
                     sqlCommand.CommandTimeout = 0;

                     sqlCommand.CommandType = CommandType.StoredProcedure;
                     sqlCommand.Parameters.AddWithValue("@FileName", fileName);
                     sqlCommand.Parameters.AddWithValue("@FileTypeNum", filenum);

                     int affectedRows = sqlCommand.ExecuteNonQuery();
                 }
             }
        }

        void DataConsumptionCompleted(String fileName, String selector)
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
            {
                SqlCommand sqlCommand = new SqlCommand("proc_DataConsumptionCompletionUpdate", con);
                con.Open();

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FileName", fileName);

                int affectedRows = sqlCommand.ExecuteNonQuery();
            }
        }

        static public bool CheckDatabaseManagerConnection()
        {
            try
            {
                using (DatabaseManagerServiceImporter.DatabaseManagerServiceClient dbmClient = new DatabaseManagerServiceImporter.DatabaseManagerServiceClient())
                {
                    dbmClient.Open();
                    bool matchingMode = dbmClient.ModeCheck((int)DataConsumer.Mode);
                    dbmClient.Close();

                    if (matchingMode)
                    {
                        Console.WriteLine("Connection OK - DatabaseManagerService");
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Connection OK - Application MODES do not MATCH");
                        return false;
                    }
                }
            }
            catch (System.ServiceModel.CommunicationException commProblem)
            {
                Console.WriteLine("Connection FAILED - DatabaseManagerService");
            }
            return false;
        }  
    }


    public class BroadcastorCallback : DataFeedService.IInternalDataFeedServiceCallback
    {
        private System.Threading.SynchronizationContext _syncContext = AsyncOperationManager.SynchronizationContext;

        private EventHandler _broadcastorCallBackHandler;
        public void SetHandler(EventHandler handler)
        {
            this._broadcastorCallBackHandler = handler;
        }

        public void BroadcastToClient(DataFeedService.EventDataTypeCollection eventData)
        {
            _syncContext.Post(new System.Threading.SendOrPostCallback(OnBroadcast), eventData);
        }

        private void OnBroadcast(object eventData)
        {
            this._broadcastorCallBackHandler.Invoke(eventData, null);
        }
    }


}

    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }

    /// <summary>
    /// Class to write data to a CSV file
    /// </summary>
    public class CsvFileWriter : StreamWriter
    {
        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename, true)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            String rowStr = "";
            try
            {
                StringBuilder builder = new StringBuilder();
                bool firstColumn = true;
                foreach (string value in row)
                {
                    // Add separator if this isn't the first value
                    if (!firstColumn)
                        builder.Append(',');
                    // Implement special handling for values that contain comma or quote
                    // Enclose in quotes and double up any double quotes
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                        builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                    else
                        builder.Append(value);
                    firstColumn = false;
                }

                row.LineText = builder.ToString();

                rowStr = row.LineText;

                WriteLine(row.LineText);
            }
            catch (Exception ex)
            {
                Console.WriteLine("WriteRow Execption - {0} - {1}" + rowStr + ex);
                ConsumerLibrary.Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
        }

    internal void WriteRecords(List<Symbol> symbols)
    {
        throw new NotImplementedException();
    }
}



