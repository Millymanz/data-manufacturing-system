﻿using Formo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerLibrary
{
    public interface IConfigHelper
    {
        string Apikey { get; }
        string DateFormat { get; }
        string WebUrl { get; }
        int BackFillCryptoLimitDays { get; }
    }
    //ToDO: use formo for reading the config
    public class ConfigHelper : IConfigHelper
    {
        private dynamic configuration;
        public ConfigHelper()
        {
            configuration = new Configuration();
        }
        public string Apikey
        {
            get
            {
                //return configuration.Apikey("8E1715C5-CE2D-487F-8CDC-8206A73D125C");
                //return configuration.Apikey("F3A597B4-C47B-47A9-997D-E9F19FC0156A");
                return configuration.Apikey("3F5E3AEB-63C0-442D-8110-D168A5A64985");
            }
        }
        public string DateFormat
        {
            get
            {
                return configuration.DateFormat("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }
        public string WebUrl
        {
            get
            {
                return configuration.WebUrl("https://rest.coinapi.io");
            }
        }

        public int BackFillCryptoLimitDays
        {
            get
            {
                return configuration.BackFillCryptoLimit ?? 20;
            }
        }
        
    }
}
