﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// added for access to RegistryKey
using Microsoft.Win32;
// added for access to socket classes
using System.Net;
using System.Net.Sockets;


namespace ConsumerLibrary
{
    public struct AdditionalData
    {
        public String Symbol;
        public String Exchange; /*= "NONE";*/
        public String TimeFrame;
        public String Path;
    }

    public class IQFeedHistoricDownloader
    {
            // socket communication global variables
            AsyncCallback m_pfnLookupCallback;
            Socket m_sockLookup;
            // we create the socket buffer global for performance
            byte[] m_szLookupSocketBuffer = new byte[262144];
            // stores unprocessed data between reads from the socket
            string m_sLookupIncompleteRecord = "";
            // flag for tracking when a call to BeginReceive needs called
            bool m_bLookupNeedBeginReceive = true;

            // delegate for updating the data display.
            public delegate void UpdateDataHandler(string sMessage);

            private String _fileName;
        
            private String currentSymbolID;
            private Queue<AdditionalData> requestedSymbolQueue = new Queue<AdditionalData>();

            private Object thisLock = new Object();
            public bool IsLocked = false;

            public static bool bResumeSendRequest = false;

            private String _currentSymbol = "";
            private String _exchange = "";
            private String _timeFrame = "";
            private String _path = "";
 
            public String OutputFileName
            {
                get { return _fileName;  }
                set { _fileName = value; }
            }

            //public IQFeedHistoricDownloader()
            //{
            //    var hostName = System.Configuration.ConfigurationManager.AppSettings["IQFEEDSOURCE"];

            //    IPHostEntry host;
            //    string localIP = "?";
            //    host = Dns.GetHostEntry(hostName);

            //    foreach (IPAddress ip in host.AddressList)
            //    {
            //        if (ip.AddressFamily.ToString() == "InterNetwork")
            //        {
            //            localIP = ip.ToString();
            //        }
            //    }

            //    // create the socket
            //    m_sockLookup = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //    IPAddress ipLocalhost = IPAddress.Parse("127.0.0.1");

            //   // IPAddress ipLocalhost = IPAddress.Parse(localIP);

            //    // Historical data is received from IQFeed on the Lookup port.
            //    // pull the Lookup port out of the registry
            //    int iPort = GetIQFeedPort("Lookup");
            //    IPEndPoint ipendLocalhost = new IPEndPoint(ipLocalhost, iPort);

            //    try
            //    {
            //        // connect the socket
            //        m_sockLookup.Connect(ipendLocalhost);

            //        // Set the protocol for the lookup socket to 5.0
            //        SendRequestToIQFeed("S,SET PROTOCOL,5.0\r\n");
            //    }
            //    catch (SocketException ex)
            //    {

            //    }
            //}

            public IQFeedHistoricDownloader()
            {
               
            }


            public void Run(string sRequest, String sym, String exchange, String timeFrame, String outputPath)
            {
                currentSymbolID = sym;
                //Daily Timeframe
                // request in the format:
                // HDT,SYMBOL,BEGINDATE,ENDDATE,MAXDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                //sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", "OXM", "20131210", "", "", "", "", "");

                //Tick Timeframe
                // request in the format:
                // HTT,SYMBOL,BEGINDATE BEGINTIME,ENDDATE ENDTIME,MAXDATAPOINTS,BEGINFILTERTIME,ENDFILTERTIME,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                //sRequest = String.Format("HTT,{0},{1},{2},{3},{4},{5},{6},{7},{8}\r\n", txtSymbol.Text, txtBeginDateTime.Text, txtEndDateTime.Text, txtDatapoints.Text, txtBeginFilterTime.Text, txtEndFilterTime.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);





                // format request string based upon user input
                /*   if (cboHistoryType.Text.Equals("Tick Datapoints"))
                   {
                       // request in the format:
                       // HTX,SYMBOL,NUMDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HTX,{0},{1},{2},{3},{4}\r\n", txtSymbol.Text, txtDatapoints.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else if (cboHistoryType.Text.Equals("Tick Days"))
                   {
                       // request in the format:
                       // HTD,SYMBOL,NUMDAYS,MAXDATAPOINTS,BEGINFILTERTIME,ENDFILTERTIME,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HTD,{0},{1},{2},{3},{4},{5},{6},{7}\r\n", txtSymbol.Text, txtDays.Text, txtDatapoints.Text, txtBeginFilterTime.Text, txtEndFilterTime.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else if (cboHistoryType.Text.Equals("Tick Timeframe"))
                   {
                       // request in the format:
                       // HTT,SYMBOL,BEGINDATE BEGINTIME,ENDDATE ENDTIME,MAXDATAPOINTS,BEGINFILTERTIME,ENDFILTERTIME,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HTT,{0},{1},{2},{3},{4},{5},{6},{7},{8}\r\n", txtSymbol.Text, txtBeginDateTime.Text, txtEndDateTime.Text, txtDatapoints.Text, txtBeginFilterTime.Text, txtEndFilterTime.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else if (cboHistoryType.Text.Equals("Interval Datapoints"))
                   {
                       // validate interval type
                       string sIntervalType = "s";
                       if (rbVolume.Checked)
                       {
                           sIntervalType = "v";
                       }
                       else if (rbTick.Checked)
                       {
                           sIntervalType = "t";
                       }

                       // request in the format:
                       // HIX,SYMBOL,INTERVAL,NUMDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND,INTERVALTYPE<CR><LF>
                       sRequest = String.Format("HIX,{0},{1},{2},{3},{4},{5},{6}\r\n", txtSymbol.Text, txtInterval.Text, txtDatapoints.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text, sIntervalType);
                   }
                   else if (cboHistoryType.Text.Equals("Interval Days"))
                   {
                       // validate interval type
                       string sIntervalType = "s";
                       if (rbVolume.Checked)
                       {
                           sIntervalType = "v";
                       }
                       else if (rbTick.Checked)
                       {
                           sIntervalType = "t";
                       }

                       // request in the format:
                       // HID,SYMBOL,INTERVAL,NUMDAYS,MAXDATAPOINTS,BEGINFILTERTIME,ENDFILTERTIME,DIRECTION,REQUESTID,DATAPOINTSPERSEND,INTERVALTYPE<CR><LF>
                       sRequest = String.Format("HID,{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\r\n", txtSymbol.Text, txtInterval.Text, txtDays.Text, txtDatapoints.Text, txtBeginFilterTime.Text, txtEndFilterTime.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text, sIntervalType);
                   }
                   else if (cboHistoryType.Text.Equals("Interval Timeframe"))
                   {
                       // validate interval type
                       string sIntervalType = "s";
                       if (rbVolume.Checked)
                       {
                           sIntervalType = "v";
                       }
                       else if (rbTick.Checked)
                       {
                           sIntervalType = "t";
                       }

                       // request in the format:
                       // HIT,SYMBOL,INTERVAL,BEGINDATE BEGINTIME,ENDDATE ENDTIME,MAXDATAPOINTS,BEGINFILTERTIME,ENDFILTERTIME,DIRECTION,REQUESTID,DATAPOINTSPERSEND,INTERVALTYPE<CR><LF>
                       sRequest = String.Format("HIT,{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\r\n", txtSymbol.Text, txtInterval.Text, txtBeginDateTime.Text, txtEndDateTime.Text, txtDatapoints.Text, txtBeginFilterTime.Text, txtEndFilterTime.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text, sIntervalType);
                   }
                   else if (cboHistoryType.Text.Equals("Daily Datapoints"))
                   {
                       // request in the format:
                       // HDX,SYMBOL,NUMDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HDX,{0},{1},{2},{3},{4}\r\n", txtSymbol.Text, txtDatapoints.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else if (cboHistoryType.Text.Equals("Daily Timeframe"))
                   {
                       // request in the format:
                       // HDT,SYMBOL,BEGINDATE,ENDDATE,MAXDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HDT,{0},{1},{2},{3},{4},{5},{6}\r\n", txtSymbol.Text, txtBeginDateTime.Text, txtEndDateTime.Text, txtDatapoints.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else if (cboHistoryType.Text.Equals("Weekly Datapoints"))
                   {
                       // request in the format:
                       // HWX,SYMBOL,NUMDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HDX,{0},{1},{2},{3},{4}\r\n", txtSymbol.Text, txtDatapoints.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else if (cboHistoryType.Text.Equals("Monthly Datapoints"))
                   {
                       // request in the format:
                       // HMX,SYMBOL,NUMDATAPOINTS,DIRECTION,REQUESTID,DATAPOINTSPERSEND<CR><LF>
                       sRequest = String.Format("HDX,{0},{1},{2},{3},{4}\r\n", txtSymbol.Text, txtDatapoints.Text, txtDirection.Text, txtRequestID.Text, txtDatapointsPerSend.Text);
                   }
                   else
                   {
                       // something unexpected happened
                       sRequest = "Error Processing Request.";
                   }*/

                // verify we have formed a request string
                if (sRequest.StartsWith("H"))
                {
                    AdditionalData ad = new AdditionalData();
                    ad.Symbol = sym;
                    ad.Exchange = exchange;
                    ad.TimeFrame = timeFrame;

                    ad.Path = outputPath;

                    requestedSymbolQueue.Enqueue(ad);

                    _currentSymbol = sym;
                    _exchange = exchange;
                    _timeFrame = timeFrame;
                    _path = outputPath;



                    // send it to the feed via the socket
                    SendRequestToIQFeed(sRequest);
                }
                // tell the socket we are ready to receive data
                WaitForData("History");
            }

            private void SendRequestToIQFeed(string sCommand)
            {
                // and we send it to the feed via the socket
                byte[] szCommand = new byte[sCommand.Length];
                szCommand = Encoding.ASCII.GetBytes(sCommand);
                int iBytesToSend = szCommand.Length;
                try
                {
                    int iBytesSent = m_sockLookup.Send(szCommand, iBytesToSend, SocketFlags.None);
                    if (iBytesSent != iBytesToSend)
                    {
                        Console.WriteLine(String.Format("Error Sending Request:\r\n{0}", sCommand.TrimEnd("\r\n".ToCharArray())));
                    }
                    else
                    {
                        Console.WriteLine(String.Format("Request Sent Successfully:\r\n{0}", sCommand.TrimEnd("\r\n".ToCharArray())));
                    }
                }
                catch (SocketException ex)
                {
                    // handle socket errors
                    Console.WriteLine(String.Format("Socket Error Sending Request:\r\n{0}\r\n{1}", sCommand.TrimEnd("\r\n".ToCharArray()), ex.Message));
                }
            }

    
            private void OnReceive(IAsyncResult asyn)
            {
                //lock (thisLock)
                //{

                // first verify we received data from the correct socket.  This check isn't really necessary in this example since we 
                // only have a single socket but if we had multiple sockets, we could use this check to use the same callback to recieve data from
                // multiple sockets
                if (asyn.AsyncState.ToString().Equals("History"))
                {
                    // read data from the socket.  The call to EndReceive tells the Framework to copy data available on the socket into our socket buffer
                    // that we supplied in the BeginReceive call.
                    int iReceivedBytes = 0;
                    iReceivedBytes = m_sockLookup.EndReceive(asyn);
                    m_bLookupNeedBeginReceive = true;
                    // add the data received from the socket to any data that was left over from the previous read off the socket.
                    string sData = m_sLookupIncompleteRecord + Encoding.ASCII.GetString(m_szLookupSocketBuffer, 0, iReceivedBytes);
                    // clear the incomplete record string so it doesn't get added again next time we read from the socket
                    m_sLookupIncompleteRecord = "";
                    // history data will be read off the socket in groups of messages.  We have no control over how many messages will be
                    // read off the socket at each read.  Likewise we have no guarantee that we won't get an incomplete message at the beginning
                    // or ending of the group of messages.  Our processing needs to handle this.
                    // history data is always terminated with a cariage return and newline characters ("\r\n").  
                    // we verify a record is complete by finding the newline character.
                    int iNewLinePos = sData.IndexOf("\n");
                    int iPos = 0;
                    // loop through the group of messages
                    while (iNewLinePos >= 0)
                    {
                        // at this point, we know we have a complete message between iPos (start of the message) and iNewLinePos (end)
                        // here we could add message processing for this single line of data but in this example, we just display the raw data
                        // so we just keep looping through the messages
                        iPos = iNewLinePos + 1;
                        iNewLinePos = sData.IndexOf("\n", iPos);
                    }
                    // at this point, iPos (start of the current message) will be less than m_strData.Length if we had an incomplete message
                    // at the end of the data.  We detect this and save off the incomplete message
                    if (sData.Length > iPos)
                    {
                        // left an incomplete record in the buffer
                        m_sLookupIncompleteRecord = sData.Substring(iPos);
                        // remove the incomplete message from the message
                        sData = sData.Remove(iPos);
                    }
                    else if (sData.EndsWith("!ENDMSG!,\r\n"))
                    {


                        int h = 0;

                        //DataConsumer.ConsumeData("EndOfDay", DataConsumer._selectedExchange);

                      }


                        // end of message.


                        //Request ID	Text	This field will only exist if the request specified a RequestID. If not specified in the request, the first field in each message will be the Timestamp.
                        //Date Stamp	CCYY-MM-DD	Example: 2013-07-15
                        //High	Decimal	Example: 928.00
                        //Low	Decimal	Example: 916.36
                        //Open	Decimal	Example: 924.30
                        //Close	Decimal	Example: 924.69
                        //Period Volume	Integer	Example: 1961361
                        //Open Interest	Integer	Example

                        // display the data to the user
                        //  Console.WriteLine(sData);


                        var results = sData.Split('\n');

                        var item = results.FirstOrDefault().Split(',');

                        DateTime testDate = new DateTime();

                        String tempExchange = "";
                        if (DateTime.TryParse(item.FirstOrDefault(), out testDate) && requestedSymbolQueue.Count > 0)
                        {
                            AdditionalData currentAdditionalData = requestedSymbolQueue.Dequeue();
                            tempExchange = _exchange;//*
                            String fileName = _path;
                            

                            //AdditionalData currentAdditionalData = requestedSymbolQueue.Dequeue();
                            //tempExchange = currentAdditionalData.Exchange;//*
                            //String fileName = currentAdditionalData.Path;

                            //wrtie data to file
                            using (CsvFileWriter writer = new CsvFileWriter(fileName))
                            {
                                foreach (var rt in results) //result.Items
                                {
                                    String[] dataProperties = rt.Split(',');
                                    CsvRow csvRow = new CsvRow();

                                    String check = rt.Trim();

                                    if (String.IsNullOrEmpty(check) == false)
                                    {
                                        if (dataProperties.Count() >= 5 && dataProperties.Count() < 9)//at least great than 5 price action and properties events
                                        {
                                            DateTime dateTimeItem = DateTime.Parse(dataProperties.FirstOrDefault().Trim());
                                            var dt = dateTimeItem.ToString("yyyy-MM-dd HH:mm:ss");
                                            csvRow.Add(dt);

                                            for (int i = 1; i < dataProperties.Count() - 2; i++)
                                            {
                                                csvRow.Add(dataProperties[i].Trim());
                                            }
                                            csvRow.Add(dataProperties[4]);//adjustment close 
                                            //csvRow.Add(dataProperties[5]);//volume

                                            csvRow.Add(_currentSymbol.Trim());
                                            csvRow.Add(_timeFrame);
                                            if (currentAdditionalData.Exchange != "Forex")
                                            {
                                                csvRow.Add(_exchange);
                                            }

                                            //csvRow.Add(currentAdditionalData.Symbol.Trim());
                                            //csvRow.Add(currentAdditionalData.TimeFrame);
                                            //if (currentAdditionalData.Exchange != "Forex")
                                            //{
                                            //    csvRow.Add(currentAdditionalData.Exchange);
                                            //}

                                            writer.WriteRow(csvRow);
                                        }
                                        else
                                        {
                                            //collect failed request symbol and date
                                        }
                                    }
                                }
                            }
                            bResumeSendRequest = true;
                        }
                        //-------------------------//
                        // clear the m_strData to verify it is empty for the next read off the socket
                        sData = "";

                        // call wait for data to notify the socket that we are ready to receive another callback
                        //WaitForData("History"); //temp
                        
                       // DataConsumer.ConsumeData("EndOfDay", DataConsumer._selectedExchange);
                    }
                    WaitForData("History"); //temp

                        //System.Threading.Thread.Sleep(2000);
                       // DataConsumer.ConsumeData("EndOfDay", DataConsumer._selectedExchange);
                    
                //}
            }

            private void WaitForData(string sSocketName)
            {
                if (sSocketName.Equals("History"))
                {
                    // make sure we have a callback created
                    if (m_pfnLookupCallback == null)
                    {
                        m_pfnLookupCallback = new AsyncCallback(OnReceive);
                    }

                    // send the notification to the socket.  It is very important that we don't call Begin Reveive more than once per call
                    // to EndReceive.  As a result, we set a flag to ignore multiple calls.
                    if (m_bLookupNeedBeginReceive)
                    {
                        m_bLookupNeedBeginReceive = false;

                    
                        // we pass in the sSocketName in the state parameter so that we can verify the socket data we receive is the data we are looking for
                        m_sockLookup.BeginReceive(m_szLookupSocketBuffer, 0, m_szLookupSocketBuffer.Length, SocketFlags.None, m_pfnLookupCallback, sSocketName);
                    }
                }
            }

            private int GetIQFeedPort(string sPort)
            {
                int iReturn = 0;
                RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\DTN\\IQFeed\\Startup");
                if (key != null)
                {
                    string sData = "";
                    switch (sPort)
                    {
                        case "Level1":
                            // the default port for Level 1 data is 5009.
                            sData = key.GetValue("Level1Port", "5009").ToString();
                            break;
                        case "Lookup":
                            // the default port for Lookup data is 9100.
                            sData = key.GetValue("LookupPort", "9100").ToString();
                            break;
                        case "Level2":
                            // the default port for Level 2 data is 9200.
                            sData = key.GetValue("Level2Port", "9200").ToString();
                            break;
                        case "Admin":
                            // the default port for Admin data is 9300.
                            sData = key.GetValue("AdminPort", "9300").ToString();
                            break;
                        case "Derivative":
                            // the default port for derivative data is 9400
                            sData = key.GetValue("DerivativePort", "9400").ToString();
                            break;
                    }
                    iReturn = Convert.ToInt32(sData);
                }
                return iReturn;
            }
    }
}
