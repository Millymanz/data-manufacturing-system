﻿namespace ConsumerLibrary.DataModel
{
    public class TimeFrameMappingDto
    {
        public string TimeFrameMappingId { get; set; }
        public string FriendlyTimeFrame { get; set; }
    }
}
