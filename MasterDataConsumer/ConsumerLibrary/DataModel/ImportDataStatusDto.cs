﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerLibrary.DataModel
{
    public class ImportDataStatusDto
    {
        public int ImportStatusId { get; set; }
        public DateTime ImportDateTime { get; set; }
        public string TimeFrame { get; set; }
        public string StockExchange { get; set; }
    }
}
