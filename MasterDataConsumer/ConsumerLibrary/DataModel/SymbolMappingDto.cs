﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerLibrary.DataModel
{
    public class SymbolMappingDto
    {
        public string SystemSymbolId { get; set; }
        public string FriendlySymbol { get; set; }
    }
}
