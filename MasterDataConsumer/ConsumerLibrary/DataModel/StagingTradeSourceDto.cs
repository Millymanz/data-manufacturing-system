﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerLibrary.DataModel
{
    public class StagingTradeSourceDto
    {
        public DateTime DateTime { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public decimal AdjustmentClose { get; set; }
        public string SymbolID { get; set; }
        public string TimeFrame { get; set; }
        public string StockExchange { get; set; }

        public StagingTradeSourceDto(DateTime dateTime, decimal open, decimal high, decimal low, decimal close, decimal volume, decimal adjustmentClose, string symbolID, string timeFrame, string stockExchange)
        {
            DateTime = dateTime;
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
            AdjustmentClose = adjustmentClose;
            SymbolID = symbolID;
            TimeFrame = timeFrame;
            StockExchange = stockExchange;
        }
    }
}
