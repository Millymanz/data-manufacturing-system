﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerLibrary {
    public class Bid {
        public decimal price { get; set; }
        public decimal size { get; set; }
    }

}
