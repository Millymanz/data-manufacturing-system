﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerLibrary {
    public class Exchange {
        public string exchange_id { get; set; }
        public string website { get; set; }
        public string name { get; set; }
    }
}
