﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsumerLibrary.Api;
using ConsumerLibrary.DataAccess;
using ConsumerLibrary.Import;
using Insight.Database;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace ConsumerLibrary
{
    public static class UnityBootStrapper
    {
        private static IUnityContainer unityContainer;

        public static IUnityContainer GetContainer()
        {
            if (unityContainer == null)
            {
                Initialise();
            }

            return unityContainer;
        }

        private static void Initialise()
        {
            unityContainer=new UnityContainer();
            //Register the dependency
            unityContainer.RegisterType<IInsightUnitOfWorkController, InsightUnitOfWorkController>(
                new ContainerControlledLifetimeManager(),
                new InjectionProperty("CryptoConnection",
                    ConfigurationManager.ConnectionStrings["TRADES_TEST_Crypto"].ConnectionString));
            unityContainer.RegisterType<IInsightCryptoStoredProcRepository, InsightCryptoStoredProcRepository>();
            SqlInsightDbProvider.RegisterProvider();
            unityContainer.RegisterType<IConfigHelper, ConfigHelper>();
            unityContainer.RegisterType<IImportProcess, CryptoImportProcess>();
            unityContainer.RegisterType<ICryptoApi, CryptoApi>();
            
        }
    }
}
