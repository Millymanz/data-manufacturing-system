﻿using System.Data.SqlClient;
using Unity.Attributes;

namespace ConsumerLibrary.DataAccess
{
    public class InsightUnitOfWorkController : IInsightUnitOfWorkController
    {
        [Dependency]
        public string CryptoConnection { get; set; }
        public SqlConnection GetCryptoConnection()
        {
            return new SqlConnection(CryptoConnection);
        }
    }
}