﻿using ConsumerLibrary.DataModel;
using System;
using System.Collections.Generic;
namespace ConsumerLibrary.DataAccess
{
    public interface IInsightCryptoStoredProcRepository
    {
        void InsertStagingTrade(IEnumerable<StagingTradeSourceDto> lstStagingTrade);
        IList<ImportDataStatusDto> GetImportStatusDto(string exchange, string timeframe, DateTime? importDateTime);
        IList<SymbolMappingDto> GetSymbolMappingDto();
        IList<TimeFrameMappingDto> TimeFrameMappingDto();
        void BulkInsertTrade();
    }
}