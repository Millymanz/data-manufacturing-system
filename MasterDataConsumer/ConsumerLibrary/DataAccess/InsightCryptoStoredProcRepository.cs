﻿using ConsumerLibrary.DataModel;
using System.Collections.Generic;
using Insight.Database;
using System;

namespace ConsumerLibrary.DataAccess
{
    public class InsightCryptoStoredProcRepository : IInsightCryptoStoredProcRepository
    {
        private readonly IInsightUnitOfWorkController insightUnitOfWorkController;

        public InsightCryptoStoredProcRepository(IInsightUnitOfWorkController insightUnitOfWorkController)
        {
            this.insightUnitOfWorkController = insightUnitOfWorkController;
        }
        public void InsertStagingTrade(IEnumerable<StagingTradeSourceDto> lstStagingTrade)
        {
            insightUnitOfWorkController.GetCryptoConnection().Execute("dbo.proc_InsertStagingTrade", new
            {
                StagingTradeSource = lstStagingTrade
            });
        }
        public void BulkInsertTrade()
        {
            insightUnitOfWorkController.GetCryptoConnection().Execute("dbo.proc_BulkInsertTrades");
        }
        public IList<ImportDataStatusDto> GetImportStatusDto(string exchange,string timeframe,DateTime? importDateTime)
        {
            return insightUnitOfWorkController.GetCryptoConnection().Query<ImportDataStatusDto>("proc_Get_ImportStatus", new
            {
                TimeFrame = timeframe,
                StockExchange = exchange,
                ImportDateTime = importDateTime
            }
           );
        }
        public IList<SymbolMappingDto> GetSymbolMappingDto()
        {
            return insightUnitOfWorkController.GetCryptoConnection().QuerySql<SymbolMappingDto>("SELECT [SystemSymbolId]      ,[FriendlySymbol]  FROM[dbo].[SymbolMapping]");
        }
        public IList<TimeFrameMappingDto> TimeFrameMappingDto()
        {
            return insightUnitOfWorkController.GetCryptoConnection().QuerySql<TimeFrameMappingDto>("SELECT [TimeFrameMappingId]      ,[FriendlyTimeFrame]  FROM [dbo].[TimeFrameMapping]");
        }
    }
}