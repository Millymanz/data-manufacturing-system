﻿using System.Data.SqlClient;

namespace ConsumerLibrary.DataAccess
{
    public interface IInsightUnitOfWorkController
    {
        string CryptoConnection { get; }
        SqlConnection GetCryptoConnection();
    }
}