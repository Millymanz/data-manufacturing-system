﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.IO;
using System.Web;
using System.Net;


using System.Collections.Specialized;
using Microsoft.Win32;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Timers;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Collections.Concurrent;


namespace ConsumerLibrary
{
    public struct CategoryPairing
    {
        public string MatchWords;
        public string Category;
        public string InstitutionBody;
        public string Event;
        public string KeyPersonnel;
        public string Role;
        public string Link;
    }

    


    public class Scraper
    {
        private static object locker = new object();

        private List<CategoryPairing> _eventToCategorization = new List<CategoryPairing>();

        private const double _lackValueCheck = -999999.9999;

        System.Timers.Timer _actionTimer;
        private String _triggerTime;

        private bool _scheduleSuccessful = false;
        private string _headLineLink;
        private DateTime _dateTime = new DateTime();

        private DateTime _ukTriggerDateTime = new DateTime();

        public bool ScheduledSuccessful 
        {
            get { return _scheduleSuccessful; }
        }

        public Scraper()
        {
            PopulateEventCategorization();
        }

        public Scraper(string triggerTime, string headlineLink, DateTime dateTime)
        {
            PopulateEventCategorization();

            _triggerTime = triggerTime;
            _headLineLink = headlineLink;

            _dateTime = dateTime;
        }

        public void ApplyScheduler()
        {
            if (HeadLineLinkExists(_headLineLink))
            {
                CreateSchedule();
            }
        }

        private bool HeadLineLinkExists(string headline)
        {
            var rowCount = GetRowCount("FUNDEF_TEST", "SELECT COUNT(*) FROM [FundamentalDataDefinitions].[dbo].[tblFundamentalKeywordDefinitions] DD " + 
                "WHERE DD.Link = '"+headline+"'");

            if (rowCount == 1) return true;
            return false;
        }

        public void CreateSchedule()
        {
            _actionTimer = new System.Timers.Timer();

            _actionTimer.Elapsed += new ElapsedEventHandler(Task);

            var timeInt = CalculateInterval();

            if (timeInt == -1) return;

            _actionTimer.Interval = timeInt;
            _actionTimer.AutoReset = true;
            _scheduleSuccessful = true;

            _actionTimer.Start();

            Library.WriteErrorLog(" :: Schedule Set For "+ _triggerTime + " :: UK >>" + _ukTriggerDateTime.ToString() + " :: "+ _headLineLink);

            Console.WriteLine(DateTime.Now + " :: Schedule Set For " + _triggerTime + " :: UK >>" + _ukTriggerDateTime.ToString() + " :: " + _headLineLink);
        }

        private void Task(object sender, ElapsedEventArgs e)
        {
            _actionTimer.Stop();
            _actionTimer.Enabled = false;


            Library.WriteErrorLog(DateTime.Now + " :: Event FIRED " + _triggerTime + " :: " + _headLineLink);
            Console.WriteLine(DateTime.Now + " :: Event FIRED " + _triggerTime + " :: " + _headLineLink);

            //wait for 3minutes before scrapping to guarantee
            //changed data
            Thread.Sleep(180000);

            ManualPageByPageScrap();
        }

        public int GetRowCount(String selector, string sqlQry)
        {
            int affectedRows = -1;
            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand(sqlQry, con);
                    con.Open();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandType = CommandType.Text;
                    affectedRows = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return affectedRows;
        }

        private double CalculateInterval()
        {
            try
            {
                int hours = 0;
                int minutes = 0;
                if (_triggerTime.Length == 5)
                {
                    hours = Convert.ToInt32(_triggerTime.Substring(0, 2));
                    minutes = Convert.ToInt32(_triggerTime.Substring(3, 2));
                }
                else
                {
                    hours = Convert.ToInt32(_triggerTime.Substring(0, 1));
                    minutes = Convert.ToInt32(_triggerTime.Substring(2, 2));
                }

                DateTime dayDueTime = new DateTime(_dateTime.Year, _dateTime.Month, _dateTime.Day,
                    hours, minutes, 0);

                var usaTimeZoneCurrentTemp = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                //testDateTime = TimeZoneInfo.ConvertTime(testDateTime, usaTimeZoneCurrentTemp);    
                hours = (usaTimeZoneCurrentTemp.BaseUtcOffset.Hours * -1) + hours;

                DateTime testDateTime = new DateTime();

                if (hours > 23)
                {
                    dayDueTime = dayDueTime.AddHours((usaTimeZoneCurrentTemp.BaseUtcOffset.Hours * -1));
                    testDateTime = dayDueTime;
                }
                else
                {
                    testDateTime = new DateTime(dayDueTime.Year, dayDueTime.Month, dayDueTime.Day,
                    hours, minutes, 0);
                }

                if (DateTime.Now > testDateTime)
                {
                    return -1;
                }

                DateTime criteriaDateTime = new DateTime(testDateTime.Year, testDateTime.Month, testDateTime.Day,
                    testDateTime.Hour, testDateTime.Minute, 0);

                var usaTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
                criteriaDateTime = TimeZoneInfo.ConvertTime(criteriaDateTime, usaTimeZone);

                long ticks = criteriaDateTime.Ticks - DateTime.Now.Ticks;

                TimeSpan tms = new TimeSpan(ticks);
                _ukTriggerDateTime = criteriaDateTime;

                return tms.TotalMilliseconds;
            }
            catch (Exception ex)
            {

            }
            return -1;
        }

        private void PopulateEventCategorization()
        {
            var exchangeInUse = "Forex";

            String selector = "FUNDEF";
            switch ((MODE)1)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;


            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand command = new SqlCommand("GetFundamentalKeywordDefinitions", con);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 200;
                    con.Open();


                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {

                            _eventToCategorization.Add(
                                new CategoryPairing() { MatchWords = dataReader["MatchWords"].ToString(),
                                                        Category = dataReader["Category"].ToString(),
                                                        InstitutionBody = dataReader["InstitutionBody"].ToString(),
                                                        Event = dataReader["EventType"].ToString(),
                                                        Link = dataReader["Link"].ToString(),
                                                        KeyPersonnel = String.IsNullOrEmpty(dataReader["LastName"].ToString()) ?  String.Empty 
                                                        : dataReader["FirstName"].ToString() + " " +
                                                        dataReader["LastName"].ToString(),

                                                        Role = dataReader["Role"].ToString()
                                });
                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
            }
          


            //economic activity
            // _eventToCategorization.Add("pmi", "economic activity");


            //central bank
            //_eventToCategorization.Add("federal open market committee (fomc) meeting minutes", new CategoryPairing() { Category = "central bank", Entity = "fomc", Event = "meeting minutes" });

            //_eventToCategorization.Add("federal open market committee (fomc) members vote on where to set the rate", new CategoryPairing() { Category = "central bank", Entity = "fomc", Event = "vote on interest rate" });


            //_eventToCategorization.Add("mpc", "central bank");
            //_eventToCategorization.Add("cbrt", "central bank");






            //Employment 
            //_eventToCategorization.Add("adp",
            //    new CategoryPairing() { Category = "employment", Entity = "adp research institute", Event = "adp national employment report" });

            //_eventToCategorization.Add("adp national employment report",
            //    new CategoryPairing() { Category = "employment", Entity = "adp research institute", Event = "adp national employment report" });

            //_eventToCategorization.Add("germany unemployment change",
            //    new CategoryPairing() { Category = "employment", Entity = "destatis", Event = "unemployment statistics" });

            //_eventToCategorization.Add("nonfarm payrolls",
            //    new CategoryPairing() { Category = "employment", Entity = "bureau of labor statistics", Event = "nonfarm payrolls" });



        }


        //private void PopulateEventCategorization()
        //{


        //    //economic activity
        //   // _eventToCategorization.Add("pmi", "economic activity");


        //    //central bank
        //    _eventToCategorization.Add("federal open market committee (fomc) meeting minutes", new CategoryPairing() 
        //    { Category = "central bank", Entity = "fomc", Event = "meeting minutes"} );

        //    _eventToCategorization.Add("federal open market committee (fomc) members vote on where to set the rate", new CategoryPairing() 
        //    { Category = "central bank", Entity = "fomc", Event = "vote on interest rate"} );


        //    //_eventToCategorization.Add("mpc", "central bank");
        //    //_eventToCategorization.Add("cbrt", "central bank");






        //    //Employment 
        //    _eventToCategorization.Add("adp",
        //        new CategoryPairing() { Category = "employment", Entity = "adp research institute", Event = "adp national employment report" });

        //    _eventToCategorization.Add("adp national employment report", 
        //        new CategoryPairing() { Category = "employment", Entity = "adp research institute", Event = "adp national employment report" });

        //    _eventToCategorization.Add("germany unemployment change",
        //        new CategoryPairing() { Category = "employment", Entity = "destatis", Event = "unemployment statistics" });

        //    _eventToCategorization.Add("nonfarm payrolls",
        //        new CategoryPairing() { Category = "employment", Entity = "bureau of labor statistics", Event = "nonfarm payrolls" });


        //    //_eventToCategorization.Add("unemployment rate", "employment");
        //    //_eventToCategorization.Add("employment change", "employment");
        //    //_eventToCategorization.Add("claimant count change", "employment");
        //    //_eventToCategorization.Add("average earnings index", "employment");
        //    //_eventToCategorization.Add("participation rate", "employment");
        //    //_eventToCategorization.Add("initial jobless claims", "employment");
        //    //_eventToCategorization.Add("jobless claims", "employment");
        //    //_eventToCategorization.Add("real earnings", "employment");
        //    //_eventToCategorization.Add("philly fed employment", "employment");
        //    //_eventToCategorization.Add("payroll", "employment");
        //    //_eventToCategorization.Add("unemployment", "employment");

            
        //    //_eventToCategorization.Add("ism services pmi",
        //    //    new CategoryPairing() { Category = "employment", Entity = "institute for supply management", Event = "ism services pmi report" });


        //    //_eventToCategorization.Add("non-manufacturing purchasing managers' index", "employment");
        //    //_eventToCategorization.Add("unit labor costs", "employment");
        //    //_eventToCategorization.Add("jobs/applications ratio", "employment");
        //    //_eventToCategorization.Add("caged", "employment");
        //    //_eventToCategorization.Add("general register of employment and unemployment", "employment");
        //    //_eventToCategorization.Add("employment level", "employment");
        //    //_eventToCategorization.Add("jobseekers total", "employment");
        //    //_eventToCategorization.Add("employment cost index", "employment");












        //    ////economic activity
        //    //_eventToCategorization.Add("pmi", "economic activity");
        //    //_eventToCategorization.Add("mom", "economic activity");


        //    ////central bank
        //    //_eventToCategorization.Add("federal open market committee (fomc) meeting minutes", "central bank");
        //    //_eventToCategorization.Add("federal open market committee (fomc) voting member", "central bank");

        //    //_eventToCategorization.Add("mpc", "central bank");
        //    //_eventToCategorization.Add("cbrt", "central bank");
            





        //    ////Employment 
        //    //_eventToCategorization.Add("adp", "employment");
        //    //_eventToCategorization.Add("adp national employment report", "employment");
        //    //_eventToCategorization.Add("unemployment change", "employment");
        //    //_eventToCategorization.Add("nonfarm payrolls", "employment");
        //    //_eventToCategorization.Add("unemployment rate", "employment");
        //    //_eventToCategorization.Add("employment change", "employment");
        //    //_eventToCategorization.Add("claimant count change", "employment");
        //    //_eventToCategorization.Add("average earnings index", "employment");
        //    //_eventToCategorization.Add("participation rate", "employment");
        //    //_eventToCategorization.Add("initial jobless claims", "employment");
        //    //_eventToCategorization.Add("jobless claims", "employment");
        //    //_eventToCategorization.Add("real earnings", "employment");
        //    //_eventToCategorization.Add("philly fed employment", "employment");
        //    //_eventToCategorization.Add("payroll", "employment");
        //    //_eventToCategorization.Add("unemployment", "employment");
        //    //_eventToCategorization.Add("ism services pmi", "employment");
        //    //_eventToCategorization.Add("non-manufacturing purchasing managers' index", "employment");
        //    //_eventToCategorization.Add("unit labor costs", "employment");
        //    //_eventToCategorization.Add("jobs/applications ratio", "employment");
        //    //_eventToCategorization.Add("caged", "employment");
        //    //_eventToCategorization.Add("general register of employment and unemployment", "employment");
        //    //_eventToCategorization.Add("employment level", "employment");
        //    //_eventToCategorization.Add("jobseekers total", "employment");
        //    //_eventToCategorization.Add("employment cost index", "employment");







            
        //}

        public void Init()
        {

            NameValueCollection values = new NameValueCollection();
            values.Add("dateFrom", "2015-12-13");
            values.Add("dateTo", "2015-12-19");
            values.Add("timeZone", "8");

            values.Add("quotes_search_text", "");
            values.Add("country%5B%5D", "29");
            values.Add("country%5B%5D", "25");
            values.Add("country%5B%5D", "54");
            values.Add("country%5B%5D", "145");

            values.Add("importance%5B%5D", "3");
            values.Add("timeFrame", "thisWeek");


            string Url = "http://www.investing.com/economic-calendar/filter";

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] result = client.UploadValues(Url, "POST", values);
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
            }
        }


        public void TestCall()
        {
            NameValueCollection values = new NameValueCollection();
            values.Add("eventID", "313100");
            values.Add("event_attr_ID", "212");
            values.Add("event_timestamp", "2015-09-10 11:00:00");

            values.Add("is_speech", "0");

            string Url = "http://www.investing.com/economic-calendar/more-history";

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                client.Headers.Add("Cookie", "__gads=ID=0ad7587633a74db0:T=1448665282:S=ALNI_MaeE26CMk-_hy3NSpZ_k4-yOn3e-w; __qca=P0-608471203-1448665350658; __unam=ce5f7e1-1514b2f08b4-4ba24879-2; _jsuid=3074970123; _bs5ee48346806819d397ace2703d74d7fc=1; _bsb66eb6270705a2d1e6757763d1fc0e3b=1; _bs48170e42f2bc1a178983022b14cd92df=1; __sonar=826254728056650399; _bsb94151c4721595e4498d7097972867ff=1; _bsd1b751454dd2f6dd03dc99092c5d9018=1; optimizelyEndUserId=oeu1455299815368r0.7910128382500261; PHPSESSID=jbtfka9vrtilo92q8pa94lc6h2; userAlertPopupNotificationUntil=2016-05-09T21:24:56.313Z; editionPostpone=1460986324869; gtmFired=OK; activeConsent-1=1.1; fpros_popup=up; _VT_content_200125139_1=1; investing_logon=a%3A3%3A%7Bs%3A4%3A%22hash%22%3Bs%3A32%3A%22749433d72bf286702956f783652380b6%22%3Bs%3A3%3A%22uid%22%3Bs%3A9%3A%22200376687%22%3Bs%3A4%3A%22type%22%3Bs%3A5%3A%22false%22%3B%7D; SideBlockUser=a%3A2%3A%7Bs%3A10%3A%22stack_size%22%3Ba%3A1%3A%7Bs%3A11%3A%22last_quotes%22%3Bi%3A8%3B%7Ds%3A6%3A%22stacks%22%3Ba%3A1%3A%7Bs%3A11%3A%22last_quotes%22%3Ba%3A2%3A%7Bi%3A0%3Ba%3A3%3A%7Bs%3A7%3A%22pair_ID%22%3Bs%3A5%3A%2213136%22%3Bs%3A10%3A%22pair_title%22%3Bs%3A0%3A%22%22%3Bs%3A9%3A%22pair_link%22%3Bs%3A16%3A%22%2Fequities%2Flukoil%22%3B%7Di%3A1%3Ba%3A3%3A%7Bs%3A7%3A%22pair_ID%22%3Bs%3A4%3A%228827%22%3Bs%3A10%3A%22pair_title%22%3Bs%3A0%3A%22%22%3Bs%3A9%3A%22pair_link%22%3Bs%3A23%3A%22%2Fquotes%2Fus-dollar-index%22%3B%7D%7D%7D%7D; geoC=GB; _gat=1; _gat_allSitesTracker=1; optimizelySegments=%7B%224225444387%22%3A%22gc%22%2C%224226973206%22%3A%22direct%22%2C%224232593061%22%3A%22false%22%2C%225010352657%22%3A%22none%22%7D; optimizelyBuckets=%7B%7D; _ga=GA1.2.1764327701.1448665276; optimizelyPendingLogEvents=%5B%22n%3Dengagement%26u%3Doeu1455299815368r0.7910128382500261%26wxhr%3Dtrue%26time%3D1461401717.437%26f%3D5638970416%2C5681120499%26g%3D4227733615%22%5D");
                client.Headers.Add("Host", "www.investing.com");
                client.Headers.Add("Referer", "http://www.investing.com/economic-calendar/mpc-meeting-minutes-212");


                byte[] result = client.UploadValues(Url, "POST", values);
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
            }
        }

        //public void Init()
        //{
        //    var encoding = new ASCIIEncoding();

        //    var postData = "dateFrom=2013-01-16&dateTo=2015-12-16&timeZone=8&quotes_search_text=&country%5B%5D=29&country%5B%5D=25&country%5B%5D=54&country%"
        //    + "5B%5D=145&country%5B%5D=34&country%5B%5D=174&country%5B%5D=163&country%5B%5D=32&country%5B%5D=70&country%5B%5D=6&country%5B%5D=27&country%5B%5D=37"
        //    + "&country%5B%5D=122&country%5B%5D=15&country%5B%5D=113&country%5B%5D=107&country%5B%5D=55&country%5B%5D=24&country%5B%5D=121&country%5B%5D=59&country%"
        //    + "5B%5D=89&country%5B%5D=72&country%5B%5D=71&country%5B%5D=22&country%5B%5D=17&country%5B%5D=51&country%5B%5D=39&country%5B%5D=93&country%5B%5D=106&country%5B%5D=14&country%5B%5D=48&country%5B%5D=33&country%5B%5D=23&country%5B%5D=10&country%5B%5D=35&country%5B%5D=92&country%5B%5D=57&country%5B%5D=94&country%5B%5D=97&country%5B%5D=68&country%5B%5D=96&country%5B%5D=103&country%5B%5D=111&country%5B%5D=42&country%5B%5D=109&country%5B%5D=188&country%5B%5D=7&country%5B%5D=105&country%5B%5D=172&country%5B%5D=21&country%5B%5D=43&country%5B%5D=20&country%5B%5D=60&country%5B%5D=87&country%5B%5D=44&country%5B%5D=193&country%5B%5D=125&country%5B%5D=45&country%5B%5D=53&country%5B%5D=38&country%5B%5D=170&country%5B%5D=100&country%5B%5D=56&country%5B%5D=80&country%5B%5D=52&country%5B%5D=36&country%5B%5D=90&country%5B%5D=112&country%5B%5D=110&country%5B%5D=11&country%5B%5D=26&country%5B%5D=162&country%5B%5D=9&country%5B%5D=12&country%5B%5D=46&country%5B%5D=85&country%5B%5D=41&country%5B%5D=202&country%5B%5D=63&country%5B%5D=123&country%5B%5D=61&country%5B%5D=143&country%5B%5D=4&country%5B%5D=5&country%5B%5D=138&country%5B%5D=178&country%5B%5D=84&country%5B%5D=75&timeFilter=timeRemain&category%5B%5D=_employment&category%5B%5D=_economicActivity&category%5B%5D=_inflation&category%5B%5D=_credit&category%5B%5D=_centralBanks&category%5B%5D=_confidenceIndex&category%5B%5D=_balance&category%5B%5D=_Bonds&importance%5B%5D=3";

        //    byte[] data = encoding.GetBytes(postData);

        //    var myRequest =
        //      (HttpWebRequest)WebRequest.Create("http://www.investing.com/economic-calendar/filter");
        //    myRequest.Method = "POST";
        //    myRequest.ContentType = "application/x-www-form-urlencoded";
        //    myRequest.ContentLength = data.Length;
        //    myRequest.Accept = "application/json, text/javascript, */*; q=0.01";

        //    var newStream = myRequest.GetRequestStream();
        //    newStream.Write(data, 0, data.Length);
        //    newStream.Close();

        //    var response = myRequest.GetResponse();
        //    var responseStream = response.GetResponseStream();
        //    var responseReader = new StreamReader(responseStream);
        //    var result = responseReader.ReadToEnd();

        //    string res = result;
        //}

        public void Scrap()
        {
            var rootLinks = GetRootLinks();

            var economicData = ScrapPage(rootLinks);
            var economicDataItems = ConvertToEconomicData(economicData);

            var econDataFiltered = ValidateCategorize(economicDataItems);
            WriteToCSVFile(econDataFiltered);
        }

        public void ManualPageByPageScrap()
        {
            var economicData = ScrapPageManual();
            var economicDataItems = ConvertToEconomicData(economicData);

            var econDataFiltered = ValidateCategorize(economicDataItems);
            
            Console.WriteLine("Writing to File");

            WriteToCSVFile(econDataFiltered);

            using (DatabaseManagerServiceImporter.DatabaseManagerServiceClient dbmClient = new DatabaseManagerServiceImporter.DatabaseManagerServiceClient())
            {
                dbmClient.Open();//_fileName
                dbmClient.RetrieveRawDataFiles("", (int)1); //Mode

                Library.WriteErrorLog(DateTime.Now + " :: Scraping Completed - Notifying Importer :: " + _headLineLink);
            }
        }

        private List<EconomicData> ConvertToEconomicData(List<EconomicPageTemplate> econTemplateList)
        {
            List<EconomicData> economicDataList = new List<EconomicData>();

            foreach (var item in econTemplateList)
            {
                foreach (var econItemTemp in item.EconomicValuesList)
                {
                    var econData = new EconomicData();
                    econData.EventHeadline = item.EventHeadline;
                    econData.CategoryType = item.CategoryType;
                    econData.EventType = item.EventType;
                    //econData.ReleaseDateTime = item.ReleaseDateTime;
                    econData.Country = item.Country;

                    econData.DescriptionOfEvent = item.DescriptionOfEvent;
                    econData.Importance = item.Importance;
                    econData.InstitutionBody = item.InstitutionBody;

                    econData.EventInfluentialPersonnelFirstName = item.EventInfluentialPersonnelFirstName;
                    econData.EventInfluentialPersonnelLastName = item.EventInfluentialPersonnelLastName;
                    econData.EconomicSource = item.EconomicSource;

                    econData.EconomicSourceTitle = item.EconomicSourceTitle;
                    econData.Currency = item.Currency;
                    econData.JSONSrc = item.JSONSrc;

                    econData.NumericType = econItemTemp.CurrentActualValue != "null" ? GetNumericType(econItemTemp.CurrentActualValue)
                         : GetNumericType(econItemTemp.CurrentPreviousValue);

                    econData.CurrentActualValue = GetNumberOnly(econItemTemp.CurrentActualValue);
                    econData.CurrentForecastValue = GetNumberOnly( econItemTemp.CurrentForecastValue);
                    econData.CurrentPreviousValue =  GetNumberOnly(econItemTemp.CurrentPreviousValue);



                    //econData.NumericType = econItemTemp.NumericType;

                    econData.ReleaseDateTime = econItemTemp.ReleaseDateTime;

                    economicDataList.Add(econData);
                }
            }

            return economicDataList;
        }

        private List<string> GetRootLinks()
        {
            List<string> rootLinks = new List<string>();

            var getHtmlWeb = new HtmlWeb();

            bool runHistorics = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_HISTORICS"].ToString());

            HtmlDocument document = null;

            if (document != null)
            {
                document = getHtmlWeb.Load(System.Configuration.ConfigurationManager
                    .AppSettings["ECONOMIC_DATA_SRC"].ToString() + "/economic-calendar");
            }
            else
            {
                document = new HtmlDocument();
                document.Load(System.Configuration.ConfigurationManager.AppSettings["HISTORICAL_ECONOMIC_DATA"].ToString());
            }

            //HtmlDocument document = new HtmlDocument();

            // document.Load("C:\\Users\\TradeRiser\\Downloads\\xPath\\EconomicScrapperApp\\EconomicScrapperApp\\HistoricalEconomics.html");

            //filter form request
            //http://www.investing.com/economic-calendar/filter

            string classToFind = "left event";
            var allElementsWithClassFloat = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

            int counter = 1;
            if (allElementsWithClassFloat != null)
            {
                foreach (var aTag in allElementsWithClassFloat)
                {
                    if (aTag.HasChildNodes)
                    {
                        if (aTag.FirstChild.HasAttributes)
                        {
                            var linksPresent = aTag.FirstChild.Attributes["href"].Value;
                            rootLinks.Add(linksPresent);

                            Console.WriteLine(linksPresent);
                        }
                    }
                    counter++;
                }
            }
            return rootLinks;
        }

        private List<EconomicPageTemplate> ScrapPageManual()
        {
            var listOfEconomicData = new List<EconomicPageTemplate>();

            bool runHistorics = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_HISTORICS"].ToString());
            string[] files = null;
            
            var runasScheduleScrapper = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RUN_AS_SCHEDULED_SCRAPPER"].ToString());

            if (runHistorics == false && runasScheduleScrapper == false)
            {
                //run live
                files = _eventToCategorization.Select(l => l.Link).ToArray();
                Console.WriteLine("Getting Live Scrap Data");
            }
            else if (runasScheduleScrapper)
            {
                files = new string[] { _headLineLink };
            }
            else
            {
                var baseURL_Files = System.Configuration.ConfigurationManager.AppSettings["HISTORICAL_ECONOMIC_DATA_FILES"].ToString();
                files = System.IO.Directory.GetFiles(baseURL_Files);
                Console.WriteLine("Using Historical Manual Data");
            }

            int breakPoint = 0;

            foreach (var item in files)
            {
                var getHtmlWeb = new HtmlWeb();

                //var item = "http://www.investing.com/economic-calendar/us-presidential-election-371";

                string url = item;

                if (string.IsNullOrEmpty(item) == false)
                {
                    HtmlDocument document = getHtmlWeb.Load(item);
                    Console.WriteLine(item);


                    string classToFind = "float_lang_base_1 js-portfolio-list";
                    var eventHeadline = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));
                    if (eventHeadline == null) eventHeadline = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", "float_lang_base_1 relativeAttr"));

                    if (eventHeadline != null)
                    {
                        var economicData = new EconomicPageTemplate();

                        economicData.EventHeadline = eventHeadline.FirstOrDefault().InnerHtml;


                        var idToFind = "releaseInfo";
                        var releaseDate = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));

                        if (releaseDate.Count > 0)
                        {
                            var aTag = releaseDate.FirstOrDefault().ChildNodes[1];
                            if (aTag.HasChildNodes)
                            {
                                var classItem = "historyTab";
                                var tableDateTime = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classItem));
                                var aTagTemp = tableDateTime.FirstOrDefault();

                                var tempText = aTagTemp.ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[3].InnerHtml;

                                String dateTimeStr = aTag.LastChild.InnerHtml + " " + tempText;
                                
                                DateTime dateTime = new DateTime();

                                if (DateTime.TryParse(dateTimeStr, out dateTime) == false)
                                {
                                    var clearnDateTime = CleanText(dateTimeStr, false);
                                    var arraySplit = clearnDateTime.Split('<');

                                    if (arraySplit.Count() > 1) clearnDateTime = arraySplit.FirstOrDefault();

                                    if (DateTime.TryParse(clearnDateTime, out dateTime))
                                    {
                                        dateTimeStr = clearnDateTime;
                                    }
                                }


                                if (DateTime.TryParse(dateTimeStr, out dateTime))
                                {
                                    var tempId = "eventTabDiv_history_0";

                                    var actual = releaseDate.FirstOrDefault().SelectNodes(string.Format("//*[contains(@id,'{0}')]", tempId));

                                    var headersLookUp = GetHeaders(actual);

                                    var temp = actual.FirstOrDefault().ChildNodes[1].ChildNodes[3].ChildNodes.Where(m => m.Name == "tr");

                                    if (temp.Any())
                                    {
                                        foreach (var tr in temp)
                                        {
                                            if (tr.ChildNodes.Where(c => c.Name == "td").Any())
                                            {
                                                var foundItems = tr.ChildNodes.Where(c => c.Name == "td").ToList();

                                                var econValues = new EconomicValues();
                                                econValues.CurrentActualValue = _lackValueCheck.ToString();
                                                econValues.CurrentPreviousValue = _lackValueCheck.ToString();
                                                econValues.CurrentForecastValue = _lackValueCheck.ToString();


                                                string dateTimeStrTemp = "";

                                                for (int i = 0; i < foundItems.Count(); i++)
                                                {
                                                    switch (headersLookUp[i])
                                                    {
                                                        case "Release Date":
                                                            {

                                                                var removeInd = foundItems[i].InnerText.IndexOf("(");
                                                                if (removeInd > 0)
                                                                {
                                                                    dateTimeStrTemp = foundItems[i].InnerText.Substring(0, removeInd);
                                                                }
                                                                else
                                                                {
                                                                    dateTimeStrTemp = foundItems[i].InnerText;
                                                                }
                                                            } break;

                                                        case "Time":
                                                            {
                                                                dateTimeStrTemp += " " + foundItems[i].InnerText;
                                                            } break;

                                                        case "Actual":
                                                            {

                                                                if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentActualValue = CleanText(foundItems[i].InnerText, false);
                                                            } break;
                                                        case "Previous":
                                                            {
                                                               if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentPreviousValue = CleanText(foundItems[i].InnerText, false);
                                                            } break;

                                                        case "Forecast":
                                                            {
                                                                if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentForecastValue = CleanText(foundItems[i].InnerText, false);
                                                            } break;
                                                    }
                                                }

                                                if (DateTime.TryParse(dateTimeStrTemp, out dateTime))
                                                {
                                                    econValues.ReleaseDateTime = dateTime;
                                                    economicData.EconomicValuesList.Add(econValues);
                                                }
                                                else
                                                {
                                                    var clearnDateTime = CleanText(dateTimeStrTemp, false);
                                                    var arraySplit = clearnDateTime.Split('<');
                                                    if (arraySplit.Count() > 1) clearnDateTime = arraySplit.FirstOrDefault();

                                                    if (DateTime.TryParse(clearnDateTime, out dateTime))
                                                    {
                                                        econValues.ReleaseDateTime = dateTime;
                                                        economicData.EconomicValuesList.Add(econValues);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            idToFind = "overViewBox";
                            var description = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));
                            aTag = description.FirstOrDefault();

                            if (aTag.HasChildNodes)
                            {
                                if (aTag.ChildNodes[3].HasChildNodes)
                                {
                                    economicData.DescriptionOfEvent = aTag.ChildNodes[1].InnerHtml;

                                    var country = aTag.ChildNodes[3].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                    economicData.Country = country;

                                    var source = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
                                    economicData.EconomicSource = source;

                                    var currency = aTag.ChildNodes[3].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
                                    economicData.Currency = currency;

                                    var sourceTitle = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                    economicData.EconomicSourceTitle = sourceTitle;


                                    classToFind = "grayFullBullishIcon";
                                    var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

                                    switch (importanceLevel.Count)
                                    {
                                        case 1: { economicData.Importance = "Low"; } break;
                                        case 2: { economicData.Importance = "Medium"; } break;
                                        case 3: { economicData.Importance = "High"; } break;
                                    }

                                    economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
                                    economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", "").Replace("\t", "");

                                }
                                else
                                {
                                    economicData.DescriptionOfEvent = economicData.EventHeadline;

                                    //aTag.ChildNodes[1]

                                    var country = aTag.ChildNodes[1].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                    economicData.Country = country;

                                    var source = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
                                    economicData.EconomicSource = source;

                                    var currency = aTag.ChildNodes[1].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
                                    economicData.Currency = currency;

                                    var sourceTitle = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                    economicData.EconomicSourceTitle = sourceTitle;


                                    classToFind = "grayFullBullishIcon";
                                    var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

                                    switch (importanceLevel.Count)
                                    {
                                        case 1: { economicData.Importance = "Low"; } break;
                                        case 2: { economicData.Importance = "Medium"; } break;
                                        case 3: { economicData.Importance = "High"; } break;
                                    }

                                    economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
                                    economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", "").Replace("\t", "");
                                }

                                listOfEconomicData.Add(economicData);
                            }
                        }
                    }
                    breakPoint++;
                }
            }
            return listOfEconomicData;
        }

        private List<EconomicPageTemplate> ScrapPage(List<string> rootLinks)
        {
            var listOfEconomicData = new List<EconomicPageTemplate>();

            int breakPoint = 0;

            foreach (var item in rootLinks)
            {
                var getHtmlWeb = new HtmlWeb();

                var baseURL = System.Configuration.ConfigurationManager.AppSettings["ECONOMIC_DATA_SRC"].ToString();

                string url = baseURL + item;
                HtmlDocument document = getHtmlWeb.Load(item);



                string classToFind = "float_lang_base_1 js-portfolio-list";
                var eventHeadline = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));


                if (eventHeadline != null)
                {
                    var economicData = new EconomicPageTemplate();

                    economicData.EventHeadline = eventHeadline.FirstOrDefault().InnerHtml;


                    var idToFind = "releaseInfo";
                    var releaseDate = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));

                    if (releaseDate.Count > 0)
                    {
                        var aTag = releaseDate.FirstOrDefault().ChildNodes[1];
                        if (aTag.HasChildNodes)
                        {
                            var classItem = "historyTab";
                            var tableDateTime = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classItem));
                            var aTagTemp = tableDateTime.FirstOrDefault();

                            var tempText = aTagTemp.ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[3].InnerHtml;

                            String dateTimeStr = aTag.LastChild.InnerHtml + " " + tempText;
                            DateTime dateTime = new DateTime();

                            if (DateTime.TryParse(dateTimeStr, out dateTime))
                            {
                               // economicData.ReleaseDateTime = dateTime;


                                var tempId = "eventTabDiv_history_0";

                                var actual = releaseDate.FirstOrDefault().SelectNodes(string.Format("//*[contains(@id,'{0}')]", tempId));

                                var headersLookUp = GetHeaders(actual);



                                var temp = actual.FirstOrDefault().ChildNodes[1].ChildNodes[3].ChildNodes.Where(m => m.Name == "tr");

                                if (temp.Any())
                                {
                                    foreach (var tr in temp)
                                    {
                                        if (tr.ChildNodes.Where(c => c.Name == "td").Any())
                                        {
                                            var foundItems = tr.ChildNodes.Where(c => c.Name == "td").ToList();

                                            var econValues = new EconomicValues();

                                            string dateTimeStrTemp = "";

                                            for (int i = 0; i < foundItems.Count(); i++)
                                            {
                                                switch (headersLookUp[i])
                                                {
                                                    case "Release Date":
                                                        {

                                                            var removeInd = foundItems[i].InnerText.IndexOf("(");
                                                            if (removeInd > 0)
                                                            {
                                                                dateTimeStrTemp = foundItems[i].InnerText.Substring(0, removeInd);
                                                            }
                                                            else
                                                            {
                                                                dateTimeStrTemp = foundItems[i].InnerText;
                                                            }
                                                        } break;

                                                    case "Time":
                                                        {
                                                            dateTimeStrTemp += " " + foundItems[i].InnerText;
                                                        } break;

                                                    case "Actual":
                                                        {
                                                            econValues.CurrentActualValue = _lackValueCheck.ToString();

                                                            if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentActualValue = CleanText(foundItems[i].InnerText, false);
                                                        } break;
                                                    case "Previous":
                                                        {
                                                            econValues.CurrentPreviousValue = _lackValueCheck.ToString();
                                                            if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentPreviousValue = CleanText(foundItems[i].InnerText, false);
                                                        } break;

                                                    case "Forecast":
                                                        {
                                                            econValues.CurrentForecastValue = _lackValueCheck.ToString();
                                                            if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentForecastValue = CleanText(foundItems[i].InnerText, false);
                                                        } break;
                                                }
                                            }

                                            if (DateTime.TryParse(dateTimeStrTemp, out dateTime))
                                            {
                                                econValues.ReleaseDateTime = dateTime;

                                                economicData.EconomicValuesList.Add(econValues);
                                            }
                                            else
                                            {
                                                var failedConversion = dateTime;
                                            }
                                        }
                                    }
                                }



                                if (actual.Where(f => f.Name == "div").Any())
                                {
                                    //if (actual.Where(f => f.Name == "div").Any())
                                    //{
                                    //    var listOfValues = actual.ToList();
                                    //    int index = -1;

                                    //    if (ValueScenarioCheck(listOfValues, "Actual", out index))
                                    //    {
                                    //        economicData.CurrentActualValue = actual[index].InnerText;
                                    //    }

                                    //    if (ValueScenarioCheck(listOfValues, "Forecast", out index))
                                    //    {
                                    //        economicData.CurrentForecastValue = actual[index].InnerText;
                                    //    }

                                    //    if (ValueScenarioCheck(listOfValues, "Previous", out index))
                                    //    {
                                    //        economicData.CurrentPreviousValue = actual[index].InnerText;
                                    //    }
                                    //}



                                    //if (valuesCount == 2)
                                    //{
                                    //    economicData.CurrentActualValue = actual[0].InnerText;
                                    //    economicData.CurrentForecastValue = actual[1].InnerText;
                                    //}

                                    //if (valuesCount == 3)
                                    //{
                                    //    economicData.CurrentActualValue = actual[0].InnerHtml;
                                    //    economicData.CurrentForecastValue = actual[1].InnerHtml;
                                    //    economicData.CurrentPreviousValue = actual[2].InnerHtml;
                                    //}
                                    //if (valuesCount == 2)
                                    //{
                                    //    economicData.CurrentActualValue = actual[0].InnerHtml;
                                    //    economicData.CurrentForecastValue = actual[1].InnerHtml;
                                    //}
                                }
                            }
                        }

                        idToFind = "overViewBox";
                        var description = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));
                        aTag = description.FirstOrDefault();

                        if (aTag.HasChildNodes)
                        {
                            if (aTag.ChildNodes[3].HasChildNodes)
                            {
                                economicData.DescriptionOfEvent = aTag.ChildNodes[1].InnerHtml;

                                var country = aTag.ChildNodes[3].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.Country = country;

                                var source = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
                                economicData.EconomicSource = source;

                                var currency = aTag.ChildNodes[3].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
                                economicData.Currency = currency;

                                var sourceTitle = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.EconomicSourceTitle = sourceTitle;


                                classToFind = "grayFullBullishIcon";
                                var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

                                switch (importanceLevel.Count)
                                {
                                    case 1: { economicData.Importance = "Low"; } break;
                                    case 2: { economicData.Importance = "Medium"; } break;
                                    case 3: { economicData.Importance = "High"; } break;
                                }

                                economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
                                economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", ""); ;


                                // economicData.EventHeadline = CleanText(economicData.EventHeadline);
                                //economicData.DescriptionOfEvent = "Temporary text";

                            }
                            else
                            {
                                economicData.DescriptionOfEvent = economicData.EventHeadline;

                                //aTag.ChildNodes[1]

                                var country = aTag.ChildNodes[1].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.Country = country;

                                var source = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
                                economicData.EconomicSource = source;

                                var currency = aTag.ChildNodes[1].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
                                economicData.Currency = currency;

                                var sourceTitle = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.EconomicSourceTitle = sourceTitle;


                                classToFind = "grayFullBullishIcon";
                                var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

                                switch (importanceLevel.Count)
                                {
                                    case 1: { economicData.Importance = "Low"; } break;
                                    case 2: { economicData.Importance = "Medium"; } break;
                                    case 3: { economicData.Importance = "High"; } break;
                                }

                                economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
                                economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", ""); ;


                            }

                            listOfEconomicData.Add(economicData);
                        }
                    }
                }

                breakPoint++;
            }
            return listOfEconomicData;
        }

        private List<EconomicPageTemplate> ScrapPageSmart(List<string> rootLinks)
        {
            var listOfEconomicData = new List<EconomicPageTemplate>();

            int breakPoint = 0;

            foreach (var item in rootLinks)
            {
                //if (breakPoint == 17) break;

                var getHtmlWeb = new HtmlWeb();

                var baseURL = System.Configuration.ConfigurationManager.AppSettings["ECONOMIC_DATA_SRC"].ToString();

                string url = baseURL + item;
                HtmlDocument document = getHtmlWeb.Load(item);



                string classToFind = "float_lang_base_1 js-portfolio-list";
                var eventHeadline = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));


                if (eventHeadline != null)
                {
                    var economicData = new EconomicPageTemplate();
                    //economicData.CurrentForecastValue = "null";
                    //economicData.CurrentActualValue = "null";
                    //economicData.CurrentPreviousValue = "null";

                    economicData.EventHeadline = eventHeadline.FirstOrDefault().InnerHtml;


                    var idToFind = "releaseInfo";
                    var releaseDate = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));

                    if (releaseDate.Count > 0)
                    {
                        var aTag = releaseDate.FirstOrDefault().ChildNodes[1];
                        if (aTag.HasChildNodes)
                        {
                            var classItem = "historyTab";
                            var tableDateTime = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classItem));
                            var aTagTemp = tableDateTime.FirstOrDefault();

                            var tempText = aTagTemp.ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[3].InnerHtml;

                            String dateTimeStr = aTag.LastChild.InnerHtml + " " + tempText;
                            DateTime dateTime = new DateTime();

                            if (DateTime.TryParse(dateTimeStr, out dateTime))
                            {
                                // economicData.ReleaseDateTime = dateTime;


                                var tempId = "eventTabDiv_history_0";

                                var actual = releaseDate.FirstOrDefault().SelectNodes(string.Format("//*[contains(@id,'{0}')]", tempId));

                                var headersLookUp = GetHeaders(actual);



                                var temp = actual.FirstOrDefault().ChildNodes[1].ChildNodes[3].ChildNodes.Where(m => m.Name == "tr");

                                if (temp.Any())
                                {
                                    foreach (var tr in temp)
                                    {
                                        if (tr.ChildNodes.Where(c => c.Name == "td").Any())
                                        {
                                            var foundItems = tr.ChildNodes.Where(c => c.Name == "td").ToList();

                                            var econValues = new EconomicValues();

                                            string dateTimeStrTemp = "";

                                            for (int i = 0; i < foundItems.Count(); i++)
                                            {
                                                switch (headersLookUp[i])
                                                {
                                                    case "Release Date":
                                                        {

                                                            var removeInd = foundItems[i].InnerText.IndexOf("(");
                                                            if (removeInd > 0)
                                                            {
                                                                dateTimeStrTemp = foundItems[i].InnerText.Substring(0, removeInd);
                                                            }
                                                            else
                                                            {
                                                                dateTimeStrTemp = foundItems[i].InnerText;
                                                            }
                                                        } break;

                                                    case "Time":
                                                        {
                                                            dateTimeStrTemp += " " + foundItems[i].InnerText;
                                                        } break;

                                                    case "Actual":
                                                        {
                                                            econValues.CurrentActualValue = "null";

                                                            if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentActualValue = CleanText(foundItems[i].InnerText, false);
                                                        } break;
                                                    case "Previous":
                                                        {
                                                            econValues.CurrentPreviousValue = "null";
                                                            if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentPreviousValue = CleanText(foundItems[i].InnerText, false);
                                                        } break;

                                                    case "Forecast":
                                                        {
                                                            econValues.CurrentForecastValue = "null";
                                                            if (IsAcceptedNumber(foundItems[i].InnerText)) econValues.CurrentForecastValue = CleanText(foundItems[i].InnerText, false);
                                                        } break;
                                                }
                                            }

                                            if (DateTime.TryParse(dateTimeStrTemp, out dateTime))
                                            {
                                                econValues.ReleaseDateTime = dateTime;

                                                economicData.EconomicValuesList.Add(econValues);
                                            }
                                        }
                                    }
                                }



                                if (actual.Where(f => f.Name == "div").Any())
                                {
                                    //if (actual.Where(f => f.Name == "div").Any())
                                    //{
                                    //    var listOfValues = actual.ToList();
                                    //    int index = -1;

                                    //    if (ValueScenarioCheck(listOfValues, "Actual", out index))
                                    //    {
                                    //        economicData.CurrentActualValue = actual[index].InnerText;
                                    //    }

                                    //    if (ValueScenarioCheck(listOfValues, "Forecast", out index))
                                    //    {
                                    //        economicData.CurrentForecastValue = actual[index].InnerText;
                                    //    }

                                    //    if (ValueScenarioCheck(listOfValues, "Previous", out index))
                                    //    {
                                    //        economicData.CurrentPreviousValue = actual[index].InnerText;
                                    //    }
                                    //}



                                    //if (valuesCount == 2)
                                    //{
                                    //    economicData.CurrentActualValue = actual[0].InnerText;
                                    //    economicData.CurrentForecastValue = actual[1].InnerText;
                                    //}

                                    //if (valuesCount == 3)
                                    //{
                                    //    economicData.CurrentActualValue = actual[0].InnerHtml;
                                    //    economicData.CurrentForecastValue = actual[1].InnerHtml;
                                    //    economicData.CurrentPreviousValue = actual[2].InnerHtml;
                                    //}
                                    //if (valuesCount == 2)
                                    //{
                                    //    economicData.CurrentActualValue = actual[0].InnerHtml;
                                    //    economicData.CurrentForecastValue = actual[1].InnerHtml;
                                    //}
                                }
                            }
                        }

                        idToFind = "overViewBox";
                        var description = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));
                        aTag = description.FirstOrDefault();

                        if (aTag.HasChildNodes)
                        {
                            if (aTag.ChildNodes[3].HasChildNodes)
                            {
                                economicData.DescriptionOfEvent = aTag.ChildNodes[1].InnerHtml;

                                var country = aTag.ChildNodes[3].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.Country = country;

                                var source = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
                                economicData.EconomicSource = source;

                                var currency = aTag.ChildNodes[3].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
                                economicData.Currency = currency;

                                var sourceTitle = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.EconomicSourceTitle = sourceTitle;


                                classToFind = "grayFullBullishIcon";
                                var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

                                switch (importanceLevel.Count)
                                {
                                    case 1: { economicData.Importance = "Low"; } break;
                                    case 2: { economicData.Importance = "Medium"; } break;
                                    case 3: { economicData.Importance = "High"; } break;
                                }

                                economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
                                economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", ""); ;


                                // economicData.EventHeadline = CleanText(economicData.EventHeadline);
                                //economicData.DescriptionOfEvent = "Temporary text";

                            }
                            else
                            {
                                economicData.DescriptionOfEvent = economicData.EventHeadline;

                                //aTag.ChildNodes[1]

                                var country = aTag.ChildNodes[1].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.Country = country;

                                var source = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
                                economicData.EconomicSource = source;

                                var currency = aTag.ChildNodes[1].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
                                economicData.Currency = currency;

                                var sourceTitle = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
                                economicData.EconomicSourceTitle = sourceTitle;


                                classToFind = "grayFullBullishIcon";
                                var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

                                switch (importanceLevel.Count)
                                {
                                    case 1: { economicData.Importance = "Low"; } break;
                                    case 2: { economicData.Importance = "Medium"; } break;
                                    case 3: { economicData.Importance = "High"; } break;
                                }

                                economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
                                economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", ""); ;


                            }

                            listOfEconomicData.Add(economicData);
                        }
                    }
                }

                breakPoint++;
            }
            return listOfEconomicData;
        }


        private bool IsAcceptedNumber(string value)
        {
            var ans = value.Substring(0, value.Length - 1);
            
            double n = 0.0;

            bool isNumeric = Double.TryParse(ans, out n);
            if (isNumeric == false)
            {
                isNumeric = Double.TryParse(value, out n);
            }
            return isNumeric;
        }

        private Dictionary<int, string> GetHeaders(HtmlNodeCollection nodeCollection)
        {
            Dictionary<int, string> headers = new Dictionary<int, string>();

            var temp = nodeCollection.FirstOrDefault().ChildNodes[1].ChildNodes[1].ChildNodes.Where(m => m.Name == "tr");

            if (temp.Any())
            {
                if (temp.FirstOrDefault().ChildNodes.Where(c => c.Name == "th").Any())
                {
                    var foundItems = temp.FirstOrDefault().ChildNodes.Where(c => c.Name == "th").ToList();

                    for (int i = 0; i < foundItems.Count();  i++)
                    {
                        headers.Add(i, foundItems[i].InnerText);
                    }
                }
            }


            //if (nodeCollection.FirstOrDefault().ChildNodes[1].ChildNodes[1].ChildNodes[1].ChildNodes.Where(m => m.Name == "tr"))
            //{
            //    int h = 3;
            //}

            return headers;
        }


        //private List<EconomicData> ScrapPage(List<string> rootLinks)
        //{
        //    var listOfEconomicData = new List<EconomicData>();

        //    int breakPoint = 0;

        //    foreach (var item in rootLinks)
        //    {
        //        //if (breakPoint == 17) break;

        //        var getHtmlWeb = new HtmlWeb();

        //        var baseURL = System.Configuration.ConfigurationManager.AppSettings["ECONOMIC_DATA_SRC"].ToString();

        //        string url = baseURL + item;
        //        HtmlDocument document = getHtmlWeb.Load(item);



        //        string classToFind = "float_lang_base_1 js-portfolio-list";
        //        var eventHeadline = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));


        //        if (eventHeadline != null)
        //        {
        //            var economicData = new EconomicData();
        //            economicData.CurrentForecastValue = "null";
        //            economicData.CurrentActualValue = "null";
        //            economicData.CurrentPreviousValue = "null";

        //            economicData.EventHeadline = eventHeadline.FirstOrDefault().InnerHtml;


        //            var idToFind = "releaseInfo";
        //            var releaseDate = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));

        //            if (releaseDate.Count > 0)
        //            {
        //                var aTag = releaseDate.FirstOrDefault().ChildNodes[1];
        //                if (aTag.HasChildNodes)
        //                {
        //                    var classItem = "historyTab";
        //                    var tableDateTime = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classItem));
        //                    var aTagTemp = tableDateTime.FirstOrDefault();

        //                    var tempText = aTagTemp.ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[3].InnerHtml;

        //                    String dateTimeStr = aTag.LastChild.InnerHtml + " " + tempText;
        //                    DateTime dateTime = new DateTime();

        //                    if (DateTime.TryParse(dateTimeStr, out dateTime))
        //                    {
        //                        economicData.ReleaseDateTime = dateTime;

        //                        //economicData.ReleaseDateTime = Convert.ToDateTime(aTag.LastChild.InnerHtml);

        //                        var tempClass = "arial_14";
        //                        var actual = releaseDate.FirstOrDefault().SelectNodes(string.Format("//*[contains(@class,'{0}')]", tempClass));

        //                        if (actual.Where(f => f.Name == "div").Any())
        //                        {
        //                            //var valuesCount = actual.Where(f => f.Name == "div").Count();

        //                            //var listOfValues = actual.ToList();
        //                            //int index = -1;

        //                            if (actual.Where(f => f.Name == "div").Any())
        //                            {
        //                                var listOfValues = actual.ToList();
        //                                int index = -1;

        //                                if (ValueScenarioCheck(listOfValues, "Actual", out index))
        //                                {
        //                                    economicData.CurrentActualValue = actual[index].InnerText;
        //                                }

        //                                if (ValueScenarioCheck(listOfValues, "Forecast", out index))
        //                                {
        //                                    economicData.CurrentForecastValue = actual[index].InnerText;
        //                                }

        //                                if (ValueScenarioCheck(listOfValues, "Previous", out index))
        //                                {
        //                                    economicData.CurrentPreviousValue = actual[index].InnerText;
        //                                }
        //                            }
        //                            //if (valuesCount == 2)
        //                            //{
        //                            //    economicData.CurrentActualValue = actual[0].InnerText;
        //                            //    economicData.CurrentForecastValue = actual[1].InnerText;
        //                            //}

        //                            //if (valuesCount == 3)
        //                            //{
        //                            //    economicData.CurrentActualValue = actual[0].InnerHtml;
        //                            //    economicData.CurrentForecastValue = actual[1].InnerHtml;
        //                            //    economicData.CurrentPreviousValue = actual[2].InnerHtml;
        //                            //}
        //                            //if (valuesCount == 2)
        //                            //{
        //                            //    economicData.CurrentActualValue = actual[0].InnerHtml;
        //                            //    economicData.CurrentForecastValue = actual[1].InnerHtml;
        //                            //}
        //                        }
        //                    }
        //                }

        //                idToFind = "overViewBox";
        //                var description = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));
        //                aTag = description.FirstOrDefault();

        //                if (aTag.HasChildNodes)
        //                {
        //                    if (aTag.ChildNodes[3].HasChildNodes)
        //                    {
        //                        economicData.DescriptionOfEvent = aTag.ChildNodes[1].InnerHtml;

        //                        var country = aTag.ChildNodes[3].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
        //                        economicData.Country = country;

        //                        var source = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
        //                        economicData.EconomicSource = source;

        //                        var currency = aTag.ChildNodes[3].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
        //                        economicData.Currency = currency;

        //                        var sourceTitle = aTag.ChildNodes[3].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
        //                        economicData.EconomicSourceTitle = sourceTitle;


        //                        classToFind = "grayFullBullishIcon";
        //                        var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

        //                        switch (importanceLevel.Count)
        //                        {
        //                            case 1: { economicData.Importance = "Low"; } break;
        //                            case 2: { economicData.Importance = "Medium"; } break;
        //                            case 3: { economicData.Importance = "High"; } break;
        //                        }

        //                        economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
        //                        economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", "");;


        //                       // economicData.EventHeadline = CleanText(economicData.EventHeadline);
        //                        //economicData.DescriptionOfEvent = "Temporary text";

        //                    }
        //                    else 
        //                    {
        //                        economicData.DescriptionOfEvent = economicData.EventHeadline;

        //                         //aTag.ChildNodes[1]

        //                        var country = aTag.ChildNodes[1].ChildNodes[3].ChildNodes[3].FirstChild.Attributes["title"].Value;
        //                        economicData.Country = country;

        //                        var source = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["href"].Value;
        //                        economicData.EconomicSource = source;

        //                        var currency = aTag.ChildNodes[1].ChildNodes[5].ChildNodes[3].LastChild.InnerHtml;
        //                        economicData.Currency = currency;

        //                        var sourceTitle = aTag.ChildNodes[1].ChildNodes[7].ChildNodes[3].FirstChild.Attributes["title"].Value;
        //                        economicData.EconomicSourceTitle = sourceTitle;


        //                        classToFind = "grayFullBullishIcon";
        //                        var importanceLevel = document.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]", classToFind));

        //                        switch (importanceLevel.Count)
        //                        {
        //                            case 1: { economicData.Importance = "Low"; } break;
        //                            case 2: { economicData.Importance = "Medium"; } break;
        //                            case 3: { economicData.Importance = "High"; } break;
        //                        }

        //                        economicData.DescriptionOfEvent = economicData.DescriptionOfEvent.Replace("\n", "").Replace("\r", "").Replace("\"", "");
        //                        economicData.EventHeadline = economicData.EventHeadline.Replace("\n", "").Replace("\r", "").Replace("\"", ""); ;


        //                     }

        //                    listOfEconomicData.Add(economicData);
        //                }
        //            }
        //        }

        //        breakPoint++;
        //    }
        //    return listOfEconomicData;
        //}

        private string CleanText(string text, bool blanksSpace)
        {
            // return textback;
            string fooNoWhiteSpace = "";
            if (blanksSpace)
            {
                //remove commas
                fooNoWhiteSpace = string.Join(
                    Environment.NewLine,
                    text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                       .Select(fooline => fooline.Trim()).Select(fooline => fooline.Replace("\"", "").Replace(",", " ").Replace("&nbsp;", " "))
                );
            }
            else
            {
                fooNoWhiteSpace = string.Join(
                    Environment.NewLine,
                    text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                       .Select(fooline => fooline.Trim()).Select(fooline => fooline.Replace("\"", "").Replace(",", "").Replace("&nbsp;", ""))
                );
            }
            return fooNoWhiteSpace;
        }

        private List<EconomicData> ValidateCategorize(List<EconomicData> econData)
        {
            var specialMatchWords = _eventToCategorization.Where(n => n.Category == "none").ToList();

            for (int i = 0; i < econData.Count; i++)
            {
                var indexList = new List<int>();
                for (int j = 0; j < _eventToCategorization.Count; j++)
                {
                    string toBeMatched = econData[i].DescriptionOfEvent.ToLower();
                    string headlineToBeMatched = econData[i].EventHeadline.ToLower();

                    if (toBeMatched.Contains(_eventToCategorization[j].MatchWords) 
                        || headlineToBeMatched.Contains(_eventToCategorization[j].MatchWords))
                    {
                        indexList.Add(j);
                    }


                    if (String.IsNullOrEmpty(_eventToCategorization[j].KeyPersonnel) == false)
                    {
                        var nameSplit = _eventToCategorization[j].KeyPersonnel.Split(' ');

                        if (nameSplit.Count() > 1)
                        {
                            //when key personnel speaks
                            for (int y = 0; y < specialMatchWords.Count; y++)
                            {
                                var criteria = nameSplit[1] + " " + specialMatchWords[y].MatchWords;
                                if (toBeMatched.Contains(criteria) ||
                                    headlineToBeMatched.Contains(criteria))
                                {
                                    if (indexList.Any(n => n == j) == false)
                                    {
                                        indexList.Add(j);
                                    }
                                }
                            }
                        }
                    }
                }


                //Handles cases where too many personnel are associated with mulitple events
                foreach (var item in indexList)
                {
                    string toBeMatched = econData[i].DescriptionOfEvent.ToLower();
                    string headlineToBeMatched = econData[i].EventHeadline.ToLower();
                    var nameSplit = _eventToCategorization[item].KeyPersonnel.Split(' ');




                    //check if surname and speech combo works for all
                    if (toBeMatched.Contains(_eventToCategorization[item].KeyPersonnel) ||
                        (headlineToBeMatched.Contains(_eventToCategorization[item].KeyPersonnel)
                        ))
                    {
                        econData[i].CategoryType = string.IsNullOrEmpty(econData[i].CategoryType) ? _eventToCategorization[item].Category : econData[i].CategoryType;
                        econData[i].EventType = string.IsNullOrEmpty(econData[i].EventType) ? _eventToCategorization[item].Event : econData[i].EventType;

                        var namesList = _eventToCategorization[item].KeyPersonnel.Split(new String[] {" "}, StringSplitOptions.None);

                        econData[i].EventInfluentialPersonnelFirstName = namesList.FirstOrDefault();
                        econData[i].EventInfluentialPersonnelLastName = namesList.LastOrDefault();

                        econData[i].InstitutionBody = string.IsNullOrEmpty(econData[i].InstitutionBody) ? _eventToCategorization[item].InstitutionBody : econData[i].InstitutionBody; 
                    }
                    else if (String.IsNullOrEmpty(econData[i].CategoryType))
                    {
                        econData[i].CategoryType = string.IsNullOrEmpty(econData[i].CategoryType) ? _eventToCategorization[item].Category : econData[i].CategoryType;
                        econData[i].EventType = string.IsNullOrEmpty(econData[i].EventType) ? _eventToCategorization[item].Event : econData[i].EventType;
                        econData[i].InstitutionBody = string.IsNullOrEmpty(econData[i].InstitutionBody) ? _eventToCategorization[item].InstitutionBody : econData[i].InstitutionBody;
                    }


                    if (nameSplit.Count() > 1)
                    {
                        //when key personnel speaks
                        for (int y = 0; y < specialMatchWords.Count; y++)
                        {
                            var criteria = nameSplit[1] + " " + specialMatchWords[y].MatchWords;
                            if (toBeMatched.Contains(criteria) ||
                                headlineToBeMatched.Contains(criteria))
                            {
                                if (headlineToBeMatched.Contains(criteria))
                                {
                                    var obtainedCategory = _eventToCategorization.Where(b =>
                                        b.KeyPersonnel.Contains(nameSplit[1])).FirstOrDefault().Category;

                                    var institutionBody = _eventToCategorization.Where(b =>
                                        b.KeyPersonnel.Contains(nameSplit[1])).FirstOrDefault().InstitutionBody;

                                    econData[i].CategoryType = obtainedCategory;
                                    econData[i].EventType = specialMatchWords[y].Event;
                                    econData[i].InstitutionBody = institutionBody;

                                }
                                else
                                {
                                    econData[i].CategoryType = _eventToCategorization[item].Category;
                                    econData[i].EventType = _eventToCategorization[item].Event;
                                    econData[i].InstitutionBody = _eventToCategorization[item].InstitutionBody;
                                }

                                var namesList = _eventToCategorization[item].KeyPersonnel.Split(new String[] { " " }, StringSplitOptions.None);

                                econData[i].EventInfluentialPersonnelFirstName = namesList.FirstOrDefault();
                                econData[i].EventInfluentialPersonnelLastName = namesList.LastOrDefault();
                            }
                        }
                    }
                }
            }

            econData.ForEach(y => y.DescriptionOfEvent = CleanText(y.DescriptionOfEvent, true));

            var filtered = econData.Where(m => String.IsNullOrEmpty(m.CategoryType) == false).ToList();
            return RemoveUnrequiredData(filtered); 
        }

        private List<EconomicData> RemoveUnrequiredData(List<EconomicData> econData)
        {
            List<EconomicData> finalCollection = new List<EconomicData>();

            foreach (var ec in econData)
            {
                var collection = econData.Where(m => m.EventHeadline == ec.EventHeadline && m.ReleaseDateTime == ec.ReleaseDateTime).ToList();
                var scoreRecord = new Dictionary<int, int>();

                for (int i = 0; i < collection.Count(); i++)
                {
                    int score = 0;
                    if (String.IsNullOrEmpty(collection[i].CurrentActualValue) || collection[i].CurrentActualValue == "null")
                    {
                        score++;
                    }

                    if (String.IsNullOrEmpty(collection[i].CurrentForecastValue) || collection[i].CurrentForecastValue == "null")
                    {
                        score++;
                    }

                    if (String.IsNullOrEmpty(collection[i].CurrentPreviousValue) || collection[i].CurrentPreviousValue == "null")
                    {
                        score++;
                    }
                    scoreRecord.Add(i, score);
                }
                var finalEcon = scoreRecord.OrderBy(m => m.Value).FirstOrDefault().Key;

                if (finalCollection.Any(j => j.EventHeadline == collection[finalEcon].EventHeadline && j.ReleaseDateTime == collection[finalEcon].ReleaseDateTime) == false)
                {
                    finalCollection.Add(collection[finalEcon]);
                }
            }
            return finalCollection;
        }

        private bool ValueScenarioCheck(List<HtmlNode> listOfValues, string scenarioType, out int index)
        {
            index = -1;
            for (int i = 0; i < listOfValues.Count; i++)
            {
                if (listOfValues[i].ParentNode.OuterHtml.Contains(scenarioType))
                {
                    index = i;
                    return true;
                }
            }
            return false;
        }

        public String OutputControl(string filenameStandard)
        {
            String directoryOutput = "";
            String selector = "JobsManager";

            switch (MODE.Test)
            {
                case MODE.Test:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_TEST"];
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVE"];
                        selector += "_LIVE";
                    }
                    break;

                case MODE.LiveTest:
                    {
                        directoryOutput += "\\" + System.Configuration.ConfigurationManager.AppSettings["OUTPUTPATH_LIVETEST"];
                        selector += "_LIVE-TEST";
                    }
                    break;
            }

            String exchangeName = filenameStandard;

            directoryOutput += "\\" + exchangeName;
            if (Directory.Exists(directoryOutput) == false)
            {
                Directory.CreateDirectory(directoryOutput);
            }
            String dateStr = String.Format("{0}{1}{2}_{3}{4}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute);

            String tempFileName = exchangeName + "_" + dateStr;
            String fileName = tempFileName + ".csv";

            String fullPath = directoryOutput + "\\" + fileName;
            if (!System.IO.File.Exists(fullPath))
            {
                //crude approach
                using (System.IO.FileStream fs = System.IO.File.Create(fullPath)) { }

              //  RegisterFile(tempFileName, selector);
            }
            //else
            //{
            //    int rowsAffected = 0;
            //    //Double check the file is in the database
            //    SqlParameter outputIdParam = new SqlParameter("@Count", SqlDbType.Int)
            //    {
            //        Direction = ParameterDirection.Output
            //    };

            //    using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[selector].ToString()))
            //    {
            //        SqlCommand sqlCommand = new SqlCommand("proc_CheckRawDataFileRegisteration", con);
            //        con.Open();

            //        sqlCommand.CommandType = CommandType.StoredProcedure;
            //        sqlCommand.Parameters.AddWithValue("@FileName", tempFileName);
            //        sqlCommand.Parameters.Add(outputIdParam);

            //        sqlCommand.CommandTimeout = 0;
            //        rowsAffected = sqlCommand.ExecuteNonQuery();
            //    }

            //    if (Convert.ToInt32(outputIdParam.Value) == 0)
            //    {
            //        RegisterFile(tempFileName, selector);
            //    }

            //}
            return directoryOutput + "\\" + fileName;
        }

        private void WriteToCSVFile(List<EconomicData> economicDataList)
        {
            OutputEventDescriptionFile(economicDataList);

            OutputEventDataFile(economicDataList);

            OutputEventKeyPersonnelFile(economicDataList);
        }

        private void OutputEventDescriptionFile(List<EconomicData> economicDataList)
        {
            lock (locker)
            {
                var fileName = DataConsumer.OutputControlT("FundamentalData_EventDescriptionFile", 1);
                try
                {
                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        foreach (var rt in economicDataList) //result.Items
                        {
                            CsvRow csvRow = new CsvRow();

                            if (String.IsNullOrEmpty(rt.EventHeadline) == false)
                            {
                                csvRow.Add(rt.EventHeadline.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.CategoryType) == false) 
                            { 
                                csvRow.Add(rt.CategoryType.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.EventType) == false) 
                            {
                                csvRow.Add(rt.EventType.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }


                            var sqlFormattedDate = rt.ReleaseDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                            csvRow.Add(sqlFormattedDate);

                            if (String.IsNullOrEmpty(rt.InstitutionBody) == false)
                            {
                                csvRow.Add(rt.InstitutionBody.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }


                            if (String.IsNullOrEmpty(rt.Country) == false)
                            {
                                csvRow.Add(rt.Country.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.Currency) == false)
                            {
                                csvRow.Add(rt.Currency.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.DescriptionOfEvent) == false)
                            {
                                csvRow.Add(rt.DescriptionOfEvent.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.Importance) == false)
                            {
                                csvRow.Add(rt.Importance.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.JSONSrc) == false)
                            {
                                csvRow.Add(rt.JSONSrc.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            writer.WriteRow(csvRow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Logger.log.Error(ex);
                    Console.WriteLine(ex.ToString());
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                }
            }
        }

        private string NumericTypeCheck(string value)
        {
            if (String.IsNullOrEmpty(value) == false)
            {
                if (value.Contains("%")) return "Percentage";
            }            
            return "Number";
        }

        private string GetNumericType(string value)
        {
            if (String.IsNullOrEmpty(value) == false)
            {
                value = value.ToLower();

                var percentageArray = value.Split('%');
                if (percentageArray.Count() > 1)
                {
                    return "%";
                }

                var thousandsChecker = value.Split('k');
                if (thousandsChecker.Count() > 1)
                {
                    return "k";
                }

                var millionChecker = value.Split('m');
                if (millionChecker.Count() > 1)
                {
                    return "m";
                }

                var billionChecker = value.Split('b');
                if (billionChecker.Count() > 1)
                {
                    return "b";
                }
            }
            return "";
        }

        private string GetNumberOnly(string value)
        {
            if (String.IsNullOrEmpty(value) == false)
            {
                value = value.ToLower();
                double numericDoubleParameter = -1;

                var percentageArray = value.Split('%');

                if (percentageArray.Count() > 1)
                {
                    if (String.IsNullOrEmpty(percentageArray[1]))
                    {
                        if (Double.TryParse(percentageArray.FirstOrDefault(), out numericDoubleParameter))
                        {
                            return numericDoubleParameter.ToString();
                        }
                    }
                }

                var thousandsChecker = value.Split('k');

                if (thousandsChecker.Count() > 1)
                {
                    if (String.IsNullOrEmpty(thousandsChecker[1]))
                    {
                        if (Double.TryParse(thousandsChecker.FirstOrDefault(), out numericDoubleParameter))
                        {
                            return numericDoubleParameter.ToString();
                        }
                    }
                }

                var millionChecker = value.Split('m');
                if (millionChecker.Count() > 1)
                {
                    if (String.IsNullOrEmpty(millionChecker[1]))
                    {
                        if (Double.TryParse(millionChecker.FirstOrDefault(), out numericDoubleParameter))
                        {
                            return numericDoubleParameter.ToString();
                        }
                    }
                }

                var billionChecker = value.Split('b');
                if (billionChecker.Count() > 1)
                {
                    if (String.IsNullOrEmpty(billionChecker[1]))
                    {
                        if (Double.TryParse(billionChecker.FirstOrDefault(), out numericDoubleParameter))
                        {
                            return numericDoubleParameter.ToString();;
                        }
                    }
                }
            }
            return value;
        }

        private void OutputEventDataFile(List<EconomicData> economicDataList)
        {
            lock (locker)
            {
                var fileName = DataConsumer.OutputControlT("FundamentalData_EventDataFile", 2);
                try
                {
                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        foreach (var rt in economicDataList) //result.Items
                        {
                            //if ((rt.CurrentActualValue.Contains("null") == false && rt.CurrentForecastValue.Contains("null") == false 
                            //    && rt.CurrentPreviousValue.Contains("null") == false)
                            //    ||
                            //    rt.CurrentActualValue.Contains("null") == false && rt.CurrentForecastValue.Contains("null") == false)
                            {
                                CsvRow csvRow = new CsvRow();

                                if (String.IsNullOrEmpty(rt.EventHeadline) == false) csvRow.Add(rt.EventHeadline.ToString());

                                var sqlFormattedDate = rt.ReleaseDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                                csvRow.Add(sqlFormattedDate);

                                if (String.IsNullOrEmpty(rt.CurrentActualValue) == false)
                                {
                                    csvRow.Add(rt.CurrentActualValue.ToString());
                                }
                                else
                                {
                                    csvRow.Add("null");
                                }

                                if (String.IsNullOrEmpty(rt.CurrentForecastValue) == false)
                                {
                                    csvRow.Add(rt.CurrentForecastValue.ToString());
                                }
                                else
                                {
                                    csvRow.Add("null");
                                }

                                if (String.IsNullOrEmpty(rt.CurrentPreviousValue) == false)
                                {
                                    csvRow.Add(rt.CurrentPreviousValue.ToString());
                                }
                                else
                                {
                                    csvRow.Add("null");
                                }

                                if (String.IsNullOrEmpty(rt.CurrentPreviousValue) == false)
                                {
                                    csvRow.Add(rt.NumericType);
                                }
                                else
                                {
                                    csvRow.Add("null");
                                }
                                writer.WriteRow(csvRow);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        private void OutputEventKeyPersonnelFile(List<EconomicData> economicDataList)
        {
            lock (locker)
            {
                var fileName = DataConsumer.OutputControlT("FundamentalData_KeyPersonnelFile", 3);
                try
                {
                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        foreach (var rt in economicDataList) //result.Items
                        {
                            CsvRow csvRow = new CsvRow();

                            if (String.IsNullOrEmpty(rt.EventHeadline) == false) 
                            {
                                csvRow.Add(rt.EventHeadline.ToString());
                            }
                            else
                            {
                                csvRow.Add("null"); 
                            }

                            var sqlFormattedDate = rt.ReleaseDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                            csvRow.Add(sqlFormattedDate);

                            if (String.IsNullOrEmpty(rt.EventInfluentialPersonnelFirstName) == false) 
                            {
                                csvRow.Add(rt.EventInfluentialPersonnelFirstName.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rt.EventInfluentialPersonnelLastName) == false)
                            {
                                csvRow.Add(rt.EventInfluentialPersonnelLastName.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            writer.WriteRow(csvRow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Library.WriteErrorLog(DateTime.Now + " :: " + ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}


/*dateFrom=2013-01-16&dateTo=2015-12-16&timeZone=8&quotes_search_text=&country%5B%5D=29&country%5B%5D=25&country%5B%5D=54&country%5B%5D=145&country%5B%5D=34&country%5B%5D=174&country%5B%5D=163&country%5B%5D=32&country%5B%5D=70&country%5B%5D=6&country%5B%5D=27&country%5B%5D=37&country%5B%5D=122&country%5B%5D=15&country%5B%5D=113&country%5B%5D=107&country%5B%5D=55&country%5B%5D=24&country%5B%5D=121&country%5B%5D=59&country%5B%5D=89&country%5B%5D=72&country%5B%5D=71&country%5B%5D=22&country%5B%5D=17&country%5B%5D=51&country%5B%5D=39&country%5B%5D=93&country%5B%5D=106&country%5B%5D=14&country%5B%5D=48&country%5B%5D=33&country%5B%5D=23&country%5B%5D=10&country%5B%5D=35&country%5B%5D=92&country%5B%5D=57&country%5B%5D=94&country%5B%5D=97&country%5B%5D=68&country%5B%5D=96&country%5B%5D=103&country%5B%5D=111&country%5B%5D=42&country%5B%5D=109&country%5B%5D=188&country%5B%5D=7&country%5B%5D=105&country%5B%5D=172&country%5B%5D=21&country%5B%5D=43&country%5B%5D=20&country%5B%5D=60&country%5B%5D=87&country%5B%5D=44&country%5B%5D=193&country%5B%5D=125&country%5B%5D=45&country%5B%5D=53&country%5B%5D=38&country%5B%5D=170&country%5B%5D=100&country%5B%5D=56&country%5B%5D=80&country%5B%5D=52&country%5B%5D=36&country%5B%5D=90&country%5B%5D=112&country%5B%5D=110&country%5B%5D=11&country%5B%5D=26&country%5B%5D=162&country%5B%5D=9&country%5B%5D=12&country%5B%5D=46&country%5B%5D=85&country%5B%5D=41&country%5B%5D=202&country%5B%5D=63&country%5B%5D=123&country%5B%5D=61&country%5B%5D=143&country%5B%5D=4&country%5B%5D=5&country%5B%5D=138&country%5B%5D=178&country%5B%5D=84&country%5B%5D=75&timeFilter=timeRemain&category%5B%5D=_employment&category%5B%5D=_economicActivity&category%5B%5D=_inflation&category%5B%5D=_credit&category%5B%5D=_centralBanks&category%5B%5D=_confidenceIndex&category%5B%5D=_balance&category%5B%5D=_Bonds&importance%5B%5D=3*/