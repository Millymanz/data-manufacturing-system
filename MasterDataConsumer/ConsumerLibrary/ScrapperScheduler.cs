﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Timers;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.IO;
using System.Web;


// added for access to RegistryKey
using Microsoft.Win32;
// added for access to socket classes
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ConsumerLibrary
{
    public class ScrapperScheduler
    {
        private List<Scraper> _scrapperList = new List<Scraper>();
        private Dictionary<string, string> _timeTable = new Dictionary<string, string>();
        private DateTime _dateTime = DateTime.Now;

        public Dictionary<string, string> TimeTable
        {
            get { return _timeTable; }
            set { _timeTable = value; }
        }

        public void StartScheduling()
        {
            GetTodaysEconomicEvents();

            SetupSchedules();            
        }

        public void StartSchedulingManualTimeTable()
        {
            SetupSchedules();
        }

        private void SetupSchedules()
        {
            foreach (var item in _timeTable)
            {
                var scraper = new Scraper(item.Value, item.Key, _dateTime);

                var threadStart = new ThreadStart(scraper.ApplyScheduler);
                var thread = new Thread(threadStart);
                thread.Start();

                if (scraper.ScheduledSuccessful)
                {
                    _scrapperList.Add(scraper);
                }
            }
        }

        private void GetTodaysEconomicEvents()
        {
            //scrap it
            var link = System.Configuration.ConfigurationManager
                .AppSettings["ECONOMIC_DATA_TIMETABLE_SRC"].ToString();

            ScrapTimeTable();
        }

        private void ScrapTimeTable()
        {
            var getHtmlWeb = new HtmlWeb();

            var item = System.Configuration.ConfigurationManager
            .AppSettings["ECONOMIC_DATA_TIMETABLE_SRC"].ToString();

            string url = item;

            if (string.IsNullOrEmpty(item) == false)
            {
                HtmlDocument document = getHtmlWeb.Load(item);
                Console.WriteLine(item);

                var idToFind = "economicCalendarData";
                var table = document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", idToFind));

                var aTagTemp = table.FirstOrDefault();
                var tempText = aTagTemp.ChildNodes[5];

                var listingRow = tempText.ChildNodes.Where(m => m.Name == "tr");

                int iter = 0; 
                foreach (var tr in listingRow)
                {
                    var time = tr.ChildNodes[1].InnerText;
                    string headlineLink = "";

                    if (tr.ChildNodes.Count > 6)
                    {
                        if (tr.ChildNodes[7].ChildNodes[0].Attributes.Count > 0)
                        {
                            headlineLink = tr.ChildNodes[7].ChildNodes[0].Attributes[0].Value;

                            var link = System.Configuration.ConfigurationManager.AppSettings["ECONOMIC_DATA_SRC"].ToString();
                            link += headlineLink;

                            _timeTable.Add(link, time);
                        }
                    }
                    if (iter == 0)
                    {
                         DateTime.TryParse(time, out _dateTime);
                    }
                    iter++;
                }
            }
        }
    }
}
