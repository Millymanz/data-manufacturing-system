﻿using System;
using System.Collections.Generic;

namespace ConsumerLibrary.Api
{
    public interface ICryptoApi
    {
        ExchangeCurrentrate Exchange_rates_get_all_current_rates(string baseId);
        Exchangerate Exchange_rates_get_specific_rate(string baseId, string quoteId);
        Exchangerate Exchange_rates_get_specific_rate(string baseId, string quoteId, DateTime time);
        T GetData<T>(string url);
        List<Asset> Metadata_list_assets();
        List<Exchange> Metadata_list_exchanges();
        List<Symbol> Metadata_list_symbols();
        List<OHLCV> Ohlcv_historical_data(string symbolId, string periodId, DateTime start);
        List<OHLCV> Ohlcv_historical_data(string symbolId, string periodId, DateTime start, DateTime end);
        List<OHLCV> Ohlcv_historical_data(string symbolId, string periodId, DateTime start, DateTime end, int limit);
        List<OHLCV> Ohlcv_historical_data(string symbolId, string periodId, DateTime start, int limit);
        List<OHLCV> Ohlcv_latest_data(string symbolId, string periodId);
        List<OHLCV> Ohlcv_latest_data(string symbolId, string periodId, int limit);
        List<Period> Ohlcv_list_all_periods();
        List<Orderbook> Orderbooks_current_data_all();
        Orderbook Orderbooks_current_data_symbol(string symbolId);
        List<Orderbook> Orderbooks_historical_data(string symbolId, DateTime start);
        List<Orderbook> Orderbooks_historical_data(string symbolId, DateTime start, DateTime end);
        List<Orderbook> Orderbooks_historical_data(string symbolId, DateTime start, DateTime end, int limit);
        List<Orderbook> Orderbooks_historical_data(string symbolId, DateTime start, int limit);
        List<Orderbook> Orderbooks_last_data(string symbolId);
        List<Orderbook> Orderbooks_last_data(string symbolId, int limit);
        List<Quote> Quotes_current_data_all();
        Quote Quotes_current_data_symbol(string symbolId);
        List<Quote> Quotes_historical_data(string symbolId, DateTime start);
        List<Quote> Quotes_historical_data(string symbolId, DateTime start, DateTime end);
        List<Quote> Quotes_historical_data(string symbolId, DateTime start, DateTime end, int limit);
        List<Quote> Quotes_historical_data(string symbolId, DateTime start, int limit);
        List<Quote> Quotes_latest_data_all();
        List<Quote> Quotes_latest_data_all(int limit);
        List<Quote> Quotes_latest_data_symbol(string symbolId);
        List<Quote> Quotes_latest_data_symbol(string symbolId, int limit);
        List<Trade> Trades_historical_data(string symbolId, DateTime start);
        List<Trade> Trades_historical_data(string symbolId, DateTime start, DateTime end);
        List<Trade> Trades_historical_data(string symbolId, DateTime start, DateTime end, int limit);
        List<Trade> Trades_historical_data(string symbolId, DateTime start, int limit);
        List<Trade> Trades_latest_data_all();
        List<Trade> Trades_latest_data_all(int limit);
        List<Trade> Trades_latest_data_symbol(string symbolId);
        List<Trade> Trades_latest_data_symbol(string symbolId, int limit);
        List<Twitter> Twitter_historical_data(DateTime start);
        List<Twitter> Twitter_historical_data(DateTime start, DateTime end);
        List<Twitter> Twitter_historical_data(DateTime start, DateTime end, int limit);
        List<Twitter> Twitter_historical_data(DateTime start, int limit);
        List<Twitter> Twitter_last_data();
        List<Twitter> Twitter_last_data(int limit);
    }
}