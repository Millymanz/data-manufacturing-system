﻿using System;
using System.Collections.Concurrent;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ConsumerLibrary.Api;
using ConsumerLibrary.DataAccess;
using ConsumerLibrary.DataModel;
using CsvHelper;
using CsvHelper.Configuration;
using log4net;

namespace ConsumerLibrary.Import
{
    public class CryptoImportProcess : DefaultImportProcess
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(Logger));
        private readonly IConfigHelper configHelper;
        private readonly ICryptoApi cryptoApi;
        private IInsightCryptoStoredProcRepository insightCryptoStoredProcRepository;
        private int requestCount = 0;
        private int? limit = null;
        public CryptoImportProcess(IConfigHelper configHelper, ICryptoApi cryptoApi, IInsightCryptoStoredProcRepository insightCryptoStoredProcRepository)
        {
            this.configHelper = configHelper;
            this.cryptoApi = cryptoApi;
            this.insightCryptoStoredProcRepository = insightCryptoStoredProcRepository;
        }

        public override void DoAction()
        {
            log.InfoFormat("Importing the data");
            var lstStagingTrade = new ConcurrentBag<StagingTradeSourceDto>();
            var timeSpan = "1DAY";
            var symbols = cryptoApi.Metadata_list_symbols();
            var interestedSymbolList = insightCryptoStoredProcRepository.GetSymbolMappingDto().Select(x=>x.FriendlySymbol);
            var importlimit = insightCryptoStoredProcRepository.GetImportStatusDto(null, "1DAY", null);
            //We are assuming that if import has run once it has already imported the data and we can get away with already imported data.
            //Having it 20 for safety but can be decreased
            if (importlimit.Count>0)
            {
                limit = configHelper.BackFillCryptoLimitDays;
            }     
           
           
            var symbolstoevaluate = symbols.Where(x => interestedSymbolList.Contains(x.symbol_id));
            Parallel.ForEach(symbols.Where(x=> interestedSymbolList.Contains(x.symbol_id)), (symbol,loopstate) =>
                {
                    //Interlocked.Increment(ref requestCount);
                    //if (requestCount > 5)
                    //{
                    //    loopstate.Break();
                    //}

                    Parallel.ForEach(cryptoApi.Ohlcv_latest_data(symbol.symbol_id, timeSpan), ohlv =>
                    {
                        try
                        {
                            lstStagingTrade.Add(
                            new StagingTradeSourceDto(ohlv.time_close, ohlv.price_open, ohlv.price_high,
                            ohlv.price_low, ohlv.price_close, ohlv.volume_traded, 0, symbol.symbol_id, "1DAY", symbol.exchange_id));
                        }
                        catch(Exception ex)
                        {
                            log.ErrorFormat("Failed importing the data for exchange {0} symbol {1}", symbol.exchange_id, symbol.symbol_id);
                            log.Error(ex.Message + ex.StackTrace);
                        }
                        
                    });
                    log.InfoFormat("Imported the data for exhcnage {0} symbol {1}", symbol.exchange_id, symbol.symbol_id);
                });               
            
            insightCryptoStoredProcRepository.InsertStagingTrade(lstStagingTrade);
            //We can get away with this proc call by directly inserting in main trade table 
            insightCryptoStoredProcRepository.BulkInsertTrade();

            for (int port = 11000; port < 11006; port++)
            {
                StartProcessingCycle(port);

                System.Threading.Thread.Sleep(120000);
            }

        }

        public override bool QueryCheck()
        {
            //Check the DB if there is any import done for today
            //Do we need to use the exchange and timeframe here or just the importdate ?
            var importStatus = insightCryptoStoredProcRepository.GetImportStatusDto(null, "1DAY", DateTime.Now);
            if(importStatus.Count==0)
            {
                return true;
            }
            return false;
        }

        protected override bool ShouldBeRunning()
        {
            return true;
        }
    }
}