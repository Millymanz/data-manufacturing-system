﻿namespace ConsumerLibrary.Import
{
    public interface IImportProcess
    {
        void Process();
        void RunAsThread();
        int GetSleepPeriod();

        void DoAction();
        bool QueryCheck();
    }
}