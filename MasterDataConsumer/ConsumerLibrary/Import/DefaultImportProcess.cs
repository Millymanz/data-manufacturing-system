﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ConsumerLibrary.Import
{
    public abstract class DefaultImportProcess:IImportProcess
    {
        public void Process()
        {
            while (ShouldBeRunning())
            {
                var result = QueryCheck();
                if (result)
                {
                    DoAction();
                }
                Thread.Sleep(GetSleepPeriod());
            }
        }

        public void RunAsThread()
        {
            Library.WriteErrorLog(DateTime.Now + " :: Run as Thread");
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(DateTime.Now + ex.Message);
            }
        }

        public virtual int GetSleepPeriod()
        {
            return 60*60*10;
        }

        public abstract void DoAction();


        public abstract bool QueryCheck();

        protected abstract bool ShouldBeRunning();
        public void StartProcessingCycle(int GroupPort)
        {

            UdpClient udp = new UdpClient();

            IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, GroupPort);
            String dateID = String.Format("{0}{1}{2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            string str4 = "bUpdateDBServicesANDStartProcessingCycle_" + dateID;

            byte[] sendBytes4 = Encoding.ASCII.GetBytes(str4);
            udp.Send(sendBytes4, sendBytes4.Length, groupEP);
        }
    }
}