﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace ConsumerLibrary
{
    public class PeriodDataGrouping
    {
        public string Type { set; get; }
        public DateTime PeriodDateTimeRepresentative { set; get; }
        public List<DateTime> ArtificalDateTimes { set; get; }
        public List<DataFeedService.TradeSummary> RealTradeValues { set; get; }
    }

    public class PeriodGenerator
    {
        private List<PeriodDataGrouping> periodDataGrouping = new List<PeriodDataGrouping>();
        MODE Mode;
        KeyValuePair<DateTime, DateTime> _kvp = new KeyValuePair<DateTime,DateTime>();

        public PeriodGenerator(MODE mode, string periodType)
        {
            Mode = mode;
            _kvp = GetDateTimeMaxMin();
        }

        public List<DataFeedService.TradeSummary> CreateDataPoints(List<PeriodDataGrouping> availablePeriods)
        {
            Logger.log.Info("WeeklyAndMonthly " + availablePeriods.Count);
            Console.WriteLine("WeeklyAndMonthly " + availablePeriods.Count);


            List<DataFeedService.TradeSummary> tradeList = new List<DataFeedService.TradeSummary>();

            foreach (var item in availablePeriods)
            {
                var tradeSum = new DataFeedService.TradeSummary();

                var trades = item.RealTradeValues.OrderBy(m => m.DateTime).ToList().ToList();
                tradeSum.Open = trades.FirstOrDefault().Open;
                tradeSum.Close = trades.LastOrDefault().Close;
                tradeSum.AdjustmentClose = trades.LastOrDefault().Close;
                tradeSum.High = trades.OrderBy(u => u.High).FirstOrDefault().High;
                tradeSum.Low = trades.OrderByDescending(u => u.Low).FirstOrDefault().Low;
                tradeSum.SymbolID = trades.FirstOrDefault().SymbolID;
                tradeSum.TimeFrame = item.Type;
                tradeSum.Volume = trades.FirstOrDefault().Volume;

                tradeSum.DateTime = item.PeriodDateTimeRepresentative;
                tradeList.Add(tradeSum);
            }
            return tradeList;
        }

        public List<DataFeedService.TradeSummary> GeneratePeriodData(string symbol, string timeframe, string periodType)
        {
            List<PeriodDataGrouping> availablePeriods = new List<PeriodDataGrouping>();
            GenerateArtificalPeriods(periodType);

            if (periodDataGrouping.Any())
            {
                var symbolDataList = GetAllSymbolData(symbol, timeframe);

                if (symbolDataList.Any())
                {
                    foreach (var pItem in periodDataGrouping)
                    {
                        var foundTradeItems = symbolDataList.Where(m => pItem.ArtificalDateTimes.Any(h => h == m.DateTime));

                        if (foundTradeItems.Any())
                        {
                            var dates = foundTradeItems.ToList().ToList().Select(d => d.DateTime);
                            var realTrades = foundTradeItems.ToList().ToList();
                            bool contained = !pItem.ArtificalDateTimes.Except(dates).Any();
                            
                            if (contained) {
                                pItem.RealTradeValues = realTrades;
                                availablePeriods.Add(pItem); 
                            }
                        }
                    }
                    //identify real trade values with 7 days add to 
                    //perioddatagrouping

                    //only return the ones with full 7 days

                    //now check the database if the proposed weekly or monthly
                    //exist in the db 
                }

                timeframe = periodType;
                var dateTimeList = CheckPeriodDataAgainstDBVersion(symbol, timeframe);
                //var dontExist = dateTimeList.Where(m => availablePeriods.Any(o => o.PeriodDateTimeRepresentative != m));

                var dontExist = availablePeriods.Where(m => dateTimeList.Any(o => o != m.PeriodDateTimeRepresentative));
                //var dontExist = availablePeriods.Where(m => dateTimeList.Any(o => o.DateTime != m.PeriodDateTimeRepresentative));

                if (dontExist.Any())
                {
                    availablePeriods = dontExist.ToList();
                }
            }
            return CreateDataPoints(availablePeriods);           
        }

        private List<DateTime> CheckPeriodDataAgainstDBVersion(string symbolID, string timeFrame)
        {
            //Get DB equvaluent
            var currentDateTimeList = new List<DateTime>();

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand command = new SqlCommand("SELECT [DateTime] FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + symbolID +
                    "' AND DD.[TimeFrame] = '" + timeFrame + "' ORDER BY [DateTime] ASC", con);

                    command.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            currentDateTimeList.Add(Convert.ToDateTime(dataReader["DateTime"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return currentDateTimeList;
        }


        //private List<DataFeedService.TradeSummary> CheckPeriodDataAgainstDBVersion(string symbolID, string timeFrame)
        //{
        //    //Get DB equvaluent
        //    var currentDateTimeList = new List<DataFeedService.TradeSummary>();

        //    var exchangeInUse = "Forex";

        //    String selector = "TRADES";
        //    switch (Mode)
        //    {
        //        case MODE.Test:
        //            {
        //                selector += "_TEST";
        //            }
        //            break;

        //        case MODE.Live:
        //            {
        //                selector += "_LIVE";
        //            }
        //            break;

        //    }
        //    String stockExchange = exchangeInUse;

        //    if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
        //    {
        //        selector += "_" + exchangeInUse;
        //    }

        //    var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

        //    try
        //    {
        //        using (SqlConnection con = new SqlConnection(settings.ToString()))
        //        {
        //            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + symbolID +
        //            "' AND DD.[TimeFrame] = '" + timeFrame + "' ORDER BY [DateTime] ASC", con);

        //            command.CommandTimeout = 0;

        //            con.Open();

        //            SqlDataReader dataReader = command.ExecuteReader();

        //            if (dataReader.HasRows)
        //            {
        //                while (dataReader.Read())
        //                {
        //                    DataFeedService.TradeSummary result = new DataFeedService.TradeSummary()
        //                    {
        //                        DateTime = Convert.ToDateTime(dataReader["DateTime"]),
        //                        Open = Convert.ToDouble(dataReader["Open"]),
        //                        Close = Convert.ToDouble(dataReader["Close"]),
        //                        Low = Convert.ToDouble(dataReader["Low"]),
        //                        High = Convert.ToDouble(dataReader["High"]),
        //                        AdjustmentClose =  Convert.ToDouble(dataReader["AdjustmentClose"])
        //                    };
        //                    currentDateTimeList.Add(result);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //    }
        //    return currentDateTimeList;
        //}


        private List<DateTime> CheckPeriodDataAgainstDBVersionOld(string symbolID, string timeFrame)
        {
            //Get DB equvaluent
            var currentDateTimeList = new List<DateTime>();

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand command = new SqlCommand("SELECT [DateTime] FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + symbolID +
                    "' AND DD.[TimeFrame] = '" + timeFrame + "' ORDER BY [DateTime] ASC", con);

                    command.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            currentDateTimeList.Add(Convert.ToDateTime(dataReader["DateTime"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return currentDateTimeList;
        }

        private void GenerateArtificalPeriods(string periodType)
        {
            if (periodType == "Weekly")
            {
                Logger.log.Info("GenerateArtificalPeriods Weekly");
                Console.WriteLine("GenerateArtificalPeriods Weekly");

                var weeklyTemp = System.Configuration.ConfigurationManager.AppSettings["WeeklyDefNo"];
                var weeklyDefNo = Convert.ToInt32(weeklyTemp);

                //create weeks
                var startDateTime = _kvp.Key;
                bool mondayNotFound = true;

                if (startDateTime.DayOfWeek != DayOfWeek.Monday)
                {
                    while (mondayNotFound)
                    {
                        startDateTime = startDateTime.AddDays(-1);
                        if (startDateTime.DayOfWeek == DayOfWeek.Monday)
                        {
                            mondayNotFound = false;
                        }
                    }
                }
                startDateTime = new DateTime(startDateTime.Year, startDateTime.Month, startDateTime.Day);

                bool keepGeneratingPeriod = true;
                while (keepGeneratingPeriod)
                {
                    startDateTime = startDateTime.AddDays(weeklyDefNo);

                    var periodDataGroup = new PeriodDataGrouping();
                    periodDataGroup.Type = periodType;

                    periodDataGroup.PeriodDateTimeRepresentative = startDateTime;

                    List<DateTime> dateTimeLising = new List<DateTime>();
                    for (int i = 0; i < weeklyDefNo; i++)
                    {
                        dateTimeLising.Add(startDateTime.AddDays(i));
                    }
                    periodDataGroup.ArtificalDateTimes = dateTimeLising;

                    periodDataGrouping.Add(periodDataGroup);

                    if (startDateTime > _kvp.Value) keepGeneratingPeriod = false;
                }
            }
            else if (periodType == "Monthly")
            {
                Logger.log.Info("GenerateArtificalPeriods Monthly");
                Console.WriteLine("GenerateArtificalPeriods Monthly");

                var startDateTime = _kvp.Key;

                startDateTime = new DateTime(startDateTime.Year, startDateTime.Month, 1, 0, 0, 0);

                bool keepGeneratingPeriod = true;

                while (keepGeneratingPeriod)
                {
                    var periodDataGroup = new PeriodDataGrouping();
                    periodDataGroup.Type = periodType;

                    periodDataGroup.PeriodDateTimeRepresentative = startDateTime;

                    int expectedWeekCount = SundaysInMonth(startDateTime);

                    var sundayNotFound = true;
                    if (startDateTime.DayOfWeek != DayOfWeek.Sunday)
                    {
                        while (sundayNotFound)
                        {
                            startDateTime = startDateTime.AddDays(1);
                            if (startDateTime.DayOfWeek == DayOfWeek.Sunday)
                            {
                                sundayNotFound = false;
                            }
                        }
                    }


                    var currentDateTime = startDateTime;

                    List<DateTime> dateTimeLising = new List<DateTime>();
                    for (int i = 0; i < expectedWeekCount; i++)
                    {
                        if (currentDateTime.Month == startDateTime.Month)
                        {
                            dateTimeLising.Add(currentDateTime);
                            currentDateTime = currentDateTime.AddDays(7);
                        }
                    }
                    periodDataGroup.ArtificalDateTimes = dateTimeLising;

                    periodDataGrouping.Add(periodDataGroup);

                    if (startDateTime > _kvp.Value) keepGeneratingPeriod = false;
                    
                    startDateTime = startDateTime.AddMonths(1);
                    startDateTime = new DateTime(startDateTime.Year, startDateTime.Month, 1, 0, 0, 0);
                }
            }



        }

        public static int SundaysInMonth(DateTime thisMonth)
        {
            int mondays = 0;
            int month = thisMonth.Month;
            int year = thisMonth.Year;
            int daysThisMonth = DateTime.DaysInMonth(year, month);
            DateTime beginingOfThisMonth = new DateTime(year, month, 1);
            for (int i = 0; i < daysThisMonth; i++)
                if (beginingOfThisMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                    mondays++;
            return mondays;
        }

        private KeyValuePair<DateTime, DateTime> GetDateTimeMaxMin()
        {
            Console.WriteLine("Getting DateTimeMaxMin For Weekly and Monthly...");

            KeyValuePair<DateTime, DateTime> kvp = new KeyValuePair<DateTime, DateTime>();
            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;

            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand command = null;

                    command = new SqlCommand("proc_GetDateMaxMin", con);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            kvp = new KeyValuePair<DateTime, DateTime>(Convert.ToDateTime(dataReader["Earliest"]),
                                Convert.ToDateTime(dataReader["Latest"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine("DateTimeMaxMin For Weekly and Monthly Obtained");

            return kvp;
        }


        private List<DataFeedService.TradeSummary> GetAllSymbolData(string symbol, string timeFrame)
        {
            var tradeListing = new List<DataFeedService.TradeSummary>();

            var exchangeInUse = "Forex";

            String selector = "TRADES";
            switch (Mode)
            {
                case MODE.Test:
                    {
                        selector += "_TEST";
                    }
                    break;

                case MODE.Live:
                    {
                        selector += "_LIVE";
                    }
                    break;
            }
            String stockExchange = exchangeInUse;

            if (exchangeInUse == "Forex" || exchangeInUse == "FOREX")
            {
                selector += "_" + exchangeInUse;
            }

            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];

            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[tblTrades] DD WHERE DD.[SymbolID] = '" + symbol +
                    "' AND DD.[TimeFrame] = '" + timeFrame + "' ORDER BY [DateTime] ASC", con);

                    command.CommandTimeout = 0;

                    con.Open();

                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var tradeSum = new DataFeedService.TradeSummary()
                            {
                                SymbolID = dataReader["SymbolID"].ToString(),
                                DateTime = Convert.ToDateTime(dataReader["DateTime"]),
                                TimeFrame = dataReader["TimeFrame"].ToString(),
                                AdjustmentClose = Convert.ToDouble(dataReader["Close"]),
                                Close = Convert.ToDouble(dataReader["Close"]),
                                High = Convert.ToDouble(dataReader["High"]),
                                Low = Convert.ToDouble(dataReader["Low"]),
                                Open = Convert.ToDouble(dataReader["Open"])
                            };
                            tradeListing.Add(tradeSum);
                            //currentDateTimeList.Add(Convert.ToDateTime(dataReader["DateTime"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return tradeListing;
        }
    }
}
