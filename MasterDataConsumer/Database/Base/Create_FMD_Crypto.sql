USE [master]
GO
/****** Object:  Database [FMD_Crypto]    Script Date: 2/19/2018 8:39:23 PM ******/
CREATE DATABASE [FMD_Crypto]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FMD_Crypto', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQL2012\MSSQL\DATA\FMD_Crypto.mdf' , SIZE = 2921472KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FMD_Crypto_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQL2012\MSSQL\DATA\FMD_Crypto_log.ldf' , SIZE = 4632576KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [FMD_Crypto] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FMD_Crypto].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FMD_Crypto] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FMD_Crypto] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FMD_Crypto] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FMD_Crypto] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FMD_Crypto] SET ARITHABORT OFF 
GO
ALTER DATABASE [FMD_Crypto] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FMD_Crypto] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FMD_Crypto] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FMD_Crypto] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FMD_Crypto] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FMD_Crypto] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FMD_Crypto] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FMD_Crypto] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FMD_Crypto] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FMD_Crypto] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FMD_Crypto] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FMD_Crypto] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FMD_Crypto] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FMD_Crypto] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FMD_Crypto] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FMD_Crypto] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FMD_Crypto] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FMD_Crypto] SET RECOVERY FULL 
GO
ALTER DATABASE [FMD_Crypto] SET  MULTI_USER 
GO
ALTER DATABASE [FMD_Crypto] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FMD_Crypto] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FMD_Crypto] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FMD_Crypto] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FMD_Crypto', N'ON'
GO
USE [FMD_Crypto]
GO
/****** Object:  User [sukrit]    Script Date: 2/19/2018 8:39:24 PM ******/
CREATE USER [sukrit] FOR LOGIN [sukrit] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [lexi]    Script Date: 2/19/2018 8:39:24 PM ******/
CREATE USER [lexi] FOR LOGIN [lexi] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [areeha]    Script Date: 2/19/2018 8:39:24 PM ******/
CREATE USER [areeha] FOR LOGIN [areeha] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [sukrit]
GO
ALTER ROLE [db_datareader] ADD MEMBER [lexi]
GO
ALTER ROLE [db_datareader] ADD MEMBER [areeha]
GO
/****** Object:  Table [dbo].[StageNotInMainTradesTable]    Script Date: 2/19/2018 8:39:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StageNotInMainTradesTable](
	[DateTime] [smalldatetime] NOT NULL,
	[Open] [decimal](18, 7) NULL,
	[High] [decimal](18, 7) NULL,
	[Low] [decimal](18, 7) NULL,
	[Close] [decimal](18, 7) NULL,
	[Volume] [int] NULL,
	[AdjustmentClose] [decimal](18, 7) NULL,
	[SymbolID] [nvarchar](20) NOT NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
	[StockExchange] [nvarchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDataFillCheck]    Script Date: 2/19/2018 8:39:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDataFillCheck](
	[ID] [nvarchar](100) NOT NULL,
	[SymbolID] [nvarchar](20) NOT NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[Checked] [bit] NULL,
	[Found] [bit] NULL,
 CONSTRAINT [PK_tblDataFillCheck] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLatestTradeDateTimes]    Script Date: 2/19/2018 8:39:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLatestTradeDateTimes](
	[ID] [nvarchar](100) NOT NULL,
	[SymbolID] [nvarchar](20) NOT NULL,
	[MaxDateTime] [smalldatetime] NULL,
	[PreviousDataPointCount] [int] NULL,
	[CurrentDataPointCount] [int] NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_tblLatestTradeDateTimes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSTAGING_DataFillCheck]    Script Date: 2/19/2018 8:39:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSTAGING_DataFillCheck](
	[SymbolID] [nvarchar](20) NOT NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[Checked] [bit] NULL,
	[Found] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSTAGING_Trades]    Script Date: 2/19/2018 8:39:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSTAGING_Trades](
	[DateTime] [smalldatetime] NOT NULL,
	[Open] [decimal](18, 7) NULL,
	[High] [decimal](18, 7) NULL,
	[Low] [decimal](18, 7) NULL,
	[Close] [decimal](18, 7) NULL,
	[Volume] [int] NULL,
	[AdjustmentClose] [decimal](18, 7) NULL,
	[SymbolID] [nvarchar](20) NOT NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
	[StockExchange] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_tblSTAGING_Trades] PRIMARY KEY CLUSTERED 
(
	[DateTime] ASC,
	[SymbolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTrades]    Script Date: 2/19/2018 8:39:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTrades](
	[ID] [nvarchar](100) NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[Open] [decimal](18, 7) NULL,
	[High] [decimal](18, 7) NULL,
	[Low] [decimal](18, 7) NULL,
	[Close] [decimal](18, 7) NULL,
	[Volume] [int] NULL,
	[AdjustmentClose] [decimal](18, 7) NULL,
	[SymbolID] [nvarchar](20) NOT NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
	[StockExchange] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_tblTrades] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Covering_DateTime]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_DateTime] ON [dbo].[tblTrades]
(
	[DateTime] ASC
)
INCLUDE ( 	[SymbolID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Covering_MaxDateTimeCheck]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_MaxDateTimeCheck] ON [dbo].[tblTrades]
(
	[TimeFrame] ASC
)
INCLUDE ( 	[SymbolID],
	[DateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Covering_SymbolID]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_SymbolID] ON [dbo].[tblTrades]
(
	[SymbolID] ASC
)
INCLUDE ( 	[Open],
	[High],
	[Low],
	[Close],
	[Volume],
	[AdjustmentClose],
	[TimeFrame],
	[DateTime],
	[StockExchange]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Covering_SymbolID_TimeFrame]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_SymbolID_TimeFrame] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[TimeFrame] ASC
)
INCLUDE ( 	[Open],
	[High],
	[Low],
	[Close],
	[Volume],
	[AdjustmentClose],
	[DateTime],
	[StockExchange]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Covering_TimeFrame]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_TimeFrame] ON [dbo].[tblTrades]
(
	[TimeFrame] ASC
)
INCLUDE ( 	[SymbolID],
	[Open],
	[High],
	[Low],
	[Close],
	[Volume],
	[AdjustmentClose],
	[DateTime],
	[StockExchange]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_10min]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_10min] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='10min')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_15min]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_15min] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='15min')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_1hour]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_1hour] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='1hour')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_1min]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_1min] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='1min')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_2hour]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_2hour] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='2hour')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_2min]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_2min] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='2min')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_30min]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_30min] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='30min')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_4hour]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_4hour] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='4hour')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_5min]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_5min] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='5min')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_EndOfDay]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_EndOfDay] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='EndOfDay')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_Monthly]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_Monthly] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='Monthly')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_tblTrades_Filtered_TimeFrame_Weekly]    Script Date: 2/19/2018 8:39:26 PM ******/
CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_Weekly] ON [dbo].[tblTrades]
(
	[SymbolID] ASC,
	[DateTime] ASC
)
WHERE ([TimeFrame]='Weekly')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[proc_AdminCheckForDuplicates]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[proc_AdminCheckForDuplicates]
AS

BEGIN
	SELECT SymbolID, TimeFrame, [DateTime], COUNT(SymbolID) FROM [dbo].tblTrades
	GROUP BY SymbolID, TimeFrame, [DateTime]
	HAVING COUNT(SymbolID) > 1
END






GO
/****** Object:  StoredProcedure [dbo].[proc_BulkInsertTrades]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








/*************************************************************************/

CREATE PROCEDURE [dbo].[proc_BulkInsertTrades]
@PATH [nvarchar](MAX),
@ROWSAFFECTED int output
AS

--DELETE FROM [dbo].tblSTAGING_Trades
--DELETE FROM [dbo].tblLatestTradeDateTimes

TRUNCATE TABLE [dbo].tblSTAGING_Trades
TRUNCATE TABLE [dbo].tblLatestTradeDateTimes

IF OBJECT_ID('StageNotInMainTradesTable') IS NOT NULL
BEGIN
	DROP TABLE StageNotInMainTradesTable
END

IF OBJECT_ID('StageTemp') IS NOT NULL
BEGIN
	DROP TABLE StageTemp
END



--DECLARE @DATACOUNT INT
--SET @DATACOUNT = 0
--SELECT @DATACOUNT = COUNT(dbo.tblTrades.SymbolID) FROM dbo.tblTrades WHERE dbo.tblTrades.TimeFrame IN ('1min', '2min', '5min', '10min', '15min', '30min', '1hour', '2hour', '4hour', 'EndOfDay', 'Weekly', 'Monthly') 
--GROUP BY dbo.tblTrades.SymbolID, dbo.tblTrades.TimeFrame

--IF @DATACOUNT > 0
--	BEGIN
--		INSERT INTO dbo.tblLatestTradeDateTimes (ID, SymbolID, MaxDateTime, PreviousDataPointCount, CurrentDataPointCount, TimeFrame)
--		SELECT NewID() AS ID, dbo.tblTrades.SymbolID, MAX(dbo.tblTrades.DateTime), COUNT(dbo.tblTrades.DateTime), 0, dbo.tblTrades.TimeFrame 
--		AS MaxDate FROM dbo.tblTrades WHERE dbo.tblTrades.TimeFrame IN ('1min', '2min', '5min', '10min', '15min', '30min', '1hour', '2hour', '4hour', 'EndOfDay', 'Weekly', 'Monthly') 
--		GROUP BY dbo.tblTrades.SymbolID, dbo.tblTrades.TimeFrame
--	END


--BULK
--INSERT [dbo].tblSTAGING_Trades
--FROM 'C:\Users\Public\Downloads\Output\Test\RawData\Staging\Temp.csv'
--WITH
--(
--FIELDTERMINATOR = ',',
--ROWTERMINATOR = '\n', --Hex equivalent of \n   0x0a
--MAXERRORS = 99999999
--)


PRINT 'Bulk Import'
DECLARE @q nvarchar(MAX);
SET @q=
    'BULK INSERT [dbo].tblSTAGING_Trades
    FROM '+char(39)+ @PATH +char(39)+'
    WITH
    (
    FIELDTERMINATOR = '','',
    ROWTERMINATOR = ''\n'', 
    MAXERRORS = 99999999  
    )'
EXEC(@q)


DECLARE @GUID [nvarchar](MAX)

--SELECT @ROWSAFFECTED = COUNT(*) FROM [dbo].tblSTAGING_Trades


/****** Object:  Index [ix_tblTrades]    Script Date: 08/02/2014 09:01:26 ******/
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Covering_SymbolID') 
    DROP INDEX [ix_tblTrades_Covering_SymbolID] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Covering_SymbolID_TimeFrame') 
    DROP INDEX [ix_tblTrades_Covering_SymbolID_TimeFrame] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Covering_TimeFrame') 
    DROP INDEX [ix_tblTrades_Covering_TimeFrame] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Covering_MaxDateTimeCheck') 
    DROP INDEX [ix_tblTrades_Covering_MaxDateTimeCheck] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Covering_DateTime') 
    DROP INDEX [ix_tblTrades_Covering_DateTime] ON [dbo].[tblTrades]; 

------------------------ Filtered ----------------------------
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_1min') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_1min] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_2min') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_2min] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_5min') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_5min] ON [dbo].[tblTrades]; 
	
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_10min') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_10min] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_15min') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_15min] ON [dbo].[tblTrades]; 
	
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_30min') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_30min] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_1hour') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_1hour] ON [dbo].[tblTrades]; 
	
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_2hour') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_2hour] ON [dbo].[tblTrades]; 
		
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_4hour') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_4hour] ON [dbo].[tblTrades]; 

IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_EndOfDay') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_EndOfDay] ON [dbo].[tblTrades]; 
			
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_Weekly') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_Weekly] ON [dbo].[tblTrades]; 
				
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblTrades_Filtered_TimeFrame_Monthly') 
    DROP INDEX [ix_tblTrades_Filtered_TimeFrame_Monthly] ON [dbo].[tblTrades]; 
----------------------------------------------------------------------------------------


BEGIN

SELECT NEWID() AS ID,
[dbo].tblSTAGING_Trades.[DateTime],
[dbo].tblSTAGING_Trades.[High],
[dbo].tblSTAGING_Trades.[Low],
[dbo].tblSTAGING_Trades.[Open],
[dbo].tblSTAGING_Trades.[Close],
[dbo].tblSTAGING_Trades.[Volume],
[dbo].tblSTAGING_Trades.[AdjustmentClose],
[dbo].tblSTAGING_Trades.SymbolID,
[dbo].tblSTAGING_Trades.TimeFrame,
[dbo].tblSTAGING_Trades.StockExchange INTO StageTemp
FROM [dbo].tblSTAGING_Trades;


/*Ensuring data set (grouped data) is DISITINCT */
WITH DistinctSet AS
(
    SELECT row_number() OVER(PARTITION BY SymbolID, [DateTime], [TimeFrame] ORDER BY SymbolID, [DateTime], [TimeFrame] DESC) AS rn
	FROM [dbo].StageTemp
)
DELETE FROM DistinctSet
WHERE rn > 1 


/*Ensure that there is only one batch of date with a given enddate no duplicates for when this sp is run several times within the day */
BEGIN
	DECLARE @TempEndDate datetime
	SELECT TOP 1  @TempEndDate = [DateTime] FROM StageTemp
	DELETE FROM [dbo].tblTrades WHERE [dbo].tblTrades.[DateTime] = @TempEndDate
END


/*Which symbols do not exist in the tblTrades?*/


SELECT 
 DD.[DateTime]
,DD.[Open]
,DD.[High]
,DD.[Low]
,DD.[Close]
,DD.[Volume]
,DD.[AdjustmentClose]
,DD.[SymbolID]
,DD.[TimeFrame]
,DD.[StockExchange]
INTO StageNotInMainTradesTable FROM [dbo].tblSTAGING_Trades DD
LEFT JOIN dbo.tblTrades FF ON DD.SymbolID = FF.SymbolID
AND DD.[DateTime] = FF.[DateTime] 
WHERE FF.ID IS NULL


--SELECT * FROM [dbo].tblSTAGING_Trades DD
--LEFT JOIN dbo.tblTrades FF ON DD.SymbolID = FF.SymbolID
--AND DD.[DateTime] = FF.[DateTime] 
--WHERE FF.ID IS NULL

--SELECT * FROM [dbo].tblSTAGING_Trades WHERE [dbo].tblSTAGING_Trades NOT IN (SELECT DISTINCT [SymbolID] FROM [dbo].tblTrades) AND (SELECT DISTINCT [TimeFrame] FROM [dbo].tblTrades)




INSERT INTO [dbo].tblTrades (
	  [ID]
      ,[DateTime]
      ,[High]
      ,[Low]
      ,[Open]
      ,[Close]
      ,[Volume]
      ,[AdjustmentClose]
      ,[SymbolID]
	  ,[TimeFrame]
	  ,[StockExchange])
SELECT DISTINCT 
	   [ID]
      ,[DateTime]
      ,[High]
      ,[Low]
      ,[Open]
      ,[Close]
      ,[Volume]
      ,[AdjustmentClose]
      ,[SymbolID]
	  ,[TimeFrame] 
	  ,[StockExchange] FROM StageTemp

SELECT @ROWSAFFECTED = COUNT(*) FROM StageTemp

DROP TABLE StageTemp

BEGIN
	/*Delete any duplicates within the main database table*/
	WITH DistinctSet AS
	(
	  SELECT row_number() OVER(PARTITION BY SymbolID, [DateTime], [Timeframe] ORDER BY SymbolID, [DateTime], [Timeframe]  DESC) AS rn
	  FROM [dbo].tblTrades
	)
	DELETE FROM DistinctSet
	WHERE rn > 1 
END

--BEGIN
	/****************** Adding Indicies ******************/
	-- Create a nonclustered index
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_SymbolID] ON [dbo].[tblTrades]
	(
		[SymbolID] ASC
	)
	INCLUDE ([Open],
		[High],
		[Low],
		[Close],
		[Volume],
		[AdjustmentClose],
		[TimeFrame],
		[DateTime],
		[StockExchange]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--END
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_SymbolID_TimeFrame] ON [dbo].[tblTrades]
	(
		[SymbolID] ASC, [TimeFrame]
	)
	INCLUDE ([Open],
		[High],
		[Low],
		[Close],
		[Volume],
		[AdjustmentClose],
		[DateTime],
		[StockExchange]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_TimeFrame] ON [dbo].[tblTrades]
	(
		[TimeFrame]
	)
	INCLUDE (
		[SymbolID],
		[Open],
		[High],
		[Low],
		[Close],
		[Volume],
		[AdjustmentClose],
		[DateTime],
		[StockExchange]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_MaxDateTimeCheck] ON [dbo].[tblTrades]
	(
		[TimeFrame]
	)
	INCLUDE (
		[SymbolID],
		[DateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Covering_DateTime] ON [dbo].[tblTrades]
	(
		[DateTime]
	)
	INCLUDE (
		[SymbolID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
		DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	------- Filtered ---------
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_1min] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '1min'

	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_2min] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '2min'

	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_5min] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '5min'
	
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_10min] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '10min'
		
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_15min] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '15min'
		
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_30min] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '30min'
			
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_1hour] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '1hour'
				
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_2hour] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '2hour'
					
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_4hour] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = '4hour'

	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_EndOfDay] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = 'EndOfDay'
	
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_Weekly] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = 'Weekly'
		
	-- Create a nonclustered index 
	CREATE NONCLUSTERED INDEX [ix_tblTrades_Filtered_TimeFrame_Monthly] ON [dbo].[tblTrades]
	(
		[SymbolID],
		[DateTime] ASC
	)
	WHERE [TimeFrame] = 'Monthly'

-------Last Step------
--UPDATE dbo.tblLatestTradeDateTimes SET dbo.tblLatestTradeDateTimes.CurrentDataPointCount = 
--(SELECT COUNT(dbo.tblTrades.DateTime) FROM dbo.tblTrades WHERE dbo.tblTrades.TimeFrame IN ('1min', '2min', '5min', '10min', '15min', '30min', '1hour', '2hour', '4hour', 'EndOfDay', 'Weekly', 'Monthly') 
--AND dbo.tblLatestTradeDateTimes.SymbolID = dbo.tblTrades.SymbolID AND dbo.tblLatestTradeDateTimes.TimeFrame = dbo.tblTrades.TimeFrame
--GROUP BY dbo.tblTrades.SymbolID, dbo.tblTrades.TimeFrame) 


--DECLARE @TempNotInCount INT
--SET @TempNotInCount = 0

--SELECT @TempNotInCount = COUNT(StageNotInMainTradesTable.SymbolID) FROM StageNotInMainTradesTable

--	IF @TempNotInCount > 0
--	BEGIN
--		INSERT INTO dbo.tblLatestTradeDateTimes (ID, SymbolID, MaxDateTime, PreviousDataPointCount, CurrentDataPointCount, TimeFrame)
--		SELECT NewID() AS ID, dbo.StageNotInMainTradesTable.SymbolID, MAX(dbo.StageNotInMainTradesTable.DateTime), 0, COUNT(dbo.StageNotInMainTradesTable.DateTime), dbo.StageNotInMainTradesTable.TimeFrame 
--		AS MaxDate FROM dbo.StageNotInMainTradesTable WHERE dbo.StageNotInMainTradesTable.TimeFrame IN ('1min', '2min', '5min', '10min', '15min', '30min', '1hour', '2hour', '4hour', 'EndOfDay', 'Weekly', 'Monthly') 
--		GROUP BY dbo.StageNotInMainTradesTable.SymbolID, dbo.StageNotInMainTradesTable.TimeFrame	
--	END

--	DROP TABLE StageNotInMainTradesTable

END


--IF @DATACOUNT = 0
--	BEGIN
--		INSERT INTO dbo.tblLatestTradeDateTimes (ID, SymbolID, MaxDateTime, PreviousDataPointCount, CurrentDataPointCount, TimeFrame)
--		SELECT NewID() AS ID, dbo.tblTrades.SymbolID, MAX(dbo.tblTrades.DateTime), 0, COUNT(dbo.tblTrades.DateTime), dbo.tblTrades.TimeFrame 
--		AS MaxDate FROM dbo.tblTrades WHERE dbo.tblTrades.TimeFrame IN ('1min', '2min', '15min', '30min','1hour','2hour', 'EndOfDay') 
--		GROUP BY dbo.tblTrades.SymbolID, dbo.tblTrades.TimeFrame	
--	END

--TRUNCATE TABLE dbo.tblTradesWorkingVersion

--INSERT INTO dbo.tblTradesWorkingVersion
--([ID]
--,[DateTime]
--,[Open]
--,[High]
--,[Low]
--,[Close]
--,[Volume]
--,[AdjustmentClose]
--,[SymbolID]
--,[TimeFrame]
--,[StockExchange])
--SELECT * FROM dbo.tblTrades






GO
/****** Object:  StoredProcedure [dbo].[proc_BulkLogMissingDateTimePoints]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









/*************************************************************************/

CREATE PROCEDURE [dbo].[proc_BulkLogMissingDateTimePoints]
AS


DELETE FROM [dbo].[tblSTAGING_DataFillCheck]


BULK
INSERT [dbo].[tblSTAGING_DataFillCheck]
FROM 'C:\Users\Public\Downloads\Output\Test\RawData\Filler\DataFillerTemp.csv'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n', --Hex equivalent of \n   0x0a
MAXERRORS = 99999999
)


IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'ix_tblDataFillCheck_Dates') 


BEGIN

INSERT INTO [dbo].[tblDataFillCheck] 
  ([ID]
      ,[SymbolID]
      ,[TimeFrame]
      ,[DateTime]
      ,[Checked]
      ,[Found])
SELECT NEWID()
	  ,[SymbolID]
      ,[TimeFrame]
      ,[DateTime]
      ,0
      ,0
 FROM [dbo].[tblSTAGING_DataFillCheck]
 WHERE NOT EXISTS 
 (SELECT [SymbolID]
      ,[TimeFrame]
      ,[DateTime]
      ,[Checked]
      ,[Found] FROM [dbo].[tblDataFillCheck] 
	WHERE [dbo].[tblDataFillCheck].SymbolID = [dbo].[tblSTAGING_DataFillCheck].SymbolID  
	AND [dbo].[tblDataFillCheck].TimeFrame = [dbo].[tblSTAGING_DataFillCheck].TimeFrame
	AND [dbo].[tblDataFillCheck].[DateTime] = [dbo].[tblSTAGING_DataFillCheck].[DateTime])


/********************************************************/
CREATE NONCLUSTERED INDEX [ix_tblDataFillCheck_Dates] ON [dbo].[tblDataFillCheck]
(
	[SymbolID],
	[Timeframe],
	[DateTime] ASC
)
WHERE [Checked] = 0 AND [Found] = 0	

/********************************************************/


DELETE FROM [dbo].[tblSTAGING_DataFillCheck]

END




GO
/****** Object:  StoredProcedure [dbo].[proc_BulkUpdateMissingDateTimePoints]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[proc_BulkUpdateMissingDateTimePoints]
AS

BEGIN

DELETE FROM [dbo].[tblSTAGING_DataFillCheck]

BULK
INSERT [dbo].[tblSTAGING_DataFillCheck]
FROM 'C:\Users\Public\Downloads\Output\Test\RawData\Filler\UpdateDataFillerTemp.csv'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n', --Hex equivalent of \n   0x0a
MAXERRORS = 99999999
)



--UPDATE table 
--SET Col1 = i.Col1, 
--    Col2 = i.Col2 
--FROM (
--    SELECT ID, Col1, Col2 
--    FROM other_table) i
--WHERE 
--    i.ID = table.ID



UPDATE 
	[dbo].[tblDataFillCheck] 
SET 
	[dbo].[tblDataFillCheck].Checked = i.Checked, 
	[dbo].[tblDataFillCheck].Found = i.Found
FROM 
(
	SELECT 
	   [SymbolID]
      ,[TimeFrame]
      ,[DateTime]
      ,[Checked]
      ,[Found]
    FROM [dbo].[tblSTAGING_DataFillCheck]
) i
WHERE [dbo].[tblDataFillCheck].SymbolID = i.SymbolID 
	AND [dbo].[tblDataFillCheck].TimeFrame = i.TimeFrame
	AND [dbo].[tblDataFillCheck].[DateTime] = i.[DateTime]





--UPDATE
--   [dbo].[tblDataFillCheck]
--SET
--	[dbo].[tblDataFillCheck].Checked = [dbo].[tblSTAGING_DataFillCheck].Checked, 
--	[dbo].[tblDataFillCheck].Found = [dbo].[tblSTAGING_DataFillCheck].Found
--FROM
--    Some_Table T
--INNER JOIN
--    Other_Table OT
--ON
--    T.id = OT.id
--WHERE [dbo].[tblDataFillCheck].SymbolID = [dbo].[tblSTAGING_DataFillCheck].SymbolID 
--	AND [dbo].[tblDataFillCheck].TimeFrame = [dbo].[tblSTAGING_DataFillCheck].TimeFrame
--	AND [dbo].[tblDataFillCheck].[DateTime] = [dbo].[tblSTAGING_DataFillCheck].[DateTime]



--	UPDATE [dbo].[tblDataFillCheck] SET [dbo].[tblDataFillCheck].Checked = @Checked, 
--	[dbo].[tblDataFillCheck].Found = @Found
--	WHERE [dbo].[tblDataFillCheck].SymbolID = @SymbolID 
--	AND [dbo].[tblDataFillCheck].TimeFrame = @TimeFrame
--	AND [dbo].[tblDataFillCheck].[DateTime] = @DateTime

DELETE FROM [dbo].[tblSTAGING_DataFillCheck]



END





GO
/****** Object:  StoredProcedure [dbo].[proc_DoesSymbolExist]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[proc_DoesSymbolExist]
@SymbolID nvarchar(10)
AS

BEGIN
	SELECT TOP 1 *
	  FROM [FMD_Forex].[dbo].[tblTrades] DD
	  WHERE DD.[SymbolID] = @SymbolID
END



GO
/****** Object:  StoredProcedure [dbo].[proc_GetDateExtents]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[proc_GetDateExtents]
@TimeFrame nvarchar(10),
@SymbolID nvarchar(10)
AS

DECLARE @Date smalldatetime
DECLARE @DateLimit smalldatetime


BEGIN
   SELECT @Date = MAX(DateTime) FROM [dbo].[tblTrades] DD
   WHERE DD.SymbolID = @SymbolID AND DD.TimeFrame = @TimeFrame

   SET @DateLimit = DateAdd(month, -12, @Date)
   PRINT @DateLimit

   SELECT MAX(DateTime) AS Minimum, @Date AS Maximum FROM [FMD_Forex].[dbo].[tblTrades] DD
   WHERE DD.SymbolID = @SymbolID AND DD.DateTime < @DateLimit AND DD.TimeFrame = @TimeFrame

   --SELECT MAX(DateTime) AS MaximumTT FROM [FMD_Forex].[dbo].[tblTrades] DD
   --WHERE DD.SymbolID = 'AUDCAD' AND DD.DateTime < @DateLimit AND DD.TimeFrame = '1min'

END


GO
/****** Object:  StoredProcedure [dbo].[proc_GetDateMaxMin]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[proc_GetDateMaxMin]
AS

BEGIN
	SELECT MIN([tblTrades].DateTime) As Earliest, MAX([tblTrades].DateTime) AS Latest FROM [dbo].[tblTrades]
END



GO
/****** Object:  StoredProcedure [dbo].[proc_GetLatestUpdatedSymbols]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[proc_GetLatestUpdatedSymbols]
@TimeFrame nvarchar(10)
AS
	BEGIN
		SELECT DISTINCT [SymbolID] FROM [dbo].[tblLatestTradeDateTimes] DD
		WHERE DD.[PreviousDataPointCount] < DD.[CurrentDataPointCount] AND 
		DD.TimeFrame = @TimeFrame
	END



GO
/****** Object:  StoredProcedure [dbo].[proc_GetRemainingData]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[proc_GetRemainingData]
@query nvarchar(MAX)
AS

BEGIN
	EXEC sp_executesql @query
END



GO
/****** Object:  StoredProcedure [dbo].[proc_GetUncheckedMissingDateTimePoints]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[proc_GetUncheckedMissingDateTimePoints]
AS

BEGIN
	SELECT * FROM [dbo].[tblDataFillCheck] WHERE
	[dbo].[tblDataFillCheck].Checked = 0
	AND [dbo].[tblDataFillCheck].Found = 0	
END





GO
/****** Object:  StoredProcedure [dbo].[proc_GetUncheckedMissingDateTimePointsCount]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[proc_GetUncheckedMissingDateTimePointsCount]
AS

BEGIN
	SELECT COUNT(*) AS 'Count' FROM [dbo].[tblDataFillCheck] WHERE
	[dbo].[tblDataFillCheck].Checked = 0
	AND [dbo].[tblDataFillCheck].Found = 0	
END






GO
/****** Object:  StoredProcedure [dbo].[proc_GetUncheckedMissingDateTimePointsDatesPara]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[proc_GetUncheckedMissingDateTimePointsDatesPara]
@startDateTime smallDateTime,
@endDateTime smallDateTime
AS

BEGIN
	SELECT * FROM [dbo].[tblDataFillCheck] WHERE
	[dbo].[tblDataFillCheck].Checked = 0
	AND [dbo].[tblDataFillCheck].Found = 0	
	AND [dbo].[tblDataFillCheck].[DateTime] BETWEEN @startDateTime AND @endDateTime
END






GO
/****** Object:  StoredProcedure [dbo].[proc_GetUncheckedMissingDateTimePointsDatesParaWithDates]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[proc_GetUncheckedMissingDateTimePointsDatesParaWithDates]
@startDateTime smallDateTime,
@endDateTime smallDateTime
AS

BEGIN
	SELECT COUNT(*) AS 'Count' FROM [dbo].[tblDataFillCheck] WHERE
	[dbo].[tblDataFillCheck].Checked = 0
	AND [dbo].[tblDataFillCheck].Found = 0	
	AND [dbo].[tblDataFillCheck].[DateTime] BETWEEN @startDateTime AND @endDateTime
END







GO
/****** Object:  StoredProcedure [dbo].[proc_LogMissingDateTimePoints]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LogMissingDateTimePoints]
@SymbolID nvarchar(10),
@TimeFrame nvarchar(10),
@DateTime smalldatetime
AS

BEGIN

--DECLARE @DataPointsVar int
--DECLARE @Checked bit
--DECLARE @Found bit
	
--	SELECT DataPointsVar = COUNT([DateTime]) FROM [dbo].[tblDataFillCheck]
--	WHERE [dbo].[tblDataFillCheck].SymbolID = @SymbolID 
--	AND [dbo].[tblDataFillCheck].TimeFrame = @TimeFrame
--	AND [dbo].[tblDataFillCheck].[DateTime] = @DateTime
	
--	SELECT @Checked = [Checked], @Found = [Found] FROM [dbo].[tblDataFillCheck]
--	WHERE [dbo].[tblDataFillCheck].SymbolID = @SymbolID 
--	AND [dbo].[tblDataFillCheck].TimeFrame = @TimeFrame
--	AND [dbo].[tblDataFillCheck].[DateTime] = @DateTime

	IF (SELECT COUNT([DateTime]) FROM [dbo].[tblDataFillCheck] WHERE [dbo].[tblDataFillCheck].SymbolID = @SymbolID 
	AND [dbo].[tblDataFillCheck].TimeFrame = @TimeFrame
	AND [dbo].[tblDataFillCheck].[DateTime] = @DateTime) = 0
		BEGIN 
				PRINT 'Reset Flags to false'
				INSERT INTO [dbo].[tblDataFillCheck] (ID, SymbolID, TimeFrame, DateTime, Checked, Found)
				VALUES (NewID(), @SymbolID, @TimeFrame, @DateTime, 0, 0)

		END


	--IF @DataPointsVar > 0
	--	BEGIN 
	--		IF @Checked = 1 AND @Found = 1
	--		BEGIN
	--			PRINT 'Reset Flags to false'
	--			UPDATE [dbo].[tblDataFillCheck] SET [dbo].[tblDataFillCheck].Checked = 0, 
	--			[dbo].[tblDataFillCheck].Found = 0
	--		END

	--	END
	--ELSE
	--	BEGIN
	--		INSERT INTO [dbo].[tblDataFillCheck] (ID, SymbolID, TimeFrame, DateTime, Checked, Found)
	--		VALUES (NewID(), @SymbolID, @TimeFrame, @DateTime, 0, 0)
	--	END
	
END





GO
/****** Object:  StoredProcedure [dbo].[proc_MarkedAsCheckedForUnFound]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[proc_MarkedAsCheckedForUnFound]
AS

BEGIN
	UPDATE [dbo].[tblDataFillCheck] SET [dbo].[tblDataFillCheck].Checked = 1, 
	[dbo].[tblDataFillCheck].Found = 0
	WHERE [dbo].[tblDataFillCheck].Checked = 0 
	AND [dbo].[tblDataFillCheck].Found = 0
END





GO
/****** Object:  StoredProcedure [dbo].[proc_RecreateDataFiller]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[proc_RecreateDataFiller]
AS



DROP TABLE [dbo].[tblDataFillCheck]


CREATE TABLE [dbo].[tblDataFillCheck](
	[ID] [nvarchar](100) NOT NULL,
	[SymbolID] [nvarchar](20) NOT NULL,
	[TimeFrame] [nvarchar](10) NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[Checked] [bit] NULL,
	[Found] [bit] NULL,
 CONSTRAINT [PK_tblDataFillCheck] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


GO
/****** Object:  StoredProcedure [dbo].[proc_UpdateMissingDateTimePoints]    Script Date: 2/19/2018 8:39:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_UpdateMissingDateTimePoints]
@SymbolID nvarchar(10),
@TimeFrame nvarchar(10),
@DateTime smalldatetime,
@Checked bit,
@Found bit
AS

BEGIN
	UPDATE [dbo].[tblDataFillCheck] SET [dbo].[tblDataFillCheck].Checked = @Checked, 
	[dbo].[tblDataFillCheck].Found = @Found
	WHERE [dbo].[tblDataFillCheck].SymbolID = @SymbolID 
	AND [dbo].[tblDataFillCheck].TimeFrame = @TimeFrame
	AND [dbo].[tblDataFillCheck].[DateTime] = @DateTime
END




GO
USE [master]
GO
ALTER DATABASE [FMD_Crypto] SET  READ_WRITE 
GO
