﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Data.SqlClient;
using System.Data;

namespace LogSys
{
    public static class ApplicationErrorLogger
    {
        public static void Log(String thread, String level, String application, String msg, String exception, String selector)
        {
            var settings = System.Configuration.ConfigurationManager.ConnectionStrings[selector];
            try
            {
                using (SqlConnection con = new SqlConnection(settings.ToString()))
                {
                    SqlCommand sqlCommand = new SqlCommand("proc_LogError", con);
                    con.Open();

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Thread", thread);
                    sqlCommand.Parameters.AddWithValue("@Level", level);
                    sqlCommand.Parameters.AddWithValue("@Application", application);
                    sqlCommand.Parameters.AddWithValue("@Msg", msg);
                    sqlCommand.Parameters.AddWithValue("@Exception", exception);
                    sqlCommand.Parameters.AddWithValue("@MachineName", Dns.GetHostName());
                    
                    Console.WriteLine("Generic Exception:: " + exception);

                    int affectedRows = sqlCommand.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic Exception:: " + ex.Message);
            }
        }
    }
}
